'use strict';
// import { Loading } from 'element-ui';


let soahttp={
    /**
     * 加载页面
     * @param  {[type]}   element  [节点ID]
     * @param  {[type]}   url      [页面地址]
     * @param  {[type]}   data     [description]
     * @param  {Function} callback [description]
     * @return {[type]}            [description]
     */
     loadHtml(element,url,data,callback ){
       $("#"+element).load(url,data,  function(responseTxt,statusTxt,xhr){
             if(statusTxt=="success")
               if(callback){
                 let context = this;
                 let args = arguments;
                 callback.call(context,args  )
               }else{
                 alert("资源加载成功！");
               }


               if(isPrintData) console.log( "资源加载成功！"+responseTxt );
             if(statusTxt=="error")
               alert("资源加载失败 Error: "+xhr.status+": "+xhr.statusText);
               if(isPrintData) console.log( "loadHtml :"+url+   " ;资源加载失败 Error: "+xhr.status+": "+xhr.statusText );
           });

     },
     /**
      * post 请求
      * @param  {[type]} url       [url]
      * @param  {[type]} _data     [数据]
      * @param  {[type]} successFn [成功调用方法]
      * @param  {[type]} errorFn   [失败调用方法]
      * @return {[type]}           [description]
      */
    post(url,_data,successFn,errorFn ){
     // debugger
     // console.log( "post  >>>" +url);
      //let loadingInstance = Loading.service({ fullscreen: true });
      try{
      // 请求生成后立即分配处理程序，请记住该请求针对 jqxhr 对象
          $.post(url,_data, function(data) {
            //console.log(data);
            if(data.success==true){
             // debugger
            if( successFn){
              //console.log(data);
              successFn.call(this,data);
            }else{
              alert(data.result);
            }
          }else{
            alert(data.result);

          }

          },'json')


        }catch(err){
          if(errorFn){
            errorFn.call(this,data);
          }else{
            alert("error");
          }

         }finally{
      //     loadingInstance.close();
         }

    },
    unAsyncPost(url,_data,successFn,errorFn ){
        //  let loadingInstance = Loading.service({ fullscreen: true });
            try{
            // 请求生成后立即分配处理程序，请记住该请求针对 jqxhr 对象
                var jqxhr = $.ajax({
                  type: 'POST',
                  url:url,
                  data:_data,
                  async:false,

                  success:function(result,status,xhr){
                    console.log(result);
                    if(isPrintData) //console.log(result );
                    if(successFn){
                      //console.log(result);
                      successFn.call(this,result);
                    }else{
                     // alert(result);
                    }

                },error:function(xhr,status,error){
                  if(isPrintData) //console.log(result );
                  if(errorFn){
                    errorFn.call(this,xhr);
                  }else{
   //                 alert(data);
                  }
                },complete:function(xhr,status){
                  if(isPrintData) console.log( url+"complete");
                //  loadingInstance.close();
                }

                });
        }catch(err){
          if(isPrintData) console.log(err );
          if(errorFn){
            errorFn.call(this,data);
          }else{
            alert(data);
          }
         }finally{
      //     loadingInstance.close();
         }

    },
/**
 * [soaget description]
 * @param  {[type]} url       [description]
 * @param  {[type]} successFn [description]
 * @param  {[type]} errorFn   [description]
 * @return {[type]}           [description]
 */
    get(_url,successFn ){
      debugger
      // let loadingInstance = Loading.service({ fullscreen: true });

      // var jqxhr = $.ajax({
      //   url:url,
      //   data:_data,
      //   type:'post',
      //   success:function(result,status,xhr){
      //     if(isPrintData) console.log(result );
      //     if(successFn){
      //       successFn.call(this,data);
      //     }else{
      //       alert(data);
      //     }
      //
      // },error:function(xhr,status,error){
      //   if(isPrintData) console.log(result );
      //   if(errorFn){
      //     errorFn.call(this,data);
      //   }else{
      //     alert(data);
      //   }
      // },complete:function(xhr,status){
      //   if(isPrintData) console.log( url+"complete");
      //   loadingInstance.close();
      // }
      //
      // });


        try{
              $.get(_url,{}, function(data,status,xhr){

                if(status=="success"){

                    alert("外部内容加载成功！");
                    if(successFn){
                    //  let context = this;
                    //  let args = arguments;
                    //  successFn.call(context,args  )
                      successFn.call(this,data);
                    }
                    if(isPrintData) console.log( "数据加载成功！"+data );
                }else{
                  if(isPrintData) console.log( "数据加载失败！"+data );
                }
              })
            }catch(err){
              //if(isPrintData)
              console.log( "数据加载失败！"+data );
             }finally{
            //   loadingInstance.close();
             }
       }
}


exports.soahttp = soahttp;
