import { ES_INIT, GET_CHARTS_DATA, GET_INDEX_INFO } from '../api/api';
import HighCharts from '../common/js/highChartsUtil';
export default {
    logInit(_this) {
        $.ajax({
            type: "POST",
            url: ES_INIT,
            data: null,
            success: function (data, status, xhr) {
                // _this.serviceUrl = data.serviceUrl;
                let serviceurl = data.serviceUrl[0].split(',');
                for (let i = 0; i < serviceurl.length; i++) {
                    let obj = new Object();
                    obj['url'] = serviceurl[i];
                    _this.serviceUrl.push(obj);
                }
                _this.serviceStaeParm.serviceUrl = serviceurl[0];
                _this.queryChartsFrom.index = data.index[0];
                for (let i = 0; i < data.index.length; i++) {
                    let obj = new Object();
                    obj['index'] = data.index[i];
                    _this.esIndex.push(obj);
                }
                var index = _this.queryChartsFrom.index;
                _this.queryChartsFrom.type = index.slice(index.indexOf('-') + 1, index.length);
                _this.queryDataInfo();
            }
        });
    },
    queryIndexInfo(_this) {
        $.ajax({
            type: "POST",
            url: GET_INDEX_INFO,
            data: null,
            success: function (data, status, xhr) {
                if (data.success = true) {
                    _this.todayIssue = data.result.todayIssue;
                    _this.lastIssue = data.result.lastIssue;
                    _this.todayRequest = data.result.todayRequest;
                    _this.lastRequest = data.result.lastRequest;
                    _this.countMonth = data.result.countMonth;
                    if (data.result.countMonth.length != 0) {
                        _this.isloadingsu = true;
                    }
                    let standard = 0;
                    for (let i = 0; i < _this.countMonth.length; i++) {
                        if (i == 0) {
                            _this.countMonPro.push(100);
                            standard = _this.countMonth[i].times;
                        } else {
                            var num = parseInt((_this.countMonth[i].times / standard) * 100);

                            _this.countMonPro.push(num);
                        }
                    }
                    // console.log(_this.countMonPro);
                } else {
                    _this.$message({
                        message: "读取数据失败，请稍后再试",
                        type: 'error'
                    });
                }
            }
        });
    },
    queryDataInfo(_this) {
        var nowDate = new Date();
        _this.queryChartsFrom.interval = nowDate.getHours();
        var year = nowDate.getFullYear();
        var month = (nowDate.getMonth() + 1) > 10 ? (nowDate.getMonth() + 1) : "0" + (nowDate.getMonth() + 1);
        var day = nowDate.getDate() > 10 ? nowDate.getDate() : "0" + nowDate.getDate();

        _this.todayDate = year + "-" + month + "-" + day;

        _this.queryChartsFrom.begin = year + "-" + month + "-" + day + " 00:00:00";
        _this.queryChartsFrom.end = year + "-" + month + "-" + day + " 24:00:00";
        $.ajax({
            type: "POST",
            url: GET_CHARTS_DATA,
            data: _this.queryChartsFrom,
            success: function (data, status, xhr) {
                if (data.success = true) {
                    _this.xlist = data.result.xlist;
                    _this.ylist = data.result.ylist;
                    _this.xylist = [];
                    for (let i = 0; i < _this.xlist.length; i++) {
                        var ar = new Array();
                        ar.push(_this.xlist[i].substring(10, _this.xlist[i].length));
                        ar.push(parseFloat(_this.ylist[i]));
                        _this.xylist.push(ar);
                    }
                    // HighCharts.getChartColumn("chartColumn", _this.xylist,_this.queryChartsFrom.statisticsType,"今日日志统计"); 
                    // _this.getChartColumn("chartColumn1", _this.xylist);    
                    HighCharts.getChartColumn("chartColumn2", _this.xylist, _this.queryChartsFrom.statisticsType, "今日日志统计");
                    // HighCharts.getChartColumn("chartColumn2", _this.xylist);                   
                } else {
                    _this.$message({
                        message: "读取数据失败，请稍后再试",
                        type: 'error'
                    });
                }
            }
        });
        _this.isGetQyeryData = true;

    }


}