/*****************************************************************************************************
 * DESC			:	API函数定义
 * Author		:	Steven.Li
 * Datetime		:	2017-07-06
 * ***************************************************************************************************
 * 函数组			函数名称			函数作用
 * 
 * 接口函数
 * 			           queryKindData		           按条件获取条款信息
 *                                             initRiskSelect                            初始化险种下拉框信息
 * ***************************************************************************************************
 */

import Constant from "../constant";

const LogAPI = {

            query(_this) {
                $.ajax({
                    type: 'POST',
                    url: Constant.urls.findLogs,
                    data: _this.formData,
                    dataType: 'json',
                    success: function(data) {
                        if(data.state === 3) {
                            _this.$router.push('/login');
                            _this.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                            _this.tableData = data.result;
                            _this.total = data.total;
                        } else {
                           _this.$message({type: 'error',message: '调用接口错误！'});
                        }
                    }
                });
                 setTimeout(function(){
                    _this.isSortRequest = true;
                },500);
            },
            openLogDetailDialog(_this) {
                _this.showLogDetailDialog = true;
            },
            logDetail(_this) {
                    $.ajax({
                    type: 'POST',
                    url: Constant.urls.findLogDetail,
                    data: {
                        businessKeyValue : _this.logId
                    },
                    dataType: 'json',
                    async: false,
                    success: function(data) {
                        if(data.state === 3) {
                            _this.$router.push('/login');
                            _this.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                            _this.logInfo = data.result;
                        } else {
                           _this.$message({type: 'error',message: '调用接口错误！'});
                        }
                    }
                });
            },

}

export default LogAPI