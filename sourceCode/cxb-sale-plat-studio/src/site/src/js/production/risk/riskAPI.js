/*****************************************************************************************************
 * DESC			:	API函数定义
 * Author		:	Steven.Li
 * Datetime		:	2017-07-06
 * ***************************************************************************************************
 * 函数组			函数名称			函数作用
 *
 * 接口函数
 * 			           queryKindData		           按条件获取条款信息
 *                                             initRiskSelect                            初始化险种下拉框信息
 * ***************************************************************************************************
 */

import Constant from "../constant";
import Validations from "../validations";

const RiskAPI = {

            initRiskSelect(_this) {
                    $.ajax({
                    type: 'POST',
                    url: Constant.urls.queryAllPrpdRisk,
                    dataType: 'json',
                    success: function(data) {
                        if(data.state === 3) {
                            _this.$router.push('/login');
                            _this.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                             _this.riskOptions = data.result;
                        } else {
                            _this.$message({type: 'error',message: '调用接口错误！'});
                        }
                    }
                });
            },

            queryFdriskconfigByRisk(_this) {
              $.ajax({
                type: 'POST',
                url: Constant.urls.queryFdriskconfigByRisk,
                data: {
                  riskcode: _this.formData.riskcode,
                },
                async: false,
                dataType: 'json',
                success: function(data) {
                  if(data.state === 3) {
                    _this.$router.push('/login');
                    _this.$message({type: 'error',message: data.result});
                    return false;
                  }
                  if(data.state == 0) {
                    for (let i = 0; i < data.result.length; i++) {
                      switch (data.result[i].codetype) {
                        case "DangerSplitMode":
                        {
                          _this.formData.dangerSplitMode = data.result[i].codevalue;
                          break;
                        }
                        case "DangerViewType":
                        {
                          _this.formData.dangerViewType = data.result[i].codevalue;
                          break;
                        }
                        case "ReinsTrialMode":
                        {
                          _this.formData.reinsTrialMode = data.result[i].codevalue;
                          break;
                        }
                        case "RiskEvaluateMode":
                        {
                          _this.formData.riskEvaluateMode = data.result[i].codevalue;
                          break;
                        }
                        case "RiskPoolMode":
                        {
                          _this.formData.riskPoolMode = data.result[i].codevalue;
                          break;
                        }
                        default: {}
                      }
                    }
                  } else {
                    _this.$message({type: 'error',message: '调用接口错误！'});
                  }
                }
              });
            },

            queryFhriskByRisk(_this) {
              $.ajax({
                type: 'POST',
                url: Constant.urls.queryFhriskByRisk,
                data: {
                  riskcode: _this.formData.riskcode,
                },
                async: false,
                dataType: 'json',
                success: function(data) {
                  if(data.state === 3) {
                    _this.$router.push('/login');
                    _this.$message({type: 'error',message: data.result});
                    return false;
                  }
                  if(data.state == 0) {
                    for (let i = 0; i < data.result.length; i++) {
                      _this.formData.fhData.push({
                        fhtreaty:data.result[i].treatyno,
                        fhsection:data.result[i].sectionno,
                        inrateflag:data.result[i].inrateflag,
                        fhtreatyOptions:_this.fhtreatyOptions,
                        fhsectionOptions:_this.fhsectionOptions,
                        inrateflagOptions:_this.inrateflagOptions,
                      });
                    }
                  } else {
                    _this.$message({type: 'error',message: '调用接口错误！'});
                  }
                }
              });
            },

            queryFhexitemkindByRisk(_this) {
              $.ajax({
                type: 'POST',
                url: Constant.urls.queryFhexitemkindByRisk,
                data: {
                  riskcode: _this.formData.riskcode,
                },
                async: false,
                dataType: 'json',
                success: function(data) {
                  if(data.state === 3) {
                    _this.$router.push('/login');
                    _this.$message({type: 'error',message: data.result});
                    return false;
                  }
                  if(data.state == 0) {
                    for (let i = 0; i < _this.formData.fhData.length; i++) {
                      _this.formData.fhData[i].fhexitemkind = '';
                      for(let j = 0; j <data.result.length; j++) {
                        if (_this.formData.fhData[i].fhtreaty === data.result[j].treatyno
                              && _this.formData.fhData[i].fhsection === data.result[j].sectionno) {
                          _this.formData.fhData[i].fhexitemkind += data.result[j].itemkind + ",";
                        }
                      }
                      _this.formData.fhData[i].fhexitemkind = _this.formData.fhData[i].fhexitemkind.substring(0,_this.formData.fhData[i].fhexitemkind.length - 1);
                    }
                  } else {
                    _this.$message({type: 'error',message: '调用接口错误！'});
                  }
                }
              });
            },

            queryFhxriskByRisk(_this) {
              $.ajax({
                type: 'POST',
                url: Constant.urls.queryFhxriskByRisk,
                data: {
                  riskcode: _this.formData.riskcode,
                },
                async: false,
                dataType: 'json',
                success: function(data) {
                  if(data.state === 3) {
                    _this.$router.push('/login');
                    _this.$message({type: 'error',message: data.result});
                    return false;
                  }
                  if(data.state == 0) {
                    for (let i = 0; i < data.result.length; i++) {
                      _this.formData.fhxData.push({
                        fhxtreaty:data.result[i].treatyno,
                        fhxlayer:data.result[i].layerno,
                        fhxsection:data.result[i].sectionno,
                        fhxtreatyOptions:_this.fhxtreatyOptions,
                        fhxlayerOptions:_this.fhxlayerOptions,
                        fhxsectionOptions:_this.fhxsectionOptions,
                      });
                    }
                  } else {
                    _this.$message({type: 'error',message: '调用接口错误！'});
                  }
                }
              });
            },

            initFhtreatyOptions(_this) {
              $.ajax({
                type: 'POST',
                url: Constant.urls.queryAllFhtreaty,
                dataType: 'json',
                async: false,
                success: function(data) {
                  if(data.state === 3) {
                    _this.$router.push('/login');
                    _this.$message({type: 'error',message: data.result});
                    return false;
                  }
                  if(data.state == 0) {
                    for (let i = 0; i < data.result.length; i++) {
                      _this.fhtreatyOptions.push({label: data.result[i].treatyname,value: data.result[i].treatyno});
                    }
                  } else {
                    _this.$message({type: 'error',message: '调用接口错误！'});
                  }
                }
              });
            },

            initFhxtreatyOptions(_this) {
              $.ajax({
                type: 'POST',
                url: Constant.urls.queryAllFhxtreaty,
                dataType: 'json',
                async: false,
                success: function(data) {
                  if(data.state === 3) {
                    _this.$router.push('/login');
                    _this.$message({type: 'error',message: data.result});
                    return false;
                  }
                  if(data.state == 0) {
                    for (let i = 0; i < data.result.length; i++) {
                      _this.fhxtreatyOptions.push({label: data.result[i].treatyname,value: data.result[i].treatyno});
                    }
                  } else {
                    _this.$message({type: 'error',message: '调用接口错误！'});
                  }
                }
              });
            },

            initFhsectionOptions(_this, index) {
              $.ajax({
                type: 'POST',
                url: Constant.urls.queryFhsections,
                data: {
                  treatyno: _this.formData.fhData[index].fhtreaty,
                },
                dataType: 'json',
                async: false,
                success: function(data) {
                  if(data.state === 3) {
                    _this.$router.push('/login');
                    _this.$message({type: 'error',message: data.result});
                    return false;
                  }
                  if(data.state == 0) {
                    _this.fhsectionOptions = [];
                    for (let i = 0; i < data.result.length; i++) {
                      _this.fhsectionOptions.push({label: data.result[i].sectioncdesc,value: data.result[i].sectionno});
                    }
                  } else {
                    _this.$message({type: 'error',message: '调用接口错误！'});
                  }
                }
              });
            },

            initFhxlayerOptions(_this,index) {
              $.ajax({
                type: 'POST',
                url: Constant.urls.queryFhxlayers,
                data: {
                  treatyno: _this.formData.fhxData[index].fhxtreaty,
                },
                dataType: 'json',
                async: false,
                success: function(data) {
                  if(data.state === 3) {
                    _this.$router.push('/login');
                    _this.$message({type: 'error',message: data.result});
                    return false;
                  }
                  if(data.state == 0) {
                    _this.fhxlayerOptions = [];
                    for (let i = 0; i < data.result.length; i++) {
                      _this.fhxlayerOptions.push({label: data.result[i].layercdesc,value: data.result[i].layerno});
                    }
                  } else {
                    _this.$message({type: 'error',message: '调用接口错误！'});
                  }
                }
              });
            },

            initFhxsectionOptions(_this,index) {
              $.ajax({
                type: 'POST',
                url: Constant.urls.queryFhxsections,
                data: {
                  treatyno: _this.formData.fhxData[index].fhxtreaty,
                  layerno: _this.formData.fhxData[index].fhxlayer
                },
                dataType: 'json',
                async: false,
                success: function(data) {
                  if(data.state === 3) {
                    _this.$router.push('/login');
                    _this.$message({type: 'error',message: data.result});
                    return false;
                  }
                  if(data.state == 0) {
                    _this.fhxsectionOptions = [];
                    for (let i = 0; i < data.result.length; i++) {
                      _this.fhxsectionOptions.push({label: data.result[i].sectioncdesc,value: data.result[i].sectionno});
                    }
                  } else {
                    _this.$message({type: 'error',message: '调用接口错误！'});
                  }
                }
              });
            },

            initFhexitemkindOptions(_this) {
              $.ajax({
                type: 'POST',
                url: Constant.urls.queryFhexitemkinds,
                data: {
                  treatyno: _this.formData.fhtreaty,
                  sectionno: _this.formData.fhsection,
                },
                dataType: 'json',
                async: false,
                success: function(data) {
                  if(data.state === 3) {
                    _this.$router.push('/login');
                    _this.$message({type: 'error',message: data.result});
                    return false;
                  }
                  if(data.state == 0) {
                    for (let i = 0; i < data.result.length; i++) {
                      _this.fhexitemkindOptions.push({itemkind: data.result[i].itemkind,itemkinddesc: data.result[i].itemkinddesc.substr(0,50)});
                    }
                  } else {
                    _this.$message({type: 'error',message: '调用接口错误！'});
                  }
                }
              });
            },

            openAddDialog(_this) {
                    _this.showAddDialog = true;
            },
            openEditDialog(_this) {
                if (_this.selectedRows.length > 1) {
                        _this.$message({type: 'error',message: '只能选择一行数据进行编辑'});
                        return;
                    } else if (_this.selectedRows.length < 1) {
                        _this.$message({type: 'error',message: '请选择险种！'});
                        return;
                    }
                    _this.showEditDialog = true;
            },
            openLinkKindDialog(_this) {
                 if (_this.selectedRows.length > 1) {
                        _this.$message({type: 'error',message: '只能选择一行数据进行关联'});
                        return;
                    } else if (_this.selectedRows.length < 1) {
                        _this.$message({type: 'error',message: '请选择险种！'});
                        return;
                    }
                    _this.showLinkKindDialog = true;
            },
             initShortRate(_this){
                 $.ajax({
                    type: 'POST',
                    url: Constant.urls.queryShortRate,
                    dataType: 'json',
                    success: function(data) {
                        if(data.state === 3) {
                            _this.$router.push('/login');
                            _this.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                             _this.shortRateOptions = data.result;
                        } else {
                            if (data.result.length > 40) {
                                alert(data.result);
                            } else {
                                _this.$message({type: 'error',message: data.result});
                            }
                        }
                    }
                });
            },
            openItemLinkDialog(_this) {
                   if (_this.selectedRows.length > 1) {
                            _this.$message({type: 'error',message: '只能选择一行数据进行关联'});
                            return;
                        } else if (_this.selectedRows.length < 1) {
                            _this.$message({type: 'error',message: '请选择险种！'});
                            return;
                        }
                    _this.showItem2RiskDialog = true;
            },

            add(_this) {
                 _this.loading = true;
                 let api = this;

                 if (!api.riskFormValidation(_this)) {
                    _this.loading = false;
                    _this.formData.riskcode = _this.tempRiskcode;
                    return ;
                 }

                 $.ajax({
                    type: 'POST',
                    url: Constant.urls.addPrpdRisk,
                    data: JSON.stringify(_this.formData),
                    dataType: 'json',
                    contentType: 'application/json',
                    success: function(data) {
                        if(data.state === 3) {
                            _this.$router.push('/login');
                            _this.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                            _this.$message({type: 'success',message: '新增险类成功！'});
                            api.query(_this.$parent);
                             _this.off();
                        } else {
                          alert(data.result);
                        }
                    },
                    complete:function(XMLHttpRequest,textStatus){  
                        if(textStatus=='timeout'){
                            _this.$message({type: 'error',message: '服务器连接超时!'});
                        }
                    },  
                    error:function(XMLHttpRequest, textStatus){  
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                       _this.$message({type: 'error',message: '服务器错误!'});
                    }
                });
                 _this.formData.riskcode = _this.tempRiskcode;
                 _this.loading = false;
            },

            edit(_this) {
                    let api = this;
                    if (!api.riskFormValidation(_this)) {
                        _this.loading = false;
                        return ;
                     }
                    _this.$confirm('此操作将修改该险种, 如升级版本，请手动将原版本置为无效。是否继续?', '提示', {
                        confirmButtonText: '确定',
                        cancelButtonText: '取消',
                        type: 'warning'
                    }).then(() => {
                         _this.loading = true;
                         let api = this;

                         $.ajax({
                            type: 'POST',
                            url: Constant.urls.updatePrpdRisk,
                            data: _this.formData,
                            dataType: 'json',
                            success: function(data) {
                                if(data.state === 3) {
                                    _this.$router.push('/login');
                                    _this.$message({type: 'error',message: data.result});
                                    return false;
                                }
                                if(data.state == 0) {
                                    _this.$message({type: 'success',message: '修改成功!'});
                                    api.query(_this.$parent);
                                    _this.off();
                                } else {
                                     alert(data.result);
                                }
                            },
                            complete:function(XMLHttpRequest,textStatus){  
                                if(textStatus=='timeout'){
                                    _this.$message({type: 'error',message: '服务器连接超时!'});
                                }
                            },  
                            error:function(XMLHttpRequest, textStatus){  
                                console.log(XMLHttpRequest);
                                console.log(textStatus);
                               _this.$message({type: 'error',message: '服务器错误!'});
                            }
                        });
                         _this.loading = false;
                    }).catch(() => {
                        _this.$message({type: 'info',message: '已取消修改'});
                    });
            },

            query(_this) {
                _this.loading = true;
                $.ajax({
                    type: 'POST',
                    url: Constant.urls.queryRisks,
                    data: JSON.stringify(_this.formData),
                    dataType: 'json',
                    contentType : 'application/json',
                    success: function(data) {
                        _this.loading = false;
                        if(data.state === 3) {
                            _this.$router.push('/login');
                            _this.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                            _this.tableData = data.result;
                            _this.total = data.total;
                        } else {
                           alert(data.result);
                        }
                    },
                    complete:function(XMLHttpRequest,textStatus){  
                        if(textStatus=='timeout'){
                            _this.$message({type: 'error',message: '服务器连接超时!'});
                        }
                        _this.loading = false;
                    },  
                    error:function(XMLHttpRequest, textStatus){  
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                       _this.$message({type: 'error',message: '服务器错误!'});
                       _this.loading = false;
                    }
                });
                 setTimeout(function(){
                    _this.isSortRequest = true;
                },500);
            },

            queryByClassAndComposite(_this) {
                _this.loading = true;
                $.ajax({
                    type: 'POST',
                    url: Constant.urls.queryByClassAndComposite,
                    data: JSON.stringify(_this.formData),
                    dataType: 'json',
                    contentType : 'application/json',
                    success: function(data) {
                        _this.loading = false;
                        if(data.state === 3) {
                            _this.$router.push('/login');
                            _this.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                            _this.tableData = data.result;
                            _this.total = data.total;
                        } else {
                           alert(data.result);
                        }
                    },
                    complete:function(XMLHttpRequest,textStatus){  
                        if(textStatus=='timeout'){
                            _this.$message({type: 'error',message: '服务器连接超时!'});
                        }
                        _this.loading = false;
                    },  
                    error:function(XMLHttpRequest, textStatus){  
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                       _this.$message({type: 'error',message: '服务器错误!'});
                       _this.loading = false;
                    }
                });
                 setTimeout(function(){
                    _this.isSortRequest = true;
                },500);
            },


            linkRisk(_this) {

                // if (_this.formData.shortratetype === '') {
                //     _this.$message({type: 'error',message: '请选择短期费率方式！'});
                //     return ;
                // }

                _this.$confirm('此操作将关联选中险种, 是否继续?', '提示', {
                        confirmButtonText: '确定',
                        cancelButtonText: '取消',
                        type: 'warning'
                    }).then(() => {
                            $.ajax({
                                type: 'POST',
                                url: Constant.urls.linkRisk,
                                data: JSON.stringify(_this.formData),
                                dataType: 'json',
                                contentType : 'application/json',
                                success: function(data) {
                                    if(data.state === 3) {
                                        _this.$router.push('/login');
                                        _this.$message({type: 'error',message: data.result});
                                        return false;
                                    }
                                    if(data.state == 0) {
                                        _this.$message({type: 'success',message: '关联险种成功！'});
                                        _this.off();
                                    } else {
                                      alert("错误提示: \n" + data.result);
                                    }
                                    _this.queryData(); //重新加载表格数据
                                }
                            });
                     }).catch(() => {
                        _this.$message({type: 'info',message: '已取消关联'});
                    });
            },

            linkKind(_this) {

                // if (_this.formData.shortratetype === '') {
                //     _this.$message({type: 'error',message: '请选择短期费率方式！'});
                //     return ;
                // }

                _this.$confirm('此操作将关联选中条款, 是否继续?', '提示', {
                        confirmButtonText: '确定',
                        cancelButtonText: '取消',
                        type: 'warning'
                    }).then(() => {
                            $.ajax({
                                type: 'POST',
                                url: Constant.urls.linkKind,
                                data: JSON.stringify(_this.formData),
                                dataType: 'json',
                                contentType : 'application/json',
                                success: function(data) {
                                    if(data.state === 3) {
                                        _this.$router.push('/login');
                                        _this.$message({type: 'error',message: data.result});
                                        return false;
                                    }
                                    if(data.state == 0) {
                                        _this.$message({type: 'success',message: '关联条款成功！'});
                                        _this.off();
                                    } else {
                                      alert("错误提示: \n" + data.result);
                                    }
                                    _this.queryData(); //重新加载表格数据
                                }
                            });
                     }).catch(() => {
                        _this.$message({type: 'info',message: '已取消关联'});
                    });
            },

            getRiskLinked(_this) {
                _this.loading = true;
                let vm = _this;
                $.ajax({
                    type: 'POST',
                    url: Constant.urls.getPrpdKind,
                    data: JSON.stringify(vm.formData),
                    dataType: 'json',
                    contentType: 'application/json',
                    success: function(data) {
                        _this.loading = false;
                        if(data.state === 3) {
                            vm.$router.push('/login');
                            vm.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                            vm.tableData = data.result;
                            vm.total = data.total;
                        } else {
                           vm.$message({type: 'error',message: '调用接口错误！'});
                        }
                    },
                    complete:function(XMLHttpRequest,textStatus){  
                        if(textStatus=='timeout'){
                            _this.$message({type: 'error',message: '服务器连接超时!'});
                        }
                        _this.loading = false;
                    },  
                    error:function(XMLHttpRequest, textStatus){  
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                       _this.$message({type: 'error',message: '服务器错误!'});
                       _this.loading = false;
                    }
                });
                 setTimeout(function(){
                    _this.isSortRequest = true;
                },500);
            },
            delLink(_this) {
                    let vm = _this;
                    let api = this;
                    _this.$confirm('此操作将取消选中险种的关联, 是否继续?', '提示', {
                        confirmButtonText: '确定',
                        cancelButtonText: '取消',
                        type: 'warning'
                    }).then(() => {

                        let beginUnLink = false;

                        let deletable = api.checkRiskLinkedDeletable(vm.formData);

                        if (deletable.flag === false) {
                            if(confirm(deletable.errorMsg)) {
                                beginUnLink = true;
                            }
                        } else {
                                beginUnLink = true;
                        }

                        if (beginUnLink) {
                                $.ajax({
                                    type: 'POST',
                                    url: Constant.urls.delRiskLinked,
                                    data: JSON.stringify(vm.formData),
                                    dataType: 'json',
                                    contentType: 'application/json',
                                    success: function(data) {
                                        if(data.state === 3) {
                                            vm.$router.push('/login');
                                            vm.$message({type: 'error',message: data.result});
                                            return false;
                                        }
                                        if(data.state == 0) {
                                            vm.$message({type: 'success',message: '取消关联成功!'});
                                            vm.off();
                                        } else {
                                            if (data.result.length > 40) {
                                                alert(data.result);
                                            } else {
                                                _this.$message({type: 'error',message: data.result});
                                            }
                                        }
                                        vm.queryData(); //重新加载表格数据
                                        vm.selectedRows = [];
                                    }
                                });
                        }
                    }).catch(() => {
                        _this.$message({type: 'info',message: '已取消关联'});
                    });
            },
            checkRiskLinkedDeletable(request) {
                let deletable = {
                    flag: true,
                    errorMsg: '',
                };
                $.ajax({
                        type: 'POST',
                        url: Constant.urls.checkRiskLinkedDeletable,
                        data: JSON.stringify(request),
                        dataType: 'json',
                        contentType : 'application/json',
                        async : false,
                        success: function(data) {
                            if(data.state == 1) {
                                deletable.flag = false;
                                deletable.errorMsg = data.result
                            }
                        }
                });
                return deletable;
            },
            queryRiskByClass(_this) {
                $.ajax({
                    type: 'POST',
                    url: Constant.urls.queryRiskByClass,
                    data: {classcode : _this.classcode},
                    dataType: 'json',
                    success: function(data) {
                        if(data.state === 3) {
                            _this.$router.push('/login');
                            _this.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                             _this.riskOptions = data.result;
                        } else {
                            _this.$message({type: 'error',message: '调用接口错误！'});
                        }
                    },
                });
            },
            queryRiskByCodes(_this) {
                $.ajax({
                    type: 'POST',
                    url: Constant.urls.queryRiskByCodes,
                    data: {riskcodes : _this.$parent.defaultRiskOptions},
                    dataType: 'json',
                    success: function(data) {
                        if(data.state === 3) {
                            _this.$router.push('/login');
                            _this.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                             _this.riskOptions = data.result;
                        } else {
                            _this.$message({type: 'error',message: '调用接口错误！'});
                        }
                    },
                });
            },
            checkRiskCodeUsed(_this) {
                let vm = _this;
                $.ajax({
                    type: 'POST',
                    url: Constant.urls.checkRiskCodeUsed,
                    data: {riskcode : _this.clazz.classcode + _this.formData.riskcode},
                    dataType: 'json',
                    success: function(data) {
                        if(data.state === 3) {
                            vm.$router.push('/login');
                            vm.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                            if(data.result) {
                                vm.warning = true;
                            } else {
                                vm.warning = false;
                            }
                        } else {
                           alert(data.result);
                        }
                    },
                    complete:function(XMLHttpRequest,textStatus){  
                        if(textStatus=='timeout'){
                            _this.$message({type: 'error',message: '服务器连接超时!'});
                        }
                    },  
                    error:function(XMLHttpRequest, textStatus){  
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                       _this.$message({type: 'error',message: '服务器错误!'});
                    }
                });
                 setTimeout(function(){
                    _this.isSortRequest = true;
                },500);
            },

            riskFormValidation(_this) {
                //添加条款，表单非空校验
                let requiredFields = [];
                requiredFields.push( _this.formData.riskcode);
                requiredFields.push( _this.formData.riskversion);
                requiredFields.push( _this.formData.riskcname);
                requiredFields.push( _this.formData.riskename);
                requiredFields.push( _this.formData.validstatus);
                if(!Validations.required(requiredFields,_this)) {
                        return false;
                }
                return true;
            },
}

export default RiskAPI
