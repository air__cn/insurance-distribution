/*****************************************************************************************************
 * DESC			:	API函数定义
 * Author		:	Steven.Li
 * Datetime		:	2017-07-06
 * ***************************************************************************************************
 * 函数组			函数名称			函数作用
 * 
 * 接口函数
 * 			           queryKindData		           按条件获取条款信息
 *                                             initRiskSelect                            初始化险种下拉框信息
 * ***************************************************************************************************
 */

import Constant from "../constant";

const ClassAPI = {

            initClassSelect(_this) {
                    $.ajax({
                    type: 'POST',
                    url: Constant.urls.queryAllPrpdClass,
                    dataType: 'json',
                    async : false,
                    success: function(data) {
                        if(data.state === 3) {
                            _this.$router.push('/login');
                            _this.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                             _this.classOptions = data.result;
                        } else {
                            _this.$message({type: 'error',message: '调用接口错误！'});
                        }
                    }
                });
            },

            add(_this) {
                 _this.loading = true;
                 let api = this;
                 $.ajax({
                    type: 'POST',
                    url: Constant.urls.insertPrpdClass,
                    data: _this.formData,
                    dataType: 'json',
                    success: function(data) {
                        if(data.state === 3) {
                            _this.$router.push('/login');
                            _this.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                            _this.$message({type: 'success',message: '新增险类成功！'});
                            api.query(_this.$parent);
                             _this.off();
                        } else {
                          alert(data.result);
                        }
                    },
                    complete:function(XMLHttpRequest,textStatus){  
                        if(textStatus=='timeout'){
                            _this.$message({type: 'error',message: '服务器连接超时!'});
                        }
                    },  
                    error:function(XMLHttpRequest, textStatus){  
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                       _this.$message({type: 'error',message: '服务器错误!'});
                    }
                });
                 _this.loading = false;
            },

            edit(_this) {
                    let api = this;
                    _this.$confirm('此操作将修改该险类, 是否继续?', '提示', {
                        confirmButtonText: '确定',
                        cancelButtonText: '取消',
                        type: 'warning'
                    }).then(() => {
                         _this.loading = true;
                         let api = this;
                         $.ajax({
                            type: 'POST',
                            url: Constant.urls.updatePrpdClass,
                            data: _this.formData,
                            dataType: 'json',
                            success: function(data) {
                                if(data.state === 3) {
                                    _this.$router.push('/login');
                                    _this.$message({type: 'error',message: data.result});
                                    return false;
                                }
                                if(data.state == 0) {
                                    _this.$message({type: 'success',message: '修改成功!'});
                                    api.query(_this.$parent);
                                    _this.off();
                                } else {
                                     alert(data.result);
                                }
                            },
                            complete:function(XMLHttpRequest,textStatus){  
                                if(textStatus=='timeout'){
                                    _this.$message({type: 'error',message: '服务器连接超时!'});
                                }
                            },  
                            error:function(XMLHttpRequest, textStatus){  
                                console.log(XMLHttpRequest);
                                console.log(textStatus);
                               _this.$message({type: 'error',message: '服务器错误!'});
                            }
                        });
                         _this.loading = false;
                    }).catch(() => {
                        _this.$message({type: 'info',message: '已取消修改'});
                    });
            },

            query(_this) {
                _this.loading = true;
                $.ajax({
                    type: 'POST',
                    url: Constant.urls.queryPrpdClass,
                    data: _this.formData,
                    dataType: 'json',
                    success: function(data) {
                        _this.loading = false;
                        if(data.state === 3) {
                            _this.$router.push('/login');
                            _this.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                            _this.tableData = data.result;
                            _this.total = data.total;
                        } else {
                           alert(data.result);
                        }
                    },
                    complete:function(XMLHttpRequest,textStatus){  
                        if(textStatus=='timeout'){
                            _this.$message({type: 'error',message: '服务器连接超时!'});
                        }
                        _this.loading = false;
                    },  
                    error:function(XMLHttpRequest, textStatus){  
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                       _this.$message({type: 'error',message: '服务器错误!'});
                       _this.loading = false;
                    }
                });
                 setTimeout(function(){
                    _this.isSortRequest = true;
                },500);
            },

            openAddDialog(_this) {
                    _this.showAddDialog = true;
            },

            openEditDialog(_this) {
                if (_this.selectedRows.length > 1) {
                        _this.$message({type: 'error',message: '只能选择一行数据进行编辑'});
                        return;
                    } else if (_this.selectedRows.length < 1) {
                        _this.$message({type: 'error',message: '请选择险类！'});
                        return;
                    }
                    _this.showEditDialog = true;
            },

            checkClassCodeUsed(_this) {
                let vm = _this;
                $.ajax({
                    type: 'POST',
                    url: Constant.urls.checkClassCodeUsed,
                    data: {classcode : _this.formData.classcode},
                    dataType: 'json',
                    success: function(data) {
                        if(data.state === 3) {
                            vm.$router.push('/login');
                            vm.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                            if(data.result) {
                                vm.warning = true;
                            } else {
                                vm.warning = false;
                            }
                        } else {
                           alert(data.result);
                        }
                    },
                    complete:function(XMLHttpRequest,textStatus){  
                        if(textStatus=='timeout'){
                            _this.$message({type: 'error',message: '服务器连接超时!'});
                        }
                    },  
                    error:function(XMLHttpRequest, textStatus){  
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                       _this.$message({type: 'error',message: '服务器错误!'});
                    }
                });
                 setTimeout(function(){
                    _this.isSortRequest = true;
                },500);
            }
}

export default ClassAPI