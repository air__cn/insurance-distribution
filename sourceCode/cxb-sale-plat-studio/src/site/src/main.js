import babelpolyfill from 'babel-polyfill'
import Vue from 'vue'
import App from './App'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-default/index.css'
//import './assets/theme/theme-green/index.css'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import store from './store/index.js'
//import NProgress from 'nprogress'
//import 'nprogress/nprogress.css'
import routes from './routes'

import VueQuillEditor from 'vue-quill-editor'

import $ from 'jquery'

import '../static/UE/ueditor.config.js'
import '../static/UE/ueditor.all.min.js'
import '../static/UE/lang/zh-cn/zh-cn.js'
import '../static/UE/ueditor.parse.min.js'

import 'font-awesome/css/font-awesome.min.css'
// import store from

Vue.use(ElementUI)
Vue.use(VueRouter)
Vue.use(Vuex)
Vue.use(VueQuillEditor)
//NProgress.configure({ showSpinner: false });

const router = new VueRouter({
  routes
})

let _this=Vue.prototype;
router.beforeEach((to, from, next) => {
  console.log("path:"+to.path);
  // this.$router.replace(this.Tourl);


  // console.log(store.state.skipUrl);
  // setTimeout(function(){
  //   console.log("nowURL:"+sessionStorage.getItem("nowURL"));
  // },5000)

 if (to.path == '/login') {
   sessionStorage.removeItem('user');
 }
 let user = JSON.parse(sessionStorage.getItem('user'));
 if (!user && to.path != '/login') {
   next({ path: '/login' })
 }
 // else if(user&&to.path !='/login'){

    // else if(store.state.skipUrl == ''||store.state.skipUrl ==null||!store.state.skipUrl){
    //   if(store.state.count>0){
    //     next()
    //   }
    //   else{
    //    next({ path:'/index'})
    //   }

    // }



 // }

 else{
      console.log(store.state.count);
      console.log(to.path);
      console.log(from.path);
      console.log('OK');
      console.log(store.state.skipUrl);

    if(to.path == '/login'||from.path=='/login'||(to.path =='/index'&&from.path=='/index')||store.state.count==0){
        next()
      // next({ path:'/index'})
    }
    else if(store.state.skipUrl){
      next()
      // next({ path: store.state.skipUrl})
    }
    else{
      next()
      // next({ path:'/index'})
      // next({ path: store.state.skipUrl})
    }

    if(store.state.count==0||store.state.count==1){
     store.state.count=2;
     next()
    }

    next()

//  } else {
//    if(to.path == '/login'||from.path=='/login'||(to.path == '/'&&from.path=='/')||store.state.count==1){
//      next()
//    }else {
//      sessionStorage.setItem("isjump", true);
//      sessionStorage.setItem("url", to.path);
//      // console.log("topath:"+to.path);
//      // console.log("formpath:"+from.path);
//      next({ path: '/' })
//    }

   // if(store.state.count==0||store.state.count==1){
   //   store.state.count=2
   //   next()
   // }
   // next()
 }
})

//router.afterEach(transition => {
//NProgress.done();
//});

new Vue({
  //el: '#app',
  //template: '<App/>',
  router,
  store,
  //components: { App }
  render: h => h(App)
}).$mount('#app')

