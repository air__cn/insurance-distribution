package cn.com.libertymutual.sys.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import cn.com.libertymutual.sys.bean.SysServiceInfo;


public interface ISysServiceInfoDao extends PagingAndSortingRepository<SysServiceInfo, Integer>,JpaSpecificationExecutor<SysServiceInfo>{
	//public	List<SysServiceInfo> getSysServiceInfo();
	
	
//	@Query("form SysServiceInfo where userName = :name")
//	List<SysServiceInfo> findSaleInitData(@Param("name") String userName);
	
	List<SysServiceInfo> findByUserName(String userid);

	@Modifying
	@Transactional
	@Query("update SysServiceInfo set url= :url ,modifyDate= :modifyDate where id = :id")
	void updateServiceInfo(@Param("id")Integer id,@Param("modifyDate")Date modifyDate, @Param("url")String url);
} 
