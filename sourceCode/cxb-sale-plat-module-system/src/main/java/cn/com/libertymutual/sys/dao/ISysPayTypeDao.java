package cn.com.libertymutual.sys.dao;

import cn.com.libertymutual.sys.bean.SysPayType;

public interface ISysPayTypeDao extends org.springframework.data.repository.CrudRepository
<SysPayType, Long>{


}
