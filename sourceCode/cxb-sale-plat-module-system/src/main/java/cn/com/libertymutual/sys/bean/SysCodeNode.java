package cn.com.libertymutual.sys.bean;

import java.io.Serializable;

import javax.persistence.*;

/**
 * Created by Ryan on 2016-09-09.
 */
@Entity
@Table(name = "tb_sys_code_node",  catalog = "")
public class SysCodeNode   implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = -8498468972990159428L;
	private int id;
    private String codeType;
    private String code;
    private String codeEname;
    private String codeCname;
    private String branchCode;
    private String validStatus;
    private int serialNo;
    private String remark;

    public SysCodeNode(){
    	super();
    	
    }
    public SysCodeNode(String code, String codeEname, String codeCname) {
		super();
		this.code = code;
		this.codeEname = codeEname;
		this.codeCname = codeCname;
	}

	@Id    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "CODE_TYPE", nullable = false, length = 50)
    public String getCodeType() {
        return codeType;
    }

    public void setCodeType(String codeType) {
        this.codeType = codeType;
    }

    @Basic
    @Column(name = "CODE", nullable = false, length = 50)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Basic
    @Column(name = "CODE_ENAME", nullable = false, length = 100)
    public String getCodeEname() {
        return codeEname;
    }

    public void setCodeEname(String codeEname) {
        this.codeEname = codeEname;
    }

    @Basic
    @Column(name = "CODE_CNAME", nullable = false, length = 200)
    public String getCodeCname() {
        return codeCname;
    }

    public void setCodeCname(String codeCname) {
        this.codeCname = codeCname;
    }

    @Basic
    @Column(name = "BRANCH_CODE", nullable = true, length = 8)
    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    @Basic
    @Column(name = "VALID_STATUS", nullable = false, length = 2)
    public String getValidStatus() {
        return validStatus;
    }

    public void setValidStatus(String validStatus) {
        this.validStatus = validStatus;
    }

    @Basic
    @Column(name = "SERIAL_NO", nullable = false)
    public int getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(int serialNo) {
        this.serialNo = serialNo;
    }

    @Basic
    @Column(name = "REMARK", nullable = true, length = 300)
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysCodeNode that = (SysCodeNode) o;

        if (id != that.id) return false;
        if (serialNo != that.serialNo) return false;
        if (codeType != null ? !codeType.equals(that.codeType) : that.codeType != null) return false;
        if (code != null ? !code.equals(that.code) : that.code != null) return false;
        if (codeEname != null ? !codeEname.equals(that.codeEname) : that.codeEname != null) return false;
        if (codeCname != null ? !codeCname.equals(that.codeCname) : that.codeCname != null) return false;
        if (branchCode != null ? !branchCode.equals(that.branchCode) : that.branchCode != null) return false;
        if (validStatus != null ? !validStatus.equals(that.validStatus) : that.validStatus != null) return false;
        if (remark != null ? !remark.equals(that.remark) : that.remark != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (codeType != null ? codeType.hashCode() : 0);
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (codeEname != null ? codeEname.hashCode() : 0);
        result = 31 * result + (codeCname != null ? codeCname.hashCode() : 0);
        result = 31 * result + (branchCode != null ? branchCode.hashCode() : 0);
        result = 31 * result + (validStatus != null ? validStatus.hashCode() : 0);
        result = 31 * result + serialNo;
        result = 31 * result + (remark != null ? remark.hashCode() : 0);
        return result;
    }

	@Override
	public String toString() {
		return "SysCodeNode [id=" + id + ", codeType=" + codeType + ", code="
				+ code + ", codeEname=" + codeEname + ", codeCname="
				+ codeCname + ", branchCode=" + branchCode + ", validStatus="
				+ validStatus + ", serialNo=" + serialNo + ", remark=" + remark
				+ "]";
	}
}
