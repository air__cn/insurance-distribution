package cn.com.libertymutual.sys.bean;

import java.io.Serializable;

import javax.persistence.*;

/**
 * Created by Ryan on 2016-09-09.
 */
@Entity
@Table(name = "tb_prp_risk",  catalog = "")
public class PrpRisk  implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 2940180027179082560L;
	private String riskCode;
    private String riskCname;
    private String riskEname;
    private String classCode;
    private String validStatus;
    private String remark;

    @Id    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "RISK_CODE", nullable = false, length = 4)
    public String getRiskCode() {
        return riskCode;
    }

    public void setRiskCode(String riskCode) {
        this.riskCode = riskCode;
    }

    @Basic
    @Column(name = "RISK_CNAME", nullable = false, length = 255)
    public String getRiskCname() {
        return riskCname;
    }

    public void setRiskCname(String riskCname) {
        this.riskCname = riskCname;
    }

    @Basic
    @Column(name = "RISK_ENAME", nullable = false, length = 255)
    public String getRiskEname() {
        return riskEname;
    }

    public void setRiskEname(String riskEname) {
        this.riskEname = riskEname;
    }

    @Basic
    @Column(name = "CLASS_CODE", nullable = false, length = 2)
    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    @Basic
    @Column(name = "VALID_STATUS", nullable = false, length = 1)
    public String getValidStatus() {
        return validStatus;
    }

    public void setValidStatus(String validStatus) {
        this.validStatus = validStatus;
    }

    @Basic
    @Column(name = "REMARK", nullable = true, length = 300)
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PrpRisk prpRisk = (PrpRisk) o;

        if (riskCode != null ? !riskCode.equals(prpRisk.riskCode) : prpRisk.riskCode != null) return false;
        if (riskCname != null ? !riskCname.equals(prpRisk.riskCname) : prpRisk.riskCname != null) return false;
        if (riskEname != null ? !riskEname.equals(prpRisk.riskEname) : prpRisk.riskEname != null) return false;
        if (classCode != null ? !classCode.equals(prpRisk.classCode) : prpRisk.classCode != null) return false;
        if (validStatus != null ? !validStatus.equals(prpRisk.validStatus) : prpRisk.validStatus != null) return false;
        if (remark != null ? !remark.equals(prpRisk.remark) : prpRisk.remark != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = riskCode != null ? riskCode.hashCode() : 0;
        result = 31 * result + (riskCname != null ? riskCname.hashCode() : 0);
        result = 31 * result + (riskEname != null ? riskEname.hashCode() : 0);
        result = 31 * result + (classCode != null ? classCode.hashCode() : 0);
        result = 31 * result + (validStatus != null ? validStatus.hashCode() : 0);
        result = 31 * result + (remark != null ? remark.hashCode() : 0);
        return result;
    }

	@Override
	public String toString() {
		return "PrpRisk [riskCode=" + riskCode + ", riskCname=" + riskCname
				+ ", riskEname=" + riskEname + ", classCode=" + classCode
				+ ", validStatus=" + validStatus + ", remark=" + remark + "]";
	}
}
