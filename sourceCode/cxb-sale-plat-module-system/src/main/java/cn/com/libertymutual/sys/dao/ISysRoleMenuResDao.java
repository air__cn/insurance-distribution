package cn.com.libertymutual.sys.dao;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cn.com.libertymutual.sys.bean.SysRoleMenuRes;
import cn.com.libertymutual.sys.bean.SysRoleMenuResPK;

@Repository
public interface ISysRoleMenuResDao extends JpaRepository<SysRoleMenuRes, SysRoleMenuResPK> {

	// @Query( "SELECT DISTINCT t1.menuid FROM tb_sys_role_menu t1 INNER JOIN
	// tb_sys_menu t2 ON t1.menuid=t2.menuid WHERE t1.roleid IN (?1) GROUP BY
	// t1.menuid ORDER BY t2.orderid")
	// List<Integer> findRoleMenu(List<String> roleList, Integer fmenuId) ;

	List<SysRoleMenuRes> findByRoleid(List<String> roleList);

	List<SysRoleMenuRes> findByRoleidAndMenuid(String roleId, int menuid);

	@Query("select t from SysRoleMenuRes t where roleid=?1 and menuid=?2 and resid=?3")
	SysRoleMenuRes findByRoleMenuRes(String roleId, int menuid, String resid);

	@Modifying
	@Transactional
	@Query("delete from SysRoleMenuRes where roleid = :roleId")
	void deleteByRoleId(@Param("roleId")String roleId);

	
	@Modifying
	@Transactional
	@Query("delete from SysRoleMenuRes where resid = ?1 ")
	void deleteByResId(String resid);

	
	void findByResid(String resid);

	// @Query("select menuid from tb_sys_role_menu where roleid=:roleId order by
	// menuid")
	// List<Integer> findMenuIdList( String roleId );
}
