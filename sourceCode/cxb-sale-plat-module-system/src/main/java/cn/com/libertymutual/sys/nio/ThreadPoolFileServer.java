package cn.com.libertymutual.sys.nio;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPoolFileServer extends FileServer {

	private ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
	
	@Override
    protected void readData(final SelectionKey key) throws IOException {  
        // 移除掉这个key的可读事件，已经在线程池里面处理
        key.interestOps(key.interestOps() & (~SelectionKey.OP_READ));  
        executorService.execute( new ReadDataThead(key, fileMap) );
        //executorService.execute( new ReadDataThead(key) );
    }
}
