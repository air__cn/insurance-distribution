package cn.com.libertymutual.sys.bean;

import java.util.Date;

import javax.persistence.*;

/**
 * Created by Ryan on 2016-12-21.
 */
@Entity
@Table(name = "tb_sys_role", catalog = "")
public class SysRole {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "ROLEID")
    private Integer roleid;
    @Column(name = "ROLENAME")
    private String rolename;
    @Basic
    @Column(name = "REMARKS")
    private String remarks;
    @Basic
    @Column(name = "APPROVEAUTH")
    private String approveAuth;
    @Basic
    @Column(name = "STATUS")
    private String status;
    @Basic
    @Column(name = "CONFIGAUTH")
    private String configAuth;
    
    
    
    @Basic
    @Column(name = "ROLE_CODE")
    private String roleCode;
    @Basic
    @Column(name = "ENAME")
    private String eName;
    @Basic
    @Column(name = "UPDATE_TIME")
    private Date updateTime;
    @Basic
    @Column(name = "CREATE_TIME")
    private Date createTime;
    @Basic
    @Column(name = "UP_CODE")
    private String upCode;
    @Basic
    @Column(name = "START_DATE")
    private Date startDate;
    @Basic
    @Column(name = "END_DATE")
    private Date endDate;
    @Basic
    @Column(name = "BASE_RATE")
    private Date baseRate;
    
    
    
    
    public SysRole() {
		super();
	}

    public Integer getRoleid() {
        return roleid;
    }

    public void setRoleid(Integer roleid) {
        this.roleid = roleid;
    }

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getApproveAuth() {
		return approveAuth;
	}

	public void setApproveAuth(String approveAuth) {
		this.approveAuth = approveAuth;
	}

	public String getConfigAuth() {
		return configAuth;
	}

	public void setConfigAuth(String configAuth) {
		this.configAuth = configAuth;
	}

	
	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public String geteName() {
		return eName;
	}

	public void seteName(String eName) {
		this.eName = eName;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getUpCode() {
		return upCode;
	}

	public void setUpCode(String upCode) {
		this.upCode = upCode;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getBaseRate() {
		return baseRate;
	}

	public void setBaseRate(Date baseRate) {
		this.baseRate = baseRate;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysRole sysRole = (SysRole) o;

        if (roleid != null ? !roleid.equals(sysRole.roleid) : sysRole.roleid != null) return false;
        if (rolename != null ? !rolename.equals(sysRole.rolename) : sysRole.rolename != null) return false;
        if (remarks != null ? !remarks.equals(sysRole.remarks) : sysRole.remarks != null) return false;
        if (status != null ? !status.equals(sysRole.status) : sysRole.status != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = roleid != null ? roleid.hashCode() : 0;
        result = 31 * result + (rolename != null ? rolename.hashCode() : 0);
        result = 31 * result + (remarks != null ? remarks.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }
}
