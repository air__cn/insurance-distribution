/*
 *	Copyright (c) liberty 2016
 *	cn.com.libertymutual.insure.service.api
 *  Create Date 2016年4月7日
 *	@author tracy.liao
 */
package cn.com.libertymutual.sys.service.api;

import cn.com.libertymutual.core.exception.CustomJDBCException;
import cn.com.libertymutual.core.exception.CustomLangException;

/**
 * 系统序列service层
 * @author tracy.liao
 * @date 2016年4月7日
 */
public interface ISequenceService {
	/**
	 * 根据序列名称获取序列下一个值
	 * @param sequenceName 序列名称
	 * @return 序列值
	 * @throws CustomLangException 
	 * @throws CustomJDBCException
	 */
	public String  getUserCodeSequence( String source );
	
	public String  getOrderSequence(String riskCode);

	
}
