package cn.com.libertymutual.sys.quartz.config;



public class QuartzPluginProperties {

	private String triggHistoryClass;
	private String shutdownhookClass;
	private boolean shutdownhookCleanShutdown;
	/**
	 * @return the triggHistoryClass
	 */
	public String getTriggHistoryClass() {
		return triggHistoryClass;
	}
	/**
	 * @param triggHistoryClass the triggHistoryClass to set
	 */
	public void setTriggHistoryClass(String triggHistoryClass) {
		this.triggHistoryClass = triggHistoryClass;
	}
	/**
	 * @return the shutdownhookClass
	 */
	public String getShutdownhookClass() {
		return shutdownhookClass;
	}
	/**
	 * @param shutdownhookClass the shutdownhookClass to set
	 */
	public void setShutdownhookClass(String shutdownhookClass) {
		this.shutdownhookClass = shutdownhookClass;
	}
	/**
	 * @return the shutdownhookCleanShutdown
	 */
	public boolean getShutdownhookCleanShutdown() {
		return shutdownhookCleanShutdown;
	}
	/**
	 * @param shutdownhookCleanShutdown the shutdownhookCleanShutdown to set
	 */
	public void setShutdownhookCleanShutdown(boolean shutdownhookCleanShutdown) {
		this.shutdownhookCleanShutdown = shutdownhookCleanShutdown;
	}
	
}
