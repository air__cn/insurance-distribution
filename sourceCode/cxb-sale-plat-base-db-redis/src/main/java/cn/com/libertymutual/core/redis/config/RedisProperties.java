package cn.com.libertymutual.core.redis.config;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration("redisProperties")
@ConfigurationProperties(prefix = "spring.redis")
public class RedisProperties {
	private int    expireSeconds;
	private List<String> clusterNodes;
	private String password;
	private long    readTimeout;
	private long    connectTimeout;
	/*
	config.setMinIdle(8);//设置最小空闲数
	config.setMaxWaitMillis(10000);
	config.setTestOnBorrow(true);
	config.setTestOnReturn(true);
	//Idle时进行连接扫描
	config.setTestWhileIdle(true);
	//表示idle object evitor两次扫描之间要sleep的毫秒数
	config.setTimeBetweenEvictionRunsMillis(30000);
	//表示idle object evitor每次扫描的最多的对象数
	config.setNumTestsPerEvictionRun(10);
	//表示一个对象至少停留在idle状态的最短时间，然后才能被idle object evitor扫描并驱逐；这一项只有在timeBetweenEvictionRunsMillis大于0时才有意义
	config.setMinEvictableIdleTimeMillis(60000);
	*/
	private boolean usePool;
	
	private int minIdle;
	private int maxIdle;
	private int maxTotal;
	private int maxWaitMillis;
	private boolean testOnBorrow;
	private int maxRedirects;
	
	private int maxSessionInactiveIntervalInSeconds = 1800;
	
	public int getExpireSeconds() {
		return expireSeconds;
	}
	public void setExpireSeconds(int expireSeconds) {
		this.expireSeconds = expireSeconds;
	}
	public List<String> getClusterNodes() {
		return clusterNodes;
	}
	public void setClusterNodes(List<String> clusterNodes) {
		this.clusterNodes = clusterNodes;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the readTimeout
	 */
	public long getReadTimeout() {
		return readTimeout;
	}
	/**
	 * @param readTimeout the readTimeout to set
	 */
	public void setReadTimeout(long readTimeout) {
		this.readTimeout = readTimeout;
	}
	/**
	 * @return the connectTimeout
	 */
	public long getConnectTimeout() {
		return connectTimeout;
	}
	/**
	 * @param connectTimeout the connectTimeout to set
	 */
	public void setConnectTimeout(long connectTimeout) {
		this.connectTimeout = connectTimeout;
	}
	public int getMinIdle() {
		return minIdle;
	}
	public void setMinIdle(int minIdle) {
		this.minIdle = minIdle;
	}
	public int getMaxIdle() {
		return maxIdle;
	}
	public void setMaxIdle(int maxIdle) {
		this.maxIdle = maxIdle;
	}
	public int getMaxTotal() {
		return maxTotal;
	}
	public void setMaxTotal(int maxTotal) {
		this.maxTotal = maxTotal;
	}
	public int getMaxWaitMillis() {
		return maxWaitMillis;
	}
	public void setMaxWaitMillis(int maxWaitMillis) {
		this.maxWaitMillis = maxWaitMillis;
	}
	public boolean isTestOnBorrow() {
		return testOnBorrow;
	}
	public void setTestOnBorrow(boolean testOnBorrow) {
		this.testOnBorrow = testOnBorrow;
	}
	public int getMaxRedirects() {
		return maxRedirects;
	}
	public void setMaxRedirects(int maxRedirects) {
		this.maxRedirects = maxRedirects;
	}
	public boolean isUsePool() {
		return usePool;
	}
	public void setUsePool(boolean usePool) {
		this.usePool = usePool;
	}
	public int getMaxSessionInactiveIntervalInSeconds() {
		return maxSessionInactiveIntervalInSeconds;
	}
	public void setMaxSessionInactiveIntervalInSeconds(
			int maxSessionInactiveIntervalInSeconds) {
		this.maxSessionInactiveIntervalInSeconds = maxSessionInactiveIntervalInSeconds;
	}
	
	
	
		/*<bean id="poolConfig" class="redis.clients.jedis.JedisPoolConfig">  
	    <property name="minIdle" value="${redis.minIdle}" />  
	    <property name="maxIdle" value="${redis.maxIdle}" />  
	    <property name="maxTotal" value="${redis.maxTotal}" />  
	    <property name="maxWaitMillis" value="${redis.maxWaitMillis}" />  
	    <property name="testOnBorrow" value="${redis.testOnBorrow}" />  
	</bean>  
	 
	<bean id="clusterConfig" class="org.springframework.data.redis.connection.RedisClusterConfiguration">
		<property name="clusterNodes">
			<set>
				<ref bean="clusterRedisNodes1"/>
				<ref bean="clusterRedisNodes2"/>
				<ref bean="clusterRedisNodes3"/>
				<ref bean="clusterRedisNodes4"/>
				<ref bean="clusterRedisNodes5"/>
				<ref bean="clusterRedisNodes6"/>
			</set>
		</property>
		<property name="maxRedirects" value="${redis.maxRedirects}"></property>
	</bean>*/
	
}
