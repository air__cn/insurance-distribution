package cn.com.libertymutual.sp.service.api;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpArticle;

public interface ArticleService {

	ServiceResult articleList(String branchCode, String name,String type,Integer pageNumber, Integer pageSize,String userCode);

	ServiceResult addOrUpdateArticle(TbSpArticle tbSpArticle);

	ServiceResult findArticle(Integer id);
	
	ServiceResult findSoftPaper(Integer id);
	ServiceResult allArticle(String productName, String riskCode, String name, String type, String status,
			Integer pageNumber, Integer pageSize);

}
