package cn.com.libertymutual.production.service.api;

import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.FhxriskKey;

/**
 * @author steven.li
 */
public interface FhxriskService {

    /**
     * 新增Fhxrisk
     * @param record
     */
    void insert(FhxriskKey record);

    List<FhxriskKey> findAll();

    List<FhxriskKey> findByRisk(String riskcode);
}
