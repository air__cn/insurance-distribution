
package cn.com.libertymutual.sp.webService.allpolicy;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for prptcarDriverDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="prptcarDriverDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="acceptLicenseDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="age" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="appliYearType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="awardLicenseOrgan" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="businessSource" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="causeTroubleTimes" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="changelessFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="driverAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="driverName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="drivingCarType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="drivingLicenseNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="drivingYears" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="flag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="identifyNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="itemNo" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="marriage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="peccancy" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="possessNature" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="proposalNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="receiveLicenseYear" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="riskCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serialNo" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="sex" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prptcarDriverDto", propOrder = {
    "acceptLicenseDate",
    "age",
    "appliYearType",
    "awardLicenseOrgan",
    "businessSource",
    "causeTroubleTimes",
    "changelessFlag",
    "driverAddress",
    "driverName",
    "drivingCarType",
    "drivingLicenseNo",
    "drivingYears",
    "flag",
    "identifyNumber",
    "itemNo",
    "marriage",
    "peccancy",
    "possessNature",
    "proposalNo",
    "receiveLicenseYear",
    "riskCode",
    "serialNo",
    "sex"
})
public class PrptcarDriverDto implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar acceptLicenseDate;
    protected double age;
    protected String appliYearType;
    protected String awardLicenseOrgan;
    protected String businessSource;
    protected double causeTroubleTimes;
    protected String changelessFlag;
    protected String driverAddress;
    protected String driverName;
    protected String drivingCarType;
    protected String drivingLicenseNo;
    protected double drivingYears;
    protected String flag;
    protected String identifyNumber;
    protected double itemNo;
    protected String marriage;
    protected double peccancy;
    protected String possessNature;
    protected String proposalNo;
    protected double receiveLicenseYear;
    protected String riskCode;
    protected double serialNo;
    protected String sex;

    /**
     * Gets the value of the acceptLicenseDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAcceptLicenseDate() {
        return acceptLicenseDate;
    }

    /**
     * Sets the value of the acceptLicenseDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAcceptLicenseDate(XMLGregorianCalendar value) {
        this.acceptLicenseDate = value;
    }

    /**
     * Gets the value of the age property.
     * 
     */
    public double getAge() {
        return age;
    }

    /**
     * Sets the value of the age property.
     * 
     */
    public void setAge(double value) {
        this.age = value;
    }

    /**
     * Gets the value of the appliYearType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppliYearType() {
        return appliYearType;
    }

    /**
     * Sets the value of the appliYearType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppliYearType(String value) {
        this.appliYearType = value;
    }

    /**
     * Gets the value of the awardLicenseOrgan property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAwardLicenseOrgan() {
        return awardLicenseOrgan;
    }

    /**
     * Sets the value of the awardLicenseOrgan property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAwardLicenseOrgan(String value) {
        this.awardLicenseOrgan = value;
    }

    /**
     * Gets the value of the businessSource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessSource() {
        return businessSource;
    }

    /**
     * Sets the value of the businessSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessSource(String value) {
        this.businessSource = value;
    }

    /**
     * Gets the value of the causeTroubleTimes property.
     * 
     */
    public double getCauseTroubleTimes() {
        return causeTroubleTimes;
    }

    /**
     * Sets the value of the causeTroubleTimes property.
     * 
     */
    public void setCauseTroubleTimes(double value) {
        this.causeTroubleTimes = value;
    }

    /**
     * Gets the value of the changelessFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChangelessFlag() {
        return changelessFlag;
    }

    /**
     * Sets the value of the changelessFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChangelessFlag(String value) {
        this.changelessFlag = value;
    }

    /**
     * Gets the value of the driverAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriverAddress() {
        return driverAddress;
    }

    /**
     * Sets the value of the driverAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriverAddress(String value) {
        this.driverAddress = value;
    }

    /**
     * Gets the value of the driverName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriverName() {
        return driverName;
    }

    /**
     * Sets the value of the driverName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriverName(String value) {
        this.driverName = value;
    }

    /**
     * Gets the value of the drivingCarType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDrivingCarType() {
        return drivingCarType;
    }

    /**
     * Sets the value of the drivingCarType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDrivingCarType(String value) {
        this.drivingCarType = value;
    }

    /**
     * Gets the value of the drivingLicenseNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDrivingLicenseNo() {
        return drivingLicenseNo;
    }

    /**
     * Sets the value of the drivingLicenseNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDrivingLicenseNo(String value) {
        this.drivingLicenseNo = value;
    }

    /**
     * Gets the value of the drivingYears property.
     * 
     */
    public double getDrivingYears() {
        return drivingYears;
    }

    /**
     * Sets the value of the drivingYears property.
     * 
     */
    public void setDrivingYears(double value) {
        this.drivingYears = value;
    }

    /**
     * Gets the value of the flag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlag() {
        return flag;
    }

    /**
     * Sets the value of the flag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlag(String value) {
        this.flag = value;
    }

    /**
     * Gets the value of the identifyNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentifyNumber() {
        return identifyNumber;
    }

    /**
     * Sets the value of the identifyNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentifyNumber(String value) {
        this.identifyNumber = value;
    }

    /**
     * Gets the value of the itemNo property.
     * 
     */
    public double getItemNo() {
        return itemNo;
    }

    /**
     * Sets the value of the itemNo property.
     * 
     */
    public void setItemNo(double value) {
        this.itemNo = value;
    }

    /**
     * Gets the value of the marriage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarriage() {
        return marriage;
    }

    /**
     * Sets the value of the marriage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarriage(String value) {
        this.marriage = value;
    }

    /**
     * Gets the value of the peccancy property.
     * 
     */
    public double getPeccancy() {
        return peccancy;
    }

    /**
     * Sets the value of the peccancy property.
     * 
     */
    public void setPeccancy(double value) {
        this.peccancy = value;
    }

    /**
     * Gets the value of the possessNature property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPossessNature() {
        return possessNature;
    }

    /**
     * Sets the value of the possessNature property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPossessNature(String value) {
        this.possessNature = value;
    }

    /**
     * Gets the value of the proposalNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProposalNo() {
        return proposalNo;
    }

    /**
     * Sets the value of the proposalNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProposalNo(String value) {
        this.proposalNo = value;
    }

    /**
     * Gets the value of the receiveLicenseYear property.
     * 
     */
    public double getReceiveLicenseYear() {
        return receiveLicenseYear;
    }

    /**
     * Sets the value of the receiveLicenseYear property.
     * 
     */
    public void setReceiveLicenseYear(double value) {
        this.receiveLicenseYear = value;
    }

    /**
     * Gets the value of the riskCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRiskCode() {
        return riskCode;
    }

    /**
     * Sets the value of the riskCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRiskCode(String value) {
        this.riskCode = value;
    }

    /**
     * Gets the value of the serialNo property.
     * 
     */
    public double getSerialNo() {
        return serialNo;
    }

    /**
     * Sets the value of the serialNo property.
     * 
     */
    public void setSerialNo(double value) {
        this.serialNo = value;
    }

    /**
     * Gets the value of the sex property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSex() {
        return sex;
    }

    /**
     * Sets the value of the sex property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSex(String value) {
        this.sex = value;
    }

}
