package cn.com.libertymutual.production.service.impl;

import cn.com.libertymutual.core.util.StringUtil;
import cn.com.libertymutual.production.dao.nomorcldatasource.FhxsectionMapper;
import cn.com.libertymutual.production.model.nomorcldatasource.Fhxsection;
import cn.com.libertymutual.production.model.nomorcldatasource.FhxsectionExample;
import cn.com.libertymutual.production.service.api.FhxsectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * @author steven.li
 * @create 2017/12/20
 */
@Service
public class FhxsectionServiceImpl implements FhxsectionService {

    @Autowired
    private FhxsectionMapper fhxsectionMapper;

    @Override
    public void insert(Fhxsection record) {
        fhxsectionMapper.insertSelective(record);
    }

    @Override
    public List<Fhxsection> findAll(String treatyno, String layerno) {
        FhxsectionExample criteria = new FhxsectionExample();
        FhxsectionExample.Criteria where = criteria.createCriteria();
        where.andTreatynoEqualTo(treatyno);
        if (StringUtils.isEmpty(layerno)) {
            where.andLayernoIsNull();
        } else {
            where.andLayernoEqualTo(Short.valueOf(layerno));
        }
        return fhxsectionMapper.selectByExample(criteria);
    }
}
