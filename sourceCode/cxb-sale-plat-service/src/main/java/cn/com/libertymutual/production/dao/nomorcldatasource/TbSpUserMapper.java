package cn.com.libertymutual.production.dao.nomorcldatasource;

import cn.com.libertymutual.production.model.nomorcldatasource.TbSpUser;
import cn.com.libertymutual.production.model.nomorcldatasource.TbSpUserExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
@Mapper
public interface TbSpUserMapper {
    int countByExample(TbSpUserExample example);

    int deleteByExample(TbSpUserExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(TbSpUser record);

    int insertSelective(TbSpUser record);

    List<TbSpUser> selectByExample(TbSpUserExample example);

    TbSpUser selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") TbSpUser record, @Param("example") TbSpUserExample example);

    int updateByExample(@Param("record") TbSpUser record, @Param("example") TbSpUserExample example);

    int updateByPrimaryKeySelective(TbSpUser record);

    int updateByPrimaryKey(TbSpUser record);

    @Select(
    		" SELECT C.* FROM ( SELECT * FROM tb_sys_branch WHERE FIND_IN_SET(BRANCH_NO, getChildList(#{comCode}))) B " + 
    		"	LEFT JOIN tb_sp_user C ON C.COM_CODE = B.branch_no " + 
    		"    				WHERE C.STATE = '1'  ORDER BY C.USER_CODE"
    		)
//    @Results(
//            @Result(property = "userCode", column = "USER_CODE")
//    )
	List<TbSpUser> selectWithComcode(@Param("comCode")String comcode);
}