package cn.com.libertymutual.sp.mq;

/**
 * SendStatus
返回值    解释
SEND_OK    发送成功
FLUSH_DISK_TIMEOUT    发送成功，但broker刷盘失败，此时如若服务器宕机，消息会丢失；
FLUSH_SLAVE_TIMEOUT    写从失败；如果主宕机，消息丢失；
SLAVE_NOT_AVAILABLE    从不可用；
注意：当配置多master无slave的集群时，若master的brokerRole为SYNC_MASTER，则发送消息会一直返回这个值；最新版本（3.1.14以上）事务消息将一直发送失败（事务消息中处理了返回值不为SEND_OK，则直接进行ROLL_BACK）；
当应用方明确指出，producer发送成功为SEND_OK状态的消息对consumer才是可见的。可以采用事务消息来完成这个功能，RocketMQ 从3.0.14版本开始，对于事务消息，开始检查SendStatus，如果不为SEND_OK，则直接执行事务消息的回滚。
 */

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class ServiceResult implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1597417539644048602L;
	private String key;
	private String status;
	
	@JsonInclude(Include.NON_NULL)
	private String message;
	
	public ServiceResult() {
		this.status = ResponseStatus.OK.getResponseStatus();
	}
	
	public void setFail() {
		this.status = ResponseStatus.ERROR.getResponseStatus();
	}
	

	public void setFail( String message ) {
		this.status = ResponseStatus.ERROR.getResponseStatus();
		this.message = message;
	}
	
	public void setFail( String errCode, String message ) {
		this.status = errCode;
		this.message = message;
	}
	
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
	/*
	public void setSendResult( SendResult sendResult ) {
		//super(sendResult.getSendStatus, sendResult.getMsgId(), sendResult.getMessageQueue(), sendResult.getQueueOffset(), sendResult.getTransactionId(),
			//	sendResult.getOffsetMsgId(), sendResult.getRegionId() );
		
		
		super.setMessageQueue(sendResult.getMessageQueue());
		super.setMsgId(sendResult.getMsgId());
		super.setOffsetMsgId(sendResult.getOffsetMsgId());
		super.setQueueOffset(sendResult.getQueueOffset());
		super.setRegionId(sendResult.getRegionId());
		super.setSendStatus(sendResult.getSendStatus());
		super.setTransactionId(sendResult.getTransactionId());
		
	}*/
	
}
