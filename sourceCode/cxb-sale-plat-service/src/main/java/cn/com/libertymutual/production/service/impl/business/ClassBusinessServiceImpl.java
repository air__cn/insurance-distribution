package cn.com.libertymutual.production.service.impl.business;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdclass;
import cn.com.libertymutual.production.pojo.request.PrpdClassRequest;
import cn.com.libertymutual.production.pojo.response.Response;
import cn.com.libertymutual.production.service.api.business.ClassBusinessService;

import com.github.pagehelper.PageInfo;

/**
 * 事务管理注意事项：
 * 1. 新增/修改操作必须启用事务
 * 2. 方法体内部抛出异常即可启用事务
 * @author Steven.Li
 * @date 2017年7月28日
 * 
 */
@Service
@Transactional(rollbackFor = Exception.class, transactionManager = "transactionManagerCoreData")
public class ClassBusinessServiceImpl extends ClassBusinessService {

	@Override
	public Response findPrpdClass(PrpdClassRequest request) {
		Response result = new Response();
		try {
			PageInfo<Prpdclass> pageInfo = prpdClassService.findByPage(request);
			result.setTotal(pageInfo.getTotal());
			result.setResult(pageInfo.getList());
			result.setSuccess();
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}

	@Override
	public Response checkClassCodeUsed(String classcode) {
		Response result = new Response();
		try {
			boolean isUsed = prpdClassService.checkClassCodeUsed(classcode);
			result.setResult(isUsed);
			result.setSuccess();
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}

	@Override
	public Response insert(PrpdClassRequest request) throws Exception {
		Response result = new Response();
		Prpdclass prpdClass = new Prpdclass();
		try {
			BeanUtils.copyProperties(request, prpdClass);
			prpdClass.setNewclasscode(request.getClasscode());
			prpdClassService.insert(prpdClass);
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			throw e;
		}
		return result;
	}

	@Override
	public Response update(PrpdClassRequest request) throws Exception {
		Response result = new Response();
		Prpdclass prpdClass = new Prpdclass();
		try {
			BeanUtils.copyProperties(request, prpdClass);
			prpdClassService.update(prpdClass);
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			throw e;
		}
		return result;
	}

}
