package cn.com.libertymutual.production.service.api;


import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdkind;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdkindlibrary;

public interface PrpdKindService {

	public void insert(Prpdkind record);
	
	public void update(Prpdkind record);
	
	public List<Prpdkind> check(Prpdkind prpdkind);
	
	public List<Prpdkind> select(Prpdkind prpdkind);
	
	public void delete(Prpdkind prpdkind);
	
	public boolean checkItemLinked(String kindCode);
	
	public List<Prpdkindlibrary> findKindsByRisk(String riskCode, String riskVersion);
	
}
