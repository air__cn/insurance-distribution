package cn.com.libertymutual.sp.dto;

import java.io.Serializable;
import java.util.List;

import cn.com.libertymutual.sp.bean.TbSpPlan;
import cn.com.libertymutual.sp.dto.queryplans.CrossSalePlan;

import com.google.common.collect.Lists;

public class SpPlansResponseDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<CrossSalePlan> planLists;
	private  List<TbSpPlan> spPlans;
	
	public List<CrossSalePlan> getPlanLists() {
		if(null==planLists){
			planLists=Lists.newArrayList();
		}
		return planLists;
	}

	public void setPlanLists(List<CrossSalePlan> planLists) {
		this.planLists = planLists;
	}

	public List<TbSpPlan> getSpPlans() {
		if(null==spPlans){
			spPlans=Lists.newArrayList();
		}
		return spPlans;
	}

	public void setSpPlans(List<TbSpPlan> spPlans) {
		this.spPlans = spPlans;
	}
	
}
