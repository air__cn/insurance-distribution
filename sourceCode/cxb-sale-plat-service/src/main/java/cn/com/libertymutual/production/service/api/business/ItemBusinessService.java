package cn.com.libertymutual.production.service.api.business;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import cn.com.libertymutual.production.pojo.request.PrpdItemLibraryRequest;
import cn.com.libertymutual.production.pojo.response.Response;
import cn.com.libertymutual.production.service.api.PrpdItemLibraryService;
import cn.com.libertymutual.production.service.api.PrpdItemService;
import cn.com.libertymutual.production.service.api.PrpdLogOperationService;
import cn.com.libertymutual.production.service.api.PrpdLogSettingService;
import cn.com.libertymutual.production.service.api.PrpdrationService;
import cn.com.libertymutual.production.utils.Constant;

/** 
 * @Description: 后台业务逻辑入口
 * @author Steven.Li
 * @date 2017年7月28日
 *  
 */
public abstract class ItemBusinessService {

	protected static final Logger log = Constant.log;
	
	@Autowired
	protected PrpdLogSettingService prpdLogSettingService;
	@Autowired
	protected PrpdLogOperationService prpdLogOperationService;
	@Autowired
	protected PrpdItemLibraryService prpdItemLibraryService;
	@Autowired
	protected PrpdItemService prpdItemService;
	@Autowired
	protected PrpdrationService prpdrationService;
	
	/**
	 * 责任管理页面查询接口
	 * @return
	 */
	public abstract Response selectItem(PrpdItemLibraryRequest request);
	
	/**
	 * 修改责任
	 * @param request
	 * @return
	 */
	public abstract Response update(PrpdItemLibraryRequest request) throws Exception;

	public abstract Response selectTopItems();
}