package cn.com.libertymutual.production.service.impl.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xiaoleilu.hutool.util.CollectionUtil;

import cn.com.libertymutual.production.exception.UserException;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdplanprops;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdprops;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdration;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdriskplan;
import cn.com.libertymutual.production.pojo.request.PrpdPlanPropsRequest;
import cn.com.libertymutual.production.pojo.request.PrpdPropsRequest;
import cn.com.libertymutual.production.pojo.response.Response;
import cn.com.libertymutual.production.service.api.business.PropsBusinessService;

/**
 * 事务管理注意事项：
 * 1. 新增/修改操作必须启用事务
 * 2. 方法体内部抛出异常即可启用事务
 * @author Steven.Li
 * @date 2017年7月28日
 * 
 */
@Service
@Transactional(rollbackFor = Exception.class, transactionManager = "transactionManagerCoreData")
public class PropsBusinessServiceImpl extends PropsBusinessService {

	@Override
	public Response findProps(PrpdPropsRequest request) {
		Response result = new Response();
		try {
			List<Prpdprops> props = prpdPropsService.select(request);
			result.setResult(props);
			result.setSuccess();
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}

	@Override
	public Response insertProps(PrpdPlanPropsRequest request) throws Exception {
		Response result = new Response();
		try {
			////////////////////////////////////////////////////////////////////////////////////////
			/////////////////2724-座位类型特殊处理，临时添加，后期将移除/////////////////////////////////
			////////////////////////////////////////////////////////////////////////////////////////
			if ("2724".equals(request.getPlan().getRiskcode()) && !request.getProps().isEmpty()) { 
				List<Prpdplanprops> multiProps = new ArrayList<Prpdplanprops>();
				for (Prpdplanprops prop : request.getProps()) {
					if ("00001".equals(prop.getPropscode())) { //00001 - 座位
						multiProps.add(prop);
					}
				}
				if (!CollectionUtil.isEmpty(multiProps)) {
					Prpdplanprops regionProp = null;
					Prpdration record = new Prpdration();
					for (Prpdplanprops prop : multiProps) {
						regionProp = prop;
						record.setFreeitem1(prop.getPropsvalue());
						record.setFreeitem2("");
						if ("1".equals(prop.getPropstype())) {   //座位区间值，优先保存
							record.setFreeitem1(regionProp.getStartvalue());
							record.setFreeitem2(regionProp.getEndvalue());
							break;
						}
					}
					Prpdration criteria = new Prpdration();
					criteria.setPlancode(regionProp.getPlancode());
					criteria.setRiskcode(regionProp.getRiskcode());
					criteria.setRiskversion(regionProp.getRiskversion());
					prpdrationService.updateByCriteria(record, criteria);
				} else {
					throw new UserException("2724相关方案必须录入【座位】因子！");
				}
			} else if(request.getProps().isEmpty()) {
				Prpdration record = new Prpdration();
				Prpdration criteria = new Prpdration();
				criteria.setPlancode(request.getPlan().getPlancode());
				criteria.setRiskcode(request.getPlan().getRiskcode());
				criteria.setRiskversion(request.getPlan().getRiskversion());
				record.setFreeitem1("");
				record.setFreeitem2("");
				prpdrationService.updateByCriteria(record, criteria);
			}
			////////////////////////////////////////////////////////////////////////////////////////
			
			prpdPlanPropsService.deleteByPlan(request.getPlan());
			Map<String, Integer> lineNos = new HashMap<String, Integer>();
			for (Prpdplanprops prop : request.getProps()) {
				Integer lineNo = lineNos.get(prop.getPropscode());
				if (lineNo == null) {
					lineNo = 1;
				} else {
					lineNo++;
				}
				lineNos.put(prop.getPropscode(), lineNo);
				prop.setLineno(lineNo.toString());
				if ("0".equals(prop.getPropstype())) {
					prop.setStartvalue(null);
					prop.setEndvalue(null);
				} else {
					prop.setPropsvalue(null);
				}
				prop.setValidstatus("1");
				prpdPlanPropsService.insert(prop);
			}
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			throw e;
		}
		return result;
	}

	@Override
	public Response findPlanProps(Prpdriskplan plan) {
		Response result = new Response();
		try {
			List<Prpdplanprops> planProps = prpdPlanPropsService.select(plan);
			result.setResult(planProps);
			result.setSuccess();
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}

}
