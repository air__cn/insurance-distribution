package cn.com.libertymutual.sp.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tb_sp_plan")
public class TbSpPlan implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String productId;
	private String planId;
	private String planName;
	private int serialNo;
	private String remark;
	private String statement;// 投保声明
	private String notice;// 理赔须知
	private String clauseUrl;// 保险条款PDF
	private String minAge;// 最小年龄
	private String maxAge;// 最大年龄
	private String maxDeadLine;// 保障期限
	private String minDeadLine;// 保障期限
	private String ageRelated;// 是否与年龄相关
	private String clauseContent;// 保险条款内容
	private String limitTerm;
	private String status;
	private String isImmediate;
	private Integer minMonth;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "MIN_MONTH")
	public Integer getMinMonth() {
		return minMonth;
	}

	public void setMinMonth(Integer minMonth) {
		this.minMonth = minMonth;
	}

	@Column(name = "IS_IMMEDIATE")
	public String getIsImmediate() {
		return isImmediate;
	}

	public void setIsImmediate(String isImmediate) {
		this.isImmediate = isImmediate;
	}

	@Column(name = "age_related", length = 10)
	public String getAgeRelated() {
		return ageRelated;
	}

	public void setAgeRelated(String ageRelated) {
		this.ageRelated = ageRelated;
	}

	@Column(name = "PRODUCT_ID", length = 10)
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	@Column(name = "PLAN_ID", length = 10)
	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	@Column(name = "PLAN_NAME", length = 100)
	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	@Column(name = "SERIAL_NO")
	public int getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(int serialNo) {
		this.serialNo = serialNo;
	}

	@Column(name = "REMARK", length = 100)
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "statement")
	public String getStatement() {
		return statement;
	}

	public void setStatement(String statement) {
		this.statement = statement;
	}

	@Column(name = "notice")
	public String getNotice() {
		return notice;
	}

	public void setNotice(String notice) {
		this.notice = notice;
	}

	@Column(name = "clause_url")
	public String getClauseUrl() {
		return clauseUrl;
	}

	public void setClauseUrl(String clause_url) {
		this.clauseUrl = clause_url;
	}

	@Column(name = "MAX_DEADLINE", length = 100)
	public String getMaxDeadLine() {
		return maxDeadLine;
	}

	public void setMaxDeadLine(String maxDeadLine) {
		this.maxDeadLine = maxDeadLine;
	}

	@Column(name = "MIN_DEADLINE", length = 100)
	public String getMinDeadLine() {
		return minDeadLine;
	}

	public void setMinDeadLine(String minDeadLine) {
		this.minDeadLine = minDeadLine;
	}

	@Column(name = "MIN_AGE")
	public String getMinAge() {
		return minAge;
	}

	public void setMinAge(String minAge) {
		this.minAge = minAge;
	}

	@Column(name = "MAX_AGE")
	public String getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(String maxAge) {
		this.maxAge = maxAge;
	}

	@Column(name = "Clause_Content")
	public String getClauseContent() {
		return clauseContent;
	}

	public void setClauseContent(String clauseContent) {
		this.clauseContent = clauseContent;
	}

	@Column(name = "limit_term")
	public String getLimitTerm() {
		return limitTerm;
	}

	public void setLimitTerm(String limitTerm) {
		this.limitTerm = limitTerm;
	}

	@Column(name = "STATUS", length = 2)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
