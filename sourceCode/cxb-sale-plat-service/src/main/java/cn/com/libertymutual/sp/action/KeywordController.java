package cn.com.libertymutual.sp.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.util.Current;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.service.api.KeywordService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/nol/keyword")
public class KeywordController {

	@Autowired
	private KeywordService keywordService;

	/*
	 * 保存店铺名称、摘要
	 */
	@ApiOperation(value = "保存店铺名称、摘要", notes = "保存店铺名称、摘要")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "userCode", value = "类型", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "shopName", value = "类型", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "shopIntroduct", value = "内容", required = true, paramType = "query", dataType = "String"), })
	@RequestMapping(value = "/saveShopName", method = RequestMethod.POST)
	public ServiceResult saveShopName(String userCode, String shopName, String shopIntroduct) {

		return keywordService.saveShopName(Current.userCode.get(), shopName, shopIntroduct);
	}

}
