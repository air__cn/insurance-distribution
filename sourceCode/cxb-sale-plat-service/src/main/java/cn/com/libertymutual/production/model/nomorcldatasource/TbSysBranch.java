package cn.com.libertymutual.production.model.nomorcldatasource;

public class TbSysBranch {
    private String branchNo;

    private String branchCname;

    private String branchEname;

    private String addressCname;

    private String addressEname;

    private String postcode;

    private String phoneNumber;

    private String upperBranchNo;

    private String branchType;

    private String branchLevel;

    private String validStatus;

    private String remark;

    private Double rate;

    public String getBranchNo() {
        return branchNo;
    }

    public void setBranchNo(String branchNo) {
        this.branchNo = branchNo == null ? null : branchNo.trim();
    }

    public String getBranchCname() {
        return branchCname;
    }

    public void setBranchCname(String branchCname) {
        this.branchCname = branchCname == null ? null : branchCname.trim();
    }

    public String getBranchEname() {
        return branchEname;
    }

    public void setBranchEname(String branchEname) {
        this.branchEname = branchEname == null ? null : branchEname.trim();
    }

    public String getAddressCname() {
        return addressCname;
    }

    public void setAddressCname(String addressCname) {
        this.addressCname = addressCname == null ? null : addressCname.trim();
    }

    public String getAddressEname() {
        return addressEname;
    }

    public void setAddressEname(String addressEname) {
        this.addressEname = addressEname == null ? null : addressEname.trim();
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode == null ? null : postcode.trim();
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber == null ? null : phoneNumber.trim();
    }

    public String getUpperBranchNo() {
        return upperBranchNo;
    }

    public void setUpperBranchNo(String upperBranchNo) {
        this.upperBranchNo = upperBranchNo == null ? null : upperBranchNo.trim();
    }

    public String getBranchType() {
        return branchType;
    }

    public void setBranchType(String branchType) {
        this.branchType = branchType == null ? null : branchType.trim();
    }

    public String getBranchLevel() {
        return branchLevel;
    }

    public void setBranchLevel(String branchLevel) {
        this.branchLevel = branchLevel == null ? null : branchLevel.trim();
    }

    public String getValidStatus() {
        return validStatus;
    }

    public void setValidStatus(String validStatus) {
        this.validStatus = validStatus == null ? null : validStatus.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }
}