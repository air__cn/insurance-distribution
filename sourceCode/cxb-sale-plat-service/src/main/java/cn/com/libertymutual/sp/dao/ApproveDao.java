package cn.com.libertymutual.sp.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import cn.com.libertymutual.sp.bean.TbSpApprove;
@Repository
public interface ApproveDao extends PagingAndSortingRepository<TbSpApprove, Integer>, JpaSpecificationExecutor<TbSpApprove>{

	
	@Query("select t from TbSpApprove t where t.type = ?1 and t.approveId = ?2 and t.status = ?3")
	TbSpApprove findByTypeAndApproveId(String type, String approveId,String status);//待审批

	@Query("select t from TbSpApprove t where t.type = ?1 and t.approveId = ?2 and t.status = ?3 and t.flowType=?4 order by id desc")
	List<TbSpApprove> findConfigByTypeAndApproveId(String type, String approveId,String status,String flowType);//待审批
	
	@Query("select t from TbSpApprove t where t.type = ?1 and t.approveId = ?2 and t.status = ?3 order by id desc")
	List<TbSpApprove> findConfigByTypeAndApproveId(String type, String approveId,String status);//待审批
	
	@Query("select t from TbSpApprove t where t.type = ?1 and t.approveId = ?2 ")
	Page<TbSpApprove> findByTypeAndApproveIdList(String type, String approveId,Pageable pageable);
	
	@Query("select t from TbSpApprove t where t.type = ?1 and t.approveId = ?2 and t.status = 1")
	Page<TbSpApprove> findByTypeAndApproveId(String type, String approveId,Pageable pageable);//审批通过
	
	@Query("select t from TbSpApprove t where t.type in('4','5','6') and t.approveId = ?1 ")
	Page<TbSpApprove> findActivityByApproveId(String approveId,Pageable pageable);
//	@Transactional
//	@Modifying
//	@Query("update TbSpApprove set status = ?1 where type=?2 and approveId = ?3 and status = '0' ")
//	void updateStatus(String status,String type,Integer approveId);
	
	//某审批类型的所有待审批记录
	@Query("select t from TbSpApprove t where t.type = ?1 and t.status = ?2")
	Page<TbSpApprove> findByTypeAndStatus(String type,String status,Pageable pageable);//审批通过


	@Query("select t from TbSpApprove t where t.type = ?1 and t.approveId = ?2 and t.status = ?3 and t.flowType=?4")
	TbSpApprove findApproveFlow(String type, String approveId,String status,String flowType);//待审批
	
	@Query("select t from TbSpApprove t where  t.status = ?1 and t.flowNo=?2")
	List<TbSpApprove> findByFlowType(String status,String flowNo);//待审批
}
