package cn.com.libertymutual.production.pojo.request;

import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.Fhexitemkind;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdkindlibrary;

public class FhFhxDto extends Request {

    private String fhtreaty;
    private String fhsection;
    private String fhexitemkind;
    private List<Fhexitemkind> fhexitemkindArray;
    private String inrateflag;
    private String fhxtreaty;
    private String fhxlayer;
    private String fhxsection;

    public String getInrateflag() {
        return inrateflag;
    }

    public void setInrateflag(String inrateflag) {
        this.inrateflag = inrateflag;
    }

    public List<Fhexitemkind> getFhexitemkindArray() {
        return fhexitemkindArray;
    }

    public void setFhexitemkindArray(List<Fhexitemkind> fhexitemkindArray) {
        this.fhexitemkindArray = fhexitemkindArray;
    }

    public String getFhtreaty() {
        return fhtreaty;
    }

    public void setFhtreaty(String fhtreaty) {
        this.fhtreaty = fhtreaty;
    }

    public String getFhsection() {
        return fhsection;
    }

    public void setFhsection(String fhsection) {
        this.fhsection = fhsection;
    }

    public String getFhexitemkind() {
        return fhexitemkind;
    }

    public void setFhexitemkind(String fhexitemkind) {
        this.fhexitemkind = fhexitemkind;
    }

    public String getFhxtreaty() {
        return fhxtreaty;
    }

    public void setFhxtreaty(String fhxtreaty) {
        this.fhxtreaty = fhxtreaty;
    }

    public String getFhxlayer() {
        return fhxlayer;
    }

    public void setFhxlayer(String fhxlayer) {
        this.fhxlayer = fhxlayer;
    }

    public String getFhxsection() {
        return fhxsection;
    }

    public void setFhxsection(String fhxsection) {
        this.fhxsection = fhxsection;
    }

}
