package cn.com.libertymutual.sp.dto.car.bean;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;
/**
 * Created by Ryan on 2016-09-05.
 */
public class TpTmain  implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = -2487182566975394891L;
	//商业险即时起保
	private boolean motorIsJS =false;
	//交强险即时起保
	private boolean mtplIsJS =false;
	
	
	private String flowId;
    public String getFlowId() {
        return flowId;
    }
    public void setFlowId(String flowId) {
        this.flowId = flowId;
    }
    private String partnerAccountCode;
    public String getPartnerAccountCode() {
        return partnerAccountCode;
    }
    public void setPartnerAccountCode(String partnerAccountCode) {
        this.partnerAccountCode = partnerAccountCode;
    }
    private Date operatorTime;
    public Date getOperatorTime() {
        return operatorTime;
    }
    public void setOperatorTime(Date operatorTime) {
        this.operatorTime = operatorTime;
    }
    private String recordCode;
    public String getRecordCode() {
        return recordCode;
    }
    public void setRecordCode(String recordCode) {
        this.recordCode = recordCode;
    }
    private String agreementNo;
    public String getAgreementNo() {
        return agreementNo;
    }
    public void setAgreementNo(String agreementNo) {
        this.agreementNo = agreementNo;
    }
    /**
     * 新增续保标识字段 
     */
    private String renewalFlag;
    
    public String getRenewalFlag() {
    	return renewalFlag;
    }
    
    public void setRenewalFlag(String renewalFlag) {
    	this.renewalFlag = renewalFlag;
    }
    private String proposalNo;
    public String getProposalNo() {
        return proposalNo;
    }
    public void setProposalNo(String proposalNo) {
        this.proposalNo = proposalNo;
    }
    private String mtplProposalNo;
    public String getMtplProposalNo() {
        return mtplProposalNo;
    }
    public void setMtplProposalNo(String mtplProposalNo) {
        this.mtplProposalNo = mtplProposalNo;
    }
    private String policyType;
    public String getPolicyType() {
        return policyType;
    }
    public void setPolicyType(String policyType) {
        this.policyType = policyType;
    }
    private String startDate;
    public String getStartDate() {
        return startDate;
    }
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }
    private String endDate;
    public String getEndDate() {
        return endDate;
    }
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
    private String startHour;
    
    public String getStartHour() {
    	return startHour;
    }
    
    public void setStartHour(String startHour) {
    	this.startHour = startHour;
    }
    
    private String endHour;
    
    public String getEndHour() {
    	return endHour;
    }
    
    public void setEndHour(String endHour) {
    	this.endHour = endHour;
    }
    
    private String mtplStartDate;
    public String getMtplStartDate() {
        return mtplStartDate;
    }
    public void setMtplStartDate(String mtplStartDate) {
        this.mtplStartDate = mtplStartDate;
    }
    private String mtplEndDate;
    public String getMtplEndDate() {
        return mtplEndDate;
    }
    public void setMtplEndDate(String mtplEndDate) {
        this.mtplEndDate = mtplEndDate;
    }
    
    
    
    
    
    
    private String mtplStartHour;
    
    public String getMtplStartHour() {
    	return mtplStartHour;
    }
    
    public void setMtplStartHour(String mtplStartHour) {
    	this.mtplStartHour = mtplStartHour;
    }
    
    private String mtplEndHour;
    
    public String getMtplEndHour() {
    	return mtplEndHour;
    }
    
    public void setMtplEndHour(String mtplEndHour) {
    	this.mtplEndHour = mtplEndHour;
    }
    private Double premium;
    private Double discountMain;
    public Double getDiscountMain() {
        return discountMain;
    }
    public void setDiscountMain(Double discountMain) {
        this.discountMain = discountMain;
    }
    
    public Double getPremium() {
        return premium;
    }
    public void setPremium(Double premium) {
        this.premium = premium;
    }
    private Double mtplPremium;
    public Double getMtplPremium() {
        return mtplPremium;
    }
    public void setMtplPremium(Double mtplPremium) {
        this.mtplPremium = mtplPremium;
    }
    private Double mtplDiscountMain;
    
    public Double getMtplDiscountMain() {
        return mtplDiscountMain;
    }
    public void setMtplDiscountMain(Double mtplDiscountMain) {
        this.mtplDiscountMain = mtplDiscountMain;
    }
    
    
    private Double tax;
    public Double getTax() {
        return tax;
    }
    public void setTax(Double tax) {
        this.tax = tax;
    }
    private String plateno;
    public String getPlateno() {
        return plateno;
    }
    public void setPlateno(String plateno) {
        this.plateno = plateno;
    }
    private String vinno;
    public String getVinno() {
        return vinno;
    }
    public void setVinno(String vinno) {
        this.vinno = vinno;
    }
    private String applicantId;
    public String getApplicantId() {
        return applicantId;
    }
    public void setApplicantId(String applicantId) {
        this.applicantId = applicantId;
    }
    private String applicantName;
    public String getApplicantName() {
        return applicantName;
    }
    public void setApplicantName(String applicantName) {
        this.applicantName = applicantName;
    }
    private String insuredId;
    public String getInsuredId() {
        return insuredId;
    }
    public void setInsuredId(String insuredId) {
        this.insuredId = insuredId;
    }
    private String insuredName;
    public String getInsuredName() {
        return insuredName;
    }
    public void setInsuredName(String insuredName) {
        this.insuredName = insuredName;
    }
    private String carholderId;
    public String getCarholderId() {
        return carholderId;
    }
    public void setCarholderId(String carholderId) {
        this.carholderId = carholderId;
    }
    private String carholderName;
    public String getCarholderName() {
        return carholderName;
    }
    public void setCarholderName(String carholderName) {
        this.carholderName = carholderName;
    }
    private String status;
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    private String updateUser;
    public String getUpdateUser() {
        return updateUser;
    }
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }
    private Date updateTime;
    public Date getUpdateTime() {
        return updateTime;
    }
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
    private Date createTime;
    public Date getCreateTime() {
        return createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    private String createUser;
    public String getCreateUser() {
        return createUser;
    }
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }
    private String remark;
    public String getRemark() {
        return remark;
    }
    public void setRemark(String remark) {
        this.remark = remark;
    }
    private String combineFlag;
    public String getCombineFlag() {
        return combineFlag;
    }
    public void setCombineFlag(String combineFlag) {
        this.combineFlag = combineFlag;
    }
  
    public String getLastyearClaimTime() {
		return lastyearClaimTime;
	}
	public void setLastyearClaimTime(String lastyearClaimTime) {
		this.lastyearClaimTime = lastyearClaimTime;
	}
	private String lastyearClaimTime;
	/**
	 * 返回新增数据 业务结算要求   20161222
	 * 业务分类   脱保车,新车,续保车
	 */
	private String  businessType;
	public String getBusinessType() {
		return businessType;
	}
	
	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}
    
	 
	private String autonomyAdjustValue	;	//自主核保使用系数
	private String channelAdjustValue	;	//渠道使用系数
	private String autonomyAdjust		;//自主核保建议系数
	private String channelAdjust		;//渠道建议系数
	/**
	 * 自主核保使用系数
	 * @return
	 */
	public String getAutonomyAdjustValue() {
		return autonomyAdjustValue;
	}
/**
 * 渠道使用系数
 * @return
 */
	public String getChannelAdjustValue() {
		return channelAdjustValue;
	}
/**
 * 自主核保建议系数
 * @return
 */
	public String getAutonomyAdjust() {
		return autonomyAdjust;
	}
/**
 * 渠道建议系数
 * @return
 */
	public String getChannelAdjust() {
		return channelAdjust;
	}
	public void setAutonomyAdjustValue(String autonomyAdjustValue) {
		this.autonomyAdjustValue = autonomyAdjustValue;
	}
	public void setChannelAdjustValue(String channelAdjustValue) {
		this.channelAdjustValue = channelAdjustValue;
	}
	public void setAutonomyAdjust(String autonomyAdjust) {
		this.autonomyAdjust = autonomyAdjust;
	}
	public void setChannelAdjust(String channelAdjust) {
		this.channelAdjust = channelAdjust;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((agreementNo == null) ? 0 : agreementNo.hashCode());
		result = prime * result
				+ ((applicantId == null) ? 0 : applicantId.hashCode());
		result = prime * result
				+ ((applicantName == null) ? 0 : applicantName.hashCode());
		result = prime * result
				+ ((autonomyAdjust == null) ? 0 : autonomyAdjust.hashCode());
		result = prime
				* result
				+ ((autonomyAdjustValue == null) ? 0 : autonomyAdjustValue
						.hashCode());
		result = prime * result
				+ ((businessType == null) ? 0 : businessType.hashCode());
		result = prime * result
				+ ((carholderId == null) ? 0 : carholderId.hashCode());
		result = prime * result
				+ ((carholderName == null) ? 0 : carholderName.hashCode());
		result = prime * result
				+ ((channelAdjust == null) ? 0 : channelAdjust.hashCode());
		result = prime
				* result
				+ ((channelAdjustValue == null) ? 0 : channelAdjustValue
						.hashCode());
		result = prime * result
				+ ((combineFlag == null) ? 0 : combineFlag.hashCode());
		result = prime * result
				+ ((createTime == null) ? 0 : createTime.hashCode());
		result = prime * result
				+ ((createUser == null) ? 0 : createUser.hashCode());
		result = prime * result
				+ ((discountMain == null) ? 0 : discountMain.hashCode());
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + ((endHour == null) ? 0 : endHour.hashCode());
		result = prime * result + ((flowId == null) ? 0 : flowId.hashCode());
		result = prime * result
				+ ((insuredId == null) ? 0 : insuredId.hashCode());
		result = prime * result
				+ ((insuredName == null) ? 0 : insuredName.hashCode());
		result = prime
				* result
				+ ((lastyearClaimTime == null) ? 0 : lastyearClaimTime
						.hashCode());
		result = prime * result + (motorIsJS ? 1231 : 1237);
		result = prime
				* result
				+ ((mtplDiscountMain == null) ? 0 : mtplDiscountMain.hashCode());
		result = prime * result
				+ ((mtplEndDate == null) ? 0 : mtplEndDate.hashCode());
		result = prime * result
				+ ((mtplEndHour == null) ? 0 : mtplEndHour.hashCode());
		result = prime * result + (mtplIsJS ? 1231 : 1237);
		result = prime * result
				+ ((mtplPremium == null) ? 0 : mtplPremium.hashCode());
		result = prime * result
				+ ((mtplProposalNo == null) ? 0 : mtplProposalNo.hashCode());
		result = prime * result
				+ ((mtplStartDate == null) ? 0 : mtplStartDate.hashCode());
		result = prime * result
				+ ((mtplStartHour == null) ? 0 : mtplStartHour.hashCode());
		result = prime * result
				+ ((operatorTime == null) ? 0 : operatorTime.hashCode());
		result = prime
				* result
				+ ((partnerAccountCode == null) ? 0 : partnerAccountCode
						.hashCode());
		result = prime * result + ((plateno == null) ? 0 : plateno.hashCode());
		result = prime * result
				+ ((policyType == null) ? 0 : policyType.hashCode());
		result = prime * result + ((premium == null) ? 0 : premium.hashCode());
		result = prime * result
				+ ((proposalNo == null) ? 0 : proposalNo.hashCode());
		result = prime * result
				+ ((recordCode == null) ? 0 : recordCode.hashCode());
		result = prime * result + ((remark == null) ? 0 : remark.hashCode());
		result = prime * result
				+ ((renewalFlag == null) ? 0 : renewalFlag.hashCode());
		result = prime * result
				+ ((startDate == null) ? 0 : startDate.hashCode());
		result = prime * result
				+ ((startHour == null) ? 0 : startHour.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((tax == null) ? 0 : tax.hashCode());
		result = prime * result
				+ ((updateTime == null) ? 0 : updateTime.hashCode());
		result = prime * result
				+ ((updateUser == null) ? 0 : updateUser.hashCode());
		result = prime * result + ((vinno == null) ? 0 : vinno.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TpTmain other = (TpTmain) obj;
		if (agreementNo == null) {
			if (other.agreementNo != null)
				return false;
		} else if (!agreementNo.equals(other.agreementNo))
			return false;
		if (applicantId == null) {
			if (other.applicantId != null)
				return false;
		} else if (!applicantId.equals(other.applicantId))
			return false;
		if (applicantName == null) {
			if (other.applicantName != null)
				return false;
		} else if (!applicantName.equals(other.applicantName))
			return false;
		if (autonomyAdjust == null) {
			if (other.autonomyAdjust != null)
				return false;
		} else if (!autonomyAdjust.equals(other.autonomyAdjust))
			return false;
		if (autonomyAdjustValue == null) {
			if (other.autonomyAdjustValue != null)
				return false;
		} else if (!autonomyAdjustValue.equals(other.autonomyAdjustValue))
			return false;
		if (businessType == null) {
			if (other.businessType != null)
				return false;
		} else if (!businessType.equals(other.businessType))
			return false;
		if (carholderId == null) {
			if (other.carholderId != null)
				return false;
		} else if (!carholderId.equals(other.carholderId))
			return false;
		if (carholderName == null) {
			if (other.carholderName != null)
				return false;
		} else if (!carholderName.equals(other.carholderName))
			return false;
		if (channelAdjust == null) {
			if (other.channelAdjust != null)
				return false;
		} else if (!channelAdjust.equals(other.channelAdjust))
			return false;
		if (channelAdjustValue == null) {
			if (other.channelAdjustValue != null)
				return false;
		} else if (!channelAdjustValue.equals(other.channelAdjustValue))
			return false;
		if (combineFlag == null) {
			if (other.combineFlag != null)
				return false;
		} else if (!combineFlag.equals(other.combineFlag))
			return false;
		if (createTime == null) {
			if (other.createTime != null)
				return false;
		} else if (!createTime.equals(other.createTime))
			return false;
		if (createUser == null) {
			if (other.createUser != null)
				return false;
		} else if (!createUser.equals(other.createUser))
			return false;
		if (discountMain == null) {
			if (other.discountMain != null)
				return false;
		} else if (!discountMain.equals(other.discountMain))
			return false;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.equals(other.endDate))
			return false;
		if (endHour == null) {
			if (other.endHour != null)
				return false;
		} else if (!endHour.equals(other.endHour))
			return false;
		if (flowId == null) {
			if (other.flowId != null)
				return false;
		} else if (!flowId.equals(other.flowId))
			return false;
		if (insuredId == null) {
			if (other.insuredId != null)
				return false;
		} else if (!insuredId.equals(other.insuredId))
			return false;
		if (insuredName == null) {
			if (other.insuredName != null)
				return false;
		} else if (!insuredName.equals(other.insuredName))
			return false;
		if (lastyearClaimTime == null) {
			if (other.lastyearClaimTime != null)
				return false;
		} else if (!lastyearClaimTime.equals(other.lastyearClaimTime))
			return false;
		if (motorIsJS != other.motorIsJS)
			return false;
		if (mtplDiscountMain == null) {
			if (other.mtplDiscountMain != null)
				return false;
		} else if (!mtplDiscountMain.equals(other.mtplDiscountMain))
			return false;
		if (mtplEndDate == null) {
			if (other.mtplEndDate != null)
				return false;
		} else if (!mtplEndDate.equals(other.mtplEndDate))
			return false;
		if (mtplEndHour == null) {
			if (other.mtplEndHour != null)
				return false;
		} else if (!mtplEndHour.equals(other.mtplEndHour))
			return false;
		if (mtplIsJS != other.mtplIsJS)
			return false;
		if (mtplPremium == null) {
			if (other.mtplPremium != null)
				return false;
		} else if (!mtplPremium.equals(other.mtplPremium))
			return false;
		if (mtplProposalNo == null) {
			if (other.mtplProposalNo != null)
				return false;
		} else if (!mtplProposalNo.equals(other.mtplProposalNo))
			return false;
		if (mtplStartDate == null) {
			if (other.mtplStartDate != null)
				return false;
		} else if (!mtplStartDate.equals(other.mtplStartDate))
			return false;
		if (mtplStartHour == null) {
			if (other.mtplStartHour != null)
				return false;
		} else if (!mtplStartHour.equals(other.mtplStartHour))
			return false;
		if (operatorTime == null) {
			if (other.operatorTime != null)
				return false;
		} else if (!operatorTime.equals(other.operatorTime))
			return false;
		if (partnerAccountCode == null) {
			if (other.partnerAccountCode != null)
				return false;
		} else if (!partnerAccountCode.equals(other.partnerAccountCode))
			return false;
		if (plateno == null) {
			if (other.plateno != null)
				return false;
		} else if (!plateno.equals(other.plateno))
			return false;
		if (policyType == null) {
			if (other.policyType != null)
				return false;
		} else if (!policyType.equals(other.policyType))
			return false;
		if (premium == null) {
			if (other.premium != null)
				return false;
		} else if (!premium.equals(other.premium))
			return false;
		if (proposalNo == null) {
			if (other.proposalNo != null)
				return false;
		} else if (!proposalNo.equals(other.proposalNo))
			return false;
		if (recordCode == null) {
			if (other.recordCode != null)
				return false;
		} else if (!recordCode.equals(other.recordCode))
			return false;
		if (remark == null) {
			if (other.remark != null)
				return false;
		} else if (!remark.equals(other.remark))
			return false;
		if (renewalFlag == null) {
			if (other.renewalFlag != null)
				return false;
		} else if (!renewalFlag.equals(other.renewalFlag))
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		if (startHour == null) {
			if (other.startHour != null)
				return false;
		} else if (!startHour.equals(other.startHour))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (tax == null) {
			if (other.tax != null)
				return false;
		} else if (!tax.equals(other.tax))
			return false;
		if (updateTime == null) {
			if (other.updateTime != null)
				return false;
		} else if (!updateTime.equals(other.updateTime))
			return false;
		if (updateUser == null) {
			if (other.updateUser != null)
				return false;
		} else if (!updateUser.equals(other.updateUser))
			return false;
		if (vinno == null) {
			if (other.vinno != null)
				return false;
		} else if (!vinno.equals(other.vinno))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "TpTmain [motorIsJS=" + motorIsJS + ", mtplIsJS=" + mtplIsJS
				+ ", flowId=" + flowId + ", partnerAccountCode="
				+ partnerAccountCode + ", operatorTime=" + operatorTime
				+ ", recordCode=" + recordCode + ", agreementNo=" + agreementNo
				+ ", renewalFlag=" + renewalFlag + ", proposalNo=" + proposalNo
				+ ", mtplProposalNo=" + mtplProposalNo + ", policyType="
				+ policyType + ", startDate=" + startDate + ", endDate="
				+ endDate + ", startHour=" + startHour + ", endHour=" + endHour
				+ ", mtplStartDate=" + mtplStartDate + ", mtplEndDate="
				+ mtplEndDate + ", mtplStartHour=" + mtplStartHour
				+ ", mtplEndHour=" + mtplEndHour + ", premium=" + premium
				+ ", discountMain=" + discountMain + ", mtplPremium="
				+ mtplPremium + ", mtplDiscountMain=" + mtplDiscountMain
				+ ", tax=" + tax + ", plateno=" + plateno + ", vinno=" + vinno
				+ ", applicantId=" + applicantId + ", applicantName="
				+ applicantName + ", insuredId=" + insuredId + ", insuredName="
				+ insuredName + ", carholderId=" + carholderId
				+ ", carholderName=" + carholderName + ", status=" + status
				+ ", updateUser=" + updateUser + ", updateTime=" + updateTime
				+ ", createTime=" + createTime + ", createUser=" + createUser
				+ ", remark=" + remark + ", combineFlag=" + combineFlag
				+ ", lastyearClaimTime=" + lastyearClaimTime
				+ ", businessType=" + businessType + ", autonomyAdjustValue="
				+ autonomyAdjustValue + ", channelAdjustValue="
				+ channelAdjustValue + ", autonomyAdjust=" + autonomyAdjust
				+ ", channelAdjust=" + channelAdjust + "]";
	}
	@Transient
	@JsonIgnore
	public boolean isMotorIsJS() {
		return motorIsJS;
	}
	@Transient  //字段数据库忽略
	@JsonIgnore //json转换忽略
	public boolean isMtplIsJS() {
		return mtplIsJS;
	}
	public void setMotorIsJS(boolean motorIsJS) {
		this.motorIsJS = motorIsJS;
	}
	public void setMtplIsJS(boolean mtplIsJS) {
		this.mtplIsJS = mtplIsJS;
	}
    
   
}