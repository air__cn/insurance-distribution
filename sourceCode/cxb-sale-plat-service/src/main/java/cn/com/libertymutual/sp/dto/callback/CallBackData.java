package cn.com.libertymutual.sp.dto.callback;

public class CallBackData {
	private String url; //回调地址
	private String requestType; // POST / GET 
	private String data;   //传送的数据
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	
	
}
