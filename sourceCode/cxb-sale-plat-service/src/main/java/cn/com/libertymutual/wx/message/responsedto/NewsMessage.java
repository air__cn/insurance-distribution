package cn.com.libertymutual.wx.message.responsedto;

import java.util.Calendar;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import cn.com.libertymutual.wx.common.MessageType;

public class NewsMessage extends ResponseBaseMessage {
	@XStreamAlias("ArticleCount")
	private String articleCount;
	@XStreamAlias("Articles")
	private List<Article> articles;

	public NewsMessage() {
		setMsgType(MessageType.RESP_MESSAGE_TYPE_NEWS);
	}

	public NewsMessage(ResponseBaseMessage rbm) {
		long ct = Calendar.getInstance().getTimeInMillis();
		setCreateTime(ct);
		setFromUserName(rbm.getFromUserName());
		setToUserName(rbm.getToUserName());
		setMsgType(MessageType.RESP_MESSAGE_TYPE_NEWS);
	}

	public String getArticleCount() {
		return this.articleCount;
	}

	public void setArticleCount(String articleCount) {
		this.articleCount = articleCount;
	}

	public List<Article> getArticles() {
		return this.articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}

	@Override
	public String toString() {
		return "NewsMessage [articleCount=" + articleCount + ", articles=" + articles + ", getArticleCount()=" + getArticleCount()
				+ ", getArticles()=" + getArticles() + ", getToUserName()=" + getToUserName() + ", getFromUserName()=" + getFromUserName()
				+ ", getCreateTime()=" + getCreateTime() + ", getMsgType()=" + getMsgType() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + ", toString()=" + super.toString() + "]";
	}
}
