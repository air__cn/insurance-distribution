package cn.com.libertymutual.sp.dao;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cn.com.libertymutual.sp.bean.TbSpScoreManual;

@Repository
public interface ScoreManualDao extends PagingAndSortingRepository<TbSpScoreManual, Integer>, JpaSpecificationExecutor<TbSpScoreManual> {

	@Transactional
	@Modifying
	@Query("update TbSpScoreManual set status = ?1 where id = ?2")
	int updateStatus(String status, Integer id);

}
