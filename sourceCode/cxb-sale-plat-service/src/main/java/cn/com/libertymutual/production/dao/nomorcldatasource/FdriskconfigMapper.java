package cn.com.libertymutual.production.dao.nomorcldatasource;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.com.libertymutual.production.model.nomorcldatasource.Fdriskconfig;
import cn.com.libertymutual.production.model.nomorcldatasource.FdriskconfigExample;

@Mapper
public interface FdriskconfigMapper {
    int countByExample(FdriskconfigExample example);

    int deleteByExample(FdriskconfigExample example);

    int insert(Fdriskconfig record);

    int insertSelective(Fdriskconfig record);

    List<Fdriskconfig> selectByExample(FdriskconfigExample example);

    int updateByExampleSelective(@Param("record") Fdriskconfig record, @Param("example") FdriskconfigExample example);

    int updateByExample(@Param("record") Fdriskconfig record, @Param("example") FdriskconfigExample example);
}