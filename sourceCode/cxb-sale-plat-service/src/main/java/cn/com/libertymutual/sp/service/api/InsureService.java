package cn.com.libertymutual.sp.service.api;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpOrder;
import cn.com.libertymutual.sp.dto.InsureQuery;
import cn.com.libertymutual.sp.dto.RateDto;
import cn.com.libertymutual.sp.dto.queryplans.CrossSalePlan;
import cn.com.libertymutual.sp.dto.savePlan.PropsalSaveRequestDto;

public interface InsureService {
	ServiceResult uploadFfle(HttpServletRequest request, HttpServletResponse response, String imgFileName, List<MultipartFile> imgFiles,
			String proposalNo);

	ServiceResult getConfigData(String inputId);

	ServiceResult getFindAllFriend(String userCode, String type, String sort, String serch);

	TbSpOrder setInsureBrachCode(TbSpOrder order, InsureQuery insureQuery, PropsalSaveRequestDto requestDto);

	TbSpOrder setAgrInfo(TbSpOrder order, String agre, String refId) throws Exception;

	TbSpOrder setCommCode(TbSpOrder order, String userCode, String defaultCode);

	String getSaleCode(String agre, String saleNumber) throws Exception;

	TbSpOrder setOrderInitData(TbSpOrder order, InsureQuery insureQuery);

	TbSpOrder setCrossSaleOrderInitData(TbSpOrder order, CrossSalePlan crossSalePlan);

	TbSpOrder setQueryInfo(TbSpOrder order, String proposalNo);

	List<String> topUser(String userCode, List<String> upNumbers);

	RateDto getLevelRate(String userCode);
}
