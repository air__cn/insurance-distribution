package cn.com.libertymutual.sp.webService.einv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 发票开具请求实体
 * @author porter.shen
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)	
@XmlRootElement(name="EinvPrintData")
public class EinvoicePrintRequestDto {
	//字段名	注释	数据类型	最大长度	是否必填	说明
	private String certiNo;	//业务号	字符	22	是	保批单号
	private String emailAddress;	//邮箱地址	字符	100	否	邮箱地址
	private String taxPayerno;	//纳锐人识别号	字符	100	否	纳税人识别号
	private String address;	//地址	字符	255	否	地址
	private String phone;	//电话	字符	20	否	电话
	private String payeeBank;	//银行名称	字符	128	否	银行名称
	private String payeeAccount;	//银行账号	字符	64	否	银行账号
	private String invoiceTitle;	//发票抬头	字符	64	否	发票抬头
	private String riskCode;	//险种代码	字符	8	否	险种代码
	private String isModify;	//是否修改抬头	字符	1	否	1为修改,其他为未修改
	private String isMotorUi;	//数据来源	字符	1	否	1为其他来源，默认为motorUi
	private String invoiceType;	//发票类型	字符	1	否	1为被保险人，其他均为投保人
	
	
	public String getCertiNo() {
		return certiNo;
	}
	public void setCertiNo(String certiNo) {
		this.certiNo = certiNo;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getTaxPayerno() {
		return taxPayerno;
	}
	public void setTaxPayerno(String taxPayerno) {
		this.taxPayerno = taxPayerno;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getPayeeBank() {
		return payeeBank;
	}
	public void setPayeeBank(String payeeBank) {
		this.payeeBank = payeeBank;
	}
	public String getPayeeAccount() {
		return payeeAccount;
	}
	public void setPayeeAccount(String payeeAccount) {
		this.payeeAccount = payeeAccount;
	}
	public String getInvoiceTitle() {
		return invoiceTitle;
	}
	public void setInvoiceTitle(String invoiceTitle) {
		this.invoiceTitle = invoiceTitle;
	}
	public String getRiskCode() {
		return riskCode;
	}
	public void setRiskCode(String riskCode) {
		this.riskCode = riskCode;
	}
	public String getIsModify() {
		return isModify;
	}
	public void setIsModify(String isModify) {
		this.isModify = isModify;
	}
	public String getIsMotorUi() {
		return isMotorUi;
	}
	public void setIsMotorUi(String isMotorUi) {
		this.isMotorUi = isMotorUi;
	}
	public String getInvoiceType() {
		return invoiceType;
	}
	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}
}
