package cn.com.libertymutual.production.pojo.request;

import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdkindlibrary;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdrisk;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdriskplan;
import cn.com.libertymutual.production.pojo.response.ItemKind;

public class PlanAddRequest extends Request {
	private String plantype;
	private Prpdrisk risk;
	private String plancode;
	private String plancname;
	private String planename;
	private String validstatus;
	private String kindcodes;
	private List<Prpdkindlibrary> kinds;
	private String itemcodes;
	private List<ItemKind> items;
	private List<AmountPremium> kinditems;
	private String ilogflag;
	private String delflag;
	private String autoundwrt;
	private String autodoc;
	private String traveldestination;
	private String periodtype;
	private String period;
	private String applycomcode;
	private String applychannel;
	private String occupationcode;
	private Prpdriskplan originalPlan;

	public String getAutodoc() {
		return autodoc;
	}

	public void setAutodoc(String autodoc) {
		this.autodoc = autodoc;
	}

	public String getPlantype() {
		return plantype;
	}

	public void setPlantype(String plantype) {
		this.plantype = plantype;
	}

	public Prpdrisk getRisk() {
		return risk;
	}

	public void setRisk(Prpdrisk risk) {
		this.risk = risk;
	}

	public String getPlancode() {
		return plancode;
	}

	public void setPlancode(String plancode) {
		this.plancode = plancode;
	}

	public String getPlancname() {
		return plancname;
	}

	public void setPlancname(String plancname) {
		this.plancname = plancname;
	}

	public String getPlanename() {
		return planename;
	}

	public void setPlanename(String planename) {
		this.planename = planename;
	}

	public String getValidstatus() {
		return validstatus;
	}

	public void setValidstatus(String validstatus) {
		this.validstatus = validstatus;
	}

	public String getKindcodes() {
		return kindcodes;
	}

	public void setKindcodes(String kindcodes) {
		this.kindcodes = kindcodes;
	}

	public List<Prpdkindlibrary> getKinds() {
		return kinds;
	}

	public void setKinds(List<Prpdkindlibrary> kinds) {
		this.kinds = kinds;
	}

	public String getItemcodes() {
		return itemcodes;
	}

	public void setItemcodes(String itemcodes) {
		this.itemcodes = itemcodes;
	}

	public List<ItemKind> getItems() {
		return items;
	}

	public void setItems(List<ItemKind> items) {
		this.items = items;
	}

	public List<AmountPremium> getKinditems() {
		return kinditems;
	}

	public void setKinditems(List<AmountPremium> kinditems) {
		this.kinditems = kinditems;
	}

	public String getIlogflag() {
		return ilogflag;
	}

	public void setIlogflag(String ilogflag) {
		this.ilogflag = ilogflag;
	}

	public String getDelflag() {
		return delflag;
	}

	public void setDelflag(String delflag) {
		this.delflag = delflag;
	}

	public String getAutoundwrt() {
		return autoundwrt;
	}

	public void setAutoundwrt(String autoundwrt) {
		this.autoundwrt = autoundwrt;
	}

	public String getTraveldestination() {
		return traveldestination;
	}

	public void setTraveldestination(String traveldestination) {
		this.traveldestination = traveldestination;
	}

	public String getPeriodtype() {
		return periodtype;
	}

	public void setPeriodtype(String periodtype) {
		this.periodtype = periodtype;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getOccupationcode() {
		return occupationcode;
	}

	public void setOccupationcode(String occupationcode) {
		this.occupationcode = occupationcode;
	}

	public String getApplycomcode() {
		return applycomcode;
	}

	public void setApplycomcode(String applycomcode) {
		this.applycomcode = applycomcode;
	}

	public String getApplychannel() {
		return applychannel;
	}

	public void setApplychannel(String applychannel) {
		this.applychannel = applychannel;
	}

	public Prpdriskplan getOriginalPlan() {
		return originalPlan;
	}

	public void setOriginalPlan(Prpdriskplan originalPlan) {
		this.originalPlan = originalPlan;
	}

}
