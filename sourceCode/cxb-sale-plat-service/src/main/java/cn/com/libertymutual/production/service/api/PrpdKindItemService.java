package cn.com.libertymutual.production.service.api;


import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdkinditem;

public interface PrpdKindItemService {

	public void insert(Prpdkinditem record);
	
	/**
	 * 关联标的时验证重复
	 * @param record
	 * @return
	 */
	public List<Prpdkinditem> check(Prpdkinditem record);
	
	public List<Prpdkinditem> select(Prpdkinditem prpdkinditem);
	
	public void delete(Prpdkinditem record);
	
}
