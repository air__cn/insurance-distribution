package cn.com.libertymutual.production.model.nomorcldatasource;

public class PrpdriskclauseKey {
    private String riskcode;

    private String clausecode;

    public String getRiskcode() {
        return riskcode;
    }

    public void setRiskcode(String riskcode) {
        this.riskcode = riskcode == null ? null : riskcode.trim();
    }

    public String getClausecode() {
        return clausecode;
    }

    public void setClausecode(String clausecode) {
        this.clausecode = clausecode == null ? null : clausecode.trim();
    }
}