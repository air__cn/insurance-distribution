package cn.com.libertymutual.production.service.api;

import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdserialno;



public interface PrpdSerialNoService {
	
	/**
	 * 查询当前流水号
	 * @param businessType
	 * @param classCode
	 * @param serialType
	 * @return
	 */
	public List<Prpdserialno> findPrpdSerialno(String businessType, String classCode, String serialType);
	
	/**
	 * 创建一条新流水
	 * @param businessType
	 * @param classCode
	 * @param serialType
	 * @return
	 */
	public void insertPrpdSerialno(String businessType, String classCode, String serialType, int serialNo);
	
	/**
	 * 增加流水号
	 * @param Prpdserialno
	 */
	public void updatePrpdSerialno(String businessType, String classCode, String serialType, int serialNo);
}
