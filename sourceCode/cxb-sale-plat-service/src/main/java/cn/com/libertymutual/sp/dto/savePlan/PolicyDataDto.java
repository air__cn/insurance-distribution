package cn.com.libertymutual.sp.dto.savePlan;

import java.util.List;

import cn.com.libertymutual.sp.dto.savePlan.base.AddressBaseDto;
import cn.com.libertymutual.sp.dto.savePlan.base.AgentBaseDto;
import cn.com.libertymutual.sp.dto.savePlan.base.AppliBaseDto;
import cn.com.libertymutual.sp.dto.savePlan.base.InsuredBaseDto;
import cn.com.libertymutual.sp.dto.savePlan.base.ItemKindBaseDto;
import cn.com.libertymutual.sp.dto.savePlan.base.MainHeadBaseDto;
import cn.com.libertymutual.sp.dto.savePlan.base.PeriodBaseDto;
import cn.com.libertymutual.sp.dto.savePlan.base.ProductBaseDto;

public class PolicyDataDto {
	private AddressBaseDto addressDto;
	private AgentBaseDto agentDto;
	private AppliBaseDto appliDto;
	private List<InsuredBaseDto> insuredItemProcutDtoList;
//	private List<ItemKindBaseDto> itemKindDtoList;
	private PeriodBaseDto periodDto;
	private MainHeadBaseDto mainHeadDto;
//	private ProductBaseDto productDto;
	
	public AddressBaseDto getAddressDto() {
		return addressDto;
	}
	public void setAddressDto(AddressBaseDto addressDto) {
		this.addressDto = addressDto;
	}
	public AgentBaseDto getAgentDto() {
		return agentDto;
	}
	public void setAgentDto(AgentBaseDto agentDto) {
		this.agentDto = agentDto;
	}
	public AppliBaseDto getAppliDto() {
		return appliDto;
	}
	public void setAppliDto(AppliBaseDto appliDto) {
		this.appliDto = appliDto;
	}

	
	
public List<InsuredBaseDto> getInsuredItemProcutDtoList() {
		return insuredItemProcutDtoList;
	}
	public void setInsuredItemProcutDtoList(List<InsuredBaseDto> insuredItemProcutDtoList) {
		this.insuredItemProcutDtoList = insuredItemProcutDtoList;
	}
	//	public List<ItemKindBaseDto> getItemKindDtoList() {
//		return itemKindDtoList;
//	}
//	public void setItemKindDtoList(List<ItemKindBaseDto> itemKindDtoList) {
//		this.itemKindDtoList = itemKindDtoList;
//	}
	public MainHeadBaseDto getMainHeadDto() {
		return mainHeadDto;
	}
	public void setMainHeadDto(MainHeadBaseDto mainHeadDto) {
		this.mainHeadDto = mainHeadDto;
	}
//	public PeriodBaseDto getPeriodDto() {
//		return periodDto;
//	}
//	public void setPeriodDto(PeriodBaseDto periodDto) {
//		this.periodDto = periodDto;
//	}
	public PeriodBaseDto getPeriodDto() {
		return periodDto;
	}
	public void setPeriodDto(PeriodBaseDto periodDto) {
		this.periodDto = periodDto;
	}

}
