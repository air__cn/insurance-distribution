package cn.com.libertymutual.production.service.api;

import java.util.List;

import com.github.pagehelper.PageInfo;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdrisk;
import cn.com.libertymutual.production.pojo.request.LinkRiskRequest;
import cn.com.libertymutual.production.pojo.request.PrpdRiskRequest;


public interface PrpdRiskService {
	
	/**
	 * 获取所有险种
	 * @return
	 */
	public List<Prpdrisk> findAllPrpdRisk();
	
	/**
	 * 根据条件查询险种
	 * @param request
	 * @return
	 */
	public List<Prpdrisk> fetchRisks(PrpdRiskRequest request);
	
	public PageInfo<Prpdrisk> findAllByPage(PrpdRiskRequest request);
	
	/**
	 * 查询已关联险种
	 * @param riskCodes
	 * @param linkRiskRequest
	 * @return
	 */
	public PageInfo<Prpdrisk> selectByPage(List<String> riskCodes, LinkRiskRequest linkRiskRequest);
	
	public PageInfo<Prpdrisk> findByClassAndComposite(PrpdRiskRequest request, List<String> compositeClasses);
	
	/**
	 * 查询险别下的所有险种
	 * @param classcode
	 * @return
	 */
	public List<Prpdrisk> findByClass(String classcode);
	
	/**
	 * 检验险种代码是否被使用
	 * @return
	 */
	public boolean cheakRiskCodeUsed(String riskcode);
	
	/**
	 * 新增险种
	 * @param record
	 */
	public void insert(Prpdrisk record);
	
	/**
	 * 修改险种
	 * @param record
	 */
	public void update(Prpdrisk record);

	/**
	 * 查询险种
	 * @param riskcodes
	 * @return
     */
	List<Prpdrisk> queryRiskByCodes(List<String> riskcodes);
}
