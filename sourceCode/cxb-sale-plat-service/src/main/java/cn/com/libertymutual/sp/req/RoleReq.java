package cn.com.libertymutual.sp.req;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;

@ApiModel
public class RoleReq implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 867647541169468179L;

	private List<Integer> fmenuIds;
	private List<String> resids;
	private String roleId;
	private String remark;
	private String roleName;
	private String approveAuth;
	private String configAuth;

	
	public String getApproveAuth() {
		return approveAuth;
	}

	public void setApproveAuth(String approveAuth) {
		this.approveAuth = approveAuth;
	}

	public List<Integer> getFmenuIds() {
		return fmenuIds;
	}

	public void setFmenuIds(List<Integer> fmenuIds) {
		this.fmenuIds = fmenuIds;
	}

	public List<String> getResids() {
		return resids;
	}

	public void setResids(List<String> resids) {
		this.resids = resids;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	
	public String getConfigAuth() {
		return configAuth;
	}

	public void setConfigAuth(String configAuth) {
		this.configAuth = configAuth;
	}

	@Override
	public String toString() {
		return "RoleReq [fmenuIds=" + fmenuIds + ", resids=" + resids + ", roleId=" + roleId + ", remark=" + remark
				+ ", roleName=" + roleName + "]";
	}

}
