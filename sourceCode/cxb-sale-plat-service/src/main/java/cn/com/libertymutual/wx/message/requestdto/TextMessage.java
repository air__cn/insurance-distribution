package cn.com.libertymutual.wx.message.requestdto;

import com.thoughtworks.xstream.annotations.XStreamAlias;

public class TextMessage extends RequestBaseMessage {
	@XStreamAlias("Content")
	private String content;

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}