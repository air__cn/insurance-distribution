package cn.com.libertymutual.sp.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import cn.com.libertymutual.sp.bean.TbSpClause;

@Repository
public interface ClauseDao extends PagingAndSortingRepository<TbSpClause, Integer>, JpaSpecificationExecutor<TbSpClause> {

	@Query(value="select * from tb_sp_clause where FIND_IN_SET(?1,planId) and Clause_title = ?2 order by  Serial_Number asc",nativeQuery=true)
	List<TbSpClause> findByPlanIdAndTitle(String planId, String clauseTitle);

	Optional<TbSpClause> findById(Integer id);

	@Query(value="select Clause_title from tb_sp_clause where FIND_IN_SET(?1,planId) order by  Serial_Number asc",nativeQuery=true)
	List<String> findByPlanId(String planId);

	
	@Query(value="select DISTINCT(Clause_title) from tb_sp_clause  order by  Serial_Number asc",nativeQuery=true)
	List<String> findTitleList();

	List<TbSpClause> findByKindCode(String kindCode);

	@Query(value="select * from tb_sp_clause  order by  Serial_Number asc",nativeQuery=true)
	List<TbSpClause> findAllClause();
	
	
	
	

}
