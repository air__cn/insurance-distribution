package cn.com.libertymutual.production.service.api;


import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.PrpdcoderiskKey;

public interface PrpdCodeRiskService {

	/**
	 * 新增特约时保存适用险种
	 * @param record
	 */
	public void insert(PrpdcoderiskKey record);
	
	/**
	 * 通过条件查询适用险种
	 * @param codeType
	 * @param codeCode
	 * @return
	 */
	public List<PrpdcoderiskKey> findCodeRiskByCondition(String codeType, String codeCode);
	
	public void update(PrpdcoderiskKey record);
	
	public void delete(PrpdcoderiskKey record);
	
	public List<PrpdcoderiskKey> findCodeRiskByClause(String codecode);
	
}
