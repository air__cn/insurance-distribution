package cn.com.libertymutual.production.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import cn.com.libertymutual.production.dao.nomorcldatasource.PrpditemMapper;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpditem;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpditemExample;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpditemWithBLOBs;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpditemExample.Criteria;
import cn.com.libertymutual.production.service.api.PrpdItemService;

/** 
 * @author Steven.Li
 * @date 2017年7月28日
 *  
 */
@Service
public class PrpdItemServiceImpl implements PrpdItemService {
	
	@Autowired
	private PrpditemMapper prpditemMapper;
	
	@Override
	public void insert(PrpditemWithBLOBs record) {
		prpditemMapper.insertSelective(record);
	}

	@Override
	public void update(PrpditemWithBLOBs record) {
		PrpditemExample example = new PrpditemExample();
		Criteria criteria = example.createCriteria();
		criteria.andItemcodeEqualTo(record.getItemcode());
		prpditemMapper.updateByExampleSelective(record, example);
	}

	@Override
	public List<PrpditemWithBLOBs> select(Prpditem criteria) {
		PrpditemExample example = new PrpditemExample();
		Criteria where = example.createCriteria();
		if (!StringUtils.isEmpty(criteria.getRiskcode())) {
			where.andRiskcodeEqualTo(criteria.getRiskcode());
		}
		if (!StringUtils.isEmpty(criteria.getRiskversion())) {
			where.andRiskversionEqualTo(criteria.getRiskversion());
		}
		if (!StringUtils.isEmpty(criteria.getItemcode())) {
			where.andItemcodeEqualTo(criteria.getItemcode());
		}
		return prpditemMapper.selectByExampleWithBLOBs(example);
	}
	
}
