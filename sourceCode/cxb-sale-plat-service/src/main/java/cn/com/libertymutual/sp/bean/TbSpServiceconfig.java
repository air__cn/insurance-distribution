package cn.com.libertymutual.sp.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;

@ApiModel
@Entity
@Table(name = "tb_sp_serviceconfig")
public class TbSpServiceconfig implements Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 3208412844274642489L;
	private Integer id;
	private String serviceCode;
	private String serviceCname;
	private String serviceEname;
	private String serviceType;
	private String isFirst;
	private String imgUrl;
	private String serviceUrl;
	private String toType;
	private Integer serialNo;
	private String isValidate;
	private String baseService;
	
	// Constructors

	
	
	/** default constructor */
	public TbSpServiceconfig() {
	}


	/** full constructor */
	public TbSpServiceconfig(String serviceCode, String serviceCname,
			String serviceEname, String serviceType, String isFirst,
			String imgUrl, String serviceUrl, String toType, Integer serialNo,
			String isValidate) {
		this.serviceCode = serviceCode;
		this.serviceCname = serviceCname;
		this.serviceEname = serviceEname;
		this.serviceType = serviceType;
		this.isFirst = isFirst;
		this.imgUrl = imgUrl;
		this.serviceUrl = serviceUrl;
		this.toType = toType;
		this.serialNo = serialNo;
		this.isValidate = isValidate;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "SERVICE_CODE", length = 50)
	public String getServiceCode() {
		return this.serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	@Column(name = "SERVICE_CNAME", length = 100)
	public String getServiceCname() {
		return this.serviceCname;
	}

	public void setServiceCname(String serviceCname) {
		this.serviceCname = serviceCname;
	}

	@Column(name = "SERVICE_ENAME", length = 100)
	public String getServiceEname() {
		return this.serviceEname;
	}

	public void setServiceEname(String serviceEname) {
		this.serviceEname = serviceEname;
	}

	@Column(name = "BASE_SERVICE")
	public String getBaseService() {
		return baseService;
	}

	
	public void setBaseService(String baseService) {
		this.baseService = baseService;
	}
	
	
	@Column(name = "SERVICE_TYPE", length = 10)
	public String getServiceType() {
		return this.serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	@Column(name = "IS_FIRST", length = 1)
	public String getIsFirst() {
		return this.isFirst;
	}

	public void setIsFirst(String isFirst) {
		this.isFirst = isFirst;
	}

	@Column(name = "IMG_URL", length = 100)
	public String getImgUrl() {
		return this.imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	@Column(name = "SERVICE_URL", length = 100)
	public String getServiceUrl() {
		return this.serviceUrl;
	}

	public void setServiceUrl(String serviceUrl) {
		this.serviceUrl = serviceUrl;
	}

	@Column(name = "TO_TYPE", length = 1)
	public String getToType() {
		return this.toType;
	}

	public void setToType(String toType) {
		this.toType = toType;
	}

	@Column(name = "SERIAL_NO")
	public Integer getSerialNo() {
		return this.serialNo;
	}

	public void setSerialNo(Integer serialNo) {
		this.serialNo = serialNo;
	}

	@Column(name = "IS_VALIDATE", length = 1)
	public String getIsValidate() {
		return this.isValidate;
	}

	public void setIsValidate(String isValidate) {
		this.isValidate = isValidate;
	}

}