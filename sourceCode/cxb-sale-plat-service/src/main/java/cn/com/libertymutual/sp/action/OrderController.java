package cn.com.libertymutual.sp.action;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.annotation.SystemValidate;
import cn.com.libertymutual.core.util.Current;
import cn.com.libertymutual.core.util.StringUtil;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpOrder;
import cn.com.libertymutual.sp.biz.IdentifierMarkBiz;
import cn.com.libertymutual.sp.service.api.IOrderService;
import cn.com.libertymutual.sp.webService.einv.EinvoicePrintRequestDto;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/nol/order")
public class OrderController {

	@Autowired
	IOrderService orderService;
	@Resource
	private IdentifierMarkBiz identifierMarkBiz;

	@ApiOperation(value = "电子发票查询", notes = "电子发票查询:ResCode开票状态  0未开，1已开，-1红冲")
	@RequestMapping(value = "/queryEinv", method = RequestMethod.POST)
	public ServiceResult queryEinv(HttpServletRequest request, HttpServletResponse response, String identCode, String policyNo, String identifyNo)
			throws Exception {

		return orderService.queryEinv(policyNo, identifyNo);
	}

	@ApiOperation(value = "开具电子发票", notes = "生成电子发票")
	@RequestMapping(value = "/sendEinvPrintData", method = RequestMethod.POST)
	public ServiceResult sendEinvPrintData(HttpServletRequest request, HttpServletResponse response, String identCode,
			@RequestBody EinvoicePrintRequestDto einvPrintData) throws Exception {

		return orderService.sendEinvPrintData(einvPrintData);
	}

	@ApiOperation(value = "电子保单下载", notes = "电子保单下载")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "policyNo", value = "policyNo", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "proposalDate", value = "proposalDate", required = false, paramType = "query", dataType = "String")

	})
	@RequestMapping(value = "/downPolicy")
	@SystemValidate(validate = false, description = "无需校验接口")
	public ServiceResult downPolicy(String policyNo, String proposalDate, String emailStr, String applicantName, HttpServletResponse response)
			throws Exception {

		ServiceResult rs = new ServiceResult();
		// policyNo="8805081100170000147000";
		// applicantName="wangkf";
		if (StringUtil.isEmpty(policyNo)) {
			rs.setFail();
			rs.setResult("保单号不能为空！");
			return rs;
		}
		if ("05".equals(policyNo.substring(2, 4)) && !"11".equals(policyNo.substring(6, 8))) {// 车险电子保单非北京
			rs.setFail();
			rs.setResult("提示：车险电子保单只支持北京地区");
			return rs;
		}
		if ("05".equals(policyNo.substring(2, 4)) && "11".equals(policyNo.substring(6, 8))) {// 北京车险电子保单
			rs = orderService.downBJPolicy(policyNo, proposalDate, emailStr, applicantName, response);
		} else {
			rs = orderService.downPolicy(policyNo, proposalDate, emailStr, applicantName, response);
		}
		return rs;
	}

	@PostMapping("/getOrderList")
	public ServiceResult orderList(HttpServletRequest request, HttpServletResponse response, String identCode, String sortfield, String sortfieldData,
			String sorttype, String sortData, int pageNumber, int pageSize, String status) {

		return orderService.getOrderList(sortfield, sortfieldData, sorttype, sortData, pageNumber, pageSize, status);
	}

	@ApiOperation(value = "保存订单", notes = "测试")
	@RequestMapping(value = "/saveOrder", method = RequestMethod.POST)
	public ServiceResult saveOrder(@RequestBody TbSpOrder spOrder) throws Exception {
		return orderService.saveOrder(spOrder);
	}

	@ApiOperation(value = "查找订单", notes = "测试")
	@RequestMapping(value = "/findOrder", method = RequestMethod.POST)
	public ServiceResult findOrder(HttpServletRequest request, HttpServletResponse response, String identCode, String valCode, String type,
			String findData, String policyNo) throws Exception {

		return orderService.findOrder(request, valCode, type, findData, policyNo);
	}

	@ApiOperation(value = "业绩统计", notes = "业绩统计")
	@RequestMapping(value = "/performance", method = RequestMethod.POST)
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "userCode", value = "用户ID", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "queryType", value = "查询类型：D:按天查询,M：按月查询，Y：按年查询", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "queryParam", value = "类型值:D:201711,M:2017", required = false, paramType = "query", dataType = "String")

	})
	public ServiceResult performance(String userCode, String queryType, String queryParam, String userCodeBs) throws Exception {

		return orderService.performance(Current.userCode.get(), queryType, queryParam, Current.userCodeBs.get());
	}

	@ApiOperation(value = "业绩统计", notes = "业绩统计")
	@RequestMapping(value = "/performanceMonth", method = RequestMethod.POST)
	public ServiceResult performanceCountMonth(String userCode, String userCodeBs) throws Exception {

		return orderService.performanceCountMonth(Current.userCode.get(), Current.userCodeBs.get());
	}

	@ApiOperation(value = "业绩明细", notes = "业绩明细")
	@RequestMapping(value = "/performanceList", method = RequestMethod.POST)
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "userCode", value = "用户ID", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "riskCode", value = "险种", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "queryParam", value = "类型值:201711", required = true, paramType = "query", dataType = "String")

	})
	public ServiceResult performanceList(String userCode, String riskCode, String queryParam, String userCodeBs) throws Exception {

		return orderService.performanceList(Current.userCode.get(), riskCode, queryParam, Current.userCodeBs.get());
	}

	@RequestMapping(value = "/performanceDetail", method = RequestMethod.POST)

	public ServiceResult performanceDetail(String userCode, String queryType, String queryParam, String userCodeBs) throws Exception {

		return orderService.performanceDetail(Current.userCode.get(), queryType, queryParam, Current.userCodeBs.get());
	}

}
