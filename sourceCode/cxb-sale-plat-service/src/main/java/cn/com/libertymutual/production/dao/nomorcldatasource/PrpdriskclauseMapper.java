package cn.com.libertymutual.production.dao.nomorcldatasource;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdriskclause;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdriskclauseExample;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdriskclauseKey;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdriskclauseWithBLOBs;
@Mapper
public interface PrpdriskclauseMapper {
    int countByExample(PrpdriskclauseExample example);

    int deleteByExample(PrpdriskclauseExample example);

    int deleteByPrimaryKey(PrpdriskclauseKey key);

    int insert(PrpdriskclauseWithBLOBs record);

    int insertSelective(PrpdriskclauseWithBLOBs record);

    List<PrpdriskclauseWithBLOBs> selectByExampleWithBLOBs(PrpdriskclauseExample example);

    List<Prpdriskclause> selectByExample(PrpdriskclauseExample example);

    PrpdriskclauseWithBLOBs selectByPrimaryKey(PrpdriskclauseKey key);

    int updateByExampleSelective(@Param("record") PrpdriskclauseWithBLOBs record, @Param("example") PrpdriskclauseExample example);

    int updateByExampleWithBLOBs(@Param("record") PrpdriskclauseWithBLOBs record, @Param("example") PrpdriskclauseExample example);

    int updateByExample(@Param("record") Prpdriskclause record, @Param("example") PrpdriskclauseExample example);

    int updateByPrimaryKeySelective(PrpdriskclauseWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(PrpdriskclauseWithBLOBs record);

    int updateByPrimaryKey(Prpdriskclause record);
}