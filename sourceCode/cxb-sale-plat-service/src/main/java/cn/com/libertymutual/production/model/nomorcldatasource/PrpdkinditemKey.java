package cn.com.libertymutual.production.model.nomorcldatasource;

public class PrpdkinditemKey {
    private String kindcode;

    private String kindversion;

    private String riskcode;

    private String riskversion;

    private String plancode;

    private String itemcode;

    public String getKindcode() {
        return kindcode;
    }

    public void setKindcode(String kindcode) {
        this.kindcode = kindcode == null ? null : kindcode.trim();
    }

    public String getKindversion() {
        return kindversion;
    }

    public void setKindversion(String kindversion) {
        this.kindversion = kindversion == null ? null : kindversion.trim();
    }

    public String getRiskcode() {
        return riskcode;
    }

    public void setRiskcode(String riskcode) {
        this.riskcode = riskcode == null ? null : riskcode.trim();
    }

    public String getRiskversion() {
        return riskversion;
    }

    public void setRiskversion(String riskversion) {
        this.riskversion = riskversion == null ? null : riskversion.trim();
    }

    public String getPlancode() {
        return plancode;
    }

    public void setPlancode(String plancode) {
        this.plancode = plancode == null ? null : plancode.trim();
    }

    public String getItemcode() {
        return itemcode;
    }

    public void setItemcode(String itemcode) {
        this.itemcode = itemcode == null ? null : itemcode.trim();
    }
}