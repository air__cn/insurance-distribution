package cn.com.libertymutual.sp.dto.wechat;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Entity - 微信用户信息
 * 
 * @author ycye
 *
 */
@Entity
@Table(name = "t_common_user_info")
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "t_sequence")
public class CommonUserInfo  implements Serializable{

	private static final long serialVersionUID = 789524218390135049L;
	private int id; 
	/**
	 * 普通用户微信号
	 */
	private String userWechatId;
	
	/**
	 * 普通用户对应的密文ID
	 */
	private String userOpenId;
	
	/**
	 * 普通用户是否有效
	 */
	private String isValid;

	/**
	 * 备注
	 */
	private String remark;
	
	/**
	 * 用户昵称
	 */
	private String nickname;
	
	/**
	 * 用户性别(1男/2女/0未知)
	 */
	private String sex;
	
	/**
	 * 所在城市
	 */
	private String city;
	
	/**
	 * 所在省份
	 */
	private String province;
	
	/**
	 * 所在国家
	 */
	private String country;
	
	/**
	 * 用户语言(简体中文zh_CN)
	 */
	private String language;
	
	/**
	 * 用户头像URL
	 */
	private String headimgurl;
	
	/**
	 * 关注时间(时间戳)
	 */
	private Date subscribeTime;
	
	/**
	 * 所属分组
	 */
	private UserGroupInfo group = new UserGroupInfo();
	
	 private Date createDate;
	 private Date modifyDate;
	/**
	 * 所属公众号ID
	 */
	private Long publicId;
	
	public String getUserWechatId() {
		return userWechatId;
	}

	public void setUserWechatId(String userWechatId) {
		this.userWechatId = userWechatId;
	}
	@Id
	 @Column(name = "ID")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	@NotEmpty
	public String getUserOpenId() {
		return userOpenId;
	}

	public void setUserOpenId(String userOpenId) {
		this.userOpenId = userOpenId;
	}

	public String getIsValid() {
		return isValid;
	}

	public void setIsValid(String isValid) {
		this.isValid = isValid;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getHeadimgurl() {
		return headimgurl;
	}

	public void setHeadimgurl(String headimgurl) {
		this.headimgurl = headimgurl;
	}

	public Date getSubscribeTime() {
		return subscribeTime;
	}

	public void setSubscribeTime(Date subscribeTime) {
		this.subscribeTime = subscribeTime;
	}
	
	@ManyToOne(targetEntity=UserGroupInfo.class, fetch=FetchType.LAZY)
	@JoinColumn(name="user_group_id", referencedColumnName="id")
	public UserGroupInfo getGroup() {
		return group;
	}

	public void setGroup(UserGroupInfo group) {
		this.group = group;
	}

	public Long getPublicId() {
		return publicId;
	}

	public void setPublicId(Long publicId) {
		this.publicId = publicId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	
	
}

