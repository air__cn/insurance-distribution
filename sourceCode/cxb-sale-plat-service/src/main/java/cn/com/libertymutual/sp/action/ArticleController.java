package cn.com.libertymutual.sp.action;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.util.Current;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.service.api.ArticleService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/nol/article")
public class ArticleController {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private ArticleService articleService;
	/*
	 * 文章查询
	 */

	@ApiOperation(value = "文章查询", notes = "文章查询")
	@PostMapping(value = "/articleList")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "branchCode", value = "机构", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "productName", value = "产品名称", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "name", value = "文章名称", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "type", value = "文章类型", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "pageNumber", value = "页码", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "每页条数", required = true, paramType = "query", dataType = "Long"), })
	public ServiceResult articleList(HttpServletRequest request, String branchCode, String serchKey, String type, Integer pageNumber,
			Integer pageSize, String userCode) {

		ServiceResult sr = new ServiceResult();
		sr = articleService.articleList(branchCode, serchKey, type, pageNumber, pageSize, Current.userCode.get());
		return sr;
	}

	@ApiOperation(value = "文章查询", notes = "文章查询")
	@GetMapping(value = "/oneArticle")
	public ServiceResult oneArticle(@RequestParam("id") Integer id) {
		ServiceResult sr = new ServiceResult();
		sr = articleService.findArticle(id);
		return sr;
	}

	@ApiOperation(value = "查找软文信息", notes = "测试")
	@PostMapping(value = "/findSoftPaper")
	public ServiceResult findSoftPaper(HttpServletRequest request, Integer id) {
		return articleService.findSoftPaper(id);
	}

}
