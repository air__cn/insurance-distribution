package cn.com.libertymutual.sp.dto.callback;

public class CallBack {
	private String topic;
	private String tag;
	private String key;
	private String timeout;
	private String charset;
	private CallBackData body;
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getTimeout() {
		return timeout;
	}
	public void setTimeout(String timeout) {
		this.timeout = timeout;
	}
	public String getCharset() {
		return charset;
	}
	public void setCharset(String charset) {
		this.charset = charset;
	}
	public CallBackData getBody() {
		return body;
	}
	public void setBody(CallBackData body) {
		this.body = body;
	}
	
	
}
