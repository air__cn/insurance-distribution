package cn.com.libertymutual.production.dao.nomorcldatasource;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.com.libertymutual.production.model.nomorcldatasource.Fhrisk;
import cn.com.libertymutual.production.model.nomorcldatasource.FhriskExample;
import cn.com.libertymutual.production.model.nomorcldatasource.FhriskKey;
@Mapper
public interface FhriskMapper {
    int countByExample(FhriskExample example);

    int deleteByExample(FhriskExample example);

    int deleteByPrimaryKey(FhriskKey key);

    int insert(Fhrisk record);

    int insertSelective(Fhrisk record);

    List<Fhrisk> selectByExample(FhriskExample example);

    Fhrisk selectByPrimaryKey(FhriskKey key);

    int updateByExampleSelective(@Param("record") Fhrisk record, @Param("example") FhriskExample example);

    int updateByExample(@Param("record") Fhrisk record, @Param("example") FhriskExample example);

    int updateByPrimaryKeySelective(Fhrisk record);

    int updateByPrimaryKey(Fhrisk record);
}