package cn.com.libertymutual.sp.dto.car.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * 车辆信息表
 * 
 * @author SHAOLIN
 *
 */
// @XmlAccessorType(XmlAccessType.FIELD)
public class TPrptItemCarDto implements Serializable {
	private static final long serialVersionUID = 7362155357166370121L;
	private String CarInsuredRelation;// 车主类型
	private String Whethelicenses;// 是否已上牌照
	private String Whethercar;// 是否外地车
	private String Newoldlogo;// 新旧车标志
	private String ClauseType;// 条款类型
	private String LicenseNo;// 车牌号码
	private String CarKindCode;// 车辆种类
	private String UseNatureCode;// 车辆使用性质
	private String EngineNo;// 发动机号
	// @XmlElement(name="VINNo")
	private String VinNo;// 车架号/VIN码
	private String LicenseKindCode;// 车牌种类
	// private String enrollDate;// 车辆初登年月
	private String BrandName;// 厂牌型号
	private String PurchasePrice;// 新车购置价
	private String TonCount;// 核定吨位
	private String ExhaustScale;// 核定排量
	private String SeatCount;// 核定座位
	private String DangerousCar;// 是否危险品车
	private String FuelType;// 能源种类
	private String carOwner;// 车主名称
	private String agreeDriverFlag;// 是否指定驾驶员
	private String newDeviceFlag;// 是否投保新增设备险
	private String carCheckStatus;// 验车标记
	private String carCheckTime;// 验车时间
	private String secondHandCarFlag;// 是否二手车
	private String secondHandCarPrice;// 二手车价格
	private String isPriceTaxed;// 是否含税(新车购置价)
	// @XmlElement(name="PURCHASEDATE")
	private String PurchaseDate;// 车辆购买日期
	private String SpeciallyModel;// 特异车
	private String industrycode;// 行业车型编码
	private String ActualValue;// 实际价值=新件购置价X(1-折旧率%)

	// @XmlElement(name="cIArmyFlag")
	private String ciArmyFlag;// 是否军车

	private String companyType;// 企业类型
	private String vinNoQueryFlag;// 是否使用VIN码解析车型标志
	// 重复添加了 在车辆扩展信息表里已经有了
	private String chgOwnerFlag;// 过户车辆标志
	private String loanVehicleFlag;// 是否车贷投保多年标志

	private String modelCode;

	private String useMonths; // 使用月份

	private String tonCountFlag;// 吨位标志
	private String exhaustScaleFlag;// 排量标志
	private String seatCountFlag;// 座位标志
	// private String purchasedate; //购买日期
	private String businessType;
	private String loyalty;// 忠诚度 1续保 0新保
	// private String certificateDate;// 开具车辆来历凭证所载日期
	// private String certificateNo;// 车辆来历凭证编号
	// private String certificateType;// 车辆来历凭证种类
	private String labelTypeName;// 厂牌型号别称
	private String registModelCode;// 行驶证车型
	private String runAreaCode;
	private String runMiles;

	private String useYears;// 使用年限
	// private String madeDate;// 出厂日期

	private String vehicleCategoryCode;
	private String vehicleModel;
	private Date enrollDate;
	private Date transferDate;
	private String loanBeneficiary;
	private String poWeight;
	private String countryCode;

	public Date getEnrollDate() {
		return enrollDate;
	}

	public void setEnrollDate(Date enrollDate) {
		this.enrollDate = enrollDate;
	}

	public Date getTransferDate() {
		return transferDate;
	}

	public void setTransferDate(Date transferDate) {
		this.transferDate = transferDate;
	}

	public String getChgOwnerFlag() {
		return chgOwnerFlag;
	}

	public void setChgOwnerFlag(String chgOwnerFlag) {
		this.chgOwnerFlag = chgOwnerFlag;
	}

	public String getVehicleCategoryCode() {
		return vehicleCategoryCode;
	}

	public void setVehicleCategoryCode(String vehicleCategoryCode) {
		this.vehicleCategoryCode = vehicleCategoryCode;
	}

	public String getVehicleModel() {
		return vehicleModel;
	}

	public void setVehicleModel(String vehicleModel) {
		this.vehicleModel = vehicleModel;
	}

	public String getLoanBeneficiary() {
		return loanBeneficiary;
	}

	public void setLoanBeneficiary(String loanBeneficiary) {
		this.loanBeneficiary = loanBeneficiary;
	}

	public String getPoWeight() {
		return poWeight;
	}

	public void setPoWeight(String poWeight) {
		this.poWeight = poWeight;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getUseMonths() {
		return useMonths;
	}

	public String getTonCountFlag() {
		return tonCountFlag;
	}

	public void setTonCountFlag(String tonCountFlag) {
		this.tonCountFlag = tonCountFlag;
	}

	public String getExhaustScaleFlag() {
		return exhaustScaleFlag;
	}

	public void setExhaustScaleFlag(String exhaustScaleFlag) {
		this.exhaustScaleFlag = exhaustScaleFlag;
	}

	public String getSeatCountFlag() {
		return seatCountFlag;
	}

	public void setSeatCountFlag(String seatCountFlag) {
		this.seatCountFlag = seatCountFlag;
	}

	public void setUseMonths(String useMonths) {
		this.useMonths = useMonths;
	}

	/*
	 * public String getPurchasedate() { try { return new
	 * SimpleDateFormat("yyyy-MM-dd").format(new Date(purchasedate)); } catch
	 * (Exception e) { return purchasedate; } }
	 */
	/*
	 * public void setPurchasedate(String purchasedate) { this.purchasedate =
	 * purchasedate; }
	 */
	public String getModelCode() {
		return modelCode;
	}

	public void setModelCode(String modelCode) {
		this.modelCode = modelCode;
	}

	public String getCiArmyFlag() {
		return ciArmyFlag;
	}

	public void setCiArmyFlag(String ciArmyFlag) {
		this.ciArmyFlag = ciArmyFlag;
	}

	public String getCompanyType() {
		return companyType;
	}

	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}

	public String getCarInsuredRelation() {
		return CarInsuredRelation;
	}

	public void setCarInsuredRelation(String carInsuredRelation) {
		CarInsuredRelation = carInsuredRelation;
	}

	public String getWhethelicenses() {
		return Whethelicenses;
	}

	public void setWhethelicenses(String whethelicenses) {
		Whethelicenses = whethelicenses;
	}

	public String getWhethercar() {
		return Whethercar;
	}

	public void setWhethercar(String whethercar) {
		Whethercar = whethercar;
	}

	public String getNewoldlogo() {
		return Newoldlogo;
	}

	public void setNewoldlogo(String newoldlogo) {
		Newoldlogo = newoldlogo;
	}

	public String getClauseType() {
		return ClauseType;
	}

	public void setClauseType(String clauseType) {
		ClauseType = clauseType;
	}

	public String getLicenseNo() {
		return LicenseNo;
	}

	public void setLicenseNo(String licenseNo) {
		LicenseNo = licenseNo;
	}

	public String getCarKindCode() {
		return CarKindCode;
	}

	public void setCarKindCode(String carKindCode) {
		CarKindCode = carKindCode;
	}

	public String getUseNatureCode() {
		return UseNatureCode;
	}

	public void setUseNatureCode(String useNatureCode) {
		UseNatureCode = useNatureCode;
	}

	public String getEngineNo() {
		return EngineNo;
	}

	public void setEngineNo(String engineNo) {
		EngineNo = engineNo;
	}

	public String getVinNo() {
		return VinNo;
	}

	public void setVinNo(String vinNo) {
		VinNo = vinNo;
	}

	public String getPurchaseDate() {
		return PurchaseDate;
	}

	public void setPurchaseDate(String purchaseDate) {
		PurchaseDate = purchaseDate;
	}

	public String getLicenseKindCode() {
		return LicenseKindCode;
	}

	public void setLicenseKindCode(String licenseKindCode) {
		LicenseKindCode = licenseKindCode;
	}

	// @SuppressWarnings("deprecation")
	// public String getEnrollDate() {
	// try {
	// return new SimpleDateFormat("yyyy-MM-dd").format(new Date(enrollDate));
	// } catch (Exception e) {
	// return enrollDate;
	// }
	// }
	//
	// public void setEnrollDate(String enrollDate) {
	// enrollDate = enrollDate;
	// }

	// public String getTransferDate() {
	// try {
	// return new SimpleDateFormat("yyyy-MM-dd").format(new Date(transferDate));
	// } catch (Exception e) {
	// return transferDate;
	// }
	// }
	//
	// public void setTransferDate(String transferDate) {
	// transferDate = transferDate;
	// }

	public String getBrandName() {
		return BrandName;
	}

	public void setBrandName(String brandName) {
		BrandName = brandName;
	}

	public String getPurchasePrice() {
		return PurchasePrice;
	}

	public void setPurchasePrice(String purchasePrice) {
		PurchasePrice = purchasePrice;
	}

	public String getTonCount() {
		return TonCount;
	}

	public void setTonCount(String tonCount) {
		TonCount = tonCount;
	}

	public String getExhaustScale() {
		return ExhaustScale;
	}

	public void setExhaustScale(String exhaustScale) {
		ExhaustScale = exhaustScale;
	}

	public String getSeatCount() {
		return SeatCount;
	}

	public void setSeatCount(String seatCount) {
		SeatCount = seatCount;
	}

	public String getDangerousCar() {
		return DangerousCar;
	}

	public void setDangerousCar(String dangerousCar) {
		DangerousCar = dangerousCar;
	}

	public String getFuelType() {
		return FuelType;
	}

	public void setFuelType(String fuelType) {
		FuelType = fuelType;
	}

	public String getCarOwner() {
		return carOwner;
	}

	public void setCarOwner(String carOwner) {
		this.carOwner = carOwner;
	}

	public String getAgreeDriverFlag() {
		return agreeDriverFlag;
	}

	public void setAgreeDriverFlag(String agreeDriverFlag) {
		this.agreeDriverFlag = agreeDriverFlag;
	}

	public String getNewDeviceFlag() {
		return newDeviceFlag;
	}

	public void setNewDeviceFlag(String newDeviceFlag) {
		this.newDeviceFlag = newDeviceFlag;
	}

	public String getCarCheckStatus() {
		return carCheckStatus;
	}

	public void setCarCheckStatus(String carCheckStatus) {
		this.carCheckStatus = carCheckStatus;
	}

	public String getCarCheckTime() {
		return carCheckTime;
	}

	public void setCarCheckTime(String carCheckTime) {
		this.carCheckTime = carCheckTime;
	}

	public String getSecondHandCarFlag() {
		return secondHandCarFlag;
	}

	public void setSecondHandCarFlag(String secondHandCarFlag) {
		this.secondHandCarFlag = secondHandCarFlag;
	}

	public String getSecondHandCarPrice() {
		return secondHandCarPrice;
	}

	public void setSecondHandCarPrice(String secondHandCarPrice) {
		this.secondHandCarPrice = secondHandCarPrice;
	}

	public String getIsPriceTaxed() {
		return isPriceTaxed;
	}

	public void setIsPriceTaxed(String isPriceTaxed) {
		this.isPriceTaxed = isPriceTaxed;
	}

	public String getSpeciallyModel() {
		return SpeciallyModel;
	}

	public void setSpeciallyModel(String speciallyModel) {
		SpeciallyModel = speciallyModel;
	}

	public String getIndustrycode() {
		return industrycode;
	}

	public void setIndustrycode(String industrycode) {
		this.industrycode = industrycode;
	}

	public String getActualValue() {
		return ActualValue;
	}

	public void setActualValue(String actualValue) {
		ActualValue = actualValue;
	}

	public String getVinNoQueryFlag() {
		return vinNoQueryFlag;
	}

	public void setVinNoQueryFlag(String vinNoQueryFlag) {
		this.vinNoQueryFlag = vinNoQueryFlag;
	}

	public String getBusinessType() {
		return businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	public String getLoyalty() {
		return loyalty;
	}

	public void setLoyalty(String loyalty) {
		this.loyalty = loyalty;
	}

	// public String getCertificateDate() {
	// return certificateDate;
	// }
	//
	// public void setCertificateDate(String certificateDate) {
	// this.certificateDate = certificateDate;
	// }

	// public String getCertificateNo() {
	// return certificateNo;
	// }
	//
	// public void setCertificateNo(String certificateNo) {
	// this.certificateNo = certificateNo;
	// }

	// public String getCertificateType() {
	// return certificateType;
	// }
	//
	// public void setCertificateType(String certificateType) {
	// this.certificateType = certificateType;
	// }

	public String getLabelTypeName() {
		return labelTypeName;
	}

	public void setLabelTypeName(String labelTypeName) {
		this.labelTypeName = labelTypeName;
	}

	public String getRegistModelCode() {
		return registModelCode;
	}

	public void setRegistModelCode(String registModelCode) {
		this.registModelCode = registModelCode;
	}

	public String getRunAreaCode() {
		return runAreaCode;
	}

	public void setRunAreaCode(String runAreaCode) {
		this.runAreaCode = runAreaCode;
	}

	public String getRunMiles() {
		return runMiles;
	}

	public void setRunMiles(String runMiles) {
		this.runMiles = runMiles;
	}

	public String getLoanVehicleFlag() {
		return loanVehicleFlag;
	}

	public void setLoanVehicleFlag(String loanVehicleFlag) {
		this.loanVehicleFlag = loanVehicleFlag;
	}

	public String getUseYears() {
		return useYears;
	}

	public void setUseYears(String useYears) {
		this.useYears = useYears;
	}

	// public String getMadeDate() {
	// return madeDate;
	// }
	//
	// public void setMadeDate(String madeDate) {
	// this.madeDate = madeDate;
	// }

	@Override
	public String toString() {
		return "PrpTitemCarDTO [CarInsuredRelation=" + CarInsuredRelation + ", Whethelicenses=" + Whethelicenses + ", Whethercar=" + Whethercar
				+ ", Newoldlogo=" + Newoldlogo + ", ClauseType=" + ClauseType + ", LicenseNo=" + LicenseNo + ", CarKindCode=" + CarKindCode
				+ ", UseNatureCode=" + UseNatureCode + ", EngineNo=" + EngineNo + ", VinNo=" + VinNo + ", LicenseKindCode=" + LicenseKindCode
				+ ", enrollDate=" + enrollDate + ", BrandName=" + BrandName + ", PurchasePrice=" + PurchasePrice + ", TonCount=" + TonCount
				+ ", ExhaustScale=" + ExhaustScale + ", SeatCount=" + SeatCount + ", DangerousCar=" + DangerousCar + ", FuelType=" + FuelType
				+ ", carOwner=" + carOwner + ", agreeDriverFlag=" + agreeDriverFlag + ", newDeviceFlag=" + newDeviceFlag + ", carCheckStatus="
				+ carCheckStatus + ", carCheckTime=" + carCheckTime + ", secondHandCarFlag=" + secondHandCarFlag + ", secondHandCarPrice="
				+ secondHandCarPrice + ", isPriceTaxed=" + isPriceTaxed + ", PurchaseDate=" + PurchaseDate + ", SpeciallyModel=" + SpeciallyModel
				+ ", industrycode=" + industrycode + ", ActualValue=" + ActualValue + ", ciarmyFlag=" + ciArmyFlag + ", companyType=" + companyType
				+ ",businessType=" + businessType + ",loyalty=" + loyalty + "]";
	}
}