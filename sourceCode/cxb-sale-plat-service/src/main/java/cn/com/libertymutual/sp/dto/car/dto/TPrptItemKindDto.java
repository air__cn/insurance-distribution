package cn.com.libertymutual.sp.dto.car.dto;


import java.io.Serializable;
/**
 * 险别对象
 * @author SHAOLIN
 *
 */
public class TPrptItemKindDto implements Serializable{
	private static final long serialVersionUID = -5147333486483438075L;
	private String ItemKindNo;
	private String KindCodeMain;// 险别名称代码
	private String EFileCodeMain;// 备案号代码
	
	private String ItemCodeMain;// 保险责任代码
	
	private String ModelNameMain;// 职业代码
	
	private String KindNameMain;// 险别名称
	
	private String EFileNameMain;// 备案名称
	
	private String ItemNameMain;// 保险责任
	
	private String ModelMain;// 职业
	
	private String OccupationFlagMain;// 职业类型
	
	private String ModelNoMain;// 职业序号
	
	private String CurrencyMain;// 币别
	
	private String UnitAmountMain;// 每人保额
	
	private String QuantityMain;// 人数
	
	private String AmountMain;// 总保额
	
	private String BenchMarkPremiumMain;// 标准保费
	
	private String DiscountMain;// 折扣率
	
	private String FinalRateMain;// 最终费率
	
	private String kindVersion;// 险别版本
	
	private String planCode;// 计划代码
	
	private String PremiumMain;// 实收保费
	
	private String RATE;// 费率
	
	private String itemNo;// 序号
	
	private String basePremium;// 基础保费
	
	private String CalculateFlag;//是否计算保额标志（Y/N）
	private String modeCode;//包装代码 ，玻璃险 1国产 / 2进口 
	private String modeName;//包装名称
	public String getKindCodeMain() {
		return KindCodeMain;
	}
	public void setKindCodeMain(String kindCodeMain) {
		KindCodeMain = kindCodeMain;
	}
	public String getEFileCodeMain() {
		return EFileCodeMain;
	}
	public void setEFileCodeMain(String eFileCodeMain) {
		EFileCodeMain = eFileCodeMain;
	}
	public String getItemCodeMain() {
		return ItemCodeMain;
	}
	public void setItemCodeMain(String itemCodeMain) {
		ItemCodeMain = itemCodeMain;
	}
	public String getModelNameMain() {
		return ModelNameMain;
	}
	public void setModelNameMain(String modelNameMain) {
		ModelNameMain = modelNameMain;
	}
	public String getKindNameMain() {
		return KindNameMain;
	}
	public void setKindNameMain(String kindNameMain) {
		KindNameMain = kindNameMain;
	}
	public String getEFileNameMain() {
		return EFileNameMain;
	}
	public void setEFileNameMain(String eFileNameMain) {
		EFileNameMain = eFileNameMain;
	}
	public String getItemNameMain() {
		return ItemNameMain;
	}
	public void setItemNameMain(String itemNameMain) {
		ItemNameMain = itemNameMain;
	}
	public String getModelMain() {
		return ModelMain;
	}
	public void setModelMain(String modelMain) {
		ModelMain = modelMain;
	}
	public String getOccupationFlagMain() {
		return OccupationFlagMain;
	}
	public void setOccupationFlagMain(String occupationFlagMain) {
		OccupationFlagMain = occupationFlagMain;
	}
	public String getModelNoMain() {
		return ModelNoMain;
	}
	public void setModelNoMain(String modelNoMain) {
		ModelNoMain = modelNoMain;
	}
	public String getCurrencyMain() {
		return CurrencyMain;
	}
	public void setCurrencyMain(String currencyMain) {
		CurrencyMain = currencyMain;
	}
	public String getUnitAmountMain() {
		return UnitAmountMain;
	}
	public void setUnitAmountMain(String unitAmountMain) {
		UnitAmountMain = unitAmountMain;
	}
	public String getQuantityMain() {
		return QuantityMain;
	}
	public void setQuantityMain(String quantityMain) {
		QuantityMain = quantityMain;
	}
	public String getAmountMain() {
		return AmountMain;
	}
	public void setAmountMain(String amountMain) {
		AmountMain = amountMain;
	}
	public String getBenchMarkPremiumMain() {
		return BenchMarkPremiumMain;
	}
	public void setBenchMarkPremiumMain(String benchMarkPremiumMain) {
		BenchMarkPremiumMain = benchMarkPremiumMain;
	}
	public String getDiscountMain() {
		return DiscountMain;
	}
	public void setDiscountMain(String discountMain) {
		DiscountMain = discountMain;
	}
	public String getFinalRateMain() {
		return FinalRateMain;
	}
	public void setFinalRateMain(String finalRateMain) {
		FinalRateMain = finalRateMain;
	}
	public String getKindVersion() {
		return kindVersion;
	}
	public void setKindVersion(String kindVersion) {
		this.kindVersion = kindVersion;
	}
	public String getPlanCode() {
		return planCode;
	}
	public void setPlanCode(String planCode) {
		this.planCode = planCode;
	}
	public String getPremiumMain() {
		return PremiumMain;
	}
	public void setPremiumMain(String premiumMain) {
		PremiumMain = premiumMain;
	}
	public String getRATE() {
		return RATE;
	}
	public void setRATE(String rATE) {
		RATE = rATE;
	}
	public String getItemNo() {
		return itemNo;
	}
	public void setItemNo(String itemNo) {
		this.itemNo = itemNo;
	}
	public String getBasePremium() {
		return basePremium;
	}
	public void setBasePremium(String basePremium) {
		this.basePremium = basePremium;
	}
	public String getCalculateFlag() {
		return CalculateFlag;
	}
	public void setCalculateFlag(String calculateFlag) {
		CalculateFlag = calculateFlag;
	}
	public String getItemKindNo() {
		return ItemKindNo;
	}
	public void setItemKindNo(String itemKindNo) {
		ItemKindNo = itemKindNo;
	}
	public String getModeCode() {
		return modeCode;
	}
	public void setModeCode(String modeCode) {
		this.modeCode = modeCode;
	}
	public String getModeName() {
		return modeName;
	}
	public void setModeName(String modeName) {
		this.modeName = modeName;
	}
	@Override
	public String toString() {
		return "PrptItemkindDTO [KindCodeMain=" + KindCodeMain + ", EFileCodeMain=" + EFileCodeMain + ", ItemCodeMain="
				+ ItemCodeMain + ", ModelNameMain=" + ModelNameMain + ", KindNameMain=" + KindNameMain
				+ ", EFileNameMain=" + EFileNameMain + ", ItemNameMain=" + ItemNameMain + ", ModelMain=" + ModelMain
				+ ", OccupationFlagMain=" + OccupationFlagMain + ", ModelNoMain=" + ModelNoMain + ", CurrencyMain="
				+ CurrencyMain + ", UnitAmountMain=" + UnitAmountMain + ", QuantityMain=" + QuantityMain
				+ ", AmountMain=" + AmountMain + ", BenchMarkPremiumMain=" + BenchMarkPremiumMain + ", DiscountMain="
				+ DiscountMain + ", FinalRateMain=" + FinalRateMain + ", kindVersion=" + kindVersion + ", planCode="
				+ planCode + ", PremiumMain=" + PremiumMain + ", RATE=" + RATE + ", itemNo=" + itemNo + ", basePremium="
				+ basePremium + ", CalculateFlag=" + CalculateFlag + "]";
	}
	
}