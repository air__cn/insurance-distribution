package cn.com.libertymutual.sp.service.api;

import java.util.List;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbWebRequestLog;

public interface HomePageService {

	ServiceResult salesCount(String startDate, String endDate);

	ServiceResult visitsCount(String type);

	void task();
	
	List<TbWebRequestLog> visitTask(String startDate, String endDate,String type);
}
