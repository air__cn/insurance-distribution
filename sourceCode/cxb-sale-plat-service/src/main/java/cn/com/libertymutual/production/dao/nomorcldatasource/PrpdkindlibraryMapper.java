package cn.com.libertymutual.production.dao.nomorcldatasource;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdkindlibrary;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdkindlibraryExample;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdkindlibraryKey;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdkindlibraryWithBLOBs;
@Mapper
public interface PrpdkindlibraryMapper {
    int countByExample(PrpdkindlibraryExample example);

    int deleteByExample(PrpdkindlibraryExample example);

    int deleteByPrimaryKey(PrpdkindlibraryKey key);

    int insert(PrpdkindlibraryWithBLOBs record);

    int insertSelective(PrpdkindlibraryWithBLOBs record);

    List<PrpdkindlibraryWithBLOBs> selectByExampleWithBLOBs(PrpdkindlibraryExample example);

    List<PrpdkindlibraryWithBLOBs> selectByExample(PrpdkindlibraryExample example);

    PrpdkindlibraryWithBLOBs selectByPrimaryKey(PrpdkindlibraryKey key);

    int updateByExampleSelective(@Param("record") PrpdkindlibraryWithBLOBs record, @Param("example") PrpdkindlibraryExample example);

    int updateByExampleWithBLOBs(@Param("record") PrpdkindlibraryWithBLOBs record, @Param("example") PrpdkindlibraryExample example);

    int updateByExample(@Param("record") Prpdkindlibrary record, @Param("example") PrpdkindlibraryExample example);

    int updateByPrimaryKeySelective(PrpdkindlibraryWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(PrpdkindlibraryWithBLOBs record);

    int updateByPrimaryKey(Prpdkindlibrary record);
    
    /**
     * 查询险种可关联的条款，排除已关联的条款
     * @param ownerriskcode
     * @param kindcode
     * @param kindcname
     * @param riskcode
     * @param riskversion
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Select({"<script>",
	    	"SELECT *",
		   	 "FROM prpdkindlibrary a ",
		   	 "where a.validstatus = '1' ",
		   	 "and (sysdate() between a.startdate and a.enddate) ",
		   	 "<when test='kindcode!=null'>",
			 		"and a.kindcode like '%'||#{kindcode}||'%' ",
			 "</when>",
			 "<when test='kindcname!=null'>",
			 		"and a.kindcname like '%'||#{kindcname}||'%' ",
			 "</when>",
		   	 "<when test='ownerriskcode != null'>",
		   			"and a.ownerriskcode = #{ownerriskcode} ",
		   	 "</when>",
		   	 "and not exists ",
		   	 "( SELECT 1 FROM prpdkind b ",
		   	 "where a.kindcode = b.kindcode ",
		   	 "and b.riskcode = #{riskcode} ",
		   	 "and b.riskversion = #{riskversion} ",
		   	 "and b.validstatus = '1') ",
    "</script>"})
	List<Prpdkindlibrary> selectByOwnerRisk(@Param("ownerriskcode") String ownerriskcode,
											@Param("kindcode") String kindcode,
											@Param("kindcname") String kindcname,
											@Param("riskcode") String riskcode,
											@Param("riskversion") String riskversion,
											@Param("pageNum") int pageNum,
											@Param("pageSize") int pageSize);
}