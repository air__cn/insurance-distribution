package cn.com.libertymutual.production.dao.nomorcldatasource;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.com.libertymutual.production.model.nomorcldatasource.Fhexitemkind;
import cn.com.libertymutual.production.model.nomorcldatasource.FhexitemkindExample;
import cn.com.libertymutual.production.model.nomorcldatasource.FhexitemkindKey;
@Mapper
public interface FhexitemkindMapper {
    int countByExample(FhexitemkindExample example);

    int deleteByExample(FhexitemkindExample example);

    int deleteByPrimaryKey(FhexitemkindKey key);

    int insert(Fhexitemkind record);

    int insertSelective(Fhexitemkind record);

    List<Fhexitemkind> selectByExampleWithBLOBs(FhexitemkindExample example);

    List<Fhexitemkind> selectByExample(FhexitemkindExample example);

    Fhexitemkind selectByPrimaryKey(FhexitemkindKey key);

    int updateByExampleSelective(@Param("record") Fhexitemkind record, @Param("example") FhexitemkindExample example);

    int updateByExampleWithBLOBs(@Param("record") Fhexitemkind record, @Param("example") FhexitemkindExample example);

    int updateByExample(@Param("record") Fhexitemkind record, @Param("example") FhexitemkindExample example);

    int updateByPrimaryKeySelective(Fhexitemkind record);

    int updateByPrimaryKeyWithBLOBs(Fhexitemkind record);

    int updateByPrimaryKey(Fhexitemkind record);
}