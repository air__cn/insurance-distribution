package cn.com.libertymutual.production.model.nomorcldatasource;

public class Fhrisk extends FhriskKey {
    private String flag;

    private String inrateflag;

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag == null ? null : flag.trim();
    }

    public String getInrateflag() {
        return inrateflag;
    }

    public void setInrateflag(String inrateflag) {
        this.inrateflag = inrateflag == null ? null : inrateflag.trim();
    }
}