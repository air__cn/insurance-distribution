package cn.com.libertymutual.production.dao.nomorcldatasource;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.com.libertymutual.production.model.nomorcldatasource.PrpdcoderiskExample;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdcoderiskKey;
@Mapper
public interface PrpdcoderiskMapper {
    int countByExample(PrpdcoderiskExample example);

    int deleteByExample(PrpdcoderiskExample example);

    int deleteByPrimaryKey(PrpdcoderiskKey key);

    int insert(PrpdcoderiskKey record);

    int insertSelective(PrpdcoderiskKey record);

    List<PrpdcoderiskKey> selectByExample(PrpdcoderiskExample example);

    int updateByExampleSelective(@Param("record") PrpdcoderiskKey record, @Param("example") PrpdcoderiskExample example);

    int updateByExample(@Param("record") PrpdcoderiskKey record, @Param("example") PrpdcoderiskExample example);
}