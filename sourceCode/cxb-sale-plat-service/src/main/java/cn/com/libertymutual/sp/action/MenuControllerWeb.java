package cn.com.libertymutual.sp.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.service.api.MenuService;
import cn.com.libertymutual.sp.service.api.RoleService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/admin/menu")
public class MenuControllerWeb {
	private Logger log = LoggerFactory.getLogger(getClass());
	@Autowired
	private RoleService roleService;
	
	
	@Autowired
	private MenuService menuService;
	
	@ApiOperation(value = "菜单列表", notes = "菜单列表")
	@PostMapping("/allMenuList")	
    public ServiceResult menuList(){
		ServiceResult sr = new ServiceResult();
		sr = roleService.menuList();
		return sr;		
	}
	
	
	/*
	 * 添加菜单
	 */
	@ApiOperation(value = "添加菜单", notes = "添加菜单")
	@PostMapping(value = "/addMenu")
	@ApiImplicitParams(value = {
            @ApiImplicitParam(name = "menuId", value = "菜单ID", required = true, paramType = "query", dataType = "Long"),
            @ApiImplicitParam(name = "type", value = "添加的类型", required = true, paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "menuname", value = "中文名字", required = true, paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "resName", value = "按钮唯一标识", required = false, paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "menunameen", value = "英文名字", required = true, paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "menuurl", value = "菜单地址", required = true, paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "icon", value = "图标地址", required = true, paramType = "query", dataType = "String"),
    })
	public ServiceResult addMenu(Integer menuId,String type,String menuname,String resName,String menunameen,String menuurl,String icon){
		ServiceResult sr = new ServiceResult();
		
		sr = menuService.addMenu(menuId,type,menuname,resName,menunameen,menuurl,icon);
		return sr;
		
	}
	
	
	
	/*
	 * 删除菜单
	 */
	@ApiOperation(value = "删除菜单", notes = "删除菜单")
	@PostMapping(value = "/removeMenu")
	@ApiImplicitParams(value = {
            @ApiImplicitParam(name = "menuId", value = "菜单ID", required = true, paramType = "query", dataType = "Long"),
            @ApiImplicitParam(name = "type", value = "添加的类型", required = true, paramType = "query", dataType = "String"),
    })
	public ServiceResult removeMenu(Integer menuId,String type){
		ServiceResult sr = new ServiceResult();
		
		sr = menuService.removeMenu(menuId,type);
		return sr;
		
	}
	
	
	
	/*
	 * 修改菜单名字
	 */
	@ApiOperation(value = "修改菜单名字", notes = "修改菜单名字")
	@PostMapping(value = "/updateMenuName")
	@ApiImplicitParams(value = {
            @ApiImplicitParam(name = "menuId", value = "菜单ID", required = true, paramType = "query", dataType = "Long"),
            @ApiImplicitParam(name = "menuname", value = "中文名字", required = true, paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "menunameen", value = "英文名字", required = true, paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "menuurl", value = "菜单地址", required = true, paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "icon", value = "图标地址", required = true, paramType = "query", dataType = "String"),
    })
	public ServiceResult updateMenuName(Integer menuId,String menuname,String menunameen,String menuurl,String icon){
		ServiceResult sr = new ServiceResult();
		
		sr = menuService.updateMenuName(menuId,menuname,menunameen,menuurl,icon);
		return sr;
		
	}
}
