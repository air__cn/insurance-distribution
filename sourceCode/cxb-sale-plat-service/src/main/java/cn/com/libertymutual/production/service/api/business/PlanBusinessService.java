package cn.com.libertymutual.production.service.api.business;

import cn.com.libertymutual.production.pojo.request.PlanAddRequest2;
import cn.com.libertymutual.production.service.api.*;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import cn.com.libertymutual.production.pojo.request.PlanAddRequest;
import cn.com.libertymutual.production.pojo.request.PrpdRiskPlanRequest;
import cn.com.libertymutual.production.pojo.request.RiskKindRequest;
import cn.com.libertymutual.production.pojo.response.Response;
import cn.com.libertymutual.production.service.impl.SystemLog;
import cn.com.libertymutual.production.utils.Constant;

/** 
 * @Description: 后台业务逻辑入口
 * @author Steven.Li
 * @date 2017年7月28日
 *  
 */
public abstract class PlanBusinessService {

	protected static final Logger log = Constant.log;
	
	@Autowired
	protected SystemLog systemLog;
	@Autowired
	protected PrpdKindService prpdKindService;
	@Autowired
	protected PrpdRiskPlanService prpdRiskPlanService;
	@Autowired
	protected PrpdItemLibraryService prpdItemLibraryService;
	@Autowired
	protected PrpdSerialNoService prpdSerialNoService;
	@Autowired
	protected PrpdrationService prpdrationService;
	@Autowired
	protected PrpdPlanPropsService prpdPlanPropsService;
	@Autowired
	protected PrpdPlanSubService prpdPlanSubService;
	@Autowired
	protected Environment env;
	
	/**
	 * 获取方案基本信息
	 * @param prpdRiskPlanRequest
	 * @return
	 */
	public abstract Response findPrpdRiskPlan(PrpdRiskPlanRequest prpdRiskPlanRequest);
	
	/**
	 * 方案管理-获取条款关联的责任
	 * @param prpdKindItemRequest
	 * @return
	 */
	public abstract Response findPrpdItemLibrary(RiskKindRequest request);
	
	/**
	 * 生成方案代码
	 * @param riskCode
	 * @return
	 * @throws Exception
	 */
	public abstract Response getNewPlanSerialno(String riskCode) throws Exception;

	/**
	 * 方案管理-获取当前险种下所有条款
	 * @param riskCode
	 * @return
	 */
	public abstract Response findKindsByRisk(String riskCode, String riskVersion);

	/**
	 * 方案管理-新增方案
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public abstract Response insertPlanInfo(PlanAddRequest request) throws Exception;

	/**
	 * 方案管理-新增方案（费率因子）
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public abstract Response insertPlanInfo2(PlanAddRequest2 request) throws Exception;

	/**
	 * 方案管理-根据方案代码获取方案的条款及标的责任
	 * @param plancode
	 * @return
	 */
	public abstract Response findKindItems(String plancode);

	/**
	 * 方案管理-校验方案是否配置费率因子
	 * @param plancode
	 * @return
	 */
	public abstract Response queryPlansub(String plancode);

	/**
	 * 方案管理-根据方案代码获取方案的条款及标的责任(费率因子)
	 * @param plancode
	 * @return
	 */
	public abstract Response findKindItems2(String plancode);

	/**
	 * 方案管理-修改方案
	 * @param request
	 * @return
	 */
	public abstract Response updatePlanInfo(PlanAddRequest request) throws Exception;

	/**
	 * 方案管理-修改方案(费率因子)
	 * @param request
	 * @return
	 */
	public abstract Response updatePlanInfo2(PlanAddRequest2 request) throws Exception;

}