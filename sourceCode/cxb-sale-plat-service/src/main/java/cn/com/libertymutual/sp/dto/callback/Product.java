package cn.com.libertymutual.sp.dto.callback;

import java.util.Date;

public class Product {
	private String productName; //产品名称
	private String productCode; //产品编码
	private String productAmount; //产品金额
	private String planCode; //险种编码
	private Date beginDate; //保险起期
	private String riskCode; //险别代码
	private Date endDate; //保险止期
	public String getProductName() {
		return productName;
	}
	
	
	public String getRiskCode() {
		return riskCode;
	}


	public void setRiskCode(String riskCode) {
		this.riskCode = riskCode;
	}


	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductAmount() {
		return productAmount;
	}
	public void setProductAmount(String productAmount) {
		this.productAmount = productAmount;
	}
	public String getPlanCode() {
		return planCode;
	}
	public void setPlanCode(String planCode) {
		this.planCode = planCode;
	}
	public Date getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	@Override
	public String toString() {
		return "Product [productName=" + productName + ", productCode=" + productCode + ", productAmount="
				+ productAmount + ", planCode=" + planCode + ", beginDate=" + beginDate + ", endDate=" + endDate + "]";
	}
	
}
