package cn.com.libertymutual.production.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import cn.com.libertymutual.production.dao.nomorcldatasource.PrpdclassMapper;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdclass;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdclassExample;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdclassExample.Criteria;
import cn.com.libertymutual.production.pojo.request.PrpdClassRequest;
import cn.com.libertymutual.production.service.api.PrpdClassService;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@Service
public class PrpdClassServiceImpl implements PrpdClassService {
	
	@Autowired
	private PrpdclassMapper prpdclassMapper;

	@Override
	public List<Prpdclass> findAllPrpdClass() {
		PrpdclassExample prpdclassExample = new PrpdclassExample();
		Criteria criteria = prpdclassExample.createCriteria();
		criteria.andValidstatusEqualTo("1");
		prpdclassExample.setOrderByClause("classcode");
		return prpdclassMapper.selectByExample(prpdclassExample);
	}

	@Override
	public List<Prpdclass> findClassNotComposite() {
		PrpdclassExample prpdclassExample = new PrpdclassExample();
		Criteria criteria = prpdclassExample.createCriteria();
		criteria.andValidstatusEqualTo("1");
		criteria.andCompositeflagNotEqualTo("1");
		
		//////////////////////////////////////////////////////////////////
		//66险类特殊处理，新建条款时排除该险类
		criteria.andClasscodeNotEqualTo("66");
		//////////////////////////////////////////////////////////////////
		
		prpdclassExample.setOrderByClause("classcode");
		return prpdclassMapper.selectByExample(prpdclassExample);
	}

	@Override
	public List<Prpdclass> findClassIsComposite() {
		PrpdclassExample prpdclassExample = new PrpdclassExample();
		Criteria criteria = prpdclassExample.createCriteria();
		criteria.andValidstatusEqualTo("1");
		criteria.andCompositeflagNotEqualTo("0");
		return prpdclassMapper.selectByExample(prpdclassExample);
	}

	@Override
	public PageInfo<Prpdclass> findByPage(PrpdClassRequest request) {
		PrpdclassExample example = new PrpdclassExample();
		Criteria criteria = example.createCriteria();
		String classCode = request.getClasscode();
		String className = request.getClassname();
		String compositeFlag = request.getCompositeflag();
		String validStatus = request.getValidstatus();
		String orderBy = request.getOrderBy();
		String order = request.getOrder();
		example.setOrderByClause("classcode");
		if(!StringUtils.isEmpty(classCode)) {
			criteria.andClasscodeLike("%" + classCode + "%");
		}
		if(!StringUtils.isEmpty(className)) {
			criteria.andClassnameLike("%" + className + "%");
		}
		if(!StringUtils.isEmpty(compositeFlag)) {
			criteria.andCompositeflagEqualTo(compositeFlag);
		}
		if(!StringUtils.isEmpty(validStatus)) {
			criteria.andValidstatusEqualTo(validStatus);
		}
		if (!StringUtils.isEmpty(orderBy) && !"undefined".equals(orderBy)) {
			example.setOrderByClause(orderBy + " " + order);
		}
		PageHelper.startPage(request.getCurrentPage(), request.getPageSize());
		PageInfo<Prpdclass> pageInfo = new PageInfo<>(prpdclassMapper.selectByExample(example));
		return pageInfo;
	}

	@Override
	public boolean checkClassCodeUsed(String classcode) {
		boolean isUsed = false;
		if(!StringUtils.isEmpty(classcode)) {
			PrpdclassExample example = new PrpdclassExample();
			Criteria criteria = example.createCriteria();
			criteria.andClasscodeEqualTo(classcode);
			List<Prpdclass> list = prpdclassMapper.selectByExample(example);
			if(list.size() > 0) {
				isUsed = true;
			}
		}
		return isUsed;
	}

	@Override
	public void insert(Prpdclass record) {
		prpdclassMapper.insertSelective(record);
	}

	@Override
	public void update(Prpdclass record) {
		PrpdclassExample example = new PrpdclassExample();
		Criteria criteria = example.createCriteria();
		criteria.andClasscodeEqualTo(record.getClasscode());
		prpdclassMapper.updateByExampleSelective(record, example);
	}
}
