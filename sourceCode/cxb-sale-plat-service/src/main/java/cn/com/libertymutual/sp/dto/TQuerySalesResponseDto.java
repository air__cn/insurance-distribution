package cn.com.libertymutual.sp.dto;


import java.io.Serializable;
import java.util.List;

import cn.com.libertymutual.core.base.dto.ResponseBaseDto;


public class TQuerySalesResponseDto extends ResponseBaseDto implements

Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7569392257181243237L;
	
	private  List< PrpSalesPerson > sealesPersos;

	public List< PrpSalesPerson > getSealesPersos() {
		return sealesPersos;
	}

	public void setSealesPersos(List< PrpSalesPerson > sealesPersos) {
		this.sealesPersos = sealesPersos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((sealesPersos == null) ? 0 : sealesPersos.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TQuerySalesResponseDto other = (TQuerySalesResponseDto) obj;
		if (sealesPersos == null) {
			if (other.sealesPersos != null)
				return false;
		} else if (!sealesPersos.equals(other.sealesPersos))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TQuerySalesResponseDto [sealesPersos=" + sealesPersos + "]";
	}

}

