package cn.com.libertymutual.sp.action;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.biz.IdentifierMarkBiz;
import cn.com.libertymutual.sp.service.api.CodeRelateService;
import cn.com.libertymutual.sys.bean.SysServiceInfo;

@RestController
@RequestMapping(value = "/nol/addressStroe")
public class addressStroeController {
	private Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private CodeRelateService codeRelateService;
	@Resource
	private IdentifierMarkBiz identifierMarkBiz;

	/** Remarks : test <br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年9月25日上午11:26:55<br>
	 * Project：liberty_sale_plat<br>
	 * 
	 * @param params
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/all")
	public ServiceResult addressList(HttpServletRequest request, HttpServletResponse response, String identCode, String url, String charset)
			throws Exception {
		ServiceResult sr = new ServiceResult();
		try {
			List<SysServiceInfo> sysList = (List<SysServiceInfo>) codeRelateService.findSaleInitUrlData("IP_REGION").getResult();
			String address = sendPost(sysList.get(0).getUrl(), null, sysList.get(0).getPassword());
			String[] strArr = address.split("	");
			sr.setResult(sendPost(sysList.get(0).getUrl(), null, sysList.get(0).getPassword()));
		} catch (Exception e) {
			log.info(e.toString());
		}
		return sr;

	}

	/** 向指定 URL 发送POST方法的请求
	 * 
	 * @param url
	 *            发送请求的 URL
	 * @param param
	 *            请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
	 * @return 所代表远程资源的响应结果
	 * @throws UnsupportedEncodingException
	 */
	public static String sendPost(String url, String param, String charset) throws UnsupportedEncodingException {
		PrintWriter out = null;
		BufferedReader in = null;
		String result = "";
		try {
			URL realUrl = new URL(url);
			// TODO 打开和URL之间的连接
			URLConnection conn = realUrl.openConnection();
			// TODO 设置通用的请求属性
			// TODO 设置接受的文件类型，*表示一切可以接受的
			conn.setRequestProperty("accept", "*/*");
			conn.setRequestProperty("connection", "Keep-Alive");
			// TODO 新添加的请求头编码
			conn.setRequestProperty("Accept-Charset", charset);
			// TODO 设置通用的请求属性
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			conn.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
			// TODO 发送POST请求必须设置如下两行
			conn.setDoOutput(true);
			conn.setDoInput(true);
			// TODO 获取URLConnection对象对应的输出流
			out = new PrintWriter(conn.getOutputStream());
			// TODO 发送请求参数
			out.print(param);
			// TODO flush输出流的缓冲
			out.flush();
			// TODO 定义BufferedReader输入流来读取URL的响应
			in = new BufferedReader(new InputStreamReader(conn.getInputStream(), charset));
			String line;
			while ((line = in.readLine()) != null) {
				result += line;
			}
		} catch (Exception e) {
			System.out.println("发送 POST 请求出现异常！" + e);
			e.printStackTrace();
		}
		// TODO 使用finally块来关闭输出流、输入流
		finally {
			try {
				if (out != null) {
					out.close();
				}
				if (in != null) {
					in.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}
}
