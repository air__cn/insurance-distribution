package cn.com.libertymutual.production.dao.nomorcldatasource;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdefile;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdefileExample;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdefileKey;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdefileWithBLOBs;
@Mapper
public interface PrpdefileMapper {
    int countByExample(PrpdefileExample example);

    int deleteByExample(PrpdefileExample example);

    int deleteByPrimaryKey(PrpdefileKey key);

    int insert(PrpdefileWithBLOBs record);

    int insertSelective(PrpdefileWithBLOBs record);

    List<PrpdefileWithBLOBs> selectByExampleWithBLOBs(PrpdefileExample example);

    List<Prpdefile> selectByExample(PrpdefileExample example);

    PrpdefileWithBLOBs selectByPrimaryKey(PrpdefileKey key);

    int updateByExampleSelective(@Param("record") PrpdefileWithBLOBs record, @Param("example") PrpdefileExample example);

    int updateByExampleWithBLOBs(@Param("record") PrpdefileWithBLOBs record, @Param("example") PrpdefileExample example);

    int updateByExample(@Param("record") Prpdefile record, @Param("example") PrpdefileExample example);

    int updateByPrimaryKeySelective(PrpdefileWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(PrpdefileWithBLOBs record);

    int updateByPrimaryKey(Prpdefile record);
    
    @Select("select case when lpad(max(substr(efilecode, length(efilecode) - 3) + 1),4,'0')= '' then '0001' else lpad(max(substr(efilecode, length(efilecode) - 3) + 1), 4, '0') end from prpDefile where efilecode like #{code} || '%'")
    public String selectEFileCode(String code);
}