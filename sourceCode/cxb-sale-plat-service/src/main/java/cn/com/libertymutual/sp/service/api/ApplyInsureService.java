package cn.com.libertymutual.sp.service.api;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.com.libertymutual.core.web.ServiceResult;

public interface ApplyInsureService {
	/**Remarks: 智通快速投保申请合作<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年12月29日下午4:56:30<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param response
	 * @param sr
	 * @param imgCode
	 * @param mobileCode
	 * @param nickName
	 * @param email
	 * @param mobile
	 * @return
	 */
	public ServiceResult platformInsure(HttpServletRequest request, HttpServletResponse response, ServiceResult sr, String imgCode, String mobileCode,
			String nickName, String email, String mobile);

}
