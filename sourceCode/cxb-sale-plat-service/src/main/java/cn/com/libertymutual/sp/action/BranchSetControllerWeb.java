package cn.com.libertymutual.sp.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.service.api.BranchSetService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/admin/branchSet")
public class BranchSetControllerWeb {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private BranchSetService branchSetService;

	@ApiOperation(value = "机构下面的地区", notes = "机构下面的地区")
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "branchCode", value = "branchCode", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "pageNumber", value = "pageNumber", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "pageSize", required = true, paramType = "query", dataType = "Long") })
	@PostMapping("/areaList")
	public ServiceResult areaList(String branchCode, int pageNumber, int pageSize) {

		ServiceResult sr = new ServiceResult();
		sr = branchSetService.areaList(branchCode, pageNumber, pageSize);
		return sr;
	}

	@ApiOperation(value = "未被添加的地区", notes = "未被添加的地区")
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "branchCode", value = "branchCode", required = true, paramType = "query", dataType = "String"), })
	@PostMapping("/notConfiguredArea")
	public ServiceResult notConfiguredArea(String branchCode) {

		ServiceResult sr = new ServiceResult();
		sr = branchSetService.notConfiguredArea(branchCode);
		return sr;
	}

	@ApiOperation(value = "添加地区", notes = "添加机构下面的地区")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "agreementNo", value = "业务关系代码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "saleCode", value = "销售人员代码 （ 广东 、四川机构特有）", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "saleName", value = "销售人员名称（ 广东 、四川机构特有）", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "branchCode", value = "branchCode", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "branchName", value = "branchName", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "areaCodes", value = "地区编码集合", required = true, paramType = "query", dataType = "String") })
	@PostMapping("/addArea")
	public ServiceResult addArea(String agreementNo, String saleCode, String saleName, String branchCode, String branchName, String areaCodes,
			String invoiceStatus, String insurancePolicyStatus, String contactMobile, String contactEmail) {
		ServiceResult sr = new ServiceResult();
		String[] list = areaCodes.split(",");
		sr = branchSetService.addArea(agreementNo, saleCode, saleName, branchCode, branchName, list, invoiceStatus, insurancePolicyStatus,
				contactMobile, contactEmail);
		return sr;
	}

	@ApiOperation(value = "删除地区", notes = "删除机构下面的地区")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "agreementNo", value = "业务关系代码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "saleCode", value = "销售人员代码 （ 广东 、四川机构特有）", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "saleName", value = "销售人员名称（ 广东 、四川机构特有）", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "branchCode", value = "branchCode", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "areaCodes", value = "地区编码集合", required = true, paramType = "query", dataType = "String") })
	@PostMapping("/removeArea")
	public ServiceResult removeArea(String agreementNo, String saleCode, String saleName, String branchCode, String areaCodes, String invoiceStatus,
			String insurancePolicyStatus, String contactMobile, String contactEmail) {

		ServiceResult sr = new ServiceResult();
		String[] list = areaCodes.split(",");
		sr = branchSetService.removeArea(agreementNo, saleCode, saleName, branchCode, list, invoiceStatus, insurancePolicyStatus, contactMobile,
				contactEmail);
		return sr;
	}

	@ApiOperation(value = "获取业务关系代码", notes = "获取业务关系代码")
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "branchCode", value = "branchCode", required = true, paramType = "query", dataType = "String") })
	@PostMapping("/getAgreementNo")
	public ServiceResult getAgreementNo(String branchCode) {

		ServiceResult sr = new ServiceResult();
		sr = branchSetService.getAgreementNo(branchCode);
		return sr;
	}

	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "agreementCode", value = "agreementCode", required = true, paramType = "query", dataType = "String") })
	@PostMapping("/getSaleName")
	public ServiceResult getSaleName(String agreementCode) {

		ServiceResult sr = new ServiceResult();
		sr = branchSetService.getSaleName(agreementCode);
		return sr;
	}

	// 新增地区
	@ApiOperation(value = "新增地区", notes = "新增地区")
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "branchCode", value = "branchCode", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "branchName", value = "branchName", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "areaCode", value = "areaCode", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "areaName", value = "areaName", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "areaEname", value = "areaEname", required = true, paramType = "query", dataType = "String") })
	@PostMapping("/addNewArea")
	public ServiceResult addNewArea(String branchCode, String branchName, String areaCode, String areaName, String areaEname) {

		ServiceResult sr = new ServiceResult();
		sr = branchSetService.addNewArea(branchCode, branchName, areaCode, areaName, areaEname);
		return sr;
	}
}
