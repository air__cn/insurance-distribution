package cn.com.libertymutual.production.model.nomorcldatasource;

public class PrpsagentWithBLOBs extends Prpsagent {
    private String addressname;

    private String roadname;

    private String carbrand;

    private String factory;

    private String carseriesname;

    public String getAddressname() {
        return addressname;
    }

    public void setAddressname(String addressname) {
        this.addressname = addressname == null ? null : addressname.trim();
    }

    public String getRoadname() {
        return roadname;
    }

    public void setRoadname(String roadname) {
        this.roadname = roadname == null ? null : roadname.trim();
    }

    public String getCarbrand() {
        return carbrand;
    }

    public void setCarbrand(String carbrand) {
        this.carbrand = carbrand == null ? null : carbrand.trim();
    }

    public String getFactory() {
        return factory;
    }

    public void setFactory(String factory) {
        this.factory = factory == null ? null : factory.trim();
    }

    public String getCarseriesname() {
        return carseriesname;
    }

    public void setCarseriesname(String carseriesname) {
        this.carseriesname = carseriesname == null ? null : carseriesname.trim();
    }
}