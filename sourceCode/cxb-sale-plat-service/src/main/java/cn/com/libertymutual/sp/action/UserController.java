package cn.com.libertymutual.sp.action;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.Device;
import org.springframework.mobile.device.DeviceUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.annotation.SystemValidate;
import cn.com.libertymutual.core.util.Current;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.biz.IdentifierMarkBiz;
import cn.com.libertymutual.sp.biz.UserBiz;
import cn.com.libertymutual.sp.dto.UserDto;
import cn.com.libertymutual.sp.service.api.LevelService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/nol/user")
public class UserController {
	@Resource
	private UserBiz userBiz;
	@Autowired
	private LevelService levelService;
	@Resource
	private IdentifierMarkBiz identifierMarkBiz;

	@ApiOperation(value = "图形码校验", notes = "图形码校验")
	@PostMapping(value = "/checkImgCode")
	@SystemValidate(validate = false, description = "无需校验接口")
	public ServiceResult checkImgCode(HttpServletRequest request, String imgCode) {
		return userBiz.checkImgCode(request, imgCode);
	}

	@ApiOperation(value = "根据ID获取用户", notes = "测试")
	// @SystemLog(validate = true, description = "需要校验")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "id", value = "id号", required = true, paramType = "query", dataType = "Integer") })
	@RequestMapping(value = "/findUserById") // 暂时关闭
	public ServiceResult findUserByIdAndLogin(@RequestAttribute(DeviceUtils.CURRENT_DEVICE_ATTRIBUTE) Device device, HttpServletRequest request,
			HttpServletResponse response, Integer id) {
		ServiceResult sr = userBiz.findUserByIdAndLogin(request, id);
		identifierMarkBiz.setHeadersToken(device, response, sr);// 根据用户信息设置token

		return sr;
	}

	@ApiOperation(value = "根据UserCode获取用户", notes = "测试")
	@RequestMapping(value = "/findUserByUserCode") // 暂时关闭
	public ServiceResult findUserByUserCode(HttpServletRequest request, String userCode) {
		return userBiz.findUserByUserCode(request, userCode);
	}

	@ApiOperation(value = "查询上级用户", notes = "测试")
	@RequestMapping(value = "/findUserByComcode") // 暂时关闭
	public ServiceResult findUserByComcode(HttpServletRequest request, String comCode) {
		return userBiz.findUserByComcode(request, comCode);
	}

	@ApiOperation(value = "根据微信openID获取用户", notes = "测试")
	@ApiImplicitParams(value = {
			// @ApiImplicitParam(name = "identCode", value = "请求标识码", required = true,
			// paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "openId", value = "微信openId", required = true, paramType = "query", dataType = "String") })
	@RequestMapping(value = "/findUserByOpenId")
	public ServiceResult updateUserByOpenId(@RequestAttribute(DeviceUtils.CURRENT_DEVICE_ATTRIBUTE) Device device, HttpServletRequest request,
			HttpServletResponse response, /* String identCode, */
			String openId) {
		ServiceResult sr = userBiz.updateUserByOpenId(request, response, /* identCode, */ openId);
		identifierMarkBiz.setHeadersToken(device, response, sr);// 根据用户信息设置token
		return sr;
	}

	@ApiOperation(value = "个人账户注册 --短信验证码", notes = "测试")
	@RequestMapping(value = "/register")
	public ServiceResult register(@RequestAttribute(DeviceUtils.CURRENT_DEVICE_ATTRIBUTE) Device device, HttpServletRequest request,
			HttpServletResponse response, UserDto userDto) {
		return userBiz.register(device, request, response, userDto, false);
	}

	@ApiOperation(value = "个人账户注册 -- 短信验证码 & 图形码", notes = "测试")
	@RequestMapping(value = "/registerImg")
	public ServiceResult registerImg(@RequestAttribute(DeviceUtils.CURRENT_DEVICE_ATTRIBUTE) Device device, HttpServletRequest request,
			HttpServletResponse response, UserDto userDto) {
		return userBiz.register(device, request, response, userDto, true);
	}

	@ApiOperation(value = "校验是否需要合并账户", notes = "测试")
	@PostMapping(value = "/isMergeUser")
	public ServiceResult isMergeUser(HttpServletRequest request, HttpServletResponse response, UserDto userDto) {
		return levelService.isMergeUser(userDto.getMobile(), "");
	}

	@ApiOperation(value = "渠道账户注册 -- 短信验证码", notes = "测试")
	@RequestMapping(value = "/registerChannel")
	public ServiceResult registerChannel(@RequestAttribute(DeviceUtils.CURRENT_DEVICE_ATTRIBUTE) Device device, HttpServletRequest request,
			HttpServletResponse response, UserDto userDto, String type) {
		return userBiz.registerChannel(device, request, response, userDto, false, type);
	}

	@ApiOperation(value = "渠道账户注册 -- 短信验证码 & 图形码", notes = "测试")
	@RequestMapping(value = "/registerChannelImg")
	public ServiceResult registerChannelImg(@RequestAttribute(DeviceUtils.CURRENT_DEVICE_ATTRIBUTE) Device device, HttpServletRequest request,
			HttpServletResponse response, UserDto userDto, String type) {
		return userBiz.registerChannel(device, request, response, userDto, true, type);
	}

	@ApiOperation(value = "密码修改_短信验证码", notes = "测试")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "identCode", value = "请求标识码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "mobileCode", value = "手机验证码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "id", value = "用户Id", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "mobile", value = "手机号", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "password", value = "新密码", required = true, paramType = "query", dataType = "String") })
	@RequestMapping(value = "/updatePwdByMobile")
	public ServiceResult updatePwdByMobile(HttpServletRequest request, HttpServletResponse response, String identCode, String mobileCode, String id,
			String mobile, String password) {
		return userBiz.updatePwdByMobile(request, response, identCode, mobileCode, id, mobile, password);
	}

	@ApiOperation(value = "密码修改_短信验证码 & 图形码", notes = "测试")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "identCode", value = "请求标识码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "imgCode", value = "图形码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "mobileCode", value = "手机验证码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "id", value = "用户id", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "mobile", value = "手机号", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "password", value = "新密码", required = true, paramType = "query", dataType = "String") })
	@RequestMapping(value = "/updatePwdByMobile_img")
	public ServiceResult updatePwdByMobile(HttpServletRequest request, HttpServletResponse response, String identCode, String imgCode,
			String mobileCode, String id, String mobile, String password) {
		return userBiz.updatePwdByMobile(request, response, identCode, imgCode, mobileCode, id, mobile, password);
	}

	@ApiOperation(value = "实名认证_短信验证码", notes = "测试")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "identCode", value = "请求标识码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "mobileCode", value = "手机验证码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "userCode", value = "用户编码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "userName", value = "用户真实姓名", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "mobile", value = "手机号", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "idNumber", value = "身份证号码", required = true, paramType = "query", dataType = "String") })
	@RequestMapping(value = "/updateIdNumberByMobile")
	public ServiceResult updateIdNumberByMobile(HttpServletRequest request, HttpServletResponse response, String identCode, String mobileCode,
			String userCode, String userName, String mobile, String idNumber) {
		return userBiz.updateIdNumberByMobile(request, response, identCode, mobileCode, Current.userCode.get(), userName, mobile, idNumber);
	}

	@ApiOperation(value = "实名认证_短信验证码 & 图形码", notes = "测试")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "identCode", value = "请求标识码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "imgCode", value = "图形码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "mobileCode", value = "手机验证码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "userCode", value = "用户编码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "userName", value = "用户真实姓名", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "mobile", value = "手机号", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "idNumber", value = "身份证号码", required = true, paramType = "query", dataType = "String") })
	@RequestMapping(value = "/updateIdNumberByMobile_img")
	public ServiceResult updateIdNumberByMobile(HttpServletRequest request, HttpServletResponse response, String identCode, String imgCode,
			String mobileCode, String userCode, String userName, String mobile, String idNumber) {

		return userBiz.updateIdNumberByMobile(request, response, identCode, imgCode, mobileCode, Current.userCode.get(), userName, mobile, idNumber);
	}

	// @ApiOperation(value = "解绑实名认证_短信验证码", notes = "测试")
	// @ApiImplicitParams(value = { @ApiImplicitParam(name = "identCode", value =
	// "请求标识码", required = true, paramType = "query", dataType = "String"),
	// @ApiImplicitParam(name = "mobileCode", value = "手机验证码", required = true,
	// paramType = "query", dataType = "String"),
	// @ApiImplicitParam(name = "userCode", value = "用户编码", required = true,
	// paramType = "query", dataType = "String"),
	// @ApiImplicitParam(name = "mobile", value = "手机号", required = true, paramType
	// = "query", dataType = "String"),
	// @ApiImplicitParam(name = "idNumber", value = "身份证号码", required = true,
	// paramType = "query", dataType = "String") })
	// @RequestMapping(value = "/unbindIdNumberByMobile")
	// public ServiceResult unbindIdNumberByMobile(HttpServletRequest request,
	// HttpServletResponse response, String identCode, String mobileCode,
	// String userCode, String mobile, String idNumber) {
	// return userBiz.unbindIdNumberByMobile(request, response, identCode,
	// mobileCode, userCode, mobile, idNumber);
	// }
	//
	// @ApiOperation(value = "解绑实名认证_短信验证码 & 图形码", notes = "测试")
	// @ApiImplicitParams(value = { @ApiImplicitParam(name = "identCode", value =
	// "请求标识码", required = true, paramType = "query", dataType = "String"),
	// @ApiImplicitParam(name = "imgCode", value = "图形码", required = true, paramType
	// = "query", dataType = "String"),
	// @ApiImplicitParam(name = "mobileCode", value = "手机验证码", required = true,
	// paramType = "query", dataType = "String"),
	// @ApiImplicitParam(name = "userCode", value = "用户编码", required = true,
	// paramType = "query", dataType = "String"),
	// @ApiImplicitParam(name = "mobile", value = "手机号", required = true, paramType
	// = "query", dataType = "String"),
	// @ApiImplicitParam(name = "idNumber", value = "身份证号码", required = true,
	// paramType = "query", dataType = "String") })
	// @RequestMapping(value = "/unbindIdNumberByMobile_img")
	// public ServiceResult unbindIdNumberByMobile(HttpServletRequest request,
	// HttpServletResponse response, String identCode, String imgCode,
	// String mobileCode, String userCode, String mobile, String idNumber) {
	// return userBiz.unbindIdNumberByMobile(request, response, identCode, imgCode,
	// mobileCode, userCode, mobile, idNumber);
	// }
	//
	// @ApiOperation(value = "姓名修改", notes = "测试")
	// @ApiImplicitParams(value = { @ApiImplicitParam(name = "identCode", value =
	// "请求标识码", required = true, paramType = "query", dataType = "String"),
	// @ApiImplicitParam(name = "imgCode", value = "图形码", required = true, paramType
	// = "query", dataType = "String"),
	// @ApiImplicitParam(name = "id", value = "用户Id", required = true, paramType =
	// "query", dataType = "String"),
	// @ApiImplicitParam(name = "userName", value = "姓名", required = true, paramType
	// = "query", dataType = "String") })
	// @RequestMapping(value = "/updateUserName")
	// public ServiceResult updateUserName(HttpServletRequest request,
	// HttpServletResponse response, String identCode, String imgCode, String id,
	// String oldUserName, String userName) {
	// return userBiz.updateUserName(request, response, identCode, imgCode, id,
	// userName);
	// }

	@ApiOperation(value = "手机号修改_短信验证码", notes = "测试")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "identCode", value = "请求标识码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "mobileCode", value = "手机验证码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "userCode", value = "用户编号", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "mobile", value = "手机号", required = true, paramType = "query", dataType = "String") })
	@RequestMapping(value = "/updateMobile")
	public ServiceResult updateMobile(HttpServletRequest request, HttpServletResponse response, String identCode, String mobileCode, String userCode,
			String mobile) {
		return userBiz.updateMobile(request, response, identCode, mobileCode, Current.userCode.get(), mobile);
	}

	@ApiOperation(value = "手机号修改_短信验证码 & 图形码", notes = "测试")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "identCode", value = "请求标识码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "imgCode", value = "图形码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "mobileCode", value = "手机验证码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "userCode", value = "用户编号", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "mobile", value = "手机号", required = true, paramType = "query", dataType = "String") })
	@RequestMapping(value = "/updateMobile_img")
	public ServiceResult updateMobile(HttpServletRequest request, HttpServletResponse response, String identCode, String imgCode, String mobileCode,
			String userCode, String mobile) {
		return userBiz.updateMobile(request, response, identCode, imgCode, mobileCode, Current.userCode.get(), mobile);
	}

	@ApiOperation(value = "合并账户", notes = "测试")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "userCode", value = "用户编码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "mobile", value = "手机号", required = true, paramType = "query", dataType = "String") })
	@RequestMapping(value = "/updateUserFormerge")
	public ServiceResult updateUserFormerge(HttpServletRequest request, String userCode, String mobile) {
		return userBiz.updateUserFormerge(Current.userCode.get(), mobile);
	}

	@ApiOperation(value = "登录_短信验证码", notes = "测试")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "identCode", value = "请求标识码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "mobile", value = "手机号", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "mobileCode", value = "短信验证码", required = true, paramType = "query", dataType = "String") })
	@RequestMapping(value = "/loginByMobile")
	public ServiceResult loginByMobile(@RequestAttribute(DeviceUtils.CURRENT_DEVICE_ATTRIBUTE) Device device, HttpServletRequest request,
			HttpServletResponse response, String identCode, String mobileCode, String mobile, String refereeId, String refereePro) {

		ServiceResult sr = userBiz.loginByMobile(request, response, identCode, mobileCode, mobile, refereeId, refereePro);
		identifierMarkBiz.setHeadersToken(device, response, sr);// 根据用户信息设置token
		return sr;
	}

	@ApiOperation(value = "登录_短信验证码 & 图形码", notes = "测试")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "identCode", value = "请求标识码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "imgCode", value = "图形码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "mobileCode", value = "短信验证码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "mobile", value = "手机号", required = true, paramType = "query", dataType = "String") })
	@RequestMapping(value = "/loginByMobile_img")
	public ServiceResult loginByMobile(@RequestAttribute(DeviceUtils.CURRENT_DEVICE_ATTRIBUTE) Device device, HttpServletRequest request,
			HttpServletResponse response, String identCode, String imgCode, String mobileCode, String mobile, String refereeId, String refereePro) {

		ServiceResult sr = userBiz.loginByMobile(request, response, identCode, imgCode, mobileCode, mobile, refereeId, refereePro);
		identifierMarkBiz.setHeadersToken(device, response, sr);// 根据用户信息设置token
		return sr;
	}

	@ApiOperation(value = "登录_密码 & 图形码", notes = "测试")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "identCode", value = "请求标识码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "imgCode", value = "图形码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "mobile", value = "手机号", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "password", value = "密码", required = true, paramType = "query", dataType = "String") })
	@RequestMapping(value = "/loginBypassword")
	public ServiceResult loginBypassword(@RequestAttribute(DeviceUtils.CURRENT_DEVICE_ATTRIBUTE) Device device, HttpServletRequest request,
			HttpServletResponse response, String identCode, String imgCode, String mobile, String password) {

		ServiceResult sr = userBiz.loginBypassword(request, response, identCode, imgCode, mobile, password);
		identifierMarkBiz.setHeadersToken(device, response, sr);// 根据用户信息设置token
		return sr;
	}

	@ApiOperation(value = "查询某用户所有有效银行卡", notes = "测试")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "userCode", value = "用户编码", required = true, paramType = "query", dataType = "String") })
	@RequestMapping(value = "/findBankListAll")
	public ServiceResult findBankListAll(HttpServletRequest request, HttpServletResponse response) {
		return userBiz.findBankListAll(request, Current.userCode.get());
	}

	@ApiOperation(value = "绑定银行卡=>第一步校验卡号", notes = "测试")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "userCode", value = "用户编码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "userName", value = "用户姓名", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "bankNumber", value = "银行卡号", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "province", value = "开户行所在省", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "city", value = "开户行所在市", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "bankBranchName", value = "支行名称", required = true, paramType = "query", dataType = "String") })
	@RequestMapping(value = "/findBankNoOfBinding")
	public ServiceResult findBankNoOfBinding(HttpServletRequest request, HttpServletResponse response, String userCode, String userName,
			String bankNumber, String province, String city, String bankBranchName) {
		return userBiz.findBankNoOfBinding(request, Current.userCode.get(), userName, bankNumber, province, city, bankBranchName);
	}

	@ApiOperation(value = "绑定银行卡=>第二步校验短信验证码并绑定", notes = "测试")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "userCode", value = "用户编码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "bankName", value = "银行名称", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "bankNumber", value = "银行卡号", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "mobileCode", value = "短信验证码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "mobile", value = "手机号", required = true, paramType = "query", dataType = "String") })
	@RequestMapping(value = "/updateBankOfBinding")
	public ServiceResult updateBankOfBinding(HttpServletRequest request, HttpServletResponse response, String userCode, String bankName,
			String bankNumber, String mobileCode, String mobile) {
		return userBiz.updateBankOfBinding(request, Current.userCode.get(), bankName, bankNumber, mobileCode, mobile);
	}

	@ApiOperation(value = "解绑银行卡", notes = "测试")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "userCode", value = "用户编码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "bankId", value = "银行ID", required = true, paramType = "query", dataType = "Integer"),
			@ApiImplicitParam(name = "bankNumber", value = "银行卡号", required = true, paramType = "query", dataType = "String") })
	@RequestMapping(value = "/updateBankUnbundling")
	public ServiceResult updateBankUnbundling(HttpServletRequest request, HttpServletResponse response, String userCode, Integer bankId,
			String number) {
		return userBiz.updateBankUnbundling(Current.userCode.get(), bankId, number);
	}

	@ApiOperation(value = "设置默认银行卡", notes = "测试")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "userCode", value = "用户编码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "bankId", value = "银行ID", required = true, paramType = "query", dataType = "Integer"),
			@ApiImplicitParam(name = "bankNumber", value = "银行卡号", required = true, paramType = "query", dataType = "String") })
	@RequestMapping(value = "/updateBankDefault")
	public ServiceResult updateBankDefault(HttpServletRequest request, HttpServletResponse response, String userCode, Integer bankId, String number) {
		return userBiz.updateBankDefault(Current.userCode.get(), bankId, number);
	}
}
