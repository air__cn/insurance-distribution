package cn.com.libertymutual.sp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import cn.com.libertymutual.sp.bean.TbSpKeywordWeChat;

public interface KeywordWeChatDao extends PagingAndSortingRepository<TbSpKeywordWeChat, Integer>, JpaSpecificationExecutor<TbSpKeywordWeChat> {

	@Query("from TbSpKeywordWeChat where state = 1")
	List<TbSpKeywordWeChat> findAll();

	@Query("from TbSpKeywordWeChat where state = :state and id = :id")
	TbSpKeywordWeChat findByStateAndId(@Param("state") String state, @Param("id") Integer id);

	@Query("select replyText from TbSpKeywordWeChat where state = '1' and keyName = :keyName")
	String findAutoReturnMsg(@Param("keyName") String keyName);

	@Query("select count(id) from TbSpKeywordWeChat where state = '1' and keyName = :keyName")
	int countByKeyName(@Param("keyName") String keyName);
}
