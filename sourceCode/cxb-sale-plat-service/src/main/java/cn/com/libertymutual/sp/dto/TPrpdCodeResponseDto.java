package cn.com.libertymutual.sp.dto;

import java.io.Serializable;
import java.util.List;

import cn.com.libertymutual.core.base.dto.ResponseBaseDto;

public class TPrpdCodeResponseDto extends ResponseBaseDto implements Serializable {
		/**
	 * 
	 */
	private static final long serialVersionUID = 1584452485163880963L;
		List<PrpdCode> prpdCodes;

		public List<PrpdCode> getPrpdCodes() {
			return prpdCodes;
		}

		public void setPrpdCodes(List<PrpdCode> prpdCodes) {
			this.prpdCodes = prpdCodes;
		}
		
}

