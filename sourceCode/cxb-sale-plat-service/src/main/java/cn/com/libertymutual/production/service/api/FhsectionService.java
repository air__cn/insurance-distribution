package cn.com.libertymutual.production.service.api;

import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.Fhsection;

/**
 * @author steven.li
 * @create 2017/12/20
 */
public interface FhsectionService {

    void insert(Fhsection record);

    List<Fhsection> findByTreatyNo(String treatyno);
}
