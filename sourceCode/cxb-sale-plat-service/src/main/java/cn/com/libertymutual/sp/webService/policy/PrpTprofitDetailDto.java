
package cn.com.libertymutual.sp.webService.policy;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for prpTprofitDetailDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="prpTprofitDetailDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ABflag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="addonCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="annual" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="carCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="carmodelFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="chooseFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="condition" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="conditionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="damagedFactorGrade" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="experienceLossRatio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fieldValue" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="flag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="itemKindNo" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="kindCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="kindName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="motorcadeManage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="profitCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="profitName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="profitPeriod" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="profitRate" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="profitType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="proposalNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="riskCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="runAreaCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="runMiles" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serialNo" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prpTprofitDetailDto", propOrder = {
    "aBflag",
    "addonCount",
    "annual",
    "carCount",
    "carmodelFlag",
    "chooseFlag",
    "condition",
    "conditionCode",
    "damagedFactorGrade",
    "experienceLossRatio",
    "fieldValue",
    "flag",
    "itemKindNo",
    "kindCode",
    "kindName",
    "motorcadeManage",
    "profitCode",
    "profitName",
    "profitPeriod",
    "profitRate",
    "profitType",
    "proposalNo",
    "riskCode",
    "runAreaCode",
    "runMiles",
    "serialNo"
})
public class PrpTprofitDetailDto implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 660224344485616765L;
	@XmlElement(name = "ABflag")
    protected String aBflag;
    protected String addonCount;
    protected String annual;
    protected String carCount;
    protected String carmodelFlag;
    protected String chooseFlag;
    protected String condition;
    protected String conditionCode;
    protected String damagedFactorGrade;
    protected String experienceLossRatio;
    protected double fieldValue;
    protected String flag;
    protected double itemKindNo;
    protected String kindCode;
    protected String kindName;
    protected String motorcadeManage;
    protected String profitCode;
    protected String profitName;
    protected double profitPeriod;
    protected double profitRate;
    protected String profitType;
    protected String proposalNo;
    protected String riskCode;
    protected String runAreaCode;
    protected String runMiles;
    protected double serialNo;

    /**
     * Gets the value of the aBflag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getABflag() {
        return aBflag;
    }

    /**
     * Sets the value of the aBflag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setABflag(String value) {
        this.aBflag = value;
    }

    /**
     * Gets the value of the addonCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddonCount() {
        return addonCount;
    }

    /**
     * Sets the value of the addonCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddonCount(String value) {
        this.addonCount = value;
    }

    /**
     * Gets the value of the annual property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnnual() {
        return annual;
    }

    /**
     * Sets the value of the annual property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnnual(String value) {
        this.annual = value;
    }

    /**
     * Gets the value of the carCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCarCount() {
        return carCount;
    }

    /**
     * Sets the value of the carCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCarCount(String value) {
        this.carCount = value;
    }

    /**
     * Gets the value of the carmodelFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCarmodelFlag() {
        return carmodelFlag;
    }

    /**
     * Sets the value of the carmodelFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCarmodelFlag(String value) {
        this.carmodelFlag = value;
    }

    /**
     * Gets the value of the chooseFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChooseFlag() {
        return chooseFlag;
    }

    /**
     * Sets the value of the chooseFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChooseFlag(String value) {
        this.chooseFlag = value;
    }

    /**
     * Gets the value of the condition property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCondition() {
        return condition;
    }

    /**
     * Sets the value of the condition property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCondition(String value) {
        this.condition = value;
    }

    /**
     * Gets the value of the conditionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConditionCode() {
        return conditionCode;
    }

    /**
     * Sets the value of the conditionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConditionCode(String value) {
        this.conditionCode = value;
    }

    /**
     * Gets the value of the damagedFactorGrade property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDamagedFactorGrade() {
        return damagedFactorGrade;
    }

    /**
     * Sets the value of the damagedFactorGrade property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDamagedFactorGrade(String value) {
        this.damagedFactorGrade = value;
    }

    /**
     * Gets the value of the experienceLossRatio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExperienceLossRatio() {
        return experienceLossRatio;
    }

    /**
     * Sets the value of the experienceLossRatio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExperienceLossRatio(String value) {
        this.experienceLossRatio = value;
    }

    /**
     * Gets the value of the fieldValue property.
     * 
     */
    public double getFieldValue() {
        return fieldValue;
    }

    /**
     * Sets the value of the fieldValue property.
     * 
     */
    public void setFieldValue(double value) {
        this.fieldValue = value;
    }

    /**
     * Gets the value of the flag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlag() {
        return flag;
    }

    /**
     * Sets the value of the flag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlag(String value) {
        this.flag = value;
    }

    /**
     * Gets the value of the itemKindNo property.
     * 
     */
    public double getItemKindNo() {
        return itemKindNo;
    }

    /**
     * Sets the value of the itemKindNo property.
     * 
     */
    public void setItemKindNo(double value) {
        this.itemKindNo = value;
    }

    /**
     * Gets the value of the kindCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKindCode() {
        return kindCode;
    }

    /**
     * Sets the value of the kindCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKindCode(String value) {
        this.kindCode = value;
    }

    /**
     * Gets the value of the kindName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKindName() {
        return kindName;
    }

    /**
     * Sets the value of the kindName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKindName(String value) {
        this.kindName = value;
    }

    /**
     * Gets the value of the motorcadeManage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMotorcadeManage() {
        return motorcadeManage;
    }

    /**
     * Sets the value of the motorcadeManage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMotorcadeManage(String value) {
        this.motorcadeManage = value;
    }

    /**
     * Gets the value of the profitCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfitCode() {
        return profitCode;
    }

    /**
     * Sets the value of the profitCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfitCode(String value) {
        this.profitCode = value;
    }

    /**
     * Gets the value of the profitName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfitName() {
        return profitName;
    }

    /**
     * Sets the value of the profitName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfitName(String value) {
        this.profitName = value;
    }

    /**
     * Gets the value of the profitPeriod property.
     * 
     */
    public double getProfitPeriod() {
        return profitPeriod;
    }

    /**
     * Sets the value of the profitPeriod property.
     * 
     */
    public void setProfitPeriod(double value) {
        this.profitPeriod = value;
    }

    /**
     * Gets the value of the profitRate property.
     * 
     */
    public double getProfitRate() {
        return profitRate;
    }

    /**
     * Sets the value of the profitRate property.
     * 
     */
    public void setProfitRate(double value) {
        this.profitRate = value;
    }

    /**
     * Gets the value of the profitType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfitType() {
        return profitType;
    }

    /**
     * Sets the value of the profitType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfitType(String value) {
        this.profitType = value;
    }

    /**
     * Gets the value of the proposalNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProposalNo() {
        return proposalNo;
    }

    /**
     * Sets the value of the proposalNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProposalNo(String value) {
        this.proposalNo = value;
    }

    /**
     * Gets the value of the riskCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRiskCode() {
        return riskCode;
    }

    /**
     * Sets the value of the riskCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRiskCode(String value) {
        this.riskCode = value;
    }

    /**
     * Gets the value of the runAreaCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRunAreaCode() {
        return runAreaCode;
    }

    /**
     * Sets the value of the runAreaCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRunAreaCode(String value) {
        this.runAreaCode = value;
    }

    /**
     * Gets the value of the runMiles property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRunMiles() {
        return runMiles;
    }

    /**
     * Sets the value of the runMiles property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRunMiles(String value) {
        this.runMiles = value;
    }

    /**
     * Gets the value of the serialNo property.
     * 
     */
    public double getSerialNo() {
        return serialNo;
    }

    /**
     * Sets the value of the serialNo property.
     * 
     */
    public void setSerialNo(double value) {
        this.serialNo = value;
    }

}
