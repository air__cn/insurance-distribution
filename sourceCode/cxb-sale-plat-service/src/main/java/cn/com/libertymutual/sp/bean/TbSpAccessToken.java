package cn.com.libertymutual.sp.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tb_sp_access_token")
public class TbSpAccessToken implements Serializable {

	private static final long serialVersionUID = 7747423147194017549L;

	private Integer id;
	private String appId;// 微信公众号APPID
	private String secret;// 微信公众号APPID对应的密钥
	private String tokenKey;// 微信公众号校验接口的key
	private String createTime;// 记录创建时间
	private String publicAccessToken;// 公众号的全局唯一接口调用凭据，至少要保留512个字符空间
	private Integer expiresIn;// 凭证有效时间，单位：秒
	private Integer dayTimes;// 当日已获取的次数
	private Integer type;// accesstoken类型，1=微信公众号全局token，2=企业微信全局token

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "APP_ID")
	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	@Column(name = "SECRET")
	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	@Column(name = "TOKEN_KEY")
	public String getTokenKey() {
		return tokenKey;
	}

	public void setTokenKey(String tokenKey) {
		this.tokenKey = tokenKey;
	}

	@Column(name = "CREATE_TIME")
	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	@Column(name = "PUBLIC_ACCESS_TOKEN")
	public String getPublicAccessToken() {
		return publicAccessToken;
	}

	public void setPublicAccessToken(String publicAccessToken) {
		this.publicAccessToken = publicAccessToken;
	}

	@Column(name = "EXPIRES_IN")
	public Integer getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(Integer expiresIn) {
		this.expiresIn = expiresIn;
	}

	@Column(name = "DAY_TIMES")
	public Integer getDayTimes() {
		return dayTimes;
	}

	public void setDayTimes(Integer dayTimes) {
		this.dayTimes = dayTimes;
	}

	@Column(name = "TYPE")
	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}
}