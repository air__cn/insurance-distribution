package cn.com.libertymutual.sp.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpClause;
import cn.com.libertymutual.sp.service.api.ClauseService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value = "/admin/clause")
public class ClauseControllerWeb {

	@Autowired
	private ClauseService clauseService;
	
	
	
	/*
	 * 条款列表
	 */
	@ApiOperation(value = "条款列表", notes = "条款列表")
	@PostMapping(value = "/clauseList")
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "clauseTitle", value = "条款标题", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "kindCode", value = "条款代码", required = true, paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "pageNumber", value = "", required = true, paramType = "query", dataType = "Long"),
            @ApiImplicitParam(name = "pageSize", value = "", required = true, paramType = "query", dataType = "Long"),
    })
	public ServiceResult clauseList(String clauseTitle,String kindCode,int pageNumber,int pageSize){
		ServiceResult sr = new ServiceResult();
		
		sr = clauseService.clauseList(clauseTitle,kindCode,pageNumber,pageSize);
		return sr;
		
	}
	
	
	
	/*
	 * 新增条款
	 */
	@ApiOperation(value = "新增条款", notes = "新增条款")
	@PostMapping(value = "/addClause")
	public ServiceResult addClause(@RequestBody @ApiParam(name = "tbSpClause", value = "tbSpClause", required = true)TbSpClause tbSpClause){
		ServiceResult sr = new ServiceResult();
		
		sr = clauseService.addClause(tbSpClause);
		return sr;
		
	}
	
	
	
	/*
	 * 删除条款
	 */
	@ApiOperation(value = "删除条款", notes = "删除条款")
	@PostMapping(value = "/removeClause")
	@ApiImplicitParams(value = {
            @ApiImplicitParam(name = "id", value = "", required = true, paramType = "query", dataType = "Long"),
    })
	public ServiceResult removeClause(Integer id){
		ServiceResult sr = new ServiceResult();
		sr = clauseService.removeClause(id);
		return sr;
		
	}
	
	
	/*
	 * 条款标题列表
	 */
	@ApiOperation(value = "条款标题列表", notes = "条款标题列表")
	@PostMapping(value = "/clauseTitleList")
	public ServiceResult clauseTitleList(){
		ServiceResult sr = new ServiceResult();
		sr = clauseService.clauseTitleList();
		return sr;
		
	}
}
