package cn.com.libertymutual.sp.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;

@ApiModel
@Entity
@Table(name = "tb_sp_inscorelog")
public class TbSpInScoreLog implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String changeType;
	private Double balance;
	private Double reChangeScore;
	private Double acChangeScore;
	private String granter;
	private String reasonNo;
	private String reason;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date changeTime;
	private String status;
	private Date effictiveDate;
	private Date invalidDate;
	private String userCode;
	private String telephone;
	private String openid;
	private String remark;
	private String versionNo;
	
	public TbSpInScoreLog() {
		super();
		// TODO Auto-generated constructor stub
	}
	public TbSpInScoreLog(String changeType, Double balance,
			Double reChangeScore, Double acChangeScore, String granter,
			String reasonNo, String reason, Date changeTime, String status,
			Date effictiveDate, Date invalidDate, String userCode,
			String telephone, String openid, String remark, String versionNo) {
		super();
		this.changeType = changeType;
		this.balance = balance;
		this.reChangeScore = reChangeScore;
		this.acChangeScore = acChangeScore;
		this.granter = granter;
		this.reasonNo = reasonNo;
		this.reason = reason;
		this.changeTime = changeTime;
		this.status = status;
		this.effictiveDate = effictiveDate;
		this.invalidDate = invalidDate;
		this.userCode = userCode;
		this.telephone = telephone;
		this.openid = openid;
		this.remark = remark;
		this.versionNo = versionNo;
	}
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	
	@Column(name = "change_type")
	public String getChangeType() {
		return changeType;
	}
	public void setChangeType(String changeType) {
		this.changeType = changeType;
	}
	
	@Column(name = "balance")
	public Double getBalance() {
		return balance;
	}
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	
	@Column(name = "re_changescore")
	public Double getReChangeScore() {
		return reChangeScore;
	}
	public void setReChangeScore(Double reChangeScore) {
		this.reChangeScore = reChangeScore;
	}
	@Column(name = "ac_changescore")
	public Double getAcChangeScore() {
		return acChangeScore;
	}
	public void setAcChangeScore(Double acChangeScore) {
		this.acChangeScore = acChangeScore;
	}
	
	@Column(name = "granter")
	public String getGranter() {
		return granter;
	}
	public void setGranter(String granter) {
		this.granter = granter;
	}
	
	@Column(name = "reason_no")
	public String getReasonNo() {
		return reasonNo;
	}
	public void setReasonNo(String reasonNo) {
		this.reasonNo = reasonNo;
	}
	
	@Column(name = "reason")
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	
	@Column(name = "status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Column(name = "change_time")
	public Date getChangeTime() {
		return changeTime;
	}
	public void setChangeTime(Date changeTime) {
		this.changeTime = changeTime;
	}
	@Column(name = "effictive_date")
	public Date getEffictiveDate() {
		return effictiveDate;
	}
	public void setEffictiveDate(Date effictiveDate) {
		this.effictiveDate = effictiveDate;
	}
	
	@Column(name = "invalid_date")
	public Date getInvalidDate() {
		return invalidDate;
	}
	public void setInvalidDate(Date invalidDate) {
		this.invalidDate = invalidDate;
	}
	
	@Column(name = "user_code")
	public String getUserCode() {
		return userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	
	@Column(name = "telephone")
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	
	@Column(name = "openid")
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	@Column(name = "remark")
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	@Column(name = "versionNo")
	public String getVersionNo() {
		return versionNo;
	}
	public void setVersionNo(String versionNo) {
		this.versionNo = versionNo;
	}
	
	
	
	
	
	
	
	
	
}
