package cn.com.libertymutual.production.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import cn.com.libertymutual.production.dao.nomorcldatasource.PrpdefileMapper;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdefile;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdefileExample;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdefileWithBLOBs;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdefileExample.Criteria;
import cn.com.libertymutual.production.pojo.request.PrpdEFileRequest;
import cn.com.libertymutual.production.service.api.PrpdEFileService;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

/** 
 * @author GuoYue
 * @date 2017年8月7日
 *  
 */
@Service
public class PrpdEFileServiceImpl implements PrpdEFileService {

	@Autowired
	private PrpdefileMapper prpdefileMapper;

	@Override
	public PageInfo<PrpdefileWithBLOBs> findPrpdEfile(PrpdEFileRequest prpdEFileRequest) {
		PrpdefileExample prpdefileExample = new PrpdefileExample();
		Criteria criteria = prpdefileExample.createCriteria();
		String kindCode = prpdEFileRequest.getKindcode();
		String eFileCName = prpdEFileRequest.getEfilecname();
		Date createDate = prpdEFileRequest.getCreatedate();
		String validStatus = prpdEFileRequest.getValidatestatus();
		String riskCode = prpdEFileRequest.getRiskcode();
		String classCode = prpdEFileRequest.getClasscode();
		String orderBy = prpdEFileRequest.getOrderBy();
		String order = prpdEFileRequest.getOrder();
		
		prpdefileExample.setOrderByClause("inserttime desc");
		if(!StringUtils.isEmpty(kindCode)) {
			criteria.andKindcodeLike("%" + kindCode.toUpperCase() + "%");
		}
		if(!StringUtils.isEmpty(eFileCName)) {
			criteria.andEfilecnameLike("%" + eFileCName + "%");
		}
		if (createDate != null) {
			criteria.andInserttimeGreaterThanOrEqualTo(createDate);
			Calendar c = Calendar.getInstance();  
	        c.setTime(createDate);  
	        c.add(Calendar.DAY_OF_MONTH, 1);// 今天+1天  
	        Date tomorrow = c.getTime();
	        criteria.andInserttimeLessThanOrEqualTo(tomorrow);
		}
		if(!StringUtils.isEmpty(validStatus)) {
			criteria.andValidatestatusEqualTo(validStatus);
		}
		if(!StringUtils.isEmpty(riskCode)) {
			criteria.andRiskcodeEqualTo(riskCode);
		}
		if(!StringUtils.isEmpty(classCode)) {
			criteria.andRiskcodeLike(classCode + "%");
		}
		if (!StringUtils.isEmpty(orderBy) && !"undefined".equals(orderBy)) {
			prpdefileExample.setOrderByClause(orderBy + " " + order);
		}
		
		PageHelper.startPage(prpdEFileRequest.getCurrentPage(), prpdEFileRequest.getPageSize());
		PageInfo<PrpdefileWithBLOBs> pageInfo = new PageInfo<>(prpdefileMapper.selectByExampleWithBLOBs(prpdefileExample));
		return pageInfo;
	}

	@Override
	public void insert(PrpdefileWithBLOBs record) {
		prpdefileMapper.insertSelective(record);
	}

	@Override
	public String findEFileCode(String code) {
		return prpdefileMapper.selectEFileCode(code);
	}

	@Override
	public void update(PrpdefileWithBLOBs record) {
		PrpdefileExample example = new PrpdefileExample();
		Criteria criteria = example.createCriteria();
		criteria.andEfilecodeEqualTo(record.getEfilecode());
		criteria.andKindcodeEqualTo(record.getKindcode());
		criteria.andKindversionEqualTo(record.getKindversion());
		prpdefileMapper.updateByExampleSelective(record, example);
	}

	@Override
	public List<PrpdefileWithBLOBs> fetchEFiles(String eFileCode, String kindCode, String kindVersion) {
		PrpdefileExample example = new PrpdefileExample();
		Criteria criteria = example.createCriteria();
		criteria.andEfilecodeEqualTo(eFileCode);
		criteria.andKindcodeEqualTo(kindCode);
		criteria.andKindversionEqualTo(kindVersion);
		return prpdefileMapper.selectByExampleWithBLOBs(example);
	}

}
