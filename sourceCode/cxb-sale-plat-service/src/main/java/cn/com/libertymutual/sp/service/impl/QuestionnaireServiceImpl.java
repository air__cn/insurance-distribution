package cn.com.libertymutual.sp.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpInputConfig;
import cn.com.libertymutual.sp.bean.TbSpQuestionnaire;
import cn.com.libertymutual.sp.dao.InputConfigDao;
import cn.com.libertymutual.sp.dao.QuestionnaireDao;
import cn.com.libertymutual.sp.service.api.QuestionnaireService;

@Service("QuestionnaireService")
public class QuestionnaireServiceImpl implements QuestionnaireService {

	private Logger log = LoggerFactory.getLogger(getClass());
	@Autowired
	private QuestionnaireDao questionnaireDao;
	@Autowired
	private InputConfigDao inputConfigDao;

	@Override
	public ServiceResult findQueId(String id) {
		ServiceResult sr = new ServiceResult();
		TbSpQuestionnaire que = questionnaireDao.findByQuestId(id);
		if (null == que) {
			sr.setFail();
			sr.setResult("没有查询到对应的信息！");
			return sr;
		}
		sr.setSuccess();
		sr.setResult(que);

		return sr;
	}

	@Override
	public ServiceResult questionList(String title, String type) {
		ServiceResult sr = new ServiceResult();
		Sort sort = new Sort(Direction.DESC, "id");
		List<TbSpQuestionnaire> list = questionnaireDao.findAll(new Specification<TbSpQuestionnaire>() {
			public Predicate toPredicate(Root<TbSpQuestionnaire> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicate = new ArrayList<Predicate>();
				if (StringUtils.isNotEmpty(title)) {
					log.info(title);
					predicate.add(cb.like(root.get("title").as(String.class), "%" + title + "%"));
				}
				if (StringUtils.isNotEmpty(type)) {
					log.info(type);
					predicate.add(cb.equal(root.get("type").as(String.class), type));
				}

				Predicate[] pre = new Predicate[predicate.size()];
				return query.where(predicate.toArray(pre)).getRestriction();
			}
		}, sort);
		sr.setResult(list);
		return sr;
	}

	@Override
	public ServiceResult addOrUpdateQuestionnaire(TbSpQuestionnaire questionnaire) {
		ServiceResult sr = new ServiceResult();
		sr.setResult(questionnaireDao.save(questionnaire));
		return sr;
	}

	@Override
	public ServiceResult addPetConfig(TbSpInputConfig inputConfig) {
		ServiceResult sr = new ServiceResult();
		sr.setResult(inputConfigDao.save(inputConfig));
		return sr;
	}

	@Override
	public ServiceResult petConfigList() {
		ServiceResult sr = new ServiceResult();
		Sort sort = new Sort(Direction.DESC, "id");
		Iterable<TbSpInputConfig> list = inputConfigDao.findAll(sort);
		sr.setResult(list);
		return sr;
	}

}
