
package cn.com.libertymutual.sp.webService.policy;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for policyDetailInquiryRequestDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="policyDetailInquiryRequestDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="policyNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="requestHeadDto" type="{http://service.liberty.com/common/bean}requestHeadDto" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "policyDetailInquiryRequestDto", namespace = "http://prpall.liberty.com/all/cb/policyDetailInquiry/bean", propOrder = {
    "policyNo",
    "requestHeadDto"
})
public class PolicyDetailInquiryRequestDto implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    protected String policyNo;
    protected RequestHeadDto requestHeadDto;

    /**
     * Gets the value of the policyNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyNo() {
        return policyNo;
    }

    /**
     * Sets the value of the policyNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyNo(String value) {
        this.policyNo = value;
    }

    /**
     * Gets the value of the requestHeadDto property.
     * 
     * @return
     *     possible object is
     *     {@link RequestHeadDto }
     *     
     */
    public RequestHeadDto getRequestHeadDto() {
        return requestHeadDto;
    }

    /**
     * Sets the value of the requestHeadDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestHeadDto }
     *     
     */
    public void setRequestHeadDto(RequestHeadDto value) {
        this.requestHeadDto = value;
    }

}
