package cn.com.libertymutual.production.model.nomorcldatasource;

import java.math.BigDecimal;

public class Prpdcompany {
    private String comcode;

    private String comcname;

    private String comename;

    private String addressename;

    private String postcode;

    private String phonenumber;

    private String taxnumber;

    private String faxnumber;

    private String uppercomcode;

    private String insurername;

    private String comattribute;

    private String comtype;

    private String comlevel;

    private String manager;

    private String accountleader;

    private String cashier;

    private String accountant;

    private String remark;

    private String newcomcode;

    private String validstatus;

    private String acntunit;

    private String articlecode;

    private String acccode;

    private String centerflag;

    private String outerpaycode;

    private String innerpaycode;

    private String flag;

    private String webaddress;

    private String servicephone;

    private String reportphone;

    private String agentcode;

    private String agreementno;

    private String sysareacode;

    private BigDecimal combvisitrate;

    private String comsign;

    private String motorareacode;

    private String actualuppercomcode;

    private String motorcountycode;

    public String getComcode() {
        return comcode;
    }

    public void setComcode(String comcode) {
        this.comcode = comcode == null ? null : comcode.trim();
    }

    public String getComcname() {
        return comcname;
    }

    public void setComcname(String comcname) {
        this.comcname = comcname == null ? null : comcname.trim();
    }

    public String getComename() {
        return comename;
    }

    public void setComename(String comename) {
        this.comename = comename == null ? null : comename.trim();
    }

    public String getAddressename() {
        return addressename;
    }

    public void setAddressename(String addressename) {
        this.addressename = addressename == null ? null : addressename.trim();
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode == null ? null : postcode.trim();
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber == null ? null : phonenumber.trim();
    }

    public String getTaxnumber() {
        return taxnumber;
    }

    public void setTaxnumber(String taxnumber) {
        this.taxnumber = taxnumber == null ? null : taxnumber.trim();
    }

    public String getFaxnumber() {
        return faxnumber;
    }

    public void setFaxnumber(String faxnumber) {
        this.faxnumber = faxnumber == null ? null : faxnumber.trim();
    }

    public String getUppercomcode() {
        return uppercomcode;
    }

    public void setUppercomcode(String uppercomcode) {
        this.uppercomcode = uppercomcode == null ? null : uppercomcode.trim();
    }

    public String getInsurername() {
        return insurername;
    }

    public void setInsurername(String insurername) {
        this.insurername = insurername == null ? null : insurername.trim();
    }

    public String getComattribute() {
        return comattribute;
    }

    public void setComattribute(String comattribute) {
        this.comattribute = comattribute == null ? null : comattribute.trim();
    }

    public String getComtype() {
        return comtype;
    }

    public void setComtype(String comtype) {
        this.comtype = comtype == null ? null : comtype.trim();
    }

    public String getComlevel() {
        return comlevel;
    }

    public void setComlevel(String comlevel) {
        this.comlevel = comlevel == null ? null : comlevel.trim();
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager == null ? null : manager.trim();
    }

    public String getAccountleader() {
        return accountleader;
    }

    public void setAccountleader(String accountleader) {
        this.accountleader = accountleader == null ? null : accountleader.trim();
    }

    public String getCashier() {
        return cashier;
    }

    public void setCashier(String cashier) {
        this.cashier = cashier == null ? null : cashier.trim();
    }

    public String getAccountant() {
        return accountant;
    }

    public void setAccountant(String accountant) {
        this.accountant = accountant == null ? null : accountant.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getNewcomcode() {
        return newcomcode;
    }

    public void setNewcomcode(String newcomcode) {
        this.newcomcode = newcomcode == null ? null : newcomcode.trim();
    }

    public String getValidstatus() {
        return validstatus;
    }

    public void setValidstatus(String validstatus) {
        this.validstatus = validstatus == null ? null : validstatus.trim();
    }

    public String getAcntunit() {
        return acntunit;
    }

    public void setAcntunit(String acntunit) {
        this.acntunit = acntunit == null ? null : acntunit.trim();
    }

    public String getArticlecode() {
        return articlecode;
    }

    public void setArticlecode(String articlecode) {
        this.articlecode = articlecode == null ? null : articlecode.trim();
    }

    public String getAcccode() {
        return acccode;
    }

    public void setAcccode(String acccode) {
        this.acccode = acccode == null ? null : acccode.trim();
    }

    public String getCenterflag() {
        return centerflag;
    }

    public void setCenterflag(String centerflag) {
        this.centerflag = centerflag == null ? null : centerflag.trim();
    }

    public String getOuterpaycode() {
        return outerpaycode;
    }

    public void setOuterpaycode(String outerpaycode) {
        this.outerpaycode = outerpaycode == null ? null : outerpaycode.trim();
    }

    public String getInnerpaycode() {
        return innerpaycode;
    }

    public void setInnerpaycode(String innerpaycode) {
        this.innerpaycode = innerpaycode == null ? null : innerpaycode.trim();
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag == null ? null : flag.trim();
    }

    public String getWebaddress() {
        return webaddress;
    }

    public void setWebaddress(String webaddress) {
        this.webaddress = webaddress == null ? null : webaddress.trim();
    }

    public String getServicephone() {
        return servicephone;
    }

    public void setServicephone(String servicephone) {
        this.servicephone = servicephone == null ? null : servicephone.trim();
    }

    public String getReportphone() {
        return reportphone;
    }

    public void setReportphone(String reportphone) {
        this.reportphone = reportphone == null ? null : reportphone.trim();
    }

    public String getAgentcode() {
        return agentcode;
    }

    public void setAgentcode(String agentcode) {
        this.agentcode = agentcode == null ? null : agentcode.trim();
    }

    public String getAgreementno() {
        return agreementno;
    }

    public void setAgreementno(String agreementno) {
        this.agreementno = agreementno == null ? null : agreementno.trim();
    }

    public String getSysareacode() {
        return sysareacode;
    }

    public void setSysareacode(String sysareacode) {
        this.sysareacode = sysareacode == null ? null : sysareacode.trim();
    }

    public BigDecimal getCombvisitrate() {
        return combvisitrate;
    }

    public void setCombvisitrate(BigDecimal combvisitrate) {
        this.combvisitrate = combvisitrate;
    }

    public String getComsign() {
        return comsign;
    }

    public void setComsign(String comsign) {
        this.comsign = comsign == null ? null : comsign.trim();
    }

    public String getMotorareacode() {
        return motorareacode;
    }

    public void setMotorareacode(String motorareacode) {
        this.motorareacode = motorareacode == null ? null : motorareacode.trim();
    }

    public String getActualuppercomcode() {
        return actualuppercomcode;
    }

    public void setActualuppercomcode(String actualuppercomcode) {
        this.actualuppercomcode = actualuppercomcode == null ? null : actualuppercomcode.trim();
    }

    public String getMotorcountycode() {
        return motorcountycode;
    }

    public void setMotorcountycode(String motorcountycode) {
        this.motorcountycode = motorcountycode == null ? null : motorcountycode.trim();
    }
}