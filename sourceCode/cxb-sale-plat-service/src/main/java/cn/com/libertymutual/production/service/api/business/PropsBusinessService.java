package cn.com.libertymutual.production.service.api.business;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdriskplan;
import cn.com.libertymutual.production.pojo.request.PrpdPlanPropsRequest;
import cn.com.libertymutual.production.pojo.request.PrpdPropsRequest;
import cn.com.libertymutual.production.pojo.response.Response;
import cn.com.libertymutual.production.service.api.PrpdPlanPropsService;
import cn.com.libertymutual.production.service.api.PrpdPropsService;
import cn.com.libertymutual.production.service.api.PrpdrationService;
import cn.com.libertymutual.production.service.impl.SystemLog;
import cn.com.libertymutual.production.utils.Constant;

/** 
 * @Description: 后台业务逻辑入口
 * @author Steven.Li
 * @date 2017年7月28日
 *  
 */
public abstract class PropsBusinessService {

	protected static final Logger log = Constant.log;
	
	@Autowired
	protected SystemLog systemLog;
	@Autowired
	protected PrpdPropsService prpdPropsService;
	@Autowired
	protected PrpdPlanPropsService prpdPlanPropsService;
	@Autowired
	protected PrpdrationService prpdrationService;
	
	/**
	 * 分页查询因子
	 * @param request
	 * @return
	 */
	public abstract Response findProps(PrpdPropsRequest request);

	/**
	 * 方案管理-配置因子
	 * @param request
	 */
	public abstract Response insertProps(PrpdPlanPropsRequest request) throws Exception;

	/**
	 * 方案管理-获取已配置因子
	 * @param planCode
	 * @param riskCode
	 * @param riskVersion
	 * @return
	 */
	public abstract Response findPlanProps(Prpdriskplan plan);
	
}