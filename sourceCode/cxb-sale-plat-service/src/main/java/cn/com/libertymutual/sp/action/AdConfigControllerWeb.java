package cn.com.libertymutual.sp.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpAdconfig;
import cn.com.libertymutual.sp.service.api.AdConfigService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
@RestController
@RequestMapping(value = "/admin/advertWeb")
public class AdConfigControllerWeb {
	
	
	private Logger log = LoggerFactory.getLogger(getClass());
	@Autowired
	private AdConfigService adConfigService;
	
	
	@ApiOperation(value = "后台广告配置列表接口", notes = "后台广告配置列表接口")
	@PostMapping(value = "/advertList")
	@ApiImplicitParams(value = {
            @ApiImplicitParam(name = "isShow", value = "是否显示", required = true, paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "toUrl", value = "跳转地址", required = true, paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "adType", value = "广告类别", required = true, paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "pageNumber", value = "页码", required = true, paramType = "query", dataType = "Long"),
            @ApiImplicitParam(name = "pageSize", value = "条数", required = true, paramType = "query", dataType = "Long")
    })
	public ServiceResult advertList(String isShow,String toUrl,String adType,int pageNumber,int pageSize){
		ServiceResult sr = new ServiceResult();
		log.info(isShow+toUrl+adType+pageNumber+pageSize);
		sr = adConfigService.adConfigListWeb(isShow,toUrl,adType,pageNumber,pageSize);
		return sr;
		
	}
	
	
	
	@ApiOperation(value = "后台广告配置列表接口新增", notes = "后台广告配置列表接口新增")
	@PostMapping(value = "/addAdvert")
	public ServiceResult addAdvert(@RequestBody @ApiParam(name = "tbSpAdconfig", value = "tbSpAdconfig", required = true) TbSpAdconfig tbSpAdconfig){
		ServiceResult sr = new ServiceResult();
		try {
			sr = adConfigService.addAdvert(tbSpAdconfig);
		} catch (Throwable e) {
			sr.setResult("字符太长！");
			e.printStackTrace();
		}
		return sr;
	}
	
	
	@ApiOperation(value = "后台广告配置列表接口修改", notes = "后台广告配置列表接口修改")
	@PostMapping(value = "/updateAdvert")
	public ServiceResult updateAdvert(@RequestBody @ApiParam(name = "tbSpAdconfig", value = "tbSpAdconfig", required = true) TbSpAdconfig tbSpAdconfig){
		ServiceResult sr = new ServiceResult();
		sr = adConfigService.updateAdvert(tbSpAdconfig);
		return sr;
		
	}
	
	@ApiOperation(value = "后台广告配置列表接口删除", notes = "后台广告配置列表接口删除")
	@PostMapping(value = "/removeAdvert")
	public ServiceResult removeAdvert(String ids){
		ServiceResult sr = new ServiceResult();
		String[] list = ids.split(",");
		
		sr = adConfigService.removeAdvert(list);
		return sr;
		
	}
	
	@ApiOperation(value = "后台广告配置列表接口", notes = "后台广告配置列表接口")
	@PostMapping(value = "/getAdDetail")
	@ApiImplicitParams(value = {
            @ApiImplicitParam(name = "id", value = "id", required = true, paramType = "query", dataType = "Long")
    })
	public ServiceResult getAdDetail(Integer id){
		ServiceResult sr = new ServiceResult();
		sr = adConfigService.getAdDetail(id);
		return sr;
		
	}
	
	
	@ApiOperation(value = "后台广告配置列表接口", notes = "后台广告配置列表接口")
	@PostMapping(value = "/updateAdDetail")
	@ApiImplicitParams(value = {
        @ApiImplicitParam(name = "id", value = "id", required = true, paramType = "query", dataType = "Long"),
        @ApiImplicitParam(name = "detail", value = "detail", required = true, paramType = "query", dataType = "String")
    })
	public ServiceResult updateAdDetail(Integer id,String detail){
		ServiceResult sr = new ServiceResult();
		sr = adConfigService.updateAdDetail(id,detail);
		return sr;
		
	}
	
}
