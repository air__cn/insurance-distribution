package cn.com.libertymutual.production.model.nomorcldatasource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PrpsagentExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PrpsagentExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andAgentcodeIsNull() {
            addCriterion("AGENTCODE is null");
            return (Criteria) this;
        }

        public Criteria andAgentcodeIsNotNull() {
            addCriterion("AGENTCODE is not null");
            return (Criteria) this;
        }

        public Criteria andAgentcodeEqualTo(String value) {
            addCriterion("AGENTCODE =", value, "agentcode");
            return (Criteria) this;
        }

        public Criteria andAgentcodeNotEqualTo(String value) {
            addCriterion("AGENTCODE <>", value, "agentcode");
            return (Criteria) this;
        }

        public Criteria andAgentcodeGreaterThan(String value) {
            addCriterion("AGENTCODE >", value, "agentcode");
            return (Criteria) this;
        }

        public Criteria andAgentcodeGreaterThanOrEqualTo(String value) {
            addCriterion("AGENTCODE >=", value, "agentcode");
            return (Criteria) this;
        }

        public Criteria andAgentcodeLessThan(String value) {
            addCriterion("AGENTCODE <", value, "agentcode");
            return (Criteria) this;
        }

        public Criteria andAgentcodeLessThanOrEqualTo(String value) {
            addCriterion("AGENTCODE <=", value, "agentcode");
            return (Criteria) this;
        }

        public Criteria andAgentcodeLike(String value) {
            addCriterion("AGENTCODE like", value, "agentcode");
            return (Criteria) this;
        }

        public Criteria andAgentcodeNotLike(String value) {
            addCriterion("AGENTCODE not like", value, "agentcode");
            return (Criteria) this;
        }

        public Criteria andAgentcodeIn(List<String> values) {
            addCriterion("AGENTCODE in", values, "agentcode");
            return (Criteria) this;
        }

        public Criteria andAgentcodeNotIn(List<String> values) {
            addCriterion("AGENTCODE not in", values, "agentcode");
            return (Criteria) this;
        }

        public Criteria andAgentcodeBetween(String value1, String value2) {
            addCriterion("AGENTCODE between", value1, value2, "agentcode");
            return (Criteria) this;
        }

        public Criteria andAgentcodeNotBetween(String value1, String value2) {
            addCriterion("AGENTCODE not between", value1, value2, "agentcode");
            return (Criteria) this;
        }

        public Criteria andAgentnameIsNull() {
            addCriterion("AGENTNAME is null");
            return (Criteria) this;
        }

        public Criteria andAgentnameIsNotNull() {
            addCriterion("AGENTNAME is not null");
            return (Criteria) this;
        }

        public Criteria andAgentnameEqualTo(String value) {
            addCriterion("AGENTNAME =", value, "agentname");
            return (Criteria) this;
        }

        public Criteria andAgentnameNotEqualTo(String value) {
            addCriterion("AGENTNAME <>", value, "agentname");
            return (Criteria) this;
        }

        public Criteria andAgentnameGreaterThan(String value) {
            addCriterion("AGENTNAME >", value, "agentname");
            return (Criteria) this;
        }

        public Criteria andAgentnameGreaterThanOrEqualTo(String value) {
            addCriterion("AGENTNAME >=", value, "agentname");
            return (Criteria) this;
        }

        public Criteria andAgentnameLessThan(String value) {
            addCriterion("AGENTNAME <", value, "agentname");
            return (Criteria) this;
        }

        public Criteria andAgentnameLessThanOrEqualTo(String value) {
            addCriterion("AGENTNAME <=", value, "agentname");
            return (Criteria) this;
        }

        public Criteria andAgentnameLike(String value) {
            addCriterion("AGENTNAME like", value, "agentname");
            return (Criteria) this;
        }

        public Criteria andAgentnameNotLike(String value) {
            addCriterion("AGENTNAME not like", value, "agentname");
            return (Criteria) this;
        }

        public Criteria andAgentnameIn(List<String> values) {
            addCriterion("AGENTNAME in", values, "agentname");
            return (Criteria) this;
        }

        public Criteria andAgentnameNotIn(List<String> values) {
            addCriterion("AGENTNAME not in", values, "agentname");
            return (Criteria) this;
        }

        public Criteria andAgentnameBetween(String value1, String value2) {
            addCriterion("AGENTNAME between", value1, value2, "agentname");
            return (Criteria) this;
        }

        public Criteria andAgentnameNotBetween(String value1, String value2) {
            addCriterion("AGENTNAME not between", value1, value2, "agentname");
            return (Criteria) this;
        }

        public Criteria andAgentenameIsNull() {
            addCriterion("AGENTENAME is null");
            return (Criteria) this;
        }

        public Criteria andAgentenameIsNotNull() {
            addCriterion("AGENTENAME is not null");
            return (Criteria) this;
        }

        public Criteria andAgentenameEqualTo(String value) {
            addCriterion("AGENTENAME =", value, "agentename");
            return (Criteria) this;
        }

        public Criteria andAgentenameNotEqualTo(String value) {
            addCriterion("AGENTENAME <>", value, "agentename");
            return (Criteria) this;
        }

        public Criteria andAgentenameGreaterThan(String value) {
            addCriterion("AGENTENAME >", value, "agentename");
            return (Criteria) this;
        }

        public Criteria andAgentenameGreaterThanOrEqualTo(String value) {
            addCriterion("AGENTENAME >=", value, "agentename");
            return (Criteria) this;
        }

        public Criteria andAgentenameLessThan(String value) {
            addCriterion("AGENTENAME <", value, "agentename");
            return (Criteria) this;
        }

        public Criteria andAgentenameLessThanOrEqualTo(String value) {
            addCriterion("AGENTENAME <=", value, "agentename");
            return (Criteria) this;
        }

        public Criteria andAgentenameLike(String value) {
            addCriterion("AGENTENAME like", value, "agentename");
            return (Criteria) this;
        }

        public Criteria andAgentenameNotLike(String value) {
            addCriterion("AGENTENAME not like", value, "agentename");
            return (Criteria) this;
        }

        public Criteria andAgentenameIn(List<String> values) {
            addCriterion("AGENTENAME in", values, "agentename");
            return (Criteria) this;
        }

        public Criteria andAgentenameNotIn(List<String> values) {
            addCriterion("AGENTENAME not in", values, "agentename");
            return (Criteria) this;
        }

        public Criteria andAgentenameBetween(String value1, String value2) {
            addCriterion("AGENTENAME between", value1, value2, "agentename");
            return (Criteria) this;
        }

        public Criteria andAgentenameNotBetween(String value1, String value2) {
            addCriterion("AGENTENAME not between", value1, value2, "agentename");
            return (Criteria) this;
        }

        public Criteria andAgentshortnameIsNull() {
            addCriterion("AGENTSHORTNAME is null");
            return (Criteria) this;
        }

        public Criteria andAgentshortnameIsNotNull() {
            addCriterion("AGENTSHORTNAME is not null");
            return (Criteria) this;
        }

        public Criteria andAgentshortnameEqualTo(String value) {
            addCriterion("AGENTSHORTNAME =", value, "agentshortname");
            return (Criteria) this;
        }

        public Criteria andAgentshortnameNotEqualTo(String value) {
            addCriterion("AGENTSHORTNAME <>", value, "agentshortname");
            return (Criteria) this;
        }

        public Criteria andAgentshortnameGreaterThan(String value) {
            addCriterion("AGENTSHORTNAME >", value, "agentshortname");
            return (Criteria) this;
        }

        public Criteria andAgentshortnameGreaterThanOrEqualTo(String value) {
            addCriterion("AGENTSHORTNAME >=", value, "agentshortname");
            return (Criteria) this;
        }

        public Criteria andAgentshortnameLessThan(String value) {
            addCriterion("AGENTSHORTNAME <", value, "agentshortname");
            return (Criteria) this;
        }

        public Criteria andAgentshortnameLessThanOrEqualTo(String value) {
            addCriterion("AGENTSHORTNAME <=", value, "agentshortname");
            return (Criteria) this;
        }

        public Criteria andAgentshortnameLike(String value) {
            addCriterion("AGENTSHORTNAME like", value, "agentshortname");
            return (Criteria) this;
        }

        public Criteria andAgentshortnameNotLike(String value) {
            addCriterion("AGENTSHORTNAME not like", value, "agentshortname");
            return (Criteria) this;
        }

        public Criteria andAgentshortnameIn(List<String> values) {
            addCriterion("AGENTSHORTNAME in", values, "agentshortname");
            return (Criteria) this;
        }

        public Criteria andAgentshortnameNotIn(List<String> values) {
            addCriterion("AGENTSHORTNAME not in", values, "agentshortname");
            return (Criteria) this;
        }

        public Criteria andAgentshortnameBetween(String value1, String value2) {
            addCriterion("AGENTSHORTNAME between", value1, value2, "agentshortname");
            return (Criteria) this;
        }

        public Criteria andAgentshortnameNotBetween(String value1, String value2) {
            addCriterion("AGENTSHORTNAME not between", value1, value2, "agentshortname");
            return (Criteria) this;
        }

        public Criteria andPostcodeIsNull() {
            addCriterion("POSTCODE is null");
            return (Criteria) this;
        }

        public Criteria andPostcodeIsNotNull() {
            addCriterion("POSTCODE is not null");
            return (Criteria) this;
        }

        public Criteria andPostcodeEqualTo(String value) {
            addCriterion("POSTCODE =", value, "postcode");
            return (Criteria) this;
        }

        public Criteria andPostcodeNotEqualTo(String value) {
            addCriterion("POSTCODE <>", value, "postcode");
            return (Criteria) this;
        }

        public Criteria andPostcodeGreaterThan(String value) {
            addCriterion("POSTCODE >", value, "postcode");
            return (Criteria) this;
        }

        public Criteria andPostcodeGreaterThanOrEqualTo(String value) {
            addCriterion("POSTCODE >=", value, "postcode");
            return (Criteria) this;
        }

        public Criteria andPostcodeLessThan(String value) {
            addCriterion("POSTCODE <", value, "postcode");
            return (Criteria) this;
        }

        public Criteria andPostcodeLessThanOrEqualTo(String value) {
            addCriterion("POSTCODE <=", value, "postcode");
            return (Criteria) this;
        }

        public Criteria andPostcodeLike(String value) {
            addCriterion("POSTCODE like", value, "postcode");
            return (Criteria) this;
        }

        public Criteria andPostcodeNotLike(String value) {
            addCriterion("POSTCODE not like", value, "postcode");
            return (Criteria) this;
        }

        public Criteria andPostcodeIn(List<String> values) {
            addCriterion("POSTCODE in", values, "postcode");
            return (Criteria) this;
        }

        public Criteria andPostcodeNotIn(List<String> values) {
            addCriterion("POSTCODE not in", values, "postcode");
            return (Criteria) this;
        }

        public Criteria andPostcodeBetween(String value1, String value2) {
            addCriterion("POSTCODE between", value1, value2, "postcode");
            return (Criteria) this;
        }

        public Criteria andPostcodeNotBetween(String value1, String value2) {
            addCriterion("POSTCODE not between", value1, value2, "postcode");
            return (Criteria) this;
        }

        public Criteria andAgenttypeIsNull() {
            addCriterion("AGENTTYPE is null");
            return (Criteria) this;
        }

        public Criteria andAgenttypeIsNotNull() {
            addCriterion("AGENTTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andAgenttypeEqualTo(String value) {
            addCriterion("AGENTTYPE =", value, "agenttype");
            return (Criteria) this;
        }

        public Criteria andAgenttypeNotEqualTo(String value) {
            addCriterion("AGENTTYPE <>", value, "agenttype");
            return (Criteria) this;
        }

        public Criteria andAgenttypeGreaterThan(String value) {
            addCriterion("AGENTTYPE >", value, "agenttype");
            return (Criteria) this;
        }

        public Criteria andAgenttypeGreaterThanOrEqualTo(String value) {
            addCriterion("AGENTTYPE >=", value, "agenttype");
            return (Criteria) this;
        }

        public Criteria andAgenttypeLessThan(String value) {
            addCriterion("AGENTTYPE <", value, "agenttype");
            return (Criteria) this;
        }

        public Criteria andAgenttypeLessThanOrEqualTo(String value) {
            addCriterion("AGENTTYPE <=", value, "agenttype");
            return (Criteria) this;
        }

        public Criteria andAgenttypeLike(String value) {
            addCriterion("AGENTTYPE like", value, "agenttype");
            return (Criteria) this;
        }

        public Criteria andAgenttypeNotLike(String value) {
            addCriterion("AGENTTYPE not like", value, "agenttype");
            return (Criteria) this;
        }

        public Criteria andAgenttypeIn(List<String> values) {
            addCriterion("AGENTTYPE in", values, "agenttype");
            return (Criteria) this;
        }

        public Criteria andAgenttypeNotIn(List<String> values) {
            addCriterion("AGENTTYPE not in", values, "agenttype");
            return (Criteria) this;
        }

        public Criteria andAgenttypeBetween(String value1, String value2) {
            addCriterion("AGENTTYPE between", value1, value2, "agenttype");
            return (Criteria) this;
        }

        public Criteria andAgenttypeNotBetween(String value1, String value2) {
            addCriterion("AGENTTYPE not between", value1, value2, "agenttype");
            return (Criteria) this;
        }

        public Criteria andIdentifynumberIsNull() {
            addCriterion("IDENTIFYNUMBER is null");
            return (Criteria) this;
        }

        public Criteria andIdentifynumberIsNotNull() {
            addCriterion("IDENTIFYNUMBER is not null");
            return (Criteria) this;
        }

        public Criteria andIdentifynumberEqualTo(String value) {
            addCriterion("IDENTIFYNUMBER =", value, "identifynumber");
            return (Criteria) this;
        }

        public Criteria andIdentifynumberNotEqualTo(String value) {
            addCriterion("IDENTIFYNUMBER <>", value, "identifynumber");
            return (Criteria) this;
        }

        public Criteria andIdentifynumberGreaterThan(String value) {
            addCriterion("IDENTIFYNUMBER >", value, "identifynumber");
            return (Criteria) this;
        }

        public Criteria andIdentifynumberGreaterThanOrEqualTo(String value) {
            addCriterion("IDENTIFYNUMBER >=", value, "identifynumber");
            return (Criteria) this;
        }

        public Criteria andIdentifynumberLessThan(String value) {
            addCriterion("IDENTIFYNUMBER <", value, "identifynumber");
            return (Criteria) this;
        }

        public Criteria andIdentifynumberLessThanOrEqualTo(String value) {
            addCriterion("IDENTIFYNUMBER <=", value, "identifynumber");
            return (Criteria) this;
        }

        public Criteria andIdentifynumberLike(String value) {
            addCriterion("IDENTIFYNUMBER like", value, "identifynumber");
            return (Criteria) this;
        }

        public Criteria andIdentifynumberNotLike(String value) {
            addCriterion("IDENTIFYNUMBER not like", value, "identifynumber");
            return (Criteria) this;
        }

        public Criteria andIdentifynumberIn(List<String> values) {
            addCriterion("IDENTIFYNUMBER in", values, "identifynumber");
            return (Criteria) this;
        }

        public Criteria andIdentifynumberNotIn(List<String> values) {
            addCriterion("IDENTIFYNUMBER not in", values, "identifynumber");
            return (Criteria) this;
        }

        public Criteria andIdentifynumberBetween(String value1, String value2) {
            addCriterion("IDENTIFYNUMBER between", value1, value2, "identifynumber");
            return (Criteria) this;
        }

        public Criteria andIdentifynumberNotBetween(String value1, String value2) {
            addCriterion("IDENTIFYNUMBER not between", value1, value2, "identifynumber");
            return (Criteria) this;
        }

        public Criteria andIdentifytypeIsNull() {
            addCriterion("IDENTIFYTYPE is null");
            return (Criteria) this;
        }

        public Criteria andIdentifytypeIsNotNull() {
            addCriterion("IDENTIFYTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andIdentifytypeEqualTo(String value) {
            addCriterion("IDENTIFYTYPE =", value, "identifytype");
            return (Criteria) this;
        }

        public Criteria andIdentifytypeNotEqualTo(String value) {
            addCriterion("IDENTIFYTYPE <>", value, "identifytype");
            return (Criteria) this;
        }

        public Criteria andIdentifytypeGreaterThan(String value) {
            addCriterion("IDENTIFYTYPE >", value, "identifytype");
            return (Criteria) this;
        }

        public Criteria andIdentifytypeGreaterThanOrEqualTo(String value) {
            addCriterion("IDENTIFYTYPE >=", value, "identifytype");
            return (Criteria) this;
        }

        public Criteria andIdentifytypeLessThan(String value) {
            addCriterion("IDENTIFYTYPE <", value, "identifytype");
            return (Criteria) this;
        }

        public Criteria andIdentifytypeLessThanOrEqualTo(String value) {
            addCriterion("IDENTIFYTYPE <=", value, "identifytype");
            return (Criteria) this;
        }

        public Criteria andIdentifytypeLike(String value) {
            addCriterion("IDENTIFYTYPE like", value, "identifytype");
            return (Criteria) this;
        }

        public Criteria andIdentifytypeNotLike(String value) {
            addCriterion("IDENTIFYTYPE not like", value, "identifytype");
            return (Criteria) this;
        }

        public Criteria andIdentifytypeIn(List<String> values) {
            addCriterion("IDENTIFYTYPE in", values, "identifytype");
            return (Criteria) this;
        }

        public Criteria andIdentifytypeNotIn(List<String> values) {
            addCriterion("IDENTIFYTYPE not in", values, "identifytype");
            return (Criteria) this;
        }

        public Criteria andIdentifytypeBetween(String value1, String value2) {
            addCriterion("IDENTIFYTYPE between", value1, value2, "identifytype");
            return (Criteria) this;
        }

        public Criteria andIdentifytypeNotBetween(String value1, String value2) {
            addCriterion("IDENTIFYTYPE not between", value1, value2, "identifytype");
            return (Criteria) this;
        }

        public Criteria andPermitflagIsNull() {
            addCriterion("PERMITFLAG is null");
            return (Criteria) this;
        }

        public Criteria andPermitflagIsNotNull() {
            addCriterion("PERMITFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andPermitflagEqualTo(String value) {
            addCriterion("PERMITFLAG =", value, "permitflag");
            return (Criteria) this;
        }

        public Criteria andPermitflagNotEqualTo(String value) {
            addCriterion("PERMITFLAG <>", value, "permitflag");
            return (Criteria) this;
        }

        public Criteria andPermitflagGreaterThan(String value) {
            addCriterion("PERMITFLAG >", value, "permitflag");
            return (Criteria) this;
        }

        public Criteria andPermitflagGreaterThanOrEqualTo(String value) {
            addCriterion("PERMITFLAG >=", value, "permitflag");
            return (Criteria) this;
        }

        public Criteria andPermitflagLessThan(String value) {
            addCriterion("PERMITFLAG <", value, "permitflag");
            return (Criteria) this;
        }

        public Criteria andPermitflagLessThanOrEqualTo(String value) {
            addCriterion("PERMITFLAG <=", value, "permitflag");
            return (Criteria) this;
        }

        public Criteria andPermitflagLike(String value) {
            addCriterion("PERMITFLAG like", value, "permitflag");
            return (Criteria) this;
        }

        public Criteria andPermitflagNotLike(String value) {
            addCriterion("PERMITFLAG not like", value, "permitflag");
            return (Criteria) this;
        }

        public Criteria andPermitflagIn(List<String> values) {
            addCriterion("PERMITFLAG in", values, "permitflag");
            return (Criteria) this;
        }

        public Criteria andPermitflagNotIn(List<String> values) {
            addCriterion("PERMITFLAG not in", values, "permitflag");
            return (Criteria) this;
        }

        public Criteria andPermitflagBetween(String value1, String value2) {
            addCriterion("PERMITFLAG between", value1, value2, "permitflag");
            return (Criteria) this;
        }

        public Criteria andPermitflagNotBetween(String value1, String value2) {
            addCriterion("PERMITFLAG not between", value1, value2, "permitflag");
            return (Criteria) this;
        }

        public Criteria andPermittypeIsNull() {
            addCriterion("PERMITTYPE is null");
            return (Criteria) this;
        }

        public Criteria andPermittypeIsNotNull() {
            addCriterion("PERMITTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andPermittypeEqualTo(String value) {
            addCriterion("PERMITTYPE =", value, "permittype");
            return (Criteria) this;
        }

        public Criteria andPermittypeNotEqualTo(String value) {
            addCriterion("PERMITTYPE <>", value, "permittype");
            return (Criteria) this;
        }

        public Criteria andPermittypeGreaterThan(String value) {
            addCriterion("PERMITTYPE >", value, "permittype");
            return (Criteria) this;
        }

        public Criteria andPermittypeGreaterThanOrEqualTo(String value) {
            addCriterion("PERMITTYPE >=", value, "permittype");
            return (Criteria) this;
        }

        public Criteria andPermittypeLessThan(String value) {
            addCriterion("PERMITTYPE <", value, "permittype");
            return (Criteria) this;
        }

        public Criteria andPermittypeLessThanOrEqualTo(String value) {
            addCriterion("PERMITTYPE <=", value, "permittype");
            return (Criteria) this;
        }

        public Criteria andPermittypeLike(String value) {
            addCriterion("PERMITTYPE like", value, "permittype");
            return (Criteria) this;
        }

        public Criteria andPermittypeNotLike(String value) {
            addCriterion("PERMITTYPE not like", value, "permittype");
            return (Criteria) this;
        }

        public Criteria andPermittypeIn(List<String> values) {
            addCriterion("PERMITTYPE in", values, "permittype");
            return (Criteria) this;
        }

        public Criteria andPermittypeNotIn(List<String> values) {
            addCriterion("PERMITTYPE not in", values, "permittype");
            return (Criteria) this;
        }

        public Criteria andPermittypeBetween(String value1, String value2) {
            addCriterion("PERMITTYPE between", value1, value2, "permittype");
            return (Criteria) this;
        }

        public Criteria andPermittypeNotBetween(String value1, String value2) {
            addCriterion("PERMITTYPE not between", value1, value2, "permittype");
            return (Criteria) this;
        }

        public Criteria andPermitnoIsNull() {
            addCriterion("PERMITNO is null");
            return (Criteria) this;
        }

        public Criteria andPermitnoIsNotNull() {
            addCriterion("PERMITNO is not null");
            return (Criteria) this;
        }

        public Criteria andPermitnoEqualTo(String value) {
            addCriterion("PERMITNO =", value, "permitno");
            return (Criteria) this;
        }

        public Criteria andPermitnoNotEqualTo(String value) {
            addCriterion("PERMITNO <>", value, "permitno");
            return (Criteria) this;
        }

        public Criteria andPermitnoGreaterThan(String value) {
            addCriterion("PERMITNO >", value, "permitno");
            return (Criteria) this;
        }

        public Criteria andPermitnoGreaterThanOrEqualTo(String value) {
            addCriterion("PERMITNO >=", value, "permitno");
            return (Criteria) this;
        }

        public Criteria andPermitnoLessThan(String value) {
            addCriterion("PERMITNO <", value, "permitno");
            return (Criteria) this;
        }

        public Criteria andPermitnoLessThanOrEqualTo(String value) {
            addCriterion("PERMITNO <=", value, "permitno");
            return (Criteria) this;
        }

        public Criteria andPermitnoLike(String value) {
            addCriterion("PERMITNO like", value, "permitno");
            return (Criteria) this;
        }

        public Criteria andPermitnoNotLike(String value) {
            addCriterion("PERMITNO not like", value, "permitno");
            return (Criteria) this;
        }

        public Criteria andPermitnoIn(List<String> values) {
            addCriterion("PERMITNO in", values, "permitno");
            return (Criteria) this;
        }

        public Criteria andPermitnoNotIn(List<String> values) {
            addCriterion("PERMITNO not in", values, "permitno");
            return (Criteria) this;
        }

        public Criteria andPermitnoBetween(String value1, String value2) {
            addCriterion("PERMITNO between", value1, value2, "permitno");
            return (Criteria) this;
        }

        public Criteria andPermitnoNotBetween(String value1, String value2) {
            addCriterion("PERMITNO not between", value1, value2, "permitno");
            return (Criteria) this;
        }

        public Criteria andCurrentstateIsNull() {
            addCriterion("CURRENTSTATE is null");
            return (Criteria) this;
        }

        public Criteria andCurrentstateIsNotNull() {
            addCriterion("CURRENTSTATE is not null");
            return (Criteria) this;
        }

        public Criteria andCurrentstateEqualTo(String value) {
            addCriterion("CURRENTSTATE =", value, "currentstate");
            return (Criteria) this;
        }

        public Criteria andCurrentstateNotEqualTo(String value) {
            addCriterion("CURRENTSTATE <>", value, "currentstate");
            return (Criteria) this;
        }

        public Criteria andCurrentstateGreaterThan(String value) {
            addCriterion("CURRENTSTATE >", value, "currentstate");
            return (Criteria) this;
        }

        public Criteria andCurrentstateGreaterThanOrEqualTo(String value) {
            addCriterion("CURRENTSTATE >=", value, "currentstate");
            return (Criteria) this;
        }

        public Criteria andCurrentstateLessThan(String value) {
            addCriterion("CURRENTSTATE <", value, "currentstate");
            return (Criteria) this;
        }

        public Criteria andCurrentstateLessThanOrEqualTo(String value) {
            addCriterion("CURRENTSTATE <=", value, "currentstate");
            return (Criteria) this;
        }

        public Criteria andCurrentstateLike(String value) {
            addCriterion("CURRENTSTATE like", value, "currentstate");
            return (Criteria) this;
        }

        public Criteria andCurrentstateNotLike(String value) {
            addCriterion("CURRENTSTATE not like", value, "currentstate");
            return (Criteria) this;
        }

        public Criteria andCurrentstateIn(List<String> values) {
            addCriterion("CURRENTSTATE in", values, "currentstate");
            return (Criteria) this;
        }

        public Criteria andCurrentstateNotIn(List<String> values) {
            addCriterion("CURRENTSTATE not in", values, "currentstate");
            return (Criteria) this;
        }

        public Criteria andCurrentstateBetween(String value1, String value2) {
            addCriterion("CURRENTSTATE between", value1, value2, "currentstate");
            return (Criteria) this;
        }

        public Criteria andCurrentstateNotBetween(String value1, String value2) {
            addCriterion("CURRENTSTATE not between", value1, value2, "currentstate");
            return (Criteria) this;
        }

        public Criteria andBargaindateIsNull() {
            addCriterion("BARGAINDATE is null");
            return (Criteria) this;
        }

        public Criteria andBargaindateIsNotNull() {
            addCriterion("BARGAINDATE is not null");
            return (Criteria) this;
        }

        public Criteria andBargaindateEqualTo(Date value) {
            addCriterion("BARGAINDATE =", value, "bargaindate");
            return (Criteria) this;
        }

        public Criteria andBargaindateNotEqualTo(Date value) {
            addCriterion("BARGAINDATE <>", value, "bargaindate");
            return (Criteria) this;
        }

        public Criteria andBargaindateGreaterThan(Date value) {
            addCriterion("BARGAINDATE >", value, "bargaindate");
            return (Criteria) this;
        }

        public Criteria andBargaindateGreaterThanOrEqualTo(Date value) {
            addCriterion("BARGAINDATE >=", value, "bargaindate");
            return (Criteria) this;
        }

        public Criteria andBargaindateLessThan(Date value) {
            addCriterion("BARGAINDATE <", value, "bargaindate");
            return (Criteria) this;
        }

        public Criteria andBargaindateLessThanOrEqualTo(Date value) {
            addCriterion("BARGAINDATE <=", value, "bargaindate");
            return (Criteria) this;
        }

        public Criteria andBargaindateIn(List<Date> values) {
            addCriterion("BARGAINDATE in", values, "bargaindate");
            return (Criteria) this;
        }

        public Criteria andBargaindateNotIn(List<Date> values) {
            addCriterion("BARGAINDATE not in", values, "bargaindate");
            return (Criteria) this;
        }

        public Criteria andBargaindateBetween(Date value1, Date value2) {
            addCriterion("BARGAINDATE between", value1, value2, "bargaindate");
            return (Criteria) this;
        }

        public Criteria andBargaindateNotBetween(Date value1, Date value2) {
            addCriterion("BARGAINDATE not between", value1, value2, "bargaindate");
            return (Criteria) this;
        }

        public Criteria andTerminatedateIsNull() {
            addCriterion("TERMINATEDATE is null");
            return (Criteria) this;
        }

        public Criteria andTerminatedateIsNotNull() {
            addCriterion("TERMINATEDATE is not null");
            return (Criteria) this;
        }

        public Criteria andTerminatedateEqualTo(Date value) {
            addCriterion("TERMINATEDATE =", value, "terminatedate");
            return (Criteria) this;
        }

        public Criteria andTerminatedateNotEqualTo(Date value) {
            addCriterion("TERMINATEDATE <>", value, "terminatedate");
            return (Criteria) this;
        }

        public Criteria andTerminatedateGreaterThan(Date value) {
            addCriterion("TERMINATEDATE >", value, "terminatedate");
            return (Criteria) this;
        }

        public Criteria andTerminatedateGreaterThanOrEqualTo(Date value) {
            addCriterion("TERMINATEDATE >=", value, "terminatedate");
            return (Criteria) this;
        }

        public Criteria andTerminatedateLessThan(Date value) {
            addCriterion("TERMINATEDATE <", value, "terminatedate");
            return (Criteria) this;
        }

        public Criteria andTerminatedateLessThanOrEqualTo(Date value) {
            addCriterion("TERMINATEDATE <=", value, "terminatedate");
            return (Criteria) this;
        }

        public Criteria andTerminatedateIn(List<Date> values) {
            addCriterion("TERMINATEDATE in", values, "terminatedate");
            return (Criteria) this;
        }

        public Criteria andTerminatedateNotIn(List<Date> values) {
            addCriterion("TERMINATEDATE not in", values, "terminatedate");
            return (Criteria) this;
        }

        public Criteria andTerminatedateBetween(Date value1, Date value2) {
            addCriterion("TERMINATEDATE between", value1, value2, "terminatedate");
            return (Criteria) this;
        }

        public Criteria andTerminatedateNotBetween(Date value1, Date value2) {
            addCriterion("TERMINATEDATE not between", value1, value2, "terminatedate");
            return (Criteria) this;
        }

        public Criteria andClasscodeIsNull() {
            addCriterion("CLASSCODE is null");
            return (Criteria) this;
        }

        public Criteria andClasscodeIsNotNull() {
            addCriterion("CLASSCODE is not null");
            return (Criteria) this;
        }

        public Criteria andClasscodeEqualTo(String value) {
            addCriterion("CLASSCODE =", value, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeNotEqualTo(String value) {
            addCriterion("CLASSCODE <>", value, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeGreaterThan(String value) {
            addCriterion("CLASSCODE >", value, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeGreaterThanOrEqualTo(String value) {
            addCriterion("CLASSCODE >=", value, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeLessThan(String value) {
            addCriterion("CLASSCODE <", value, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeLessThanOrEqualTo(String value) {
            addCriterion("CLASSCODE <=", value, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeLike(String value) {
            addCriterion("CLASSCODE like", value, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeNotLike(String value) {
            addCriterion("CLASSCODE not like", value, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeIn(List<String> values) {
            addCriterion("CLASSCODE in", values, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeNotIn(List<String> values) {
            addCriterion("CLASSCODE not in", values, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeBetween(String value1, String value2) {
            addCriterion("CLASSCODE between", value1, value2, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeNotBetween(String value1, String value2) {
            addCriterion("CLASSCODE not between", value1, value2, "classcode");
            return (Criteria) this;
        }

        public Criteria andComcodeIsNull() {
            addCriterion("COMCODE is null");
            return (Criteria) this;
        }

        public Criteria andComcodeIsNotNull() {
            addCriterion("COMCODE is not null");
            return (Criteria) this;
        }

        public Criteria andComcodeEqualTo(String value) {
            addCriterion("COMCODE =", value, "comcode");
            return (Criteria) this;
        }

        public Criteria andComcodeNotEqualTo(String value) {
            addCriterion("COMCODE <>", value, "comcode");
            return (Criteria) this;
        }

        public Criteria andComcodeGreaterThan(String value) {
            addCriterion("COMCODE >", value, "comcode");
            return (Criteria) this;
        }

        public Criteria andComcodeGreaterThanOrEqualTo(String value) {
            addCriterion("COMCODE >=", value, "comcode");
            return (Criteria) this;
        }

        public Criteria andComcodeLessThan(String value) {
            addCriterion("COMCODE <", value, "comcode");
            return (Criteria) this;
        }

        public Criteria andComcodeLessThanOrEqualTo(String value) {
            addCriterion("COMCODE <=", value, "comcode");
            return (Criteria) this;
        }

        public Criteria andComcodeLike(String value) {
            addCriterion("COMCODE like", value, "comcode");
            return (Criteria) this;
        }

        public Criteria andComcodeNotLike(String value) {
            addCriterion("COMCODE not like", value, "comcode");
            return (Criteria) this;
        }

        public Criteria andComcodeIn(List<String> values) {
            addCriterion("COMCODE in", values, "comcode");
            return (Criteria) this;
        }

        public Criteria andComcodeNotIn(List<String> values) {
            addCriterion("COMCODE not in", values, "comcode");
            return (Criteria) this;
        }

        public Criteria andComcodeBetween(String value1, String value2) {
            addCriterion("COMCODE between", value1, value2, "comcode");
            return (Criteria) this;
        }

        public Criteria andComcodeNotBetween(String value1, String value2) {
            addCriterion("COMCODE not between", value1, value2, "comcode");
            return (Criteria) this;
        }

        public Criteria andHandlercodeIsNull() {
            addCriterion("HANDLERCODE is null");
            return (Criteria) this;
        }

        public Criteria andHandlercodeIsNotNull() {
            addCriterion("HANDLERCODE is not null");
            return (Criteria) this;
        }

        public Criteria andHandlercodeEqualTo(String value) {
            addCriterion("HANDLERCODE =", value, "handlercode");
            return (Criteria) this;
        }

        public Criteria andHandlercodeNotEqualTo(String value) {
            addCriterion("HANDLERCODE <>", value, "handlercode");
            return (Criteria) this;
        }

        public Criteria andHandlercodeGreaterThan(String value) {
            addCriterion("HANDLERCODE >", value, "handlercode");
            return (Criteria) this;
        }

        public Criteria andHandlercodeGreaterThanOrEqualTo(String value) {
            addCriterion("HANDLERCODE >=", value, "handlercode");
            return (Criteria) this;
        }

        public Criteria andHandlercodeLessThan(String value) {
            addCriterion("HANDLERCODE <", value, "handlercode");
            return (Criteria) this;
        }

        public Criteria andHandlercodeLessThanOrEqualTo(String value) {
            addCriterion("HANDLERCODE <=", value, "handlercode");
            return (Criteria) this;
        }

        public Criteria andHandlercodeLike(String value) {
            addCriterion("HANDLERCODE like", value, "handlercode");
            return (Criteria) this;
        }

        public Criteria andHandlercodeNotLike(String value) {
            addCriterion("HANDLERCODE not like", value, "handlercode");
            return (Criteria) this;
        }

        public Criteria andHandlercodeIn(List<String> values) {
            addCriterion("HANDLERCODE in", values, "handlercode");
            return (Criteria) this;
        }

        public Criteria andHandlercodeNotIn(List<String> values) {
            addCriterion("HANDLERCODE not in", values, "handlercode");
            return (Criteria) this;
        }

        public Criteria andHandlercodeBetween(String value1, String value2) {
            addCriterion("HANDLERCODE between", value1, value2, "handlercode");
            return (Criteria) this;
        }

        public Criteria andHandlercodeNotBetween(String value1, String value2) {
            addCriterion("HANDLERCODE not between", value1, value2, "handlercode");
            return (Criteria) this;
        }

        public Criteria andTeamcodeIsNull() {
            addCriterion("TEAMCODE is null");
            return (Criteria) this;
        }

        public Criteria andTeamcodeIsNotNull() {
            addCriterion("TEAMCODE is not null");
            return (Criteria) this;
        }

        public Criteria andTeamcodeEqualTo(String value) {
            addCriterion("TEAMCODE =", value, "teamcode");
            return (Criteria) this;
        }

        public Criteria andTeamcodeNotEqualTo(String value) {
            addCriterion("TEAMCODE <>", value, "teamcode");
            return (Criteria) this;
        }

        public Criteria andTeamcodeGreaterThan(String value) {
            addCriterion("TEAMCODE >", value, "teamcode");
            return (Criteria) this;
        }

        public Criteria andTeamcodeGreaterThanOrEqualTo(String value) {
            addCriterion("TEAMCODE >=", value, "teamcode");
            return (Criteria) this;
        }

        public Criteria andTeamcodeLessThan(String value) {
            addCriterion("TEAMCODE <", value, "teamcode");
            return (Criteria) this;
        }

        public Criteria andTeamcodeLessThanOrEqualTo(String value) {
            addCriterion("TEAMCODE <=", value, "teamcode");
            return (Criteria) this;
        }

        public Criteria andTeamcodeLike(String value) {
            addCriterion("TEAMCODE like", value, "teamcode");
            return (Criteria) this;
        }

        public Criteria andTeamcodeNotLike(String value) {
            addCriterion("TEAMCODE not like", value, "teamcode");
            return (Criteria) this;
        }

        public Criteria andTeamcodeIn(List<String> values) {
            addCriterion("TEAMCODE in", values, "teamcode");
            return (Criteria) this;
        }

        public Criteria andTeamcodeNotIn(List<String> values) {
            addCriterion("TEAMCODE not in", values, "teamcode");
            return (Criteria) this;
        }

        public Criteria andTeamcodeBetween(String value1, String value2) {
            addCriterion("TEAMCODE between", value1, value2, "teamcode");
            return (Criteria) this;
        }

        public Criteria andTeamcodeNotBetween(String value1, String value2) {
            addCriterion("TEAMCODE not between", value1, value2, "teamcode");
            return (Criteria) this;
        }

        public Criteria andUpperagentcodeIsNull() {
            addCriterion("UPPERAGENTCODE is null");
            return (Criteria) this;
        }

        public Criteria andUpperagentcodeIsNotNull() {
            addCriterion("UPPERAGENTCODE is not null");
            return (Criteria) this;
        }

        public Criteria andUpperagentcodeEqualTo(String value) {
            addCriterion("UPPERAGENTCODE =", value, "upperagentcode");
            return (Criteria) this;
        }

        public Criteria andUpperagentcodeNotEqualTo(String value) {
            addCriterion("UPPERAGENTCODE <>", value, "upperagentcode");
            return (Criteria) this;
        }

        public Criteria andUpperagentcodeGreaterThan(String value) {
            addCriterion("UPPERAGENTCODE >", value, "upperagentcode");
            return (Criteria) this;
        }

        public Criteria andUpperagentcodeGreaterThanOrEqualTo(String value) {
            addCriterion("UPPERAGENTCODE >=", value, "upperagentcode");
            return (Criteria) this;
        }

        public Criteria andUpperagentcodeLessThan(String value) {
            addCriterion("UPPERAGENTCODE <", value, "upperagentcode");
            return (Criteria) this;
        }

        public Criteria andUpperagentcodeLessThanOrEqualTo(String value) {
            addCriterion("UPPERAGENTCODE <=", value, "upperagentcode");
            return (Criteria) this;
        }

        public Criteria andUpperagentcodeLike(String value) {
            addCriterion("UPPERAGENTCODE like", value, "upperagentcode");
            return (Criteria) this;
        }

        public Criteria andUpperagentcodeNotLike(String value) {
            addCriterion("UPPERAGENTCODE not like", value, "upperagentcode");
            return (Criteria) this;
        }

        public Criteria andUpperagentcodeIn(List<String> values) {
            addCriterion("UPPERAGENTCODE in", values, "upperagentcode");
            return (Criteria) this;
        }

        public Criteria andUpperagentcodeNotIn(List<String> values) {
            addCriterion("UPPERAGENTCODE not in", values, "upperagentcode");
            return (Criteria) this;
        }

        public Criteria andUpperagentcodeBetween(String value1, String value2) {
            addCriterion("UPPERAGENTCODE between", value1, value2, "upperagentcode");
            return (Criteria) this;
        }

        public Criteria andUpperagentcodeNotBetween(String value1, String value2) {
            addCriterion("UPPERAGENTCODE not between", value1, value2, "upperagentcode");
            return (Criteria) this;
        }

        public Criteria andNewagentcodeIsNull() {
            addCriterion("NEWAGENTCODE is null");
            return (Criteria) this;
        }

        public Criteria andNewagentcodeIsNotNull() {
            addCriterion("NEWAGENTCODE is not null");
            return (Criteria) this;
        }

        public Criteria andNewagentcodeEqualTo(String value) {
            addCriterion("NEWAGENTCODE =", value, "newagentcode");
            return (Criteria) this;
        }

        public Criteria andNewagentcodeNotEqualTo(String value) {
            addCriterion("NEWAGENTCODE <>", value, "newagentcode");
            return (Criteria) this;
        }

        public Criteria andNewagentcodeGreaterThan(String value) {
            addCriterion("NEWAGENTCODE >", value, "newagentcode");
            return (Criteria) this;
        }

        public Criteria andNewagentcodeGreaterThanOrEqualTo(String value) {
            addCriterion("NEWAGENTCODE >=", value, "newagentcode");
            return (Criteria) this;
        }

        public Criteria andNewagentcodeLessThan(String value) {
            addCriterion("NEWAGENTCODE <", value, "newagentcode");
            return (Criteria) this;
        }

        public Criteria andNewagentcodeLessThanOrEqualTo(String value) {
            addCriterion("NEWAGENTCODE <=", value, "newagentcode");
            return (Criteria) this;
        }

        public Criteria andNewagentcodeLike(String value) {
            addCriterion("NEWAGENTCODE like", value, "newagentcode");
            return (Criteria) this;
        }

        public Criteria andNewagentcodeNotLike(String value) {
            addCriterion("NEWAGENTCODE not like", value, "newagentcode");
            return (Criteria) this;
        }

        public Criteria andNewagentcodeIn(List<String> values) {
            addCriterion("NEWAGENTCODE in", values, "newagentcode");
            return (Criteria) this;
        }

        public Criteria andNewagentcodeNotIn(List<String> values) {
            addCriterion("NEWAGENTCODE not in", values, "newagentcode");
            return (Criteria) this;
        }

        public Criteria andNewagentcodeBetween(String value1, String value2) {
            addCriterion("NEWAGENTCODE between", value1, value2, "newagentcode");
            return (Criteria) this;
        }

        public Criteria andNewagentcodeNotBetween(String value1, String value2) {
            addCriterion("NEWAGENTCODE not between", value1, value2, "newagentcode");
            return (Criteria) this;
        }

        public Criteria andLinkernameIsNull() {
            addCriterion("LINKERNAME is null");
            return (Criteria) this;
        }

        public Criteria andLinkernameIsNotNull() {
            addCriterion("LINKERNAME is not null");
            return (Criteria) this;
        }

        public Criteria andLinkernameEqualTo(String value) {
            addCriterion("LINKERNAME =", value, "linkername");
            return (Criteria) this;
        }

        public Criteria andLinkernameNotEqualTo(String value) {
            addCriterion("LINKERNAME <>", value, "linkername");
            return (Criteria) this;
        }

        public Criteria andLinkernameGreaterThan(String value) {
            addCriterion("LINKERNAME >", value, "linkername");
            return (Criteria) this;
        }

        public Criteria andLinkernameGreaterThanOrEqualTo(String value) {
            addCriterion("LINKERNAME >=", value, "linkername");
            return (Criteria) this;
        }

        public Criteria andLinkernameLessThan(String value) {
            addCriterion("LINKERNAME <", value, "linkername");
            return (Criteria) this;
        }

        public Criteria andLinkernameLessThanOrEqualTo(String value) {
            addCriterion("LINKERNAME <=", value, "linkername");
            return (Criteria) this;
        }

        public Criteria andLinkernameLike(String value) {
            addCriterion("LINKERNAME like", value, "linkername");
            return (Criteria) this;
        }

        public Criteria andLinkernameNotLike(String value) {
            addCriterion("LINKERNAME not like", value, "linkername");
            return (Criteria) this;
        }

        public Criteria andLinkernameIn(List<String> values) {
            addCriterion("LINKERNAME in", values, "linkername");
            return (Criteria) this;
        }

        public Criteria andLinkernameNotIn(List<String> values) {
            addCriterion("LINKERNAME not in", values, "linkername");
            return (Criteria) this;
        }

        public Criteria andLinkernameBetween(String value1, String value2) {
            addCriterion("LINKERNAME between", value1, value2, "linkername");
            return (Criteria) this;
        }

        public Criteria andLinkernameNotBetween(String value1, String value2) {
            addCriterion("LINKERNAME not between", value1, value2, "linkername");
            return (Criteria) this;
        }

        public Criteria andLinkernumberIsNull() {
            addCriterion("LINKERNUMBER is null");
            return (Criteria) this;
        }

        public Criteria andLinkernumberIsNotNull() {
            addCriterion("LINKERNUMBER is not null");
            return (Criteria) this;
        }

        public Criteria andLinkernumberEqualTo(String value) {
            addCriterion("LINKERNUMBER =", value, "linkernumber");
            return (Criteria) this;
        }

        public Criteria andLinkernumberNotEqualTo(String value) {
            addCriterion("LINKERNUMBER <>", value, "linkernumber");
            return (Criteria) this;
        }

        public Criteria andLinkernumberGreaterThan(String value) {
            addCriterion("LINKERNUMBER >", value, "linkernumber");
            return (Criteria) this;
        }

        public Criteria andLinkernumberGreaterThanOrEqualTo(String value) {
            addCriterion("LINKERNUMBER >=", value, "linkernumber");
            return (Criteria) this;
        }

        public Criteria andLinkernumberLessThan(String value) {
            addCriterion("LINKERNUMBER <", value, "linkernumber");
            return (Criteria) this;
        }

        public Criteria andLinkernumberLessThanOrEqualTo(String value) {
            addCriterion("LINKERNUMBER <=", value, "linkernumber");
            return (Criteria) this;
        }

        public Criteria andLinkernumberLike(String value) {
            addCriterion("LINKERNUMBER like", value, "linkernumber");
            return (Criteria) this;
        }

        public Criteria andLinkernumberNotLike(String value) {
            addCriterion("LINKERNUMBER not like", value, "linkernumber");
            return (Criteria) this;
        }

        public Criteria andLinkernumberIn(List<String> values) {
            addCriterion("LINKERNUMBER in", values, "linkernumber");
            return (Criteria) this;
        }

        public Criteria andLinkernumberNotIn(List<String> values) {
            addCriterion("LINKERNUMBER not in", values, "linkernumber");
            return (Criteria) this;
        }

        public Criteria andLinkernumberBetween(String value1, String value2) {
            addCriterion("LINKERNUMBER between", value1, value2, "linkernumber");
            return (Criteria) this;
        }

        public Criteria andLinkernumberNotBetween(String value1, String value2) {
            addCriterion("LINKERNUMBER not between", value1, value2, "linkernumber");
            return (Criteria) this;
        }

        public Criteria andPrincipalnameIsNull() {
            addCriterion("PRINCIPALNAME is null");
            return (Criteria) this;
        }

        public Criteria andPrincipalnameIsNotNull() {
            addCriterion("PRINCIPALNAME is not null");
            return (Criteria) this;
        }

        public Criteria andPrincipalnameEqualTo(String value) {
            addCriterion("PRINCIPALNAME =", value, "principalname");
            return (Criteria) this;
        }

        public Criteria andPrincipalnameNotEqualTo(String value) {
            addCriterion("PRINCIPALNAME <>", value, "principalname");
            return (Criteria) this;
        }

        public Criteria andPrincipalnameGreaterThan(String value) {
            addCriterion("PRINCIPALNAME >", value, "principalname");
            return (Criteria) this;
        }

        public Criteria andPrincipalnameGreaterThanOrEqualTo(String value) {
            addCriterion("PRINCIPALNAME >=", value, "principalname");
            return (Criteria) this;
        }

        public Criteria andPrincipalnameLessThan(String value) {
            addCriterion("PRINCIPALNAME <", value, "principalname");
            return (Criteria) this;
        }

        public Criteria andPrincipalnameLessThanOrEqualTo(String value) {
            addCriterion("PRINCIPALNAME <=", value, "principalname");
            return (Criteria) this;
        }

        public Criteria andPrincipalnameLike(String value) {
            addCriterion("PRINCIPALNAME like", value, "principalname");
            return (Criteria) this;
        }

        public Criteria andPrincipalnameNotLike(String value) {
            addCriterion("PRINCIPALNAME not like", value, "principalname");
            return (Criteria) this;
        }

        public Criteria andPrincipalnameIn(List<String> values) {
            addCriterion("PRINCIPALNAME in", values, "principalname");
            return (Criteria) this;
        }

        public Criteria andPrincipalnameNotIn(List<String> values) {
            addCriterion("PRINCIPALNAME not in", values, "principalname");
            return (Criteria) this;
        }

        public Criteria andPrincipalnameBetween(String value1, String value2) {
            addCriterion("PRINCIPALNAME between", value1, value2, "principalname");
            return (Criteria) this;
        }

        public Criteria andPrincipalnameNotBetween(String value1, String value2) {
            addCriterion("PRINCIPALNAME not between", value1, value2, "principalname");
            return (Criteria) this;
        }

        public Criteria andPriphonenumberIsNull() {
            addCriterion("PRIPHONENUMBER is null");
            return (Criteria) this;
        }

        public Criteria andPriphonenumberIsNotNull() {
            addCriterion("PRIPHONENUMBER is not null");
            return (Criteria) this;
        }

        public Criteria andPriphonenumberEqualTo(String value) {
            addCriterion("PRIPHONENUMBER =", value, "priphonenumber");
            return (Criteria) this;
        }

        public Criteria andPriphonenumberNotEqualTo(String value) {
            addCriterion("PRIPHONENUMBER <>", value, "priphonenumber");
            return (Criteria) this;
        }

        public Criteria andPriphonenumberGreaterThan(String value) {
            addCriterion("PRIPHONENUMBER >", value, "priphonenumber");
            return (Criteria) this;
        }

        public Criteria andPriphonenumberGreaterThanOrEqualTo(String value) {
            addCriterion("PRIPHONENUMBER >=", value, "priphonenumber");
            return (Criteria) this;
        }

        public Criteria andPriphonenumberLessThan(String value) {
            addCriterion("PRIPHONENUMBER <", value, "priphonenumber");
            return (Criteria) this;
        }

        public Criteria andPriphonenumberLessThanOrEqualTo(String value) {
            addCriterion("PRIPHONENUMBER <=", value, "priphonenumber");
            return (Criteria) this;
        }

        public Criteria andPriphonenumberLike(String value) {
            addCriterion("PRIPHONENUMBER like", value, "priphonenumber");
            return (Criteria) this;
        }

        public Criteria andPriphonenumberNotLike(String value) {
            addCriterion("PRIPHONENUMBER not like", value, "priphonenumber");
            return (Criteria) this;
        }

        public Criteria andPriphonenumberIn(List<String> values) {
            addCriterion("PRIPHONENUMBER in", values, "priphonenumber");
            return (Criteria) this;
        }

        public Criteria andPriphonenumberNotIn(List<String> values) {
            addCriterion("PRIPHONENUMBER not in", values, "priphonenumber");
            return (Criteria) this;
        }

        public Criteria andPriphonenumberBetween(String value1, String value2) {
            addCriterion("PRIPHONENUMBER between", value1, value2, "priphonenumber");
            return (Criteria) this;
        }

        public Criteria andPriphonenumberNotBetween(String value1, String value2) {
            addCriterion("PRIPHONENUMBER not between", value1, value2, "priphonenumber");
            return (Criteria) this;
        }

        public Criteria andPrifaxnumberIsNull() {
            addCriterion("PRIFAXNUMBER is null");
            return (Criteria) this;
        }

        public Criteria andPrifaxnumberIsNotNull() {
            addCriterion("PRIFAXNUMBER is not null");
            return (Criteria) this;
        }

        public Criteria andPrifaxnumberEqualTo(String value) {
            addCriterion("PRIFAXNUMBER =", value, "prifaxnumber");
            return (Criteria) this;
        }

        public Criteria andPrifaxnumberNotEqualTo(String value) {
            addCriterion("PRIFAXNUMBER <>", value, "prifaxnumber");
            return (Criteria) this;
        }

        public Criteria andPrifaxnumberGreaterThan(String value) {
            addCriterion("PRIFAXNUMBER >", value, "prifaxnumber");
            return (Criteria) this;
        }

        public Criteria andPrifaxnumberGreaterThanOrEqualTo(String value) {
            addCriterion("PRIFAXNUMBER >=", value, "prifaxnumber");
            return (Criteria) this;
        }

        public Criteria andPrifaxnumberLessThan(String value) {
            addCriterion("PRIFAXNUMBER <", value, "prifaxnumber");
            return (Criteria) this;
        }

        public Criteria andPrifaxnumberLessThanOrEqualTo(String value) {
            addCriterion("PRIFAXNUMBER <=", value, "prifaxnumber");
            return (Criteria) this;
        }

        public Criteria andPrifaxnumberLike(String value) {
            addCriterion("PRIFAXNUMBER like", value, "prifaxnumber");
            return (Criteria) this;
        }

        public Criteria andPrifaxnumberNotLike(String value) {
            addCriterion("PRIFAXNUMBER not like", value, "prifaxnumber");
            return (Criteria) this;
        }

        public Criteria andPrifaxnumberIn(List<String> values) {
            addCriterion("PRIFAXNUMBER in", values, "prifaxnumber");
            return (Criteria) this;
        }

        public Criteria andPrifaxnumberNotIn(List<String> values) {
            addCriterion("PRIFAXNUMBER not in", values, "prifaxnumber");
            return (Criteria) this;
        }

        public Criteria andPrifaxnumberBetween(String value1, String value2) {
            addCriterion("PRIFAXNUMBER between", value1, value2, "prifaxnumber");
            return (Criteria) this;
        }

        public Criteria andPrifaxnumberNotBetween(String value1, String value2) {
            addCriterion("PRIFAXNUMBER not between", value1, value2, "prifaxnumber");
            return (Criteria) this;
        }

        public Criteria andPrimobileIsNull() {
            addCriterion("PRIMOBILE is null");
            return (Criteria) this;
        }

        public Criteria andPrimobileIsNotNull() {
            addCriterion("PRIMOBILE is not null");
            return (Criteria) this;
        }

        public Criteria andPrimobileEqualTo(String value) {
            addCriterion("PRIMOBILE =", value, "primobile");
            return (Criteria) this;
        }

        public Criteria andPrimobileNotEqualTo(String value) {
            addCriterion("PRIMOBILE <>", value, "primobile");
            return (Criteria) this;
        }

        public Criteria andPrimobileGreaterThan(String value) {
            addCriterion("PRIMOBILE >", value, "primobile");
            return (Criteria) this;
        }

        public Criteria andPrimobileGreaterThanOrEqualTo(String value) {
            addCriterion("PRIMOBILE >=", value, "primobile");
            return (Criteria) this;
        }

        public Criteria andPrimobileLessThan(String value) {
            addCriterion("PRIMOBILE <", value, "primobile");
            return (Criteria) this;
        }

        public Criteria andPrimobileLessThanOrEqualTo(String value) {
            addCriterion("PRIMOBILE <=", value, "primobile");
            return (Criteria) this;
        }

        public Criteria andPrimobileLike(String value) {
            addCriterion("PRIMOBILE like", value, "primobile");
            return (Criteria) this;
        }

        public Criteria andPrimobileNotLike(String value) {
            addCriterion("PRIMOBILE not like", value, "primobile");
            return (Criteria) this;
        }

        public Criteria andPrimobileIn(List<String> values) {
            addCriterion("PRIMOBILE in", values, "primobile");
            return (Criteria) this;
        }

        public Criteria andPrimobileNotIn(List<String> values) {
            addCriterion("PRIMOBILE not in", values, "primobile");
            return (Criteria) this;
        }

        public Criteria andPrimobileBetween(String value1, String value2) {
            addCriterion("PRIMOBILE between", value1, value2, "primobile");
            return (Criteria) this;
        }

        public Criteria andPrimobileNotBetween(String value1, String value2) {
            addCriterion("PRIMOBILE not between", value1, value2, "primobile");
            return (Criteria) this;
        }

        public Criteria andPriemailIsNull() {
            addCriterion("PRIEMAIL is null");
            return (Criteria) this;
        }

        public Criteria andPriemailIsNotNull() {
            addCriterion("PRIEMAIL is not null");
            return (Criteria) this;
        }

        public Criteria andPriemailEqualTo(String value) {
            addCriterion("PRIEMAIL =", value, "priemail");
            return (Criteria) this;
        }

        public Criteria andPriemailNotEqualTo(String value) {
            addCriterion("PRIEMAIL <>", value, "priemail");
            return (Criteria) this;
        }

        public Criteria andPriemailGreaterThan(String value) {
            addCriterion("PRIEMAIL >", value, "priemail");
            return (Criteria) this;
        }

        public Criteria andPriemailGreaterThanOrEqualTo(String value) {
            addCriterion("PRIEMAIL >=", value, "priemail");
            return (Criteria) this;
        }

        public Criteria andPriemailLessThan(String value) {
            addCriterion("PRIEMAIL <", value, "priemail");
            return (Criteria) this;
        }

        public Criteria andPriemailLessThanOrEqualTo(String value) {
            addCriterion("PRIEMAIL <=", value, "priemail");
            return (Criteria) this;
        }

        public Criteria andPriemailLike(String value) {
            addCriterion("PRIEMAIL like", value, "priemail");
            return (Criteria) this;
        }

        public Criteria andPriemailNotLike(String value) {
            addCriterion("PRIEMAIL not like", value, "priemail");
            return (Criteria) this;
        }

        public Criteria andPriemailIn(List<String> values) {
            addCriterion("PRIEMAIL in", values, "priemail");
            return (Criteria) this;
        }

        public Criteria andPriemailNotIn(List<String> values) {
            addCriterion("PRIEMAIL not in", values, "priemail");
            return (Criteria) this;
        }

        public Criteria andPriemailBetween(String value1, String value2) {
            addCriterion("PRIEMAIL between", value1, value2, "priemail");
            return (Criteria) this;
        }

        public Criteria andPriemailNotBetween(String value1, String value2) {
            addCriterion("PRIEMAIL not between", value1, value2, "priemail");
            return (Criteria) this;
        }

        public Criteria andPrinetaddressIsNull() {
            addCriterion("PRINETADDRESS is null");
            return (Criteria) this;
        }

        public Criteria andPrinetaddressIsNotNull() {
            addCriterion("PRINETADDRESS is not null");
            return (Criteria) this;
        }

        public Criteria andPrinetaddressEqualTo(String value) {
            addCriterion("PRINETADDRESS =", value, "prinetaddress");
            return (Criteria) this;
        }

        public Criteria andPrinetaddressNotEqualTo(String value) {
            addCriterion("PRINETADDRESS <>", value, "prinetaddress");
            return (Criteria) this;
        }

        public Criteria andPrinetaddressGreaterThan(String value) {
            addCriterion("PRINETADDRESS >", value, "prinetaddress");
            return (Criteria) this;
        }

        public Criteria andPrinetaddressGreaterThanOrEqualTo(String value) {
            addCriterion("PRINETADDRESS >=", value, "prinetaddress");
            return (Criteria) this;
        }

        public Criteria andPrinetaddressLessThan(String value) {
            addCriterion("PRINETADDRESS <", value, "prinetaddress");
            return (Criteria) this;
        }

        public Criteria andPrinetaddressLessThanOrEqualTo(String value) {
            addCriterion("PRINETADDRESS <=", value, "prinetaddress");
            return (Criteria) this;
        }

        public Criteria andPrinetaddressLike(String value) {
            addCriterion("PRINETADDRESS like", value, "prinetaddress");
            return (Criteria) this;
        }

        public Criteria andPrinetaddressNotLike(String value) {
            addCriterion("PRINETADDRESS not like", value, "prinetaddress");
            return (Criteria) this;
        }

        public Criteria andPrinetaddressIn(List<String> values) {
            addCriterion("PRINETADDRESS in", values, "prinetaddress");
            return (Criteria) this;
        }

        public Criteria andPrinetaddressNotIn(List<String> values) {
            addCriterion("PRINETADDRESS not in", values, "prinetaddress");
            return (Criteria) this;
        }

        public Criteria andPrinetaddressBetween(String value1, String value2) {
            addCriterion("PRINETADDRESS between", value1, value2, "prinetaddress");
            return (Criteria) this;
        }

        public Criteria andPrinetaddressNotBetween(String value1, String value2) {
            addCriterion("PRINETADDRESS not between", value1, value2, "prinetaddress");
            return (Criteria) this;
        }

        public Criteria andBankIsNull() {
            addCriterion("BANK is null");
            return (Criteria) this;
        }

        public Criteria andBankIsNotNull() {
            addCriterion("BANK is not null");
            return (Criteria) this;
        }

        public Criteria andBankEqualTo(String value) {
            addCriterion("BANK =", value, "bank");
            return (Criteria) this;
        }

        public Criteria andBankNotEqualTo(String value) {
            addCriterion("BANK <>", value, "bank");
            return (Criteria) this;
        }

        public Criteria andBankGreaterThan(String value) {
            addCriterion("BANK >", value, "bank");
            return (Criteria) this;
        }

        public Criteria andBankGreaterThanOrEqualTo(String value) {
            addCriterion("BANK >=", value, "bank");
            return (Criteria) this;
        }

        public Criteria andBankLessThan(String value) {
            addCriterion("BANK <", value, "bank");
            return (Criteria) this;
        }

        public Criteria andBankLessThanOrEqualTo(String value) {
            addCriterion("BANK <=", value, "bank");
            return (Criteria) this;
        }

        public Criteria andBankLike(String value) {
            addCriterion("BANK like", value, "bank");
            return (Criteria) this;
        }

        public Criteria andBankNotLike(String value) {
            addCriterion("BANK not like", value, "bank");
            return (Criteria) this;
        }

        public Criteria andBankIn(List<String> values) {
            addCriterion("BANK in", values, "bank");
            return (Criteria) this;
        }

        public Criteria andBankNotIn(List<String> values) {
            addCriterion("BANK not in", values, "bank");
            return (Criteria) this;
        }

        public Criteria andBankBetween(String value1, String value2) {
            addCriterion("BANK between", value1, value2, "bank");
            return (Criteria) this;
        }

        public Criteria andBankNotBetween(String value1, String value2) {
            addCriterion("BANK not between", value1, value2, "bank");
            return (Criteria) this;
        }

        public Criteria andDepositbankIsNull() {
            addCriterion("DEPOSITBANK is null");
            return (Criteria) this;
        }

        public Criteria andDepositbankIsNotNull() {
            addCriterion("DEPOSITBANK is not null");
            return (Criteria) this;
        }

        public Criteria andDepositbankEqualTo(String value) {
            addCriterion("DEPOSITBANK =", value, "depositbank");
            return (Criteria) this;
        }

        public Criteria andDepositbankNotEqualTo(String value) {
            addCriterion("DEPOSITBANK <>", value, "depositbank");
            return (Criteria) this;
        }

        public Criteria andDepositbankGreaterThan(String value) {
            addCriterion("DEPOSITBANK >", value, "depositbank");
            return (Criteria) this;
        }

        public Criteria andDepositbankGreaterThanOrEqualTo(String value) {
            addCriterion("DEPOSITBANK >=", value, "depositbank");
            return (Criteria) this;
        }

        public Criteria andDepositbankLessThan(String value) {
            addCriterion("DEPOSITBANK <", value, "depositbank");
            return (Criteria) this;
        }

        public Criteria andDepositbankLessThanOrEqualTo(String value) {
            addCriterion("DEPOSITBANK <=", value, "depositbank");
            return (Criteria) this;
        }

        public Criteria andDepositbankLike(String value) {
            addCriterion("DEPOSITBANK like", value, "depositbank");
            return (Criteria) this;
        }

        public Criteria andDepositbankNotLike(String value) {
            addCriterion("DEPOSITBANK not like", value, "depositbank");
            return (Criteria) this;
        }

        public Criteria andDepositbankIn(List<String> values) {
            addCriterion("DEPOSITBANK in", values, "depositbank");
            return (Criteria) this;
        }

        public Criteria andDepositbankNotIn(List<String> values) {
            addCriterion("DEPOSITBANK not in", values, "depositbank");
            return (Criteria) this;
        }

        public Criteria andDepositbankBetween(String value1, String value2) {
            addCriterion("DEPOSITBANK between", value1, value2, "depositbank");
            return (Criteria) this;
        }

        public Criteria andDepositbankNotBetween(String value1, String value2) {
            addCriterion("DEPOSITBANK not between", value1, value2, "depositbank");
            return (Criteria) this;
        }

        public Criteria andAccountIsNull() {
            addCriterion("ACCOUNT is null");
            return (Criteria) this;
        }

        public Criteria andAccountIsNotNull() {
            addCriterion("ACCOUNT is not null");
            return (Criteria) this;
        }

        public Criteria andAccountEqualTo(String value) {
            addCriterion("ACCOUNT =", value, "account");
            return (Criteria) this;
        }

        public Criteria andAccountNotEqualTo(String value) {
            addCriterion("ACCOUNT <>", value, "account");
            return (Criteria) this;
        }

        public Criteria andAccountGreaterThan(String value) {
            addCriterion("ACCOUNT >", value, "account");
            return (Criteria) this;
        }

        public Criteria andAccountGreaterThanOrEqualTo(String value) {
            addCriterion("ACCOUNT >=", value, "account");
            return (Criteria) this;
        }

        public Criteria andAccountLessThan(String value) {
            addCriterion("ACCOUNT <", value, "account");
            return (Criteria) this;
        }

        public Criteria andAccountLessThanOrEqualTo(String value) {
            addCriterion("ACCOUNT <=", value, "account");
            return (Criteria) this;
        }

        public Criteria andAccountLike(String value) {
            addCriterion("ACCOUNT like", value, "account");
            return (Criteria) this;
        }

        public Criteria andAccountNotLike(String value) {
            addCriterion("ACCOUNT not like", value, "account");
            return (Criteria) this;
        }

        public Criteria andAccountIn(List<String> values) {
            addCriterion("ACCOUNT in", values, "account");
            return (Criteria) this;
        }

        public Criteria andAccountNotIn(List<String> values) {
            addCriterion("ACCOUNT not in", values, "account");
            return (Criteria) this;
        }

        public Criteria andAccountBetween(String value1, String value2) {
            addCriterion("ACCOUNT between", value1, value2, "account");
            return (Criteria) this;
        }

        public Criteria andAccountNotBetween(String value1, String value2) {
            addCriterion("ACCOUNT not between", value1, value2, "account");
            return (Criteria) this;
        }

        public Criteria andAccountholderIsNull() {
            addCriterion("ACCOUNTHOLDER is null");
            return (Criteria) this;
        }

        public Criteria andAccountholderIsNotNull() {
            addCriterion("ACCOUNTHOLDER is not null");
            return (Criteria) this;
        }

        public Criteria andAccountholderEqualTo(String value) {
            addCriterion("ACCOUNTHOLDER =", value, "accountholder");
            return (Criteria) this;
        }

        public Criteria andAccountholderNotEqualTo(String value) {
            addCriterion("ACCOUNTHOLDER <>", value, "accountholder");
            return (Criteria) this;
        }

        public Criteria andAccountholderGreaterThan(String value) {
            addCriterion("ACCOUNTHOLDER >", value, "accountholder");
            return (Criteria) this;
        }

        public Criteria andAccountholderGreaterThanOrEqualTo(String value) {
            addCriterion("ACCOUNTHOLDER >=", value, "accountholder");
            return (Criteria) this;
        }

        public Criteria andAccountholderLessThan(String value) {
            addCriterion("ACCOUNTHOLDER <", value, "accountholder");
            return (Criteria) this;
        }

        public Criteria andAccountholderLessThanOrEqualTo(String value) {
            addCriterion("ACCOUNTHOLDER <=", value, "accountholder");
            return (Criteria) this;
        }

        public Criteria andAccountholderLike(String value) {
            addCriterion("ACCOUNTHOLDER like", value, "accountholder");
            return (Criteria) this;
        }

        public Criteria andAccountholderNotLike(String value) {
            addCriterion("ACCOUNTHOLDER not like", value, "accountholder");
            return (Criteria) this;
        }

        public Criteria andAccountholderIn(List<String> values) {
            addCriterion("ACCOUNTHOLDER in", values, "accountholder");
            return (Criteria) this;
        }

        public Criteria andAccountholderNotIn(List<String> values) {
            addCriterion("ACCOUNTHOLDER not in", values, "accountholder");
            return (Criteria) this;
        }

        public Criteria andAccountholderBetween(String value1, String value2) {
            addCriterion("ACCOUNTHOLDER between", value1, value2, "accountholder");
            return (Criteria) this;
        }

        public Criteria andAccountholderNotBetween(String value1, String value2) {
            addCriterion("ACCOUNTHOLDER not between", value1, value2, "accountholder");
            return (Criteria) this;
        }

        public Criteria andLowerviewflagIsNull() {
            addCriterion("LOWERVIEWFLAG is null");
            return (Criteria) this;
        }

        public Criteria andLowerviewflagIsNotNull() {
            addCriterion("LOWERVIEWFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andLowerviewflagEqualTo(String value) {
            addCriterion("LOWERVIEWFLAG =", value, "lowerviewflag");
            return (Criteria) this;
        }

        public Criteria andLowerviewflagNotEqualTo(String value) {
            addCriterion("LOWERVIEWFLAG <>", value, "lowerviewflag");
            return (Criteria) this;
        }

        public Criteria andLowerviewflagGreaterThan(String value) {
            addCriterion("LOWERVIEWFLAG >", value, "lowerviewflag");
            return (Criteria) this;
        }

        public Criteria andLowerviewflagGreaterThanOrEqualTo(String value) {
            addCriterion("LOWERVIEWFLAG >=", value, "lowerviewflag");
            return (Criteria) this;
        }

        public Criteria andLowerviewflagLessThan(String value) {
            addCriterion("LOWERVIEWFLAG <", value, "lowerviewflag");
            return (Criteria) this;
        }

        public Criteria andLowerviewflagLessThanOrEqualTo(String value) {
            addCriterion("LOWERVIEWFLAG <=", value, "lowerviewflag");
            return (Criteria) this;
        }

        public Criteria andLowerviewflagLike(String value) {
            addCriterion("LOWERVIEWFLAG like", value, "lowerviewflag");
            return (Criteria) this;
        }

        public Criteria andLowerviewflagNotLike(String value) {
            addCriterion("LOWERVIEWFLAG not like", value, "lowerviewflag");
            return (Criteria) this;
        }

        public Criteria andLowerviewflagIn(List<String> values) {
            addCriterion("LOWERVIEWFLAG in", values, "lowerviewflag");
            return (Criteria) this;
        }

        public Criteria andLowerviewflagNotIn(List<String> values) {
            addCriterion("LOWERVIEWFLAG not in", values, "lowerviewflag");
            return (Criteria) this;
        }

        public Criteria andLowerviewflagBetween(String value1, String value2) {
            addCriterion("LOWERVIEWFLAG between", value1, value2, "lowerviewflag");
            return (Criteria) this;
        }

        public Criteria andLowerviewflagNotBetween(String value1, String value2) {
            addCriterion("LOWERVIEWFLAG not between", value1, value2, "lowerviewflag");
            return (Criteria) this;
        }

        public Criteria andBusinesslicenseIsNull() {
            addCriterion("BUSINESSLICENSE is null");
            return (Criteria) this;
        }

        public Criteria andBusinesslicenseIsNotNull() {
            addCriterion("BUSINESSLICENSE is not null");
            return (Criteria) this;
        }

        public Criteria andBusinesslicenseEqualTo(String value) {
            addCriterion("BUSINESSLICENSE =", value, "businesslicense");
            return (Criteria) this;
        }

        public Criteria andBusinesslicenseNotEqualTo(String value) {
            addCriterion("BUSINESSLICENSE <>", value, "businesslicense");
            return (Criteria) this;
        }

        public Criteria andBusinesslicenseGreaterThan(String value) {
            addCriterion("BUSINESSLICENSE >", value, "businesslicense");
            return (Criteria) this;
        }

        public Criteria andBusinesslicenseGreaterThanOrEqualTo(String value) {
            addCriterion("BUSINESSLICENSE >=", value, "businesslicense");
            return (Criteria) this;
        }

        public Criteria andBusinesslicenseLessThan(String value) {
            addCriterion("BUSINESSLICENSE <", value, "businesslicense");
            return (Criteria) this;
        }

        public Criteria andBusinesslicenseLessThanOrEqualTo(String value) {
            addCriterion("BUSINESSLICENSE <=", value, "businesslicense");
            return (Criteria) this;
        }

        public Criteria andBusinesslicenseLike(String value) {
            addCriterion("BUSINESSLICENSE like", value, "businesslicense");
            return (Criteria) this;
        }

        public Criteria andBusinesslicenseNotLike(String value) {
            addCriterion("BUSINESSLICENSE not like", value, "businesslicense");
            return (Criteria) this;
        }

        public Criteria andBusinesslicenseIn(List<String> values) {
            addCriterion("BUSINESSLICENSE in", values, "businesslicense");
            return (Criteria) this;
        }

        public Criteria andBusinesslicenseNotIn(List<String> values) {
            addCriterion("BUSINESSLICENSE not in", values, "businesslicense");
            return (Criteria) this;
        }

        public Criteria andBusinesslicenseBetween(String value1, String value2) {
            addCriterion("BUSINESSLICENSE between", value1, value2, "businesslicense");
            return (Criteria) this;
        }

        public Criteria andBusinesslicenseNotBetween(String value1, String value2) {
            addCriterion("BUSINESSLICENSE not between", value1, value2, "businesslicense");
            return (Criteria) this;
        }

        public Criteria andOrgcodeIsNull() {
            addCriterion("ORGCODE is null");
            return (Criteria) this;
        }

        public Criteria andOrgcodeIsNotNull() {
            addCriterion("ORGCODE is not null");
            return (Criteria) this;
        }

        public Criteria andOrgcodeEqualTo(String value) {
            addCriterion("ORGCODE =", value, "orgcode");
            return (Criteria) this;
        }

        public Criteria andOrgcodeNotEqualTo(String value) {
            addCriterion("ORGCODE <>", value, "orgcode");
            return (Criteria) this;
        }

        public Criteria andOrgcodeGreaterThan(String value) {
            addCriterion("ORGCODE >", value, "orgcode");
            return (Criteria) this;
        }

        public Criteria andOrgcodeGreaterThanOrEqualTo(String value) {
            addCriterion("ORGCODE >=", value, "orgcode");
            return (Criteria) this;
        }

        public Criteria andOrgcodeLessThan(String value) {
            addCriterion("ORGCODE <", value, "orgcode");
            return (Criteria) this;
        }

        public Criteria andOrgcodeLessThanOrEqualTo(String value) {
            addCriterion("ORGCODE <=", value, "orgcode");
            return (Criteria) this;
        }

        public Criteria andOrgcodeLike(String value) {
            addCriterion("ORGCODE like", value, "orgcode");
            return (Criteria) this;
        }

        public Criteria andOrgcodeNotLike(String value) {
            addCriterion("ORGCODE not like", value, "orgcode");
            return (Criteria) this;
        }

        public Criteria andOrgcodeIn(List<String> values) {
            addCriterion("ORGCODE in", values, "orgcode");
            return (Criteria) this;
        }

        public Criteria andOrgcodeNotIn(List<String> values) {
            addCriterion("ORGCODE not in", values, "orgcode");
            return (Criteria) this;
        }

        public Criteria andOrgcodeBetween(String value1, String value2) {
            addCriterion("ORGCODE between", value1, value2, "orgcode");
            return (Criteria) this;
        }

        public Criteria andOrgcodeNotBetween(String value1, String value2) {
            addCriterion("ORGCODE not between", value1, value2, "orgcode");
            return (Criteria) this;
        }

        public Criteria andAgentadminIsNull() {
            addCriterion("AGENTADMIN is null");
            return (Criteria) this;
        }

        public Criteria andAgentadminIsNotNull() {
            addCriterion("AGENTADMIN is not null");
            return (Criteria) this;
        }

        public Criteria andAgentadminEqualTo(String value) {
            addCriterion("AGENTADMIN =", value, "agentadmin");
            return (Criteria) this;
        }

        public Criteria andAgentadminNotEqualTo(String value) {
            addCriterion("AGENTADMIN <>", value, "agentadmin");
            return (Criteria) this;
        }

        public Criteria andAgentadminGreaterThan(String value) {
            addCriterion("AGENTADMIN >", value, "agentadmin");
            return (Criteria) this;
        }

        public Criteria andAgentadminGreaterThanOrEqualTo(String value) {
            addCriterion("AGENTADMIN >=", value, "agentadmin");
            return (Criteria) this;
        }

        public Criteria andAgentadminLessThan(String value) {
            addCriterion("AGENTADMIN <", value, "agentadmin");
            return (Criteria) this;
        }

        public Criteria andAgentadminLessThanOrEqualTo(String value) {
            addCriterion("AGENTADMIN <=", value, "agentadmin");
            return (Criteria) this;
        }

        public Criteria andAgentadminLike(String value) {
            addCriterion("AGENTADMIN like", value, "agentadmin");
            return (Criteria) this;
        }

        public Criteria andAgentadminNotLike(String value) {
            addCriterion("AGENTADMIN not like", value, "agentadmin");
            return (Criteria) this;
        }

        public Criteria andAgentadminIn(List<String> values) {
            addCriterion("AGENTADMIN in", values, "agentadmin");
            return (Criteria) this;
        }

        public Criteria andAgentadminNotIn(List<String> values) {
            addCriterion("AGENTADMIN not in", values, "agentadmin");
            return (Criteria) this;
        }

        public Criteria andAgentadminBetween(String value1, String value2) {
            addCriterion("AGENTADMIN between", value1, value2, "agentadmin");
            return (Criteria) this;
        }

        public Criteria andAgentadminNotBetween(String value1, String value2) {
            addCriterion("AGENTADMIN not between", value1, value2, "agentadmin");
            return (Criteria) this;
        }

        public Criteria andCooperationlevelIsNull() {
            addCriterion("COOPERATIONLEVEL is null");
            return (Criteria) this;
        }

        public Criteria andCooperationlevelIsNotNull() {
            addCriterion("COOPERATIONLEVEL is not null");
            return (Criteria) this;
        }

        public Criteria andCooperationlevelEqualTo(String value) {
            addCriterion("COOPERATIONLEVEL =", value, "cooperationlevel");
            return (Criteria) this;
        }

        public Criteria andCooperationlevelNotEqualTo(String value) {
            addCriterion("COOPERATIONLEVEL <>", value, "cooperationlevel");
            return (Criteria) this;
        }

        public Criteria andCooperationlevelGreaterThan(String value) {
            addCriterion("COOPERATIONLEVEL >", value, "cooperationlevel");
            return (Criteria) this;
        }

        public Criteria andCooperationlevelGreaterThanOrEqualTo(String value) {
            addCriterion("COOPERATIONLEVEL >=", value, "cooperationlevel");
            return (Criteria) this;
        }

        public Criteria andCooperationlevelLessThan(String value) {
            addCriterion("COOPERATIONLEVEL <", value, "cooperationlevel");
            return (Criteria) this;
        }

        public Criteria andCooperationlevelLessThanOrEqualTo(String value) {
            addCriterion("COOPERATIONLEVEL <=", value, "cooperationlevel");
            return (Criteria) this;
        }

        public Criteria andCooperationlevelLike(String value) {
            addCriterion("COOPERATIONLEVEL like", value, "cooperationlevel");
            return (Criteria) this;
        }

        public Criteria andCooperationlevelNotLike(String value) {
            addCriterion("COOPERATIONLEVEL not like", value, "cooperationlevel");
            return (Criteria) this;
        }

        public Criteria andCooperationlevelIn(List<String> values) {
            addCriterion("COOPERATIONLEVEL in", values, "cooperationlevel");
            return (Criteria) this;
        }

        public Criteria andCooperationlevelNotIn(List<String> values) {
            addCriterion("COOPERATIONLEVEL not in", values, "cooperationlevel");
            return (Criteria) this;
        }

        public Criteria andCooperationlevelBetween(String value1, String value2) {
            addCriterion("COOPERATIONLEVEL between", value1, value2, "cooperationlevel");
            return (Criteria) this;
        }

        public Criteria andCooperationlevelNotBetween(String value1, String value2) {
            addCriterion("COOPERATIONLEVEL not between", value1, value2, "cooperationlevel");
            return (Criteria) this;
        }

        public Criteria andIdnoIsNull() {
            addCriterion("IDNO is null");
            return (Criteria) this;
        }

        public Criteria andIdnoIsNotNull() {
            addCriterion("IDNO is not null");
            return (Criteria) this;
        }

        public Criteria andIdnoEqualTo(String value) {
            addCriterion("IDNO =", value, "idno");
            return (Criteria) this;
        }

        public Criteria andIdnoNotEqualTo(String value) {
            addCriterion("IDNO <>", value, "idno");
            return (Criteria) this;
        }

        public Criteria andIdnoGreaterThan(String value) {
            addCriterion("IDNO >", value, "idno");
            return (Criteria) this;
        }

        public Criteria andIdnoGreaterThanOrEqualTo(String value) {
            addCriterion("IDNO >=", value, "idno");
            return (Criteria) this;
        }

        public Criteria andIdnoLessThan(String value) {
            addCriterion("IDNO <", value, "idno");
            return (Criteria) this;
        }

        public Criteria andIdnoLessThanOrEqualTo(String value) {
            addCriterion("IDNO <=", value, "idno");
            return (Criteria) this;
        }

        public Criteria andIdnoLike(String value) {
            addCriterion("IDNO like", value, "idno");
            return (Criteria) this;
        }

        public Criteria andIdnoNotLike(String value) {
            addCriterion("IDNO not like", value, "idno");
            return (Criteria) this;
        }

        public Criteria andIdnoIn(List<String> values) {
            addCriterion("IDNO in", values, "idno");
            return (Criteria) this;
        }

        public Criteria andIdnoNotIn(List<String> values) {
            addCriterion("IDNO not in", values, "idno");
            return (Criteria) this;
        }

        public Criteria andIdnoBetween(String value1, String value2) {
            addCriterion("IDNO between", value1, value2, "idno");
            return (Criteria) this;
        }

        public Criteria andIdnoNotBetween(String value1, String value2) {
            addCriterion("IDNO not between", value1, value2, "idno");
            return (Criteria) this;
        }

        public Criteria andSexIsNull() {
            addCriterion("SEX is null");
            return (Criteria) this;
        }

        public Criteria andSexIsNotNull() {
            addCriterion("SEX is not null");
            return (Criteria) this;
        }

        public Criteria andSexEqualTo(String value) {
            addCriterion("SEX =", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotEqualTo(String value) {
            addCriterion("SEX <>", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexGreaterThan(String value) {
            addCriterion("SEX >", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexGreaterThanOrEqualTo(String value) {
            addCriterion("SEX >=", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexLessThan(String value) {
            addCriterion("SEX <", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexLessThanOrEqualTo(String value) {
            addCriterion("SEX <=", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexLike(String value) {
            addCriterion("SEX like", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotLike(String value) {
            addCriterion("SEX not like", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexIn(List<String> values) {
            addCriterion("SEX in", values, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotIn(List<String> values) {
            addCriterion("SEX not in", values, "sex");
            return (Criteria) this;
        }

        public Criteria andSexBetween(String value1, String value2) {
            addCriterion("SEX between", value1, value2, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotBetween(String value1, String value2) {
            addCriterion("SEX not between", value1, value2, "sex");
            return (Criteria) this;
        }

        public Criteria andBirthIsNull() {
            addCriterion("BIRTH is null");
            return (Criteria) this;
        }

        public Criteria andBirthIsNotNull() {
            addCriterion("BIRTH is not null");
            return (Criteria) this;
        }

        public Criteria andBirthEqualTo(Date value) {
            addCriterion("BIRTH =", value, "birth");
            return (Criteria) this;
        }

        public Criteria andBirthNotEqualTo(Date value) {
            addCriterion("BIRTH <>", value, "birth");
            return (Criteria) this;
        }

        public Criteria andBirthGreaterThan(Date value) {
            addCriterion("BIRTH >", value, "birth");
            return (Criteria) this;
        }

        public Criteria andBirthGreaterThanOrEqualTo(Date value) {
            addCriterion("BIRTH >=", value, "birth");
            return (Criteria) this;
        }

        public Criteria andBirthLessThan(Date value) {
            addCriterion("BIRTH <", value, "birth");
            return (Criteria) this;
        }

        public Criteria andBirthLessThanOrEqualTo(Date value) {
            addCriterion("BIRTH <=", value, "birth");
            return (Criteria) this;
        }

        public Criteria andBirthIn(List<Date> values) {
            addCriterion("BIRTH in", values, "birth");
            return (Criteria) this;
        }

        public Criteria andBirthNotIn(List<Date> values) {
            addCriterion("BIRTH not in", values, "birth");
            return (Criteria) this;
        }

        public Criteria andBirthBetween(Date value1, Date value2) {
            addCriterion("BIRTH between", value1, value2, "birth");
            return (Criteria) this;
        }

        public Criteria andBirthNotBetween(Date value1, Date value2) {
            addCriterion("BIRTH not between", value1, value2, "birth");
            return (Criteria) this;
        }

        public Criteria andResidenceaddrIsNull() {
            addCriterion("RESIDENCEADDR is null");
            return (Criteria) this;
        }

        public Criteria andResidenceaddrIsNotNull() {
            addCriterion("RESIDENCEADDR is not null");
            return (Criteria) this;
        }

        public Criteria andResidenceaddrEqualTo(String value) {
            addCriterion("RESIDENCEADDR =", value, "residenceaddr");
            return (Criteria) this;
        }

        public Criteria andResidenceaddrNotEqualTo(String value) {
            addCriterion("RESIDENCEADDR <>", value, "residenceaddr");
            return (Criteria) this;
        }

        public Criteria andResidenceaddrGreaterThan(String value) {
            addCriterion("RESIDENCEADDR >", value, "residenceaddr");
            return (Criteria) this;
        }

        public Criteria andResidenceaddrGreaterThanOrEqualTo(String value) {
            addCriterion("RESIDENCEADDR >=", value, "residenceaddr");
            return (Criteria) this;
        }

        public Criteria andResidenceaddrLessThan(String value) {
            addCriterion("RESIDENCEADDR <", value, "residenceaddr");
            return (Criteria) this;
        }

        public Criteria andResidenceaddrLessThanOrEqualTo(String value) {
            addCriterion("RESIDENCEADDR <=", value, "residenceaddr");
            return (Criteria) this;
        }

        public Criteria andResidenceaddrLike(String value) {
            addCriterion("RESIDENCEADDR like", value, "residenceaddr");
            return (Criteria) this;
        }

        public Criteria andResidenceaddrNotLike(String value) {
            addCriterion("RESIDENCEADDR not like", value, "residenceaddr");
            return (Criteria) this;
        }

        public Criteria andResidenceaddrIn(List<String> values) {
            addCriterion("RESIDENCEADDR in", values, "residenceaddr");
            return (Criteria) this;
        }

        public Criteria andResidenceaddrNotIn(List<String> values) {
            addCriterion("RESIDENCEADDR not in", values, "residenceaddr");
            return (Criteria) this;
        }

        public Criteria andResidenceaddrBetween(String value1, String value2) {
            addCriterion("RESIDENCEADDR between", value1, value2, "residenceaddr");
            return (Criteria) this;
        }

        public Criteria andResidenceaddrNotBetween(String value1, String value2) {
            addCriterion("RESIDENCEADDR not between", value1, value2, "residenceaddr");
            return (Criteria) this;
        }

        public Criteria andMaritalstatusIsNull() {
            addCriterion("MARITALSTATUS is null");
            return (Criteria) this;
        }

        public Criteria andMaritalstatusIsNotNull() {
            addCriterion("MARITALSTATUS is not null");
            return (Criteria) this;
        }

        public Criteria andMaritalstatusEqualTo(String value) {
            addCriterion("MARITALSTATUS =", value, "maritalstatus");
            return (Criteria) this;
        }

        public Criteria andMaritalstatusNotEqualTo(String value) {
            addCriterion("MARITALSTATUS <>", value, "maritalstatus");
            return (Criteria) this;
        }

        public Criteria andMaritalstatusGreaterThan(String value) {
            addCriterion("MARITALSTATUS >", value, "maritalstatus");
            return (Criteria) this;
        }

        public Criteria andMaritalstatusGreaterThanOrEqualTo(String value) {
            addCriterion("MARITALSTATUS >=", value, "maritalstatus");
            return (Criteria) this;
        }

        public Criteria andMaritalstatusLessThan(String value) {
            addCriterion("MARITALSTATUS <", value, "maritalstatus");
            return (Criteria) this;
        }

        public Criteria andMaritalstatusLessThanOrEqualTo(String value) {
            addCriterion("MARITALSTATUS <=", value, "maritalstatus");
            return (Criteria) this;
        }

        public Criteria andMaritalstatusLike(String value) {
            addCriterion("MARITALSTATUS like", value, "maritalstatus");
            return (Criteria) this;
        }

        public Criteria andMaritalstatusNotLike(String value) {
            addCriterion("MARITALSTATUS not like", value, "maritalstatus");
            return (Criteria) this;
        }

        public Criteria andMaritalstatusIn(List<String> values) {
            addCriterion("MARITALSTATUS in", values, "maritalstatus");
            return (Criteria) this;
        }

        public Criteria andMaritalstatusNotIn(List<String> values) {
            addCriterion("MARITALSTATUS not in", values, "maritalstatus");
            return (Criteria) this;
        }

        public Criteria andMaritalstatusBetween(String value1, String value2) {
            addCriterion("MARITALSTATUS between", value1, value2, "maritalstatus");
            return (Criteria) this;
        }

        public Criteria andMaritalstatusNotBetween(String value1, String value2) {
            addCriterion("MARITALSTATUS not between", value1, value2, "maritalstatus");
            return (Criteria) this;
        }

        public Criteria andEducationIsNull() {
            addCriterion("EDUCATION is null");
            return (Criteria) this;
        }

        public Criteria andEducationIsNotNull() {
            addCriterion("EDUCATION is not null");
            return (Criteria) this;
        }

        public Criteria andEducationEqualTo(String value) {
            addCriterion("EDUCATION =", value, "education");
            return (Criteria) this;
        }

        public Criteria andEducationNotEqualTo(String value) {
            addCriterion("EDUCATION <>", value, "education");
            return (Criteria) this;
        }

        public Criteria andEducationGreaterThan(String value) {
            addCriterion("EDUCATION >", value, "education");
            return (Criteria) this;
        }

        public Criteria andEducationGreaterThanOrEqualTo(String value) {
            addCriterion("EDUCATION >=", value, "education");
            return (Criteria) this;
        }

        public Criteria andEducationLessThan(String value) {
            addCriterion("EDUCATION <", value, "education");
            return (Criteria) this;
        }

        public Criteria andEducationLessThanOrEqualTo(String value) {
            addCriterion("EDUCATION <=", value, "education");
            return (Criteria) this;
        }

        public Criteria andEducationLike(String value) {
            addCriterion("EDUCATION like", value, "education");
            return (Criteria) this;
        }

        public Criteria andEducationNotLike(String value) {
            addCriterion("EDUCATION not like", value, "education");
            return (Criteria) this;
        }

        public Criteria andEducationIn(List<String> values) {
            addCriterion("EDUCATION in", values, "education");
            return (Criteria) this;
        }

        public Criteria andEducationNotIn(List<String> values) {
            addCriterion("EDUCATION not in", values, "education");
            return (Criteria) this;
        }

        public Criteria andEducationBetween(String value1, String value2) {
            addCriterion("EDUCATION between", value1, value2, "education");
            return (Criteria) this;
        }

        public Criteria andEducationNotBetween(String value1, String value2) {
            addCriterion("EDUCATION not between", value1, value2, "education");
            return (Criteria) this;
        }

        public Criteria andDegreeIsNull() {
            addCriterion("DEGREE is null");
            return (Criteria) this;
        }

        public Criteria andDegreeIsNotNull() {
            addCriterion("DEGREE is not null");
            return (Criteria) this;
        }

        public Criteria andDegreeEqualTo(String value) {
            addCriterion("DEGREE =", value, "degree");
            return (Criteria) this;
        }

        public Criteria andDegreeNotEqualTo(String value) {
            addCriterion("DEGREE <>", value, "degree");
            return (Criteria) this;
        }

        public Criteria andDegreeGreaterThan(String value) {
            addCriterion("DEGREE >", value, "degree");
            return (Criteria) this;
        }

        public Criteria andDegreeGreaterThanOrEqualTo(String value) {
            addCriterion("DEGREE >=", value, "degree");
            return (Criteria) this;
        }

        public Criteria andDegreeLessThan(String value) {
            addCriterion("DEGREE <", value, "degree");
            return (Criteria) this;
        }

        public Criteria andDegreeLessThanOrEqualTo(String value) {
            addCriterion("DEGREE <=", value, "degree");
            return (Criteria) this;
        }

        public Criteria andDegreeLike(String value) {
            addCriterion("DEGREE like", value, "degree");
            return (Criteria) this;
        }

        public Criteria andDegreeNotLike(String value) {
            addCriterion("DEGREE not like", value, "degree");
            return (Criteria) this;
        }

        public Criteria andDegreeIn(List<String> values) {
            addCriterion("DEGREE in", values, "degree");
            return (Criteria) this;
        }

        public Criteria andDegreeNotIn(List<String> values) {
            addCriterion("DEGREE not in", values, "degree");
            return (Criteria) this;
        }

        public Criteria andDegreeBetween(String value1, String value2) {
            addCriterion("DEGREE between", value1, value2, "degree");
            return (Criteria) this;
        }

        public Criteria andDegreeNotBetween(String value1, String value2) {
            addCriterion("DEGREE not between", value1, value2, "degree");
            return (Criteria) this;
        }

        public Criteria andProfessionIsNull() {
            addCriterion("PROFESSION is null");
            return (Criteria) this;
        }

        public Criteria andProfessionIsNotNull() {
            addCriterion("PROFESSION is not null");
            return (Criteria) this;
        }

        public Criteria andProfessionEqualTo(String value) {
            addCriterion("PROFESSION =", value, "profession");
            return (Criteria) this;
        }

        public Criteria andProfessionNotEqualTo(String value) {
            addCriterion("PROFESSION <>", value, "profession");
            return (Criteria) this;
        }

        public Criteria andProfessionGreaterThan(String value) {
            addCriterion("PROFESSION >", value, "profession");
            return (Criteria) this;
        }

        public Criteria andProfessionGreaterThanOrEqualTo(String value) {
            addCriterion("PROFESSION >=", value, "profession");
            return (Criteria) this;
        }

        public Criteria andProfessionLessThan(String value) {
            addCriterion("PROFESSION <", value, "profession");
            return (Criteria) this;
        }

        public Criteria andProfessionLessThanOrEqualTo(String value) {
            addCriterion("PROFESSION <=", value, "profession");
            return (Criteria) this;
        }

        public Criteria andProfessionLike(String value) {
            addCriterion("PROFESSION like", value, "profession");
            return (Criteria) this;
        }

        public Criteria andProfessionNotLike(String value) {
            addCriterion("PROFESSION not like", value, "profession");
            return (Criteria) this;
        }

        public Criteria andProfessionIn(List<String> values) {
            addCriterion("PROFESSION in", values, "profession");
            return (Criteria) this;
        }

        public Criteria andProfessionNotIn(List<String> values) {
            addCriterion("PROFESSION not in", values, "profession");
            return (Criteria) this;
        }

        public Criteria andProfessionBetween(String value1, String value2) {
            addCriterion("PROFESSION between", value1, value2, "profession");
            return (Criteria) this;
        }

        public Criteria andProfessionNotBetween(String value1, String value2) {
            addCriterion("PROFESSION not between", value1, value2, "profession");
            return (Criteria) this;
        }

        public Criteria andGraduateschoolIsNull() {
            addCriterion("GRADUATESCHOOL is null");
            return (Criteria) this;
        }

        public Criteria andGraduateschoolIsNotNull() {
            addCriterion("GRADUATESCHOOL is not null");
            return (Criteria) this;
        }

        public Criteria andGraduateschoolEqualTo(String value) {
            addCriterion("GRADUATESCHOOL =", value, "graduateschool");
            return (Criteria) this;
        }

        public Criteria andGraduateschoolNotEqualTo(String value) {
            addCriterion("GRADUATESCHOOL <>", value, "graduateschool");
            return (Criteria) this;
        }

        public Criteria andGraduateschoolGreaterThan(String value) {
            addCriterion("GRADUATESCHOOL >", value, "graduateschool");
            return (Criteria) this;
        }

        public Criteria andGraduateschoolGreaterThanOrEqualTo(String value) {
            addCriterion("GRADUATESCHOOL >=", value, "graduateschool");
            return (Criteria) this;
        }

        public Criteria andGraduateschoolLessThan(String value) {
            addCriterion("GRADUATESCHOOL <", value, "graduateschool");
            return (Criteria) this;
        }

        public Criteria andGraduateschoolLessThanOrEqualTo(String value) {
            addCriterion("GRADUATESCHOOL <=", value, "graduateschool");
            return (Criteria) this;
        }

        public Criteria andGraduateschoolLike(String value) {
            addCriterion("GRADUATESCHOOL like", value, "graduateschool");
            return (Criteria) this;
        }

        public Criteria andGraduateschoolNotLike(String value) {
            addCriterion("GRADUATESCHOOL not like", value, "graduateschool");
            return (Criteria) this;
        }

        public Criteria andGraduateschoolIn(List<String> values) {
            addCriterion("GRADUATESCHOOL in", values, "graduateschool");
            return (Criteria) this;
        }

        public Criteria andGraduateschoolNotIn(List<String> values) {
            addCriterion("GRADUATESCHOOL not in", values, "graduateschool");
            return (Criteria) this;
        }

        public Criteria andGraduateschoolBetween(String value1, String value2) {
            addCriterion("GRADUATESCHOOL between", value1, value2, "graduateschool");
            return (Criteria) this;
        }

        public Criteria andGraduateschoolNotBetween(String value1, String value2) {
            addCriterion("GRADUATESCHOOL not between", value1, value2, "graduateschool");
            return (Criteria) this;
        }

        public Criteria andJobexperienceIsNull() {
            addCriterion("JOBEXPERIENCE is null");
            return (Criteria) this;
        }

        public Criteria andJobexperienceIsNotNull() {
            addCriterion("JOBEXPERIENCE is not null");
            return (Criteria) this;
        }

        public Criteria andJobexperienceEqualTo(String value) {
            addCriterion("JOBEXPERIENCE =", value, "jobexperience");
            return (Criteria) this;
        }

        public Criteria andJobexperienceNotEqualTo(String value) {
            addCriterion("JOBEXPERIENCE <>", value, "jobexperience");
            return (Criteria) this;
        }

        public Criteria andJobexperienceGreaterThan(String value) {
            addCriterion("JOBEXPERIENCE >", value, "jobexperience");
            return (Criteria) this;
        }

        public Criteria andJobexperienceGreaterThanOrEqualTo(String value) {
            addCriterion("JOBEXPERIENCE >=", value, "jobexperience");
            return (Criteria) this;
        }

        public Criteria andJobexperienceLessThan(String value) {
            addCriterion("JOBEXPERIENCE <", value, "jobexperience");
            return (Criteria) this;
        }

        public Criteria andJobexperienceLessThanOrEqualTo(String value) {
            addCriterion("JOBEXPERIENCE <=", value, "jobexperience");
            return (Criteria) this;
        }

        public Criteria andJobexperienceLike(String value) {
            addCriterion("JOBEXPERIENCE like", value, "jobexperience");
            return (Criteria) this;
        }

        public Criteria andJobexperienceNotLike(String value) {
            addCriterion("JOBEXPERIENCE not like", value, "jobexperience");
            return (Criteria) this;
        }

        public Criteria andJobexperienceIn(List<String> values) {
            addCriterion("JOBEXPERIENCE in", values, "jobexperience");
            return (Criteria) this;
        }

        public Criteria andJobexperienceNotIn(List<String> values) {
            addCriterion("JOBEXPERIENCE not in", values, "jobexperience");
            return (Criteria) this;
        }

        public Criteria andJobexperienceBetween(String value1, String value2) {
            addCriterion("JOBEXPERIENCE between", value1, value2, "jobexperience");
            return (Criteria) this;
        }

        public Criteria andJobexperienceNotBetween(String value1, String value2) {
            addCriterion("JOBEXPERIENCE not between", value1, value2, "jobexperience");
            return (Criteria) this;
        }

        public Criteria andBadremarkIsNull() {
            addCriterion("BADREMARK is null");
            return (Criteria) this;
        }

        public Criteria andBadremarkIsNotNull() {
            addCriterion("BADREMARK is not null");
            return (Criteria) this;
        }

        public Criteria andBadremarkEqualTo(String value) {
            addCriterion("BADREMARK =", value, "badremark");
            return (Criteria) this;
        }

        public Criteria andBadremarkNotEqualTo(String value) {
            addCriterion("BADREMARK <>", value, "badremark");
            return (Criteria) this;
        }

        public Criteria andBadremarkGreaterThan(String value) {
            addCriterion("BADREMARK >", value, "badremark");
            return (Criteria) this;
        }

        public Criteria andBadremarkGreaterThanOrEqualTo(String value) {
            addCriterion("BADREMARK >=", value, "badremark");
            return (Criteria) this;
        }

        public Criteria andBadremarkLessThan(String value) {
            addCriterion("BADREMARK <", value, "badremark");
            return (Criteria) this;
        }

        public Criteria andBadremarkLessThanOrEqualTo(String value) {
            addCriterion("BADREMARK <=", value, "badremark");
            return (Criteria) this;
        }

        public Criteria andBadremarkLike(String value) {
            addCriterion("BADREMARK like", value, "badremark");
            return (Criteria) this;
        }

        public Criteria andBadremarkNotLike(String value) {
            addCriterion("BADREMARK not like", value, "badremark");
            return (Criteria) this;
        }

        public Criteria andBadremarkIn(List<String> values) {
            addCriterion("BADREMARK in", values, "badremark");
            return (Criteria) this;
        }

        public Criteria andBadremarkNotIn(List<String> values) {
            addCriterion("BADREMARK not in", values, "badremark");
            return (Criteria) this;
        }

        public Criteria andBadremarkBetween(String value1, String value2) {
            addCriterion("BADREMARK between", value1, value2, "badremark");
            return (Criteria) this;
        }

        public Criteria andBadremarkNotBetween(String value1, String value2) {
            addCriterion("BADREMARK not between", value1, value2, "badremark");
            return (Criteria) this;
        }

        public Criteria andBadremarkstatusIsNull() {
            addCriterion("BADREMARKSTATUS is null");
            return (Criteria) this;
        }

        public Criteria andBadremarkstatusIsNotNull() {
            addCriterion("BADREMARKSTATUS is not null");
            return (Criteria) this;
        }

        public Criteria andBadremarkstatusEqualTo(String value) {
            addCriterion("BADREMARKSTATUS =", value, "badremarkstatus");
            return (Criteria) this;
        }

        public Criteria andBadremarkstatusNotEqualTo(String value) {
            addCriterion("BADREMARKSTATUS <>", value, "badremarkstatus");
            return (Criteria) this;
        }

        public Criteria andBadremarkstatusGreaterThan(String value) {
            addCriterion("BADREMARKSTATUS >", value, "badremarkstatus");
            return (Criteria) this;
        }

        public Criteria andBadremarkstatusGreaterThanOrEqualTo(String value) {
            addCriterion("BADREMARKSTATUS >=", value, "badremarkstatus");
            return (Criteria) this;
        }

        public Criteria andBadremarkstatusLessThan(String value) {
            addCriterion("BADREMARKSTATUS <", value, "badremarkstatus");
            return (Criteria) this;
        }

        public Criteria andBadremarkstatusLessThanOrEqualTo(String value) {
            addCriterion("BADREMARKSTATUS <=", value, "badremarkstatus");
            return (Criteria) this;
        }

        public Criteria andBadremarkstatusLike(String value) {
            addCriterion("BADREMARKSTATUS like", value, "badremarkstatus");
            return (Criteria) this;
        }

        public Criteria andBadremarkstatusNotLike(String value) {
            addCriterion("BADREMARKSTATUS not like", value, "badremarkstatus");
            return (Criteria) this;
        }

        public Criteria andBadremarkstatusIn(List<String> values) {
            addCriterion("BADREMARKSTATUS in", values, "badremarkstatus");
            return (Criteria) this;
        }

        public Criteria andBadremarkstatusNotIn(List<String> values) {
            addCriterion("BADREMARKSTATUS not in", values, "badremarkstatus");
            return (Criteria) this;
        }

        public Criteria andBadremarkstatusBetween(String value1, String value2) {
            addCriterion("BADREMARKSTATUS between", value1, value2, "badremarkstatus");
            return (Criteria) this;
        }

        public Criteria andBadremarkstatusNotBetween(String value1, String value2) {
            addCriterion("BADREMARKSTATUS not between", value1, value2, "badremarkstatus");
            return (Criteria) this;
        }

        public Criteria andStaffsequenceIsNull() {
            addCriterion("STAFFSEQUENCE is null");
            return (Criteria) this;
        }

        public Criteria andStaffsequenceIsNotNull() {
            addCriterion("STAFFSEQUENCE is not null");
            return (Criteria) this;
        }

        public Criteria andStaffsequenceEqualTo(String value) {
            addCriterion("STAFFSEQUENCE =", value, "staffsequence");
            return (Criteria) this;
        }

        public Criteria andStaffsequenceNotEqualTo(String value) {
            addCriterion("STAFFSEQUENCE <>", value, "staffsequence");
            return (Criteria) this;
        }

        public Criteria andStaffsequenceGreaterThan(String value) {
            addCriterion("STAFFSEQUENCE >", value, "staffsequence");
            return (Criteria) this;
        }

        public Criteria andStaffsequenceGreaterThanOrEqualTo(String value) {
            addCriterion("STAFFSEQUENCE >=", value, "staffsequence");
            return (Criteria) this;
        }

        public Criteria andStaffsequenceLessThan(String value) {
            addCriterion("STAFFSEQUENCE <", value, "staffsequence");
            return (Criteria) this;
        }

        public Criteria andStaffsequenceLessThanOrEqualTo(String value) {
            addCriterion("STAFFSEQUENCE <=", value, "staffsequence");
            return (Criteria) this;
        }

        public Criteria andStaffsequenceLike(String value) {
            addCriterion("STAFFSEQUENCE like", value, "staffsequence");
            return (Criteria) this;
        }

        public Criteria andStaffsequenceNotLike(String value) {
            addCriterion("STAFFSEQUENCE not like", value, "staffsequence");
            return (Criteria) this;
        }

        public Criteria andStaffsequenceIn(List<String> values) {
            addCriterion("STAFFSEQUENCE in", values, "staffsequence");
            return (Criteria) this;
        }

        public Criteria andStaffsequenceNotIn(List<String> values) {
            addCriterion("STAFFSEQUENCE not in", values, "staffsequence");
            return (Criteria) this;
        }

        public Criteria andStaffsequenceBetween(String value1, String value2) {
            addCriterion("STAFFSEQUENCE between", value1, value2, "staffsequence");
            return (Criteria) this;
        }

        public Criteria andStaffsequenceNotBetween(String value1, String value2) {
            addCriterion("STAFFSEQUENCE not between", value1, value2, "staffsequence");
            return (Criteria) this;
        }

        public Criteria andStaffrankIsNull() {
            addCriterion("STAFFRANK is null");
            return (Criteria) this;
        }

        public Criteria andStaffrankIsNotNull() {
            addCriterion("STAFFRANK is not null");
            return (Criteria) this;
        }

        public Criteria andStaffrankEqualTo(String value) {
            addCriterion("STAFFRANK =", value, "staffrank");
            return (Criteria) this;
        }

        public Criteria andStaffrankNotEqualTo(String value) {
            addCriterion("STAFFRANK <>", value, "staffrank");
            return (Criteria) this;
        }

        public Criteria andStaffrankGreaterThan(String value) {
            addCriterion("STAFFRANK >", value, "staffrank");
            return (Criteria) this;
        }

        public Criteria andStaffrankGreaterThanOrEqualTo(String value) {
            addCriterion("STAFFRANK >=", value, "staffrank");
            return (Criteria) this;
        }

        public Criteria andStaffrankLessThan(String value) {
            addCriterion("STAFFRANK <", value, "staffrank");
            return (Criteria) this;
        }

        public Criteria andStaffrankLessThanOrEqualTo(String value) {
            addCriterion("STAFFRANK <=", value, "staffrank");
            return (Criteria) this;
        }

        public Criteria andStaffrankLike(String value) {
            addCriterion("STAFFRANK like", value, "staffrank");
            return (Criteria) this;
        }

        public Criteria andStaffrankNotLike(String value) {
            addCriterion("STAFFRANK not like", value, "staffrank");
            return (Criteria) this;
        }

        public Criteria andStaffrankIn(List<String> values) {
            addCriterion("STAFFRANK in", values, "staffrank");
            return (Criteria) this;
        }

        public Criteria andStaffrankNotIn(List<String> values) {
            addCriterion("STAFFRANK not in", values, "staffrank");
            return (Criteria) this;
        }

        public Criteria andStaffrankBetween(String value1, String value2) {
            addCriterion("STAFFRANK between", value1, value2, "staffrank");
            return (Criteria) this;
        }

        public Criteria andStaffrankNotBetween(String value1, String value2) {
            addCriterion("STAFFRANK not between", value1, value2, "staffrank");
            return (Criteria) this;
        }

        public Criteria andWorkingdateIsNull() {
            addCriterion("WORKINGDATE is null");
            return (Criteria) this;
        }

        public Criteria andWorkingdateIsNotNull() {
            addCriterion("WORKINGDATE is not null");
            return (Criteria) this;
        }

        public Criteria andWorkingdateEqualTo(Date value) {
            addCriterion("WORKINGDATE =", value, "workingdate");
            return (Criteria) this;
        }

        public Criteria andWorkingdateNotEqualTo(Date value) {
            addCriterion("WORKINGDATE <>", value, "workingdate");
            return (Criteria) this;
        }

        public Criteria andWorkingdateGreaterThan(Date value) {
            addCriterion("WORKINGDATE >", value, "workingdate");
            return (Criteria) this;
        }

        public Criteria andWorkingdateGreaterThanOrEqualTo(Date value) {
            addCriterion("WORKINGDATE >=", value, "workingdate");
            return (Criteria) this;
        }

        public Criteria andWorkingdateLessThan(Date value) {
            addCriterion("WORKINGDATE <", value, "workingdate");
            return (Criteria) this;
        }

        public Criteria andWorkingdateLessThanOrEqualTo(Date value) {
            addCriterion("WORKINGDATE <=", value, "workingdate");
            return (Criteria) this;
        }

        public Criteria andWorkingdateIn(List<Date> values) {
            addCriterion("WORKINGDATE in", values, "workingdate");
            return (Criteria) this;
        }

        public Criteria andWorkingdateNotIn(List<Date> values) {
            addCriterion("WORKINGDATE not in", values, "workingdate");
            return (Criteria) this;
        }

        public Criteria andWorkingdateBetween(Date value1, Date value2) {
            addCriterion("WORKINGDATE between", value1, value2, "workingdate");
            return (Criteria) this;
        }

        public Criteria andWorkingdateNotBetween(Date value1, Date value2) {
            addCriterion("WORKINGDATE not between", value1, value2, "workingdate");
            return (Criteria) this;
        }

        public Criteria andBusinessplanIsNull() {
            addCriterion("BUSINESSPLAN is null");
            return (Criteria) this;
        }

        public Criteria andBusinessplanIsNotNull() {
            addCriterion("BUSINESSPLAN is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessplanEqualTo(BigDecimal value) {
            addCriterion("BUSINESSPLAN =", value, "businessplan");
            return (Criteria) this;
        }

        public Criteria andBusinessplanNotEqualTo(BigDecimal value) {
            addCriterion("BUSINESSPLAN <>", value, "businessplan");
            return (Criteria) this;
        }

        public Criteria andBusinessplanGreaterThan(BigDecimal value) {
            addCriterion("BUSINESSPLAN >", value, "businessplan");
            return (Criteria) this;
        }

        public Criteria andBusinessplanGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("BUSINESSPLAN >=", value, "businessplan");
            return (Criteria) this;
        }

        public Criteria andBusinessplanLessThan(BigDecimal value) {
            addCriterion("BUSINESSPLAN <", value, "businessplan");
            return (Criteria) this;
        }

        public Criteria andBusinessplanLessThanOrEqualTo(BigDecimal value) {
            addCriterion("BUSINESSPLAN <=", value, "businessplan");
            return (Criteria) this;
        }

        public Criteria andBusinessplanIn(List<BigDecimal> values) {
            addCriterion("BUSINESSPLAN in", values, "businessplan");
            return (Criteria) this;
        }

        public Criteria andBusinessplanNotIn(List<BigDecimal> values) {
            addCriterion("BUSINESSPLAN not in", values, "businessplan");
            return (Criteria) this;
        }

        public Criteria andBusinessplanBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("BUSINESSPLAN between", value1, value2, "businessplan");
            return (Criteria) this;
        }

        public Criteria andBusinessplanNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("BUSINESSPLAN not between", value1, value2, "businessplan");
            return (Criteria) this;
        }

        public Criteria andCompletedpremiumIsNull() {
            addCriterion("COMPLETEDPREMIUM is null");
            return (Criteria) this;
        }

        public Criteria andCompletedpremiumIsNotNull() {
            addCriterion("COMPLETEDPREMIUM is not null");
            return (Criteria) this;
        }

        public Criteria andCompletedpremiumEqualTo(BigDecimal value) {
            addCriterion("COMPLETEDPREMIUM =", value, "completedpremium");
            return (Criteria) this;
        }

        public Criteria andCompletedpremiumNotEqualTo(BigDecimal value) {
            addCriterion("COMPLETEDPREMIUM <>", value, "completedpremium");
            return (Criteria) this;
        }

        public Criteria andCompletedpremiumGreaterThan(BigDecimal value) {
            addCriterion("COMPLETEDPREMIUM >", value, "completedpremium");
            return (Criteria) this;
        }

        public Criteria andCompletedpremiumGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("COMPLETEDPREMIUM >=", value, "completedpremium");
            return (Criteria) this;
        }

        public Criteria andCompletedpremiumLessThan(BigDecimal value) {
            addCriterion("COMPLETEDPREMIUM <", value, "completedpremium");
            return (Criteria) this;
        }

        public Criteria andCompletedpremiumLessThanOrEqualTo(BigDecimal value) {
            addCriterion("COMPLETEDPREMIUM <=", value, "completedpremium");
            return (Criteria) this;
        }

        public Criteria andCompletedpremiumIn(List<BigDecimal> values) {
            addCriterion("COMPLETEDPREMIUM in", values, "completedpremium");
            return (Criteria) this;
        }

        public Criteria andCompletedpremiumNotIn(List<BigDecimal> values) {
            addCriterion("COMPLETEDPREMIUM not in", values, "completedpremium");
            return (Criteria) this;
        }

        public Criteria andCompletedpremiumBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("COMPLETEDPREMIUM between", value1, value2, "completedpremium");
            return (Criteria) this;
        }

        public Criteria andCompletedpremiumNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("COMPLETEDPREMIUM not between", value1, value2, "completedpremium");
            return (Criteria) this;
        }

        public Criteria andExhibitionnoIsNull() {
            addCriterion("EXHIBITIONNO is null");
            return (Criteria) this;
        }

        public Criteria andExhibitionnoIsNotNull() {
            addCriterion("EXHIBITIONNO is not null");
            return (Criteria) this;
        }

        public Criteria andExhibitionnoEqualTo(String value) {
            addCriterion("EXHIBITIONNO =", value, "exhibitionno");
            return (Criteria) this;
        }

        public Criteria andExhibitionnoNotEqualTo(String value) {
            addCriterion("EXHIBITIONNO <>", value, "exhibitionno");
            return (Criteria) this;
        }

        public Criteria andExhibitionnoGreaterThan(String value) {
            addCriterion("EXHIBITIONNO >", value, "exhibitionno");
            return (Criteria) this;
        }

        public Criteria andExhibitionnoGreaterThanOrEqualTo(String value) {
            addCriterion("EXHIBITIONNO >=", value, "exhibitionno");
            return (Criteria) this;
        }

        public Criteria andExhibitionnoLessThan(String value) {
            addCriterion("EXHIBITIONNO <", value, "exhibitionno");
            return (Criteria) this;
        }

        public Criteria andExhibitionnoLessThanOrEqualTo(String value) {
            addCriterion("EXHIBITIONNO <=", value, "exhibitionno");
            return (Criteria) this;
        }

        public Criteria andExhibitionnoLike(String value) {
            addCriterion("EXHIBITIONNO like", value, "exhibitionno");
            return (Criteria) this;
        }

        public Criteria andExhibitionnoNotLike(String value) {
            addCriterion("EXHIBITIONNO not like", value, "exhibitionno");
            return (Criteria) this;
        }

        public Criteria andExhibitionnoIn(List<String> values) {
            addCriterion("EXHIBITIONNO in", values, "exhibitionno");
            return (Criteria) this;
        }

        public Criteria andExhibitionnoNotIn(List<String> values) {
            addCriterion("EXHIBITIONNO not in", values, "exhibitionno");
            return (Criteria) this;
        }

        public Criteria andExhibitionnoBetween(String value1, String value2) {
            addCriterion("EXHIBITIONNO between", value1, value2, "exhibitionno");
            return (Criteria) this;
        }

        public Criteria andExhibitionnoNotBetween(String value1, String value2) {
            addCriterion("EXHIBITIONNO not between", value1, value2, "exhibitionno");
            return (Criteria) this;
        }

        public Criteria andContractnoIsNull() {
            addCriterion("CONTRACTNO is null");
            return (Criteria) this;
        }

        public Criteria andContractnoIsNotNull() {
            addCriterion("CONTRACTNO is not null");
            return (Criteria) this;
        }

        public Criteria andContractnoEqualTo(String value) {
            addCriterion("CONTRACTNO =", value, "contractno");
            return (Criteria) this;
        }

        public Criteria andContractnoNotEqualTo(String value) {
            addCriterion("CONTRACTNO <>", value, "contractno");
            return (Criteria) this;
        }

        public Criteria andContractnoGreaterThan(String value) {
            addCriterion("CONTRACTNO >", value, "contractno");
            return (Criteria) this;
        }

        public Criteria andContractnoGreaterThanOrEqualTo(String value) {
            addCriterion("CONTRACTNO >=", value, "contractno");
            return (Criteria) this;
        }

        public Criteria andContractnoLessThan(String value) {
            addCriterion("CONTRACTNO <", value, "contractno");
            return (Criteria) this;
        }

        public Criteria andContractnoLessThanOrEqualTo(String value) {
            addCriterion("CONTRACTNO <=", value, "contractno");
            return (Criteria) this;
        }

        public Criteria andContractnoLike(String value) {
            addCriterion("CONTRACTNO like", value, "contractno");
            return (Criteria) this;
        }

        public Criteria andContractnoNotLike(String value) {
            addCriterion("CONTRACTNO not like", value, "contractno");
            return (Criteria) this;
        }

        public Criteria andContractnoIn(List<String> values) {
            addCriterion("CONTRACTNO in", values, "contractno");
            return (Criteria) this;
        }

        public Criteria andContractnoNotIn(List<String> values) {
            addCriterion("CONTRACTNO not in", values, "contractno");
            return (Criteria) this;
        }

        public Criteria andContractnoBetween(String value1, String value2) {
            addCriterion("CONTRACTNO between", value1, value2, "contractno");
            return (Criteria) this;
        }

        public Criteria andContractnoNotBetween(String value1, String value2) {
            addCriterion("CONTRACTNO not between", value1, value2, "contractno");
            return (Criteria) this;
        }

        public Criteria andFundaccountIsNull() {
            addCriterion("FUNDACCOUNT is null");
            return (Criteria) this;
        }

        public Criteria andFundaccountIsNotNull() {
            addCriterion("FUNDACCOUNT is not null");
            return (Criteria) this;
        }

        public Criteria andFundaccountEqualTo(String value) {
            addCriterion("FUNDACCOUNT =", value, "fundaccount");
            return (Criteria) this;
        }

        public Criteria andFundaccountNotEqualTo(String value) {
            addCriterion("FUNDACCOUNT <>", value, "fundaccount");
            return (Criteria) this;
        }

        public Criteria andFundaccountGreaterThan(String value) {
            addCriterion("FUNDACCOUNT >", value, "fundaccount");
            return (Criteria) this;
        }

        public Criteria andFundaccountGreaterThanOrEqualTo(String value) {
            addCriterion("FUNDACCOUNT >=", value, "fundaccount");
            return (Criteria) this;
        }

        public Criteria andFundaccountLessThan(String value) {
            addCriterion("FUNDACCOUNT <", value, "fundaccount");
            return (Criteria) this;
        }

        public Criteria andFundaccountLessThanOrEqualTo(String value) {
            addCriterion("FUNDACCOUNT <=", value, "fundaccount");
            return (Criteria) this;
        }

        public Criteria andFundaccountLike(String value) {
            addCriterion("FUNDACCOUNT like", value, "fundaccount");
            return (Criteria) this;
        }

        public Criteria andFundaccountNotLike(String value) {
            addCriterion("FUNDACCOUNT not like", value, "fundaccount");
            return (Criteria) this;
        }

        public Criteria andFundaccountIn(List<String> values) {
            addCriterion("FUNDACCOUNT in", values, "fundaccount");
            return (Criteria) this;
        }

        public Criteria andFundaccountNotIn(List<String> values) {
            addCriterion("FUNDACCOUNT not in", values, "fundaccount");
            return (Criteria) this;
        }

        public Criteria andFundaccountBetween(String value1, String value2) {
            addCriterion("FUNDACCOUNT between", value1, value2, "fundaccount");
            return (Criteria) this;
        }

        public Criteria andFundaccountNotBetween(String value1, String value2) {
            addCriterion("FUNDACCOUNT not between", value1, value2, "fundaccount");
            return (Criteria) this;
        }

        public Criteria andFundaccount1IsNull() {
            addCriterion("FUNDACCOUNT1 is null");
            return (Criteria) this;
        }

        public Criteria andFundaccount1IsNotNull() {
            addCriterion("FUNDACCOUNT1 is not null");
            return (Criteria) this;
        }

        public Criteria andFundaccount1EqualTo(String value) {
            addCriterion("FUNDACCOUNT1 =", value, "fundaccount1");
            return (Criteria) this;
        }

        public Criteria andFundaccount1NotEqualTo(String value) {
            addCriterion("FUNDACCOUNT1 <>", value, "fundaccount1");
            return (Criteria) this;
        }

        public Criteria andFundaccount1GreaterThan(String value) {
            addCriterion("FUNDACCOUNT1 >", value, "fundaccount1");
            return (Criteria) this;
        }

        public Criteria andFundaccount1GreaterThanOrEqualTo(String value) {
            addCriterion("FUNDACCOUNT1 >=", value, "fundaccount1");
            return (Criteria) this;
        }

        public Criteria andFundaccount1LessThan(String value) {
            addCriterion("FUNDACCOUNT1 <", value, "fundaccount1");
            return (Criteria) this;
        }

        public Criteria andFundaccount1LessThanOrEqualTo(String value) {
            addCriterion("FUNDACCOUNT1 <=", value, "fundaccount1");
            return (Criteria) this;
        }

        public Criteria andFundaccount1Like(String value) {
            addCriterion("FUNDACCOUNT1 like", value, "fundaccount1");
            return (Criteria) this;
        }

        public Criteria andFundaccount1NotLike(String value) {
            addCriterion("FUNDACCOUNT1 not like", value, "fundaccount1");
            return (Criteria) this;
        }

        public Criteria andFundaccount1In(List<String> values) {
            addCriterion("FUNDACCOUNT1 in", values, "fundaccount1");
            return (Criteria) this;
        }

        public Criteria andFundaccount1NotIn(List<String> values) {
            addCriterion("FUNDACCOUNT1 not in", values, "fundaccount1");
            return (Criteria) this;
        }

        public Criteria andFundaccount1Between(String value1, String value2) {
            addCriterion("FUNDACCOUNT1 between", value1, value2, "fundaccount1");
            return (Criteria) this;
        }

        public Criteria andFundaccount1NotBetween(String value1, String value2) {
            addCriterion("FUNDACCOUNT1 not between", value1, value2, "fundaccount1");
            return (Criteria) this;
        }

        public Criteria andIncreasenumIsNull() {
            addCriterion("INCREASENUM is null");
            return (Criteria) this;
        }

        public Criteria andIncreasenumIsNotNull() {
            addCriterion("INCREASENUM is not null");
            return (Criteria) this;
        }

        public Criteria andIncreasenumEqualTo(String value) {
            addCriterion("INCREASENUM =", value, "increasenum");
            return (Criteria) this;
        }

        public Criteria andIncreasenumNotEqualTo(String value) {
            addCriterion("INCREASENUM <>", value, "increasenum");
            return (Criteria) this;
        }

        public Criteria andIncreasenumGreaterThan(String value) {
            addCriterion("INCREASENUM >", value, "increasenum");
            return (Criteria) this;
        }

        public Criteria andIncreasenumGreaterThanOrEqualTo(String value) {
            addCriterion("INCREASENUM >=", value, "increasenum");
            return (Criteria) this;
        }

        public Criteria andIncreasenumLessThan(String value) {
            addCriterion("INCREASENUM <", value, "increasenum");
            return (Criteria) this;
        }

        public Criteria andIncreasenumLessThanOrEqualTo(String value) {
            addCriterion("INCREASENUM <=", value, "increasenum");
            return (Criteria) this;
        }

        public Criteria andIncreasenumLike(String value) {
            addCriterion("INCREASENUM like", value, "increasenum");
            return (Criteria) this;
        }

        public Criteria andIncreasenumNotLike(String value) {
            addCriterion("INCREASENUM not like", value, "increasenum");
            return (Criteria) this;
        }

        public Criteria andIncreasenumIn(List<String> values) {
            addCriterion("INCREASENUM in", values, "increasenum");
            return (Criteria) this;
        }

        public Criteria andIncreasenumNotIn(List<String> values) {
            addCriterion("INCREASENUM not in", values, "increasenum");
            return (Criteria) this;
        }

        public Criteria andIncreasenumBetween(String value1, String value2) {
            addCriterion("INCREASENUM between", value1, value2, "increasenum");
            return (Criteria) this;
        }

        public Criteria andIncreasenumNotBetween(String value1, String value2) {
            addCriterion("INCREASENUM not between", value1, value2, "increasenum");
            return (Criteria) this;
        }

        public Criteria andIncreasenameIsNull() {
            addCriterion("INCREASENAME is null");
            return (Criteria) this;
        }

        public Criteria andIncreasenameIsNotNull() {
            addCriterion("INCREASENAME is not null");
            return (Criteria) this;
        }

        public Criteria andIncreasenameEqualTo(String value) {
            addCriterion("INCREASENAME =", value, "increasename");
            return (Criteria) this;
        }

        public Criteria andIncreasenameNotEqualTo(String value) {
            addCriterion("INCREASENAME <>", value, "increasename");
            return (Criteria) this;
        }

        public Criteria andIncreasenameGreaterThan(String value) {
            addCriterion("INCREASENAME >", value, "increasename");
            return (Criteria) this;
        }

        public Criteria andIncreasenameGreaterThanOrEqualTo(String value) {
            addCriterion("INCREASENAME >=", value, "increasename");
            return (Criteria) this;
        }

        public Criteria andIncreasenameLessThan(String value) {
            addCriterion("INCREASENAME <", value, "increasename");
            return (Criteria) this;
        }

        public Criteria andIncreasenameLessThanOrEqualTo(String value) {
            addCriterion("INCREASENAME <=", value, "increasename");
            return (Criteria) this;
        }

        public Criteria andIncreasenameLike(String value) {
            addCriterion("INCREASENAME like", value, "increasename");
            return (Criteria) this;
        }

        public Criteria andIncreasenameNotLike(String value) {
            addCriterion("INCREASENAME not like", value, "increasename");
            return (Criteria) this;
        }

        public Criteria andIncreasenameIn(List<String> values) {
            addCriterion("INCREASENAME in", values, "increasename");
            return (Criteria) this;
        }

        public Criteria andIncreasenameNotIn(List<String> values) {
            addCriterion("INCREASENAME not in", values, "increasename");
            return (Criteria) this;
        }

        public Criteria andIncreasenameBetween(String value1, String value2) {
            addCriterion("INCREASENAME between", value1, value2, "increasename");
            return (Criteria) this;
        }

        public Criteria andIncreasenameNotBetween(String value1, String value2) {
            addCriterion("INCREASENAME not between", value1, value2, "increasename");
            return (Criteria) this;
        }

        public Criteria andHonorIsNull() {
            addCriterion("HONOR is null");
            return (Criteria) this;
        }

        public Criteria andHonorIsNotNull() {
            addCriterion("HONOR is not null");
            return (Criteria) this;
        }

        public Criteria andHonorEqualTo(String value) {
            addCriterion("HONOR =", value, "honor");
            return (Criteria) this;
        }

        public Criteria andHonorNotEqualTo(String value) {
            addCriterion("HONOR <>", value, "honor");
            return (Criteria) this;
        }

        public Criteria andHonorGreaterThan(String value) {
            addCriterion("HONOR >", value, "honor");
            return (Criteria) this;
        }

        public Criteria andHonorGreaterThanOrEqualTo(String value) {
            addCriterion("HONOR >=", value, "honor");
            return (Criteria) this;
        }

        public Criteria andHonorLessThan(String value) {
            addCriterion("HONOR <", value, "honor");
            return (Criteria) this;
        }

        public Criteria andHonorLessThanOrEqualTo(String value) {
            addCriterion("HONOR <=", value, "honor");
            return (Criteria) this;
        }

        public Criteria andHonorLike(String value) {
            addCriterion("HONOR like", value, "honor");
            return (Criteria) this;
        }

        public Criteria andHonorNotLike(String value) {
            addCriterion("HONOR not like", value, "honor");
            return (Criteria) this;
        }

        public Criteria andHonorIn(List<String> values) {
            addCriterion("HONOR in", values, "honor");
            return (Criteria) this;
        }

        public Criteria andHonorNotIn(List<String> values) {
            addCriterion("HONOR not in", values, "honor");
            return (Criteria) this;
        }

        public Criteria andHonorBetween(String value1, String value2) {
            addCriterion("HONOR between", value1, value2, "honor");
            return (Criteria) this;
        }

        public Criteria andHonorNotBetween(String value1, String value2) {
            addCriterion("HONOR not between", value1, value2, "honor");
            return (Criteria) this;
        }

        public Criteria andTrainingplanIsNull() {
            addCriterion("TRAININGPLAN is null");
            return (Criteria) this;
        }

        public Criteria andTrainingplanIsNotNull() {
            addCriterion("TRAININGPLAN is not null");
            return (Criteria) this;
        }

        public Criteria andTrainingplanEqualTo(String value) {
            addCriterion("TRAININGPLAN =", value, "trainingplan");
            return (Criteria) this;
        }

        public Criteria andTrainingplanNotEqualTo(String value) {
            addCriterion("TRAININGPLAN <>", value, "trainingplan");
            return (Criteria) this;
        }

        public Criteria andTrainingplanGreaterThan(String value) {
            addCriterion("TRAININGPLAN >", value, "trainingplan");
            return (Criteria) this;
        }

        public Criteria andTrainingplanGreaterThanOrEqualTo(String value) {
            addCriterion("TRAININGPLAN >=", value, "trainingplan");
            return (Criteria) this;
        }

        public Criteria andTrainingplanLessThan(String value) {
            addCriterion("TRAININGPLAN <", value, "trainingplan");
            return (Criteria) this;
        }

        public Criteria andTrainingplanLessThanOrEqualTo(String value) {
            addCriterion("TRAININGPLAN <=", value, "trainingplan");
            return (Criteria) this;
        }

        public Criteria andTrainingplanLike(String value) {
            addCriterion("TRAININGPLAN like", value, "trainingplan");
            return (Criteria) this;
        }

        public Criteria andTrainingplanNotLike(String value) {
            addCriterion("TRAININGPLAN not like", value, "trainingplan");
            return (Criteria) this;
        }

        public Criteria andTrainingplanIn(List<String> values) {
            addCriterion("TRAININGPLAN in", values, "trainingplan");
            return (Criteria) this;
        }

        public Criteria andTrainingplanNotIn(List<String> values) {
            addCriterion("TRAININGPLAN not in", values, "trainingplan");
            return (Criteria) this;
        }

        public Criteria andTrainingplanBetween(String value1, String value2) {
            addCriterion("TRAININGPLAN between", value1, value2, "trainingplan");
            return (Criteria) this;
        }

        public Criteria andTrainingplanNotBetween(String value1, String value2) {
            addCriterion("TRAININGPLAN not between", value1, value2, "trainingplan");
            return (Criteria) this;
        }

        public Criteria andBigagenttypeIsNull() {
            addCriterion("BIGAGENTTYPE is null");
            return (Criteria) this;
        }

        public Criteria andBigagenttypeIsNotNull() {
            addCriterion("BIGAGENTTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andBigagenttypeEqualTo(String value) {
            addCriterion("BIGAGENTTYPE =", value, "bigagenttype");
            return (Criteria) this;
        }

        public Criteria andBigagenttypeNotEqualTo(String value) {
            addCriterion("BIGAGENTTYPE <>", value, "bigagenttype");
            return (Criteria) this;
        }

        public Criteria andBigagenttypeGreaterThan(String value) {
            addCriterion("BIGAGENTTYPE >", value, "bigagenttype");
            return (Criteria) this;
        }

        public Criteria andBigagenttypeGreaterThanOrEqualTo(String value) {
            addCriterion("BIGAGENTTYPE >=", value, "bigagenttype");
            return (Criteria) this;
        }

        public Criteria andBigagenttypeLessThan(String value) {
            addCriterion("BIGAGENTTYPE <", value, "bigagenttype");
            return (Criteria) this;
        }

        public Criteria andBigagenttypeLessThanOrEqualTo(String value) {
            addCriterion("BIGAGENTTYPE <=", value, "bigagenttype");
            return (Criteria) this;
        }

        public Criteria andBigagenttypeLike(String value) {
            addCriterion("BIGAGENTTYPE like", value, "bigagenttype");
            return (Criteria) this;
        }

        public Criteria andBigagenttypeNotLike(String value) {
            addCriterion("BIGAGENTTYPE not like", value, "bigagenttype");
            return (Criteria) this;
        }

        public Criteria andBigagenttypeIn(List<String> values) {
            addCriterion("BIGAGENTTYPE in", values, "bigagenttype");
            return (Criteria) this;
        }

        public Criteria andBigagenttypeNotIn(List<String> values) {
            addCriterion("BIGAGENTTYPE not in", values, "bigagenttype");
            return (Criteria) this;
        }

        public Criteria andBigagenttypeBetween(String value1, String value2) {
            addCriterion("BIGAGENTTYPE between", value1, value2, "bigagenttype");
            return (Criteria) this;
        }

        public Criteria andBigagenttypeNotBetween(String value1, String value2) {
            addCriterion("BIGAGENTTYPE not between", value1, value2, "bigagenttype");
            return (Criteria) this;
        }

        public Criteria andCredentialsnoIsNull() {
            addCriterion("CREDENTIALSNO is null");
            return (Criteria) this;
        }

        public Criteria andCredentialsnoIsNotNull() {
            addCriterion("CREDENTIALSNO is not null");
            return (Criteria) this;
        }

        public Criteria andCredentialsnoEqualTo(String value) {
            addCriterion("CREDENTIALSNO =", value, "credentialsno");
            return (Criteria) this;
        }

        public Criteria andCredentialsnoNotEqualTo(String value) {
            addCriterion("CREDENTIALSNO <>", value, "credentialsno");
            return (Criteria) this;
        }

        public Criteria andCredentialsnoGreaterThan(String value) {
            addCriterion("CREDENTIALSNO >", value, "credentialsno");
            return (Criteria) this;
        }

        public Criteria andCredentialsnoGreaterThanOrEqualTo(String value) {
            addCriterion("CREDENTIALSNO >=", value, "credentialsno");
            return (Criteria) this;
        }

        public Criteria andCredentialsnoLessThan(String value) {
            addCriterion("CREDENTIALSNO <", value, "credentialsno");
            return (Criteria) this;
        }

        public Criteria andCredentialsnoLessThanOrEqualTo(String value) {
            addCriterion("CREDENTIALSNO <=", value, "credentialsno");
            return (Criteria) this;
        }

        public Criteria andCredentialsnoLike(String value) {
            addCriterion("CREDENTIALSNO like", value, "credentialsno");
            return (Criteria) this;
        }

        public Criteria andCredentialsnoNotLike(String value) {
            addCriterion("CREDENTIALSNO not like", value, "credentialsno");
            return (Criteria) this;
        }

        public Criteria andCredentialsnoIn(List<String> values) {
            addCriterion("CREDENTIALSNO in", values, "credentialsno");
            return (Criteria) this;
        }

        public Criteria andCredentialsnoNotIn(List<String> values) {
            addCriterion("CREDENTIALSNO not in", values, "credentialsno");
            return (Criteria) this;
        }

        public Criteria andCredentialsnoBetween(String value1, String value2) {
            addCriterion("CREDENTIALSNO between", value1, value2, "credentialsno");
            return (Criteria) this;
        }

        public Criteria andCredentialsnoNotBetween(String value1, String value2) {
            addCriterion("CREDENTIALSNO not between", value1, value2, "credentialsno");
            return (Criteria) this;
        }

        public Criteria andCredentialsstartdateIsNull() {
            addCriterion("CREDENTIALSSTARTDATE is null");
            return (Criteria) this;
        }

        public Criteria andCredentialsstartdateIsNotNull() {
            addCriterion("CREDENTIALSSTARTDATE is not null");
            return (Criteria) this;
        }

        public Criteria andCredentialsstartdateEqualTo(Date value) {
            addCriterion("CREDENTIALSSTARTDATE =", value, "credentialsstartdate");
            return (Criteria) this;
        }

        public Criteria andCredentialsstartdateNotEqualTo(Date value) {
            addCriterion("CREDENTIALSSTARTDATE <>", value, "credentialsstartdate");
            return (Criteria) this;
        }

        public Criteria andCredentialsstartdateGreaterThan(Date value) {
            addCriterion("CREDENTIALSSTARTDATE >", value, "credentialsstartdate");
            return (Criteria) this;
        }

        public Criteria andCredentialsstartdateGreaterThanOrEqualTo(Date value) {
            addCriterion("CREDENTIALSSTARTDATE >=", value, "credentialsstartdate");
            return (Criteria) this;
        }

        public Criteria andCredentialsstartdateLessThan(Date value) {
            addCriterion("CREDENTIALSSTARTDATE <", value, "credentialsstartdate");
            return (Criteria) this;
        }

        public Criteria andCredentialsstartdateLessThanOrEqualTo(Date value) {
            addCriterion("CREDENTIALSSTARTDATE <=", value, "credentialsstartdate");
            return (Criteria) this;
        }

        public Criteria andCredentialsstartdateIn(List<Date> values) {
            addCriterion("CREDENTIALSSTARTDATE in", values, "credentialsstartdate");
            return (Criteria) this;
        }

        public Criteria andCredentialsstartdateNotIn(List<Date> values) {
            addCriterion("CREDENTIALSSTARTDATE not in", values, "credentialsstartdate");
            return (Criteria) this;
        }

        public Criteria andCredentialsstartdateBetween(Date value1, Date value2) {
            addCriterion("CREDENTIALSSTARTDATE between", value1, value2, "credentialsstartdate");
            return (Criteria) this;
        }

        public Criteria andCredentialsstartdateNotBetween(Date value1, Date value2) {
            addCriterion("CREDENTIALSSTARTDATE not between", value1, value2, "credentialsstartdate");
            return (Criteria) this;
        }

        public Criteria andCredentialsenddateIsNull() {
            addCriterion("CREDENTIALSENDDATE is null");
            return (Criteria) this;
        }

        public Criteria andCredentialsenddateIsNotNull() {
            addCriterion("CREDENTIALSENDDATE is not null");
            return (Criteria) this;
        }

        public Criteria andCredentialsenddateEqualTo(Date value) {
            addCriterion("CREDENTIALSENDDATE =", value, "credentialsenddate");
            return (Criteria) this;
        }

        public Criteria andCredentialsenddateNotEqualTo(Date value) {
            addCriterion("CREDENTIALSENDDATE <>", value, "credentialsenddate");
            return (Criteria) this;
        }

        public Criteria andCredentialsenddateGreaterThan(Date value) {
            addCriterion("CREDENTIALSENDDATE >", value, "credentialsenddate");
            return (Criteria) this;
        }

        public Criteria andCredentialsenddateGreaterThanOrEqualTo(Date value) {
            addCriterion("CREDENTIALSENDDATE >=", value, "credentialsenddate");
            return (Criteria) this;
        }

        public Criteria andCredentialsenddateLessThan(Date value) {
            addCriterion("CREDENTIALSENDDATE <", value, "credentialsenddate");
            return (Criteria) this;
        }

        public Criteria andCredentialsenddateLessThanOrEqualTo(Date value) {
            addCriterion("CREDENTIALSENDDATE <=", value, "credentialsenddate");
            return (Criteria) this;
        }

        public Criteria andCredentialsenddateIn(List<Date> values) {
            addCriterion("CREDENTIALSENDDATE in", values, "credentialsenddate");
            return (Criteria) this;
        }

        public Criteria andCredentialsenddateNotIn(List<Date> values) {
            addCriterion("CREDENTIALSENDDATE not in", values, "credentialsenddate");
            return (Criteria) this;
        }

        public Criteria andCredentialsenddateBetween(Date value1, Date value2) {
            addCriterion("CREDENTIALSENDDATE between", value1, value2, "credentialsenddate");
            return (Criteria) this;
        }

        public Criteria andCredentialsenddateNotBetween(Date value1, Date value2) {
            addCriterion("CREDENTIALSENDDATE not between", value1, value2, "credentialsenddate");
            return (Criteria) this;
        }

        public Criteria andAccountnameIsNull() {
            addCriterion("ACCOUNTNAME is null");
            return (Criteria) this;
        }

        public Criteria andAccountnameIsNotNull() {
            addCriterion("ACCOUNTNAME is not null");
            return (Criteria) this;
        }

        public Criteria andAccountnameEqualTo(String value) {
            addCriterion("ACCOUNTNAME =", value, "accountname");
            return (Criteria) this;
        }

        public Criteria andAccountnameNotEqualTo(String value) {
            addCriterion("ACCOUNTNAME <>", value, "accountname");
            return (Criteria) this;
        }

        public Criteria andAccountnameGreaterThan(String value) {
            addCriterion("ACCOUNTNAME >", value, "accountname");
            return (Criteria) this;
        }

        public Criteria andAccountnameGreaterThanOrEqualTo(String value) {
            addCriterion("ACCOUNTNAME >=", value, "accountname");
            return (Criteria) this;
        }

        public Criteria andAccountnameLessThan(String value) {
            addCriterion("ACCOUNTNAME <", value, "accountname");
            return (Criteria) this;
        }

        public Criteria andAccountnameLessThanOrEqualTo(String value) {
            addCriterion("ACCOUNTNAME <=", value, "accountname");
            return (Criteria) this;
        }

        public Criteria andAccountnameLike(String value) {
            addCriterion("ACCOUNTNAME like", value, "accountname");
            return (Criteria) this;
        }

        public Criteria andAccountnameNotLike(String value) {
            addCriterion("ACCOUNTNAME not like", value, "accountname");
            return (Criteria) this;
        }

        public Criteria andAccountnameIn(List<String> values) {
            addCriterion("ACCOUNTNAME in", values, "accountname");
            return (Criteria) this;
        }

        public Criteria andAccountnameNotIn(List<String> values) {
            addCriterion("ACCOUNTNAME not in", values, "accountname");
            return (Criteria) this;
        }

        public Criteria andAccountnameBetween(String value1, String value2) {
            addCriterion("ACCOUNTNAME between", value1, value2, "accountname");
            return (Criteria) this;
        }

        public Criteria andAccountnameNotBetween(String value1, String value2) {
            addCriterion("ACCOUNTNAME not between", value1, value2, "accountname");
            return (Criteria) this;
        }

        public Criteria andPermitstartdateIsNull() {
            addCriterion("PERMITSTARTDATE is null");
            return (Criteria) this;
        }

        public Criteria andPermitstartdateIsNotNull() {
            addCriterion("PERMITSTARTDATE is not null");
            return (Criteria) this;
        }

        public Criteria andPermitstartdateEqualTo(Date value) {
            addCriterion("PERMITSTARTDATE =", value, "permitstartdate");
            return (Criteria) this;
        }

        public Criteria andPermitstartdateNotEqualTo(Date value) {
            addCriterion("PERMITSTARTDATE <>", value, "permitstartdate");
            return (Criteria) this;
        }

        public Criteria andPermitstartdateGreaterThan(Date value) {
            addCriterion("PERMITSTARTDATE >", value, "permitstartdate");
            return (Criteria) this;
        }

        public Criteria andPermitstartdateGreaterThanOrEqualTo(Date value) {
            addCriterion("PERMITSTARTDATE >=", value, "permitstartdate");
            return (Criteria) this;
        }

        public Criteria andPermitstartdateLessThan(Date value) {
            addCriterion("PERMITSTARTDATE <", value, "permitstartdate");
            return (Criteria) this;
        }

        public Criteria andPermitstartdateLessThanOrEqualTo(Date value) {
            addCriterion("PERMITSTARTDATE <=", value, "permitstartdate");
            return (Criteria) this;
        }

        public Criteria andPermitstartdateIn(List<Date> values) {
            addCriterion("PERMITSTARTDATE in", values, "permitstartdate");
            return (Criteria) this;
        }

        public Criteria andPermitstartdateNotIn(List<Date> values) {
            addCriterion("PERMITSTARTDATE not in", values, "permitstartdate");
            return (Criteria) this;
        }

        public Criteria andPermitstartdateBetween(Date value1, Date value2) {
            addCriterion("PERMITSTARTDATE between", value1, value2, "permitstartdate");
            return (Criteria) this;
        }

        public Criteria andPermitstartdateNotBetween(Date value1, Date value2) {
            addCriterion("PERMITSTARTDATE not between", value1, value2, "permitstartdate");
            return (Criteria) this;
        }

        public Criteria andPermitenddateIsNull() {
            addCriterion("PERMITENDDATE is null");
            return (Criteria) this;
        }

        public Criteria andPermitenddateIsNotNull() {
            addCriterion("PERMITENDDATE is not null");
            return (Criteria) this;
        }

        public Criteria andPermitenddateEqualTo(Date value) {
            addCriterion("PERMITENDDATE =", value, "permitenddate");
            return (Criteria) this;
        }

        public Criteria andPermitenddateNotEqualTo(Date value) {
            addCriterion("PERMITENDDATE <>", value, "permitenddate");
            return (Criteria) this;
        }

        public Criteria andPermitenddateGreaterThan(Date value) {
            addCriterion("PERMITENDDATE >", value, "permitenddate");
            return (Criteria) this;
        }

        public Criteria andPermitenddateGreaterThanOrEqualTo(Date value) {
            addCriterion("PERMITENDDATE >=", value, "permitenddate");
            return (Criteria) this;
        }

        public Criteria andPermitenddateLessThan(Date value) {
            addCriterion("PERMITENDDATE <", value, "permitenddate");
            return (Criteria) this;
        }

        public Criteria andPermitenddateLessThanOrEqualTo(Date value) {
            addCriterion("PERMITENDDATE <=", value, "permitenddate");
            return (Criteria) this;
        }

        public Criteria andPermitenddateIn(List<Date> values) {
            addCriterion("PERMITENDDATE in", values, "permitenddate");
            return (Criteria) this;
        }

        public Criteria andPermitenddateNotIn(List<Date> values) {
            addCriterion("PERMITENDDATE not in", values, "permitenddate");
            return (Criteria) this;
        }

        public Criteria andPermitenddateBetween(Date value1, Date value2) {
            addCriterion("PERMITENDDATE between", value1, value2, "permitenddate");
            return (Criteria) this;
        }

        public Criteria andPermitenddateNotBetween(Date value1, Date value2) {
            addCriterion("PERMITENDDATE not between", value1, value2, "permitenddate");
            return (Criteria) this;
        }

        public Criteria andCompletedproportionIsNull() {
            addCriterion("COMPLETEDPROPORTION is null");
            return (Criteria) this;
        }

        public Criteria andCompletedproportionIsNotNull() {
            addCriterion("COMPLETEDPROPORTION is not null");
            return (Criteria) this;
        }

        public Criteria andCompletedproportionEqualTo(BigDecimal value) {
            addCriterion("COMPLETEDPROPORTION =", value, "completedproportion");
            return (Criteria) this;
        }

        public Criteria andCompletedproportionNotEqualTo(BigDecimal value) {
            addCriterion("COMPLETEDPROPORTION <>", value, "completedproportion");
            return (Criteria) this;
        }

        public Criteria andCompletedproportionGreaterThan(BigDecimal value) {
            addCriterion("COMPLETEDPROPORTION >", value, "completedproportion");
            return (Criteria) this;
        }

        public Criteria andCompletedproportionGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("COMPLETEDPROPORTION >=", value, "completedproportion");
            return (Criteria) this;
        }

        public Criteria andCompletedproportionLessThan(BigDecimal value) {
            addCriterion("COMPLETEDPROPORTION <", value, "completedproportion");
            return (Criteria) this;
        }

        public Criteria andCompletedproportionLessThanOrEqualTo(BigDecimal value) {
            addCriterion("COMPLETEDPROPORTION <=", value, "completedproportion");
            return (Criteria) this;
        }

        public Criteria andCompletedproportionIn(List<BigDecimal> values) {
            addCriterion("COMPLETEDPROPORTION in", values, "completedproportion");
            return (Criteria) this;
        }

        public Criteria andCompletedproportionNotIn(List<BigDecimal> values) {
            addCriterion("COMPLETEDPROPORTION not in", values, "completedproportion");
            return (Criteria) this;
        }

        public Criteria andCompletedproportionBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("COMPLETEDPROPORTION between", value1, value2, "completedproportion");
            return (Criteria) this;
        }

        public Criteria andCompletedproportionNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("COMPLETEDPROPORTION not between", value1, value2, "completedproportion");
            return (Criteria) this;
        }

        public Criteria andPrepremiumIsNull() {
            addCriterion("PREPREMIUM is null");
            return (Criteria) this;
        }

        public Criteria andPrepremiumIsNotNull() {
            addCriterion("PREPREMIUM is not null");
            return (Criteria) this;
        }

        public Criteria andPrepremiumEqualTo(BigDecimal value) {
            addCriterion("PREPREMIUM =", value, "prepremium");
            return (Criteria) this;
        }

        public Criteria andPrepremiumNotEqualTo(BigDecimal value) {
            addCriterion("PREPREMIUM <>", value, "prepremium");
            return (Criteria) this;
        }

        public Criteria andPrepremiumGreaterThan(BigDecimal value) {
            addCriterion("PREPREMIUM >", value, "prepremium");
            return (Criteria) this;
        }

        public Criteria andPrepremiumGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("PREPREMIUM >=", value, "prepremium");
            return (Criteria) this;
        }

        public Criteria andPrepremiumLessThan(BigDecimal value) {
            addCriterion("PREPREMIUM <", value, "prepremium");
            return (Criteria) this;
        }

        public Criteria andPrepremiumLessThanOrEqualTo(BigDecimal value) {
            addCriterion("PREPREMIUM <=", value, "prepremium");
            return (Criteria) this;
        }

        public Criteria andPrepremiumIn(List<BigDecimal> values) {
            addCriterion("PREPREMIUM in", values, "prepremium");
            return (Criteria) this;
        }

        public Criteria andPrepremiumNotIn(List<BigDecimal> values) {
            addCriterion("PREPREMIUM not in", values, "prepremium");
            return (Criteria) this;
        }

        public Criteria andPrepremiumBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("PREPREMIUM between", value1, value2, "prepremium");
            return (Criteria) this;
        }

        public Criteria andPrepremiumNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("PREPREMIUM not between", value1, value2, "prepremium");
            return (Criteria) this;
        }

        public Criteria andTranoIsNull() {
            addCriterion("TRANO is null");
            return (Criteria) this;
        }

        public Criteria andTranoIsNotNull() {
            addCriterion("TRANO is not null");
            return (Criteria) this;
        }

        public Criteria andTranoEqualTo(String value) {
            addCriterion("TRANO =", value, "trano");
            return (Criteria) this;
        }

        public Criteria andTranoNotEqualTo(String value) {
            addCriterion("TRANO <>", value, "trano");
            return (Criteria) this;
        }

        public Criteria andTranoGreaterThan(String value) {
            addCriterion("TRANO >", value, "trano");
            return (Criteria) this;
        }

        public Criteria andTranoGreaterThanOrEqualTo(String value) {
            addCriterion("TRANO >=", value, "trano");
            return (Criteria) this;
        }

        public Criteria andTranoLessThan(String value) {
            addCriterion("TRANO <", value, "trano");
            return (Criteria) this;
        }

        public Criteria andTranoLessThanOrEqualTo(String value) {
            addCriterion("TRANO <=", value, "trano");
            return (Criteria) this;
        }

        public Criteria andTranoLike(String value) {
            addCriterion("TRANO like", value, "trano");
            return (Criteria) this;
        }

        public Criteria andTranoNotLike(String value) {
            addCriterion("TRANO not like", value, "trano");
            return (Criteria) this;
        }

        public Criteria andTranoIn(List<String> values) {
            addCriterion("TRANO in", values, "trano");
            return (Criteria) this;
        }

        public Criteria andTranoNotIn(List<String> values) {
            addCriterion("TRANO not in", values, "trano");
            return (Criteria) this;
        }

        public Criteria andTranoBetween(String value1, String value2) {
            addCriterion("TRANO between", value1, value2, "trano");
            return (Criteria) this;
        }

        public Criteria andTranoNotBetween(String value1, String value2) {
            addCriterion("TRANO not between", value1, value2, "trano");
            return (Criteria) this;
        }

        public Criteria andCheckoutstatusIsNull() {
            addCriterion("CHECKOUTSTATUS is null");
            return (Criteria) this;
        }

        public Criteria andCheckoutstatusIsNotNull() {
            addCriterion("CHECKOUTSTATUS is not null");
            return (Criteria) this;
        }

        public Criteria andCheckoutstatusEqualTo(String value) {
            addCriterion("CHECKOUTSTATUS =", value, "checkoutstatus");
            return (Criteria) this;
        }

        public Criteria andCheckoutstatusNotEqualTo(String value) {
            addCriterion("CHECKOUTSTATUS <>", value, "checkoutstatus");
            return (Criteria) this;
        }

        public Criteria andCheckoutstatusGreaterThan(String value) {
            addCriterion("CHECKOUTSTATUS >", value, "checkoutstatus");
            return (Criteria) this;
        }

        public Criteria andCheckoutstatusGreaterThanOrEqualTo(String value) {
            addCriterion("CHECKOUTSTATUS >=", value, "checkoutstatus");
            return (Criteria) this;
        }

        public Criteria andCheckoutstatusLessThan(String value) {
            addCriterion("CHECKOUTSTATUS <", value, "checkoutstatus");
            return (Criteria) this;
        }

        public Criteria andCheckoutstatusLessThanOrEqualTo(String value) {
            addCriterion("CHECKOUTSTATUS <=", value, "checkoutstatus");
            return (Criteria) this;
        }

        public Criteria andCheckoutstatusLike(String value) {
            addCriterion("CHECKOUTSTATUS like", value, "checkoutstatus");
            return (Criteria) this;
        }

        public Criteria andCheckoutstatusNotLike(String value) {
            addCriterion("CHECKOUTSTATUS not like", value, "checkoutstatus");
            return (Criteria) this;
        }

        public Criteria andCheckoutstatusIn(List<String> values) {
            addCriterion("CHECKOUTSTATUS in", values, "checkoutstatus");
            return (Criteria) this;
        }

        public Criteria andCheckoutstatusNotIn(List<String> values) {
            addCriterion("CHECKOUTSTATUS not in", values, "checkoutstatus");
            return (Criteria) this;
        }

        public Criteria andCheckoutstatusBetween(String value1, String value2) {
            addCriterion("CHECKOUTSTATUS between", value1, value2, "checkoutstatus");
            return (Criteria) this;
        }

        public Criteria andCheckoutstatusNotBetween(String value1, String value2) {
            addCriterion("CHECKOUTSTATUS not between", value1, value2, "checkoutstatus");
            return (Criteria) this;
        }

        public Criteria andCreatorcodeIsNull() {
            addCriterion("CREATORCODE is null");
            return (Criteria) this;
        }

        public Criteria andCreatorcodeIsNotNull() {
            addCriterion("CREATORCODE is not null");
            return (Criteria) this;
        }

        public Criteria andCreatorcodeEqualTo(String value) {
            addCriterion("CREATORCODE =", value, "creatorcode");
            return (Criteria) this;
        }

        public Criteria andCreatorcodeNotEqualTo(String value) {
            addCriterion("CREATORCODE <>", value, "creatorcode");
            return (Criteria) this;
        }

        public Criteria andCreatorcodeGreaterThan(String value) {
            addCriterion("CREATORCODE >", value, "creatorcode");
            return (Criteria) this;
        }

        public Criteria andCreatorcodeGreaterThanOrEqualTo(String value) {
            addCriterion("CREATORCODE >=", value, "creatorcode");
            return (Criteria) this;
        }

        public Criteria andCreatorcodeLessThan(String value) {
            addCriterion("CREATORCODE <", value, "creatorcode");
            return (Criteria) this;
        }

        public Criteria andCreatorcodeLessThanOrEqualTo(String value) {
            addCriterion("CREATORCODE <=", value, "creatorcode");
            return (Criteria) this;
        }

        public Criteria andCreatorcodeLike(String value) {
            addCriterion("CREATORCODE like", value, "creatorcode");
            return (Criteria) this;
        }

        public Criteria andCreatorcodeNotLike(String value) {
            addCriterion("CREATORCODE not like", value, "creatorcode");
            return (Criteria) this;
        }

        public Criteria andCreatorcodeIn(List<String> values) {
            addCriterion("CREATORCODE in", values, "creatorcode");
            return (Criteria) this;
        }

        public Criteria andCreatorcodeNotIn(List<String> values) {
            addCriterion("CREATORCODE not in", values, "creatorcode");
            return (Criteria) this;
        }

        public Criteria andCreatorcodeBetween(String value1, String value2) {
            addCriterion("CREATORCODE between", value1, value2, "creatorcode");
            return (Criteria) this;
        }

        public Criteria andCreatorcodeNotBetween(String value1, String value2) {
            addCriterion("CREATORCODE not between", value1, value2, "creatorcode");
            return (Criteria) this;
        }

        public Criteria andCreatedateIsNull() {
            addCriterion("CREATEDATE is null");
            return (Criteria) this;
        }

        public Criteria andCreatedateIsNotNull() {
            addCriterion("CREATEDATE is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedateEqualTo(Date value) {
            addCriterion("CREATEDATE =", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateNotEqualTo(Date value) {
            addCriterion("CREATEDATE <>", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateGreaterThan(Date value) {
            addCriterion("CREATEDATE >", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateGreaterThanOrEqualTo(Date value) {
            addCriterion("CREATEDATE >=", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateLessThan(Date value) {
            addCriterion("CREATEDATE <", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateLessThanOrEqualTo(Date value) {
            addCriterion("CREATEDATE <=", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateIn(List<Date> values) {
            addCriterion("CREATEDATE in", values, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateNotIn(List<Date> values) {
            addCriterion("CREATEDATE not in", values, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateBetween(Date value1, Date value2) {
            addCriterion("CREATEDATE between", value1, value2, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateNotBetween(Date value1, Date value2) {
            addCriterion("CREATEDATE not between", value1, value2, "createdate");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeIsNull() {
            addCriterion("UPDATERCODE is null");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeIsNotNull() {
            addCriterion("UPDATERCODE is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeEqualTo(String value) {
            addCriterion("UPDATERCODE =", value, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeNotEqualTo(String value) {
            addCriterion("UPDATERCODE <>", value, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeGreaterThan(String value) {
            addCriterion("UPDATERCODE >", value, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeGreaterThanOrEqualTo(String value) {
            addCriterion("UPDATERCODE >=", value, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeLessThan(String value) {
            addCriterion("UPDATERCODE <", value, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeLessThanOrEqualTo(String value) {
            addCriterion("UPDATERCODE <=", value, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeLike(String value) {
            addCriterion("UPDATERCODE like", value, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeNotLike(String value) {
            addCriterion("UPDATERCODE not like", value, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeIn(List<String> values) {
            addCriterion("UPDATERCODE in", values, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeNotIn(List<String> values) {
            addCriterion("UPDATERCODE not in", values, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeBetween(String value1, String value2) {
            addCriterion("UPDATERCODE between", value1, value2, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatercodeNotBetween(String value1, String value2) {
            addCriterion("UPDATERCODE not between", value1, value2, "updatercode");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNull() {
            addCriterion("UPDATEDATE is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNotNull() {
            addCriterion("UPDATEDATE is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateEqualTo(Date value) {
            addCriterion("UPDATEDATE =", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotEqualTo(Date value) {
            addCriterion("UPDATEDATE <>", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThan(Date value) {
            addCriterion("UPDATEDATE >", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThanOrEqualTo(Date value) {
            addCriterion("UPDATEDATE >=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThan(Date value) {
            addCriterion("UPDATEDATE <", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThanOrEqualTo(Date value) {
            addCriterion("UPDATEDATE <=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIn(List<Date> values) {
            addCriterion("UPDATEDATE in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotIn(List<Date> values) {
            addCriterion("UPDATEDATE not in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateBetween(Date value1, Date value2) {
            addCriterion("UPDATEDATE between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotBetween(Date value1, Date value2) {
            addCriterion("UPDATEDATE not between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andValidstatusIsNull() {
            addCriterion("VALIDSTATUS is null");
            return (Criteria) this;
        }

        public Criteria andValidstatusIsNotNull() {
            addCriterion("VALIDSTATUS is not null");
            return (Criteria) this;
        }

        public Criteria andValidstatusEqualTo(String value) {
            addCriterion("VALIDSTATUS =", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotEqualTo(String value) {
            addCriterion("VALIDSTATUS <>", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusGreaterThan(String value) {
            addCriterion("VALIDSTATUS >", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusGreaterThanOrEqualTo(String value) {
            addCriterion("VALIDSTATUS >=", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusLessThan(String value) {
            addCriterion("VALIDSTATUS <", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusLessThanOrEqualTo(String value) {
            addCriterion("VALIDSTATUS <=", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusLike(String value) {
            addCriterion("VALIDSTATUS like", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotLike(String value) {
            addCriterion("VALIDSTATUS not like", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusIn(List<String> values) {
            addCriterion("VALIDSTATUS in", values, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotIn(List<String> values) {
            addCriterion("VALIDSTATUS not in", values, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusBetween(String value1, String value2) {
            addCriterion("VALIDSTATUS between", value1, value2, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotBetween(String value1, String value2) {
            addCriterion("VALIDSTATUS not between", value1, value2, "validstatus");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("REMARK is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("REMARK is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("REMARK =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("REMARK <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("REMARK >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("REMARK >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("REMARK <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("REMARK <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("REMARK like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("REMARK not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("REMARK in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("REMARK not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("REMARK between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("REMARK not between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andFlagIsNull() {
            addCriterion("FLAG is null");
            return (Criteria) this;
        }

        public Criteria andFlagIsNotNull() {
            addCriterion("FLAG is not null");
            return (Criteria) this;
        }

        public Criteria andFlagEqualTo(String value) {
            addCriterion("FLAG =", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotEqualTo(String value) {
            addCriterion("FLAG <>", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThan(String value) {
            addCriterion("FLAG >", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThanOrEqualTo(String value) {
            addCriterion("FLAG >=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThan(String value) {
            addCriterion("FLAG <", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThanOrEqualTo(String value) {
            addCriterion("FLAG <=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLike(String value) {
            addCriterion("FLAG like", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotLike(String value) {
            addCriterion("FLAG not like", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagIn(List<String> values) {
            addCriterion("FLAG in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotIn(List<String> values) {
            addCriterion("FLAG not in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagBetween(String value1, String value2) {
            addCriterion("FLAG between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotBetween(String value1, String value2) {
            addCriterion("FLAG not between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andIcflagIsNull() {
            addCriterion("ICFLAG is null");
            return (Criteria) this;
        }

        public Criteria andIcflagIsNotNull() {
            addCriterion("ICFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andIcflagEqualTo(String value) {
            addCriterion("ICFLAG =", value, "icflag");
            return (Criteria) this;
        }

        public Criteria andIcflagNotEqualTo(String value) {
            addCriterion("ICFLAG <>", value, "icflag");
            return (Criteria) this;
        }

        public Criteria andIcflagGreaterThan(String value) {
            addCriterion("ICFLAG >", value, "icflag");
            return (Criteria) this;
        }

        public Criteria andIcflagGreaterThanOrEqualTo(String value) {
            addCriterion("ICFLAG >=", value, "icflag");
            return (Criteria) this;
        }

        public Criteria andIcflagLessThan(String value) {
            addCriterion("ICFLAG <", value, "icflag");
            return (Criteria) this;
        }

        public Criteria andIcflagLessThanOrEqualTo(String value) {
            addCriterion("ICFLAG <=", value, "icflag");
            return (Criteria) this;
        }

        public Criteria andIcflagLike(String value) {
            addCriterion("ICFLAG like", value, "icflag");
            return (Criteria) this;
        }

        public Criteria andIcflagNotLike(String value) {
            addCriterion("ICFLAG not like", value, "icflag");
            return (Criteria) this;
        }

        public Criteria andIcflagIn(List<String> values) {
            addCriterion("ICFLAG in", values, "icflag");
            return (Criteria) this;
        }

        public Criteria andIcflagNotIn(List<String> values) {
            addCriterion("ICFLAG not in", values, "icflag");
            return (Criteria) this;
        }

        public Criteria andIcflagBetween(String value1, String value2) {
            addCriterion("ICFLAG between", value1, value2, "icflag");
            return (Criteria) this;
        }

        public Criteria andIcflagNotBetween(String value1, String value2) {
            addCriterion("ICFLAG not between", value1, value2, "icflag");
            return (Criteria) this;
        }

        public Criteria andSalechannelcodeIsNull() {
            addCriterion("SALECHANNELCODE is null");
            return (Criteria) this;
        }

        public Criteria andSalechannelcodeIsNotNull() {
            addCriterion("SALECHANNELCODE is not null");
            return (Criteria) this;
        }

        public Criteria andSalechannelcodeEqualTo(String value) {
            addCriterion("SALECHANNELCODE =", value, "salechannelcode");
            return (Criteria) this;
        }

        public Criteria andSalechannelcodeNotEqualTo(String value) {
            addCriterion("SALECHANNELCODE <>", value, "salechannelcode");
            return (Criteria) this;
        }

        public Criteria andSalechannelcodeGreaterThan(String value) {
            addCriterion("SALECHANNELCODE >", value, "salechannelcode");
            return (Criteria) this;
        }

        public Criteria andSalechannelcodeGreaterThanOrEqualTo(String value) {
            addCriterion("SALECHANNELCODE >=", value, "salechannelcode");
            return (Criteria) this;
        }

        public Criteria andSalechannelcodeLessThan(String value) {
            addCriterion("SALECHANNELCODE <", value, "salechannelcode");
            return (Criteria) this;
        }

        public Criteria andSalechannelcodeLessThanOrEqualTo(String value) {
            addCriterion("SALECHANNELCODE <=", value, "salechannelcode");
            return (Criteria) this;
        }

        public Criteria andSalechannelcodeLike(String value) {
            addCriterion("SALECHANNELCODE like", value, "salechannelcode");
            return (Criteria) this;
        }

        public Criteria andSalechannelcodeNotLike(String value) {
            addCriterion("SALECHANNELCODE not like", value, "salechannelcode");
            return (Criteria) this;
        }

        public Criteria andSalechannelcodeIn(List<String> values) {
            addCriterion("SALECHANNELCODE in", values, "salechannelcode");
            return (Criteria) this;
        }

        public Criteria andSalechannelcodeNotIn(List<String> values) {
            addCriterion("SALECHANNELCODE not in", values, "salechannelcode");
            return (Criteria) this;
        }

        public Criteria andSalechannelcodeBetween(String value1, String value2) {
            addCriterion("SALECHANNELCODE between", value1, value2, "salechannelcode");
            return (Criteria) this;
        }

        public Criteria andSalechannelcodeNotBetween(String value1, String value2) {
            addCriterion("SALECHANNELCODE not between", value1, value2, "salechannelcode");
            return (Criteria) this;
        }

        public Criteria andBusinessnatureIsNull() {
            addCriterion("BUSINESSNATURE is null");
            return (Criteria) this;
        }

        public Criteria andBusinessnatureIsNotNull() {
            addCriterion("BUSINESSNATURE is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessnatureEqualTo(String value) {
            addCriterion("BUSINESSNATURE =", value, "businessnature");
            return (Criteria) this;
        }

        public Criteria andBusinessnatureNotEqualTo(String value) {
            addCriterion("BUSINESSNATURE <>", value, "businessnature");
            return (Criteria) this;
        }

        public Criteria andBusinessnatureGreaterThan(String value) {
            addCriterion("BUSINESSNATURE >", value, "businessnature");
            return (Criteria) this;
        }

        public Criteria andBusinessnatureGreaterThanOrEqualTo(String value) {
            addCriterion("BUSINESSNATURE >=", value, "businessnature");
            return (Criteria) this;
        }

        public Criteria andBusinessnatureLessThan(String value) {
            addCriterion("BUSINESSNATURE <", value, "businessnature");
            return (Criteria) this;
        }

        public Criteria andBusinessnatureLessThanOrEqualTo(String value) {
            addCriterion("BUSINESSNATURE <=", value, "businessnature");
            return (Criteria) this;
        }

        public Criteria andBusinessnatureLike(String value) {
            addCriterion("BUSINESSNATURE like", value, "businessnature");
            return (Criteria) this;
        }

        public Criteria andBusinessnatureNotLike(String value) {
            addCriterion("BUSINESSNATURE not like", value, "businessnature");
            return (Criteria) this;
        }

        public Criteria andBusinessnatureIn(List<String> values) {
            addCriterion("BUSINESSNATURE in", values, "businessnature");
            return (Criteria) this;
        }

        public Criteria andBusinessnatureNotIn(List<String> values) {
            addCriterion("BUSINESSNATURE not in", values, "businessnature");
            return (Criteria) this;
        }

        public Criteria andBusinessnatureBetween(String value1, String value2) {
            addCriterion("BUSINESSNATURE between", value1, value2, "businessnature");
            return (Criteria) this;
        }

        public Criteria andBusinessnatureNotBetween(String value1, String value2) {
            addCriterion("BUSINESSNATURE not between", value1, value2, "businessnature");
            return (Criteria) this;
        }

        public Criteria andAttribute4IsNull() {
            addCriterion("ATTRIBUTE4 is null");
            return (Criteria) this;
        }

        public Criteria andAttribute4IsNotNull() {
            addCriterion("ATTRIBUTE4 is not null");
            return (Criteria) this;
        }

        public Criteria andAttribute4EqualTo(String value) {
            addCriterion("ATTRIBUTE4 =", value, "attribute4");
            return (Criteria) this;
        }

        public Criteria andAttribute4NotEqualTo(String value) {
            addCriterion("ATTRIBUTE4 <>", value, "attribute4");
            return (Criteria) this;
        }

        public Criteria andAttribute4GreaterThan(String value) {
            addCriterion("ATTRIBUTE4 >", value, "attribute4");
            return (Criteria) this;
        }

        public Criteria andAttribute4GreaterThanOrEqualTo(String value) {
            addCriterion("ATTRIBUTE4 >=", value, "attribute4");
            return (Criteria) this;
        }

        public Criteria andAttribute4LessThan(String value) {
            addCriterion("ATTRIBUTE4 <", value, "attribute4");
            return (Criteria) this;
        }

        public Criteria andAttribute4LessThanOrEqualTo(String value) {
            addCriterion("ATTRIBUTE4 <=", value, "attribute4");
            return (Criteria) this;
        }

        public Criteria andAttribute4Like(String value) {
            addCriterion("ATTRIBUTE4 like", value, "attribute4");
            return (Criteria) this;
        }

        public Criteria andAttribute4NotLike(String value) {
            addCriterion("ATTRIBUTE4 not like", value, "attribute4");
            return (Criteria) this;
        }

        public Criteria andAttribute4In(List<String> values) {
            addCriterion("ATTRIBUTE4 in", values, "attribute4");
            return (Criteria) this;
        }

        public Criteria andAttribute4NotIn(List<String> values) {
            addCriterion("ATTRIBUTE4 not in", values, "attribute4");
            return (Criteria) this;
        }

        public Criteria andAttribute4Between(String value1, String value2) {
            addCriterion("ATTRIBUTE4 between", value1, value2, "attribute4");
            return (Criteria) this;
        }

        public Criteria andAttribute4NotBetween(String value1, String value2) {
            addCriterion("ATTRIBUTE4 not between", value1, value2, "attribute4");
            return (Criteria) this;
        }

        public Criteria andAttribute5IsNull() {
            addCriterion("ATTRIBUTE5 is null");
            return (Criteria) this;
        }

        public Criteria andAttribute5IsNotNull() {
            addCriterion("ATTRIBUTE5 is not null");
            return (Criteria) this;
        }

        public Criteria andAttribute5EqualTo(String value) {
            addCriterion("ATTRIBUTE5 =", value, "attribute5");
            return (Criteria) this;
        }

        public Criteria andAttribute5NotEqualTo(String value) {
            addCriterion("ATTRIBUTE5 <>", value, "attribute5");
            return (Criteria) this;
        }

        public Criteria andAttribute5GreaterThan(String value) {
            addCriterion("ATTRIBUTE5 >", value, "attribute5");
            return (Criteria) this;
        }

        public Criteria andAttribute5GreaterThanOrEqualTo(String value) {
            addCriterion("ATTRIBUTE5 >=", value, "attribute5");
            return (Criteria) this;
        }

        public Criteria andAttribute5LessThan(String value) {
            addCriterion("ATTRIBUTE5 <", value, "attribute5");
            return (Criteria) this;
        }

        public Criteria andAttribute5LessThanOrEqualTo(String value) {
            addCriterion("ATTRIBUTE5 <=", value, "attribute5");
            return (Criteria) this;
        }

        public Criteria andAttribute5Like(String value) {
            addCriterion("ATTRIBUTE5 like", value, "attribute5");
            return (Criteria) this;
        }

        public Criteria andAttribute5NotLike(String value) {
            addCriterion("ATTRIBUTE5 not like", value, "attribute5");
            return (Criteria) this;
        }

        public Criteria andAttribute5In(List<String> values) {
            addCriterion("ATTRIBUTE5 in", values, "attribute5");
            return (Criteria) this;
        }

        public Criteria andAttribute5NotIn(List<String> values) {
            addCriterion("ATTRIBUTE5 not in", values, "attribute5");
            return (Criteria) this;
        }

        public Criteria andAttribute5Between(String value1, String value2) {
            addCriterion("ATTRIBUTE5 between", value1, value2, "attribute5");
            return (Criteria) this;
        }

        public Criteria andAttribute5NotBetween(String value1, String value2) {
            addCriterion("ATTRIBUTE5 not between", value1, value2, "attribute5");
            return (Criteria) this;
        }

        public Criteria andAttribute6IsNull() {
            addCriterion("ATTRIBUTE6 is null");
            return (Criteria) this;
        }

        public Criteria andAttribute6IsNotNull() {
            addCriterion("ATTRIBUTE6 is not null");
            return (Criteria) this;
        }

        public Criteria andAttribute6EqualTo(String value) {
            addCriterion("ATTRIBUTE6 =", value, "attribute6");
            return (Criteria) this;
        }

        public Criteria andAttribute6NotEqualTo(String value) {
            addCriterion("ATTRIBUTE6 <>", value, "attribute6");
            return (Criteria) this;
        }

        public Criteria andAttribute6GreaterThan(String value) {
            addCriterion("ATTRIBUTE6 >", value, "attribute6");
            return (Criteria) this;
        }

        public Criteria andAttribute6GreaterThanOrEqualTo(String value) {
            addCriterion("ATTRIBUTE6 >=", value, "attribute6");
            return (Criteria) this;
        }

        public Criteria andAttribute6LessThan(String value) {
            addCriterion("ATTRIBUTE6 <", value, "attribute6");
            return (Criteria) this;
        }

        public Criteria andAttribute6LessThanOrEqualTo(String value) {
            addCriterion("ATTRIBUTE6 <=", value, "attribute6");
            return (Criteria) this;
        }

        public Criteria andAttribute6Like(String value) {
            addCriterion("ATTRIBUTE6 like", value, "attribute6");
            return (Criteria) this;
        }

        public Criteria andAttribute6NotLike(String value) {
            addCriterion("ATTRIBUTE6 not like", value, "attribute6");
            return (Criteria) this;
        }

        public Criteria andAttribute6In(List<String> values) {
            addCriterion("ATTRIBUTE6 in", values, "attribute6");
            return (Criteria) this;
        }

        public Criteria andAttribute6NotIn(List<String> values) {
            addCriterion("ATTRIBUTE6 not in", values, "attribute6");
            return (Criteria) this;
        }

        public Criteria andAttribute6Between(String value1, String value2) {
            addCriterion("ATTRIBUTE6 between", value1, value2, "attribute6");
            return (Criteria) this;
        }

        public Criteria andAttribute6NotBetween(String value1, String value2) {
            addCriterion("ATTRIBUTE6 not between", value1, value2, "attribute6");
            return (Criteria) this;
        }

        public Criteria andAttribute7IsNull() {
            addCriterion("ATTRIBUTE7 is null");
            return (Criteria) this;
        }

        public Criteria andAttribute7IsNotNull() {
            addCriterion("ATTRIBUTE7 is not null");
            return (Criteria) this;
        }

        public Criteria andAttribute7EqualTo(String value) {
            addCriterion("ATTRIBUTE7 =", value, "attribute7");
            return (Criteria) this;
        }

        public Criteria andAttribute7NotEqualTo(String value) {
            addCriterion("ATTRIBUTE7 <>", value, "attribute7");
            return (Criteria) this;
        }

        public Criteria andAttribute7GreaterThan(String value) {
            addCriterion("ATTRIBUTE7 >", value, "attribute7");
            return (Criteria) this;
        }

        public Criteria andAttribute7GreaterThanOrEqualTo(String value) {
            addCriterion("ATTRIBUTE7 >=", value, "attribute7");
            return (Criteria) this;
        }

        public Criteria andAttribute7LessThan(String value) {
            addCriterion("ATTRIBUTE7 <", value, "attribute7");
            return (Criteria) this;
        }

        public Criteria andAttribute7LessThanOrEqualTo(String value) {
            addCriterion("ATTRIBUTE7 <=", value, "attribute7");
            return (Criteria) this;
        }

        public Criteria andAttribute7Like(String value) {
            addCriterion("ATTRIBUTE7 like", value, "attribute7");
            return (Criteria) this;
        }

        public Criteria andAttribute7NotLike(String value) {
            addCriterion("ATTRIBUTE7 not like", value, "attribute7");
            return (Criteria) this;
        }

        public Criteria andAttribute7In(List<String> values) {
            addCriterion("ATTRIBUTE7 in", values, "attribute7");
            return (Criteria) this;
        }

        public Criteria andAttribute7NotIn(List<String> values) {
            addCriterion("ATTRIBUTE7 not in", values, "attribute7");
            return (Criteria) this;
        }

        public Criteria andAttribute7Between(String value1, String value2) {
            addCriterion("ATTRIBUTE7 between", value1, value2, "attribute7");
            return (Criteria) this;
        }

        public Criteria andAttribute7NotBetween(String value1, String value2) {
            addCriterion("ATTRIBUTE7 not between", value1, value2, "attribute7");
            return (Criteria) this;
        }

        public Criteria andAttribute8IsNull() {
            addCriterion("ATTRIBUTE8 is null");
            return (Criteria) this;
        }

        public Criteria andAttribute8IsNotNull() {
            addCriterion("ATTRIBUTE8 is not null");
            return (Criteria) this;
        }

        public Criteria andAttribute8EqualTo(String value) {
            addCriterion("ATTRIBUTE8 =", value, "attribute8");
            return (Criteria) this;
        }

        public Criteria andAttribute8NotEqualTo(String value) {
            addCriterion("ATTRIBUTE8 <>", value, "attribute8");
            return (Criteria) this;
        }

        public Criteria andAttribute8GreaterThan(String value) {
            addCriterion("ATTRIBUTE8 >", value, "attribute8");
            return (Criteria) this;
        }

        public Criteria andAttribute8GreaterThanOrEqualTo(String value) {
            addCriterion("ATTRIBUTE8 >=", value, "attribute8");
            return (Criteria) this;
        }

        public Criteria andAttribute8LessThan(String value) {
            addCriterion("ATTRIBUTE8 <", value, "attribute8");
            return (Criteria) this;
        }

        public Criteria andAttribute8LessThanOrEqualTo(String value) {
            addCriterion("ATTRIBUTE8 <=", value, "attribute8");
            return (Criteria) this;
        }

        public Criteria andAttribute8Like(String value) {
            addCriterion("ATTRIBUTE8 like", value, "attribute8");
            return (Criteria) this;
        }

        public Criteria andAttribute8NotLike(String value) {
            addCriterion("ATTRIBUTE8 not like", value, "attribute8");
            return (Criteria) this;
        }

        public Criteria andAttribute8In(List<String> values) {
            addCriterion("ATTRIBUTE8 in", values, "attribute8");
            return (Criteria) this;
        }

        public Criteria andAttribute8NotIn(List<String> values) {
            addCriterion("ATTRIBUTE8 not in", values, "attribute8");
            return (Criteria) this;
        }

        public Criteria andAttribute8Between(String value1, String value2) {
            addCriterion("ATTRIBUTE8 between", value1, value2, "attribute8");
            return (Criteria) this;
        }

        public Criteria andAttribute8NotBetween(String value1, String value2) {
            addCriterion("ATTRIBUTE8 not between", value1, value2, "attribute8");
            return (Criteria) this;
        }

        public Criteria andAttribute9IsNull() {
            addCriterion("ATTRIBUTE9 is null");
            return (Criteria) this;
        }

        public Criteria andAttribute9IsNotNull() {
            addCriterion("ATTRIBUTE9 is not null");
            return (Criteria) this;
        }

        public Criteria andAttribute9EqualTo(Date value) {
            addCriterion("ATTRIBUTE9 =", value, "attribute9");
            return (Criteria) this;
        }

        public Criteria andAttribute9NotEqualTo(Date value) {
            addCriterion("ATTRIBUTE9 <>", value, "attribute9");
            return (Criteria) this;
        }

        public Criteria andAttribute9GreaterThan(Date value) {
            addCriterion("ATTRIBUTE9 >", value, "attribute9");
            return (Criteria) this;
        }

        public Criteria andAttribute9GreaterThanOrEqualTo(Date value) {
            addCriterion("ATTRIBUTE9 >=", value, "attribute9");
            return (Criteria) this;
        }

        public Criteria andAttribute9LessThan(Date value) {
            addCriterion("ATTRIBUTE9 <", value, "attribute9");
            return (Criteria) this;
        }

        public Criteria andAttribute9LessThanOrEqualTo(Date value) {
            addCriterion("ATTRIBUTE9 <=", value, "attribute9");
            return (Criteria) this;
        }

        public Criteria andAttribute9In(List<Date> values) {
            addCriterion("ATTRIBUTE9 in", values, "attribute9");
            return (Criteria) this;
        }

        public Criteria andAttribute9NotIn(List<Date> values) {
            addCriterion("ATTRIBUTE9 not in", values, "attribute9");
            return (Criteria) this;
        }

        public Criteria andAttribute9Between(Date value1, Date value2) {
            addCriterion("ATTRIBUTE9 between", value1, value2, "attribute9");
            return (Criteria) this;
        }

        public Criteria andAttribute9NotBetween(Date value1, Date value2) {
            addCriterion("ATTRIBUTE9 not between", value1, value2, "attribute9");
            return (Criteria) this;
        }

        public Criteria andAttribute10IsNull() {
            addCriterion("ATTRIBUTE10 is null");
            return (Criteria) this;
        }

        public Criteria andAttribute10IsNotNull() {
            addCriterion("ATTRIBUTE10 is not null");
            return (Criteria) this;
        }

        public Criteria andAttribute10EqualTo(Date value) {
            addCriterion("ATTRIBUTE10 =", value, "attribute10");
            return (Criteria) this;
        }

        public Criteria andAttribute10NotEqualTo(Date value) {
            addCriterion("ATTRIBUTE10 <>", value, "attribute10");
            return (Criteria) this;
        }

        public Criteria andAttribute10GreaterThan(Date value) {
            addCriterion("ATTRIBUTE10 >", value, "attribute10");
            return (Criteria) this;
        }

        public Criteria andAttribute10GreaterThanOrEqualTo(Date value) {
            addCriterion("ATTRIBUTE10 >=", value, "attribute10");
            return (Criteria) this;
        }

        public Criteria andAttribute10LessThan(Date value) {
            addCriterion("ATTRIBUTE10 <", value, "attribute10");
            return (Criteria) this;
        }

        public Criteria andAttribute10LessThanOrEqualTo(Date value) {
            addCriterion("ATTRIBUTE10 <=", value, "attribute10");
            return (Criteria) this;
        }

        public Criteria andAttribute10In(List<Date> values) {
            addCriterion("ATTRIBUTE10 in", values, "attribute10");
            return (Criteria) this;
        }

        public Criteria andAttribute10NotIn(List<Date> values) {
            addCriterion("ATTRIBUTE10 not in", values, "attribute10");
            return (Criteria) this;
        }

        public Criteria andAttribute10Between(Date value1, Date value2) {
            addCriterion("ATTRIBUTE10 between", value1, value2, "attribute10");
            return (Criteria) this;
        }

        public Criteria andAttribute10NotBetween(Date value1, Date value2) {
            addCriterion("ATTRIBUTE10 not between", value1, value2, "attribute10");
            return (Criteria) this;
        }

        public Criteria andExhibitionstartdateIsNull() {
            addCriterion("EXHIBITIONSTARTDATE is null");
            return (Criteria) this;
        }

        public Criteria andExhibitionstartdateIsNotNull() {
            addCriterion("EXHIBITIONSTARTDATE is not null");
            return (Criteria) this;
        }

        public Criteria andExhibitionstartdateEqualTo(Date value) {
            addCriterion("EXHIBITIONSTARTDATE =", value, "exhibitionstartdate");
            return (Criteria) this;
        }

        public Criteria andExhibitionstartdateNotEqualTo(Date value) {
            addCriterion("EXHIBITIONSTARTDATE <>", value, "exhibitionstartdate");
            return (Criteria) this;
        }

        public Criteria andExhibitionstartdateGreaterThan(Date value) {
            addCriterion("EXHIBITIONSTARTDATE >", value, "exhibitionstartdate");
            return (Criteria) this;
        }

        public Criteria andExhibitionstartdateGreaterThanOrEqualTo(Date value) {
            addCriterion("EXHIBITIONSTARTDATE >=", value, "exhibitionstartdate");
            return (Criteria) this;
        }

        public Criteria andExhibitionstartdateLessThan(Date value) {
            addCriterion("EXHIBITIONSTARTDATE <", value, "exhibitionstartdate");
            return (Criteria) this;
        }

        public Criteria andExhibitionstartdateLessThanOrEqualTo(Date value) {
            addCriterion("EXHIBITIONSTARTDATE <=", value, "exhibitionstartdate");
            return (Criteria) this;
        }

        public Criteria andExhibitionstartdateIn(List<Date> values) {
            addCriterion("EXHIBITIONSTARTDATE in", values, "exhibitionstartdate");
            return (Criteria) this;
        }

        public Criteria andExhibitionstartdateNotIn(List<Date> values) {
            addCriterion("EXHIBITIONSTARTDATE not in", values, "exhibitionstartdate");
            return (Criteria) this;
        }

        public Criteria andExhibitionstartdateBetween(Date value1, Date value2) {
            addCriterion("EXHIBITIONSTARTDATE between", value1, value2, "exhibitionstartdate");
            return (Criteria) this;
        }

        public Criteria andExhibitionstartdateNotBetween(Date value1, Date value2) {
            addCriterion("EXHIBITIONSTARTDATE not between", value1, value2, "exhibitionstartdate");
            return (Criteria) this;
        }

        public Criteria andExhibitionenddateIsNull() {
            addCriterion("EXHIBITIONENDDATE is null");
            return (Criteria) this;
        }

        public Criteria andExhibitionenddateIsNotNull() {
            addCriterion("EXHIBITIONENDDATE is not null");
            return (Criteria) this;
        }

        public Criteria andExhibitionenddateEqualTo(Date value) {
            addCriterion("EXHIBITIONENDDATE =", value, "exhibitionenddate");
            return (Criteria) this;
        }

        public Criteria andExhibitionenddateNotEqualTo(Date value) {
            addCriterion("EXHIBITIONENDDATE <>", value, "exhibitionenddate");
            return (Criteria) this;
        }

        public Criteria andExhibitionenddateGreaterThan(Date value) {
            addCriterion("EXHIBITIONENDDATE >", value, "exhibitionenddate");
            return (Criteria) this;
        }

        public Criteria andExhibitionenddateGreaterThanOrEqualTo(Date value) {
            addCriterion("EXHIBITIONENDDATE >=", value, "exhibitionenddate");
            return (Criteria) this;
        }

        public Criteria andExhibitionenddateLessThan(Date value) {
            addCriterion("EXHIBITIONENDDATE <", value, "exhibitionenddate");
            return (Criteria) this;
        }

        public Criteria andExhibitionenddateLessThanOrEqualTo(Date value) {
            addCriterion("EXHIBITIONENDDATE <=", value, "exhibitionenddate");
            return (Criteria) this;
        }

        public Criteria andExhibitionenddateIn(List<Date> values) {
            addCriterion("EXHIBITIONENDDATE in", values, "exhibitionenddate");
            return (Criteria) this;
        }

        public Criteria andExhibitionenddateNotIn(List<Date> values) {
            addCriterion("EXHIBITIONENDDATE not in", values, "exhibitionenddate");
            return (Criteria) this;
        }

        public Criteria andExhibitionenddateBetween(Date value1, Date value2) {
            addCriterion("EXHIBITIONENDDATE between", value1, value2, "exhibitionenddate");
            return (Criteria) this;
        }

        public Criteria andExhibitionenddateNotBetween(Date value1, Date value2) {
            addCriterion("EXHIBITIONENDDATE not between", value1, value2, "exhibitionenddate");
            return (Criteria) this;
        }

        public Criteria andContractstartdateIsNull() {
            addCriterion("CONTRACTSTARTDATE is null");
            return (Criteria) this;
        }

        public Criteria andContractstartdateIsNotNull() {
            addCriterion("CONTRACTSTARTDATE is not null");
            return (Criteria) this;
        }

        public Criteria andContractstartdateEqualTo(Date value) {
            addCriterion("CONTRACTSTARTDATE =", value, "contractstartdate");
            return (Criteria) this;
        }

        public Criteria andContractstartdateNotEqualTo(Date value) {
            addCriterion("CONTRACTSTARTDATE <>", value, "contractstartdate");
            return (Criteria) this;
        }

        public Criteria andContractstartdateGreaterThan(Date value) {
            addCriterion("CONTRACTSTARTDATE >", value, "contractstartdate");
            return (Criteria) this;
        }

        public Criteria andContractstartdateGreaterThanOrEqualTo(Date value) {
            addCriterion("CONTRACTSTARTDATE >=", value, "contractstartdate");
            return (Criteria) this;
        }

        public Criteria andContractstartdateLessThan(Date value) {
            addCriterion("CONTRACTSTARTDATE <", value, "contractstartdate");
            return (Criteria) this;
        }

        public Criteria andContractstartdateLessThanOrEqualTo(Date value) {
            addCriterion("CONTRACTSTARTDATE <=", value, "contractstartdate");
            return (Criteria) this;
        }

        public Criteria andContractstartdateIn(List<Date> values) {
            addCriterion("CONTRACTSTARTDATE in", values, "contractstartdate");
            return (Criteria) this;
        }

        public Criteria andContractstartdateNotIn(List<Date> values) {
            addCriterion("CONTRACTSTARTDATE not in", values, "contractstartdate");
            return (Criteria) this;
        }

        public Criteria andContractstartdateBetween(Date value1, Date value2) {
            addCriterion("CONTRACTSTARTDATE between", value1, value2, "contractstartdate");
            return (Criteria) this;
        }

        public Criteria andContractstartdateNotBetween(Date value1, Date value2) {
            addCriterion("CONTRACTSTARTDATE not between", value1, value2, "contractstartdate");
            return (Criteria) this;
        }

        public Criteria andContractenddateIsNull() {
            addCriterion("CONTRACTENDDATE is null");
            return (Criteria) this;
        }

        public Criteria andContractenddateIsNotNull() {
            addCriterion("CONTRACTENDDATE is not null");
            return (Criteria) this;
        }

        public Criteria andContractenddateEqualTo(Date value) {
            addCriterion("CONTRACTENDDATE =", value, "contractenddate");
            return (Criteria) this;
        }

        public Criteria andContractenddateNotEqualTo(Date value) {
            addCriterion("CONTRACTENDDATE <>", value, "contractenddate");
            return (Criteria) this;
        }

        public Criteria andContractenddateGreaterThan(Date value) {
            addCriterion("CONTRACTENDDATE >", value, "contractenddate");
            return (Criteria) this;
        }

        public Criteria andContractenddateGreaterThanOrEqualTo(Date value) {
            addCriterion("CONTRACTENDDATE >=", value, "contractenddate");
            return (Criteria) this;
        }

        public Criteria andContractenddateLessThan(Date value) {
            addCriterion("CONTRACTENDDATE <", value, "contractenddate");
            return (Criteria) this;
        }

        public Criteria andContractenddateLessThanOrEqualTo(Date value) {
            addCriterion("CONTRACTENDDATE <=", value, "contractenddate");
            return (Criteria) this;
        }

        public Criteria andContractenddateIn(List<Date> values) {
            addCriterion("CONTRACTENDDATE in", values, "contractenddate");
            return (Criteria) this;
        }

        public Criteria andContractenddateNotIn(List<Date> values) {
            addCriterion("CONTRACTENDDATE not in", values, "contractenddate");
            return (Criteria) this;
        }

        public Criteria andContractenddateBetween(Date value1, Date value2) {
            addCriterion("CONTRACTENDDATE between", value1, value2, "contractenddate");
            return (Criteria) this;
        }

        public Criteria andContractenddateNotBetween(Date value1, Date value2) {
            addCriterion("CONTRACTENDDATE not between", value1, value2, "contractenddate");
            return (Criteria) this;
        }

        public Criteria andAccountadressIsNull() {
            addCriterion("ACCOUNTADRESS is null");
            return (Criteria) this;
        }

        public Criteria andAccountadressIsNotNull() {
            addCriterion("ACCOUNTADRESS is not null");
            return (Criteria) this;
        }

        public Criteria andAccountadressEqualTo(String value) {
            addCriterion("ACCOUNTADRESS =", value, "accountadress");
            return (Criteria) this;
        }

        public Criteria andAccountadressNotEqualTo(String value) {
            addCriterion("ACCOUNTADRESS <>", value, "accountadress");
            return (Criteria) this;
        }

        public Criteria andAccountadressGreaterThan(String value) {
            addCriterion("ACCOUNTADRESS >", value, "accountadress");
            return (Criteria) this;
        }

        public Criteria andAccountadressGreaterThanOrEqualTo(String value) {
            addCriterion("ACCOUNTADRESS >=", value, "accountadress");
            return (Criteria) this;
        }

        public Criteria andAccountadressLessThan(String value) {
            addCriterion("ACCOUNTADRESS <", value, "accountadress");
            return (Criteria) this;
        }

        public Criteria andAccountadressLessThanOrEqualTo(String value) {
            addCriterion("ACCOUNTADRESS <=", value, "accountadress");
            return (Criteria) this;
        }

        public Criteria andAccountadressLike(String value) {
            addCriterion("ACCOUNTADRESS like", value, "accountadress");
            return (Criteria) this;
        }

        public Criteria andAccountadressNotLike(String value) {
            addCriterion("ACCOUNTADRESS not like", value, "accountadress");
            return (Criteria) this;
        }

        public Criteria andAccountadressIn(List<String> values) {
            addCriterion("ACCOUNTADRESS in", values, "accountadress");
            return (Criteria) this;
        }

        public Criteria andAccountadressNotIn(List<String> values) {
            addCriterion("ACCOUNTADRESS not in", values, "accountadress");
            return (Criteria) this;
        }

        public Criteria andAccountadressBetween(String value1, String value2) {
            addCriterion("ACCOUNTADRESS between", value1, value2, "accountadress");
            return (Criteria) this;
        }

        public Criteria andAccountadressNotBetween(String value1, String value2) {
            addCriterion("ACCOUNTADRESS not between", value1, value2, "accountadress");
            return (Criteria) this;
        }

        public Criteria andProfessionnameIsNull() {
            addCriterion("PROFESSIONNAME is null");
            return (Criteria) this;
        }

        public Criteria andProfessionnameIsNotNull() {
            addCriterion("PROFESSIONNAME is not null");
            return (Criteria) this;
        }

        public Criteria andProfessionnameEqualTo(String value) {
            addCriterion("PROFESSIONNAME =", value, "professionname");
            return (Criteria) this;
        }

        public Criteria andProfessionnameNotEqualTo(String value) {
            addCriterion("PROFESSIONNAME <>", value, "professionname");
            return (Criteria) this;
        }

        public Criteria andProfessionnameGreaterThan(String value) {
            addCriterion("PROFESSIONNAME >", value, "professionname");
            return (Criteria) this;
        }

        public Criteria andProfessionnameGreaterThanOrEqualTo(String value) {
            addCriterion("PROFESSIONNAME >=", value, "professionname");
            return (Criteria) this;
        }

        public Criteria andProfessionnameLessThan(String value) {
            addCriterion("PROFESSIONNAME <", value, "professionname");
            return (Criteria) this;
        }

        public Criteria andProfessionnameLessThanOrEqualTo(String value) {
            addCriterion("PROFESSIONNAME <=", value, "professionname");
            return (Criteria) this;
        }

        public Criteria andProfessionnameLike(String value) {
            addCriterion("PROFESSIONNAME like", value, "professionname");
            return (Criteria) this;
        }

        public Criteria andProfessionnameNotLike(String value) {
            addCriterion("PROFESSIONNAME not like", value, "professionname");
            return (Criteria) this;
        }

        public Criteria andProfessionnameIn(List<String> values) {
            addCriterion("PROFESSIONNAME in", values, "professionname");
            return (Criteria) this;
        }

        public Criteria andProfessionnameNotIn(List<String> values) {
            addCriterion("PROFESSIONNAME not in", values, "professionname");
            return (Criteria) this;
        }

        public Criteria andProfessionnameBetween(String value1, String value2) {
            addCriterion("PROFESSIONNAME between", value1, value2, "professionname");
            return (Criteria) this;
        }

        public Criteria andProfessionnameNotBetween(String value1, String value2) {
            addCriterion("PROFESSIONNAME not between", value1, value2, "professionname");
            return (Criteria) this;
        }

        public Criteria andProfessionnoIsNull() {
            addCriterion("PROFESSIONNO is null");
            return (Criteria) this;
        }

        public Criteria andProfessionnoIsNotNull() {
            addCriterion("PROFESSIONNO is not null");
            return (Criteria) this;
        }

        public Criteria andProfessionnoEqualTo(String value) {
            addCriterion("PROFESSIONNO =", value, "professionno");
            return (Criteria) this;
        }

        public Criteria andProfessionnoNotEqualTo(String value) {
            addCriterion("PROFESSIONNO <>", value, "professionno");
            return (Criteria) this;
        }

        public Criteria andProfessionnoGreaterThan(String value) {
            addCriterion("PROFESSIONNO >", value, "professionno");
            return (Criteria) this;
        }

        public Criteria andProfessionnoGreaterThanOrEqualTo(String value) {
            addCriterion("PROFESSIONNO >=", value, "professionno");
            return (Criteria) this;
        }

        public Criteria andProfessionnoLessThan(String value) {
            addCriterion("PROFESSIONNO <", value, "professionno");
            return (Criteria) this;
        }

        public Criteria andProfessionnoLessThanOrEqualTo(String value) {
            addCriterion("PROFESSIONNO <=", value, "professionno");
            return (Criteria) this;
        }

        public Criteria andProfessionnoLike(String value) {
            addCriterion("PROFESSIONNO like", value, "professionno");
            return (Criteria) this;
        }

        public Criteria andProfessionnoNotLike(String value) {
            addCriterion("PROFESSIONNO not like", value, "professionno");
            return (Criteria) this;
        }

        public Criteria andProfessionnoIn(List<String> values) {
            addCriterion("PROFESSIONNO in", values, "professionno");
            return (Criteria) this;
        }

        public Criteria andProfessionnoNotIn(List<String> values) {
            addCriterion("PROFESSIONNO not in", values, "professionno");
            return (Criteria) this;
        }

        public Criteria andProfessionnoBetween(String value1, String value2) {
            addCriterion("PROFESSIONNO between", value1, value2, "professionno");
            return (Criteria) this;
        }

        public Criteria andProfessionnoNotBetween(String value1, String value2) {
            addCriterion("PROFESSIONNO not between", value1, value2, "professionno");
            return (Criteria) this;
        }

        public Criteria andProfessionstartdateIsNull() {
            addCriterion("PROFESSIONSTARTDATE is null");
            return (Criteria) this;
        }

        public Criteria andProfessionstartdateIsNotNull() {
            addCriterion("PROFESSIONSTARTDATE is not null");
            return (Criteria) this;
        }

        public Criteria andProfessionstartdateEqualTo(Date value) {
            addCriterion("PROFESSIONSTARTDATE =", value, "professionstartdate");
            return (Criteria) this;
        }

        public Criteria andProfessionstartdateNotEqualTo(Date value) {
            addCriterion("PROFESSIONSTARTDATE <>", value, "professionstartdate");
            return (Criteria) this;
        }

        public Criteria andProfessionstartdateGreaterThan(Date value) {
            addCriterion("PROFESSIONSTARTDATE >", value, "professionstartdate");
            return (Criteria) this;
        }

        public Criteria andProfessionstartdateGreaterThanOrEqualTo(Date value) {
            addCriterion("PROFESSIONSTARTDATE >=", value, "professionstartdate");
            return (Criteria) this;
        }

        public Criteria andProfessionstartdateLessThan(Date value) {
            addCriterion("PROFESSIONSTARTDATE <", value, "professionstartdate");
            return (Criteria) this;
        }

        public Criteria andProfessionstartdateLessThanOrEqualTo(Date value) {
            addCriterion("PROFESSIONSTARTDATE <=", value, "professionstartdate");
            return (Criteria) this;
        }

        public Criteria andProfessionstartdateIn(List<Date> values) {
            addCriterion("PROFESSIONSTARTDATE in", values, "professionstartdate");
            return (Criteria) this;
        }

        public Criteria andProfessionstartdateNotIn(List<Date> values) {
            addCriterion("PROFESSIONSTARTDATE not in", values, "professionstartdate");
            return (Criteria) this;
        }

        public Criteria andProfessionstartdateBetween(Date value1, Date value2) {
            addCriterion("PROFESSIONSTARTDATE between", value1, value2, "professionstartdate");
            return (Criteria) this;
        }

        public Criteria andProfessionstartdateNotBetween(Date value1, Date value2) {
            addCriterion("PROFESSIONSTARTDATE not between", value1, value2, "professionstartdate");
            return (Criteria) this;
        }

        public Criteria andIndustryIsNull() {
            addCriterion("INDUSTRY is null");
            return (Criteria) this;
        }

        public Criteria andIndustryIsNotNull() {
            addCriterion("INDUSTRY is not null");
            return (Criteria) this;
        }

        public Criteria andIndustryEqualTo(String value) {
            addCriterion("INDUSTRY =", value, "industry");
            return (Criteria) this;
        }

        public Criteria andIndustryNotEqualTo(String value) {
            addCriterion("INDUSTRY <>", value, "industry");
            return (Criteria) this;
        }

        public Criteria andIndustryGreaterThan(String value) {
            addCriterion("INDUSTRY >", value, "industry");
            return (Criteria) this;
        }

        public Criteria andIndustryGreaterThanOrEqualTo(String value) {
            addCriterion("INDUSTRY >=", value, "industry");
            return (Criteria) this;
        }

        public Criteria andIndustryLessThan(String value) {
            addCriterion("INDUSTRY <", value, "industry");
            return (Criteria) this;
        }

        public Criteria andIndustryLessThanOrEqualTo(String value) {
            addCriterion("INDUSTRY <=", value, "industry");
            return (Criteria) this;
        }

        public Criteria andIndustryLike(String value) {
            addCriterion("INDUSTRY like", value, "industry");
            return (Criteria) this;
        }

        public Criteria andIndustryNotLike(String value) {
            addCriterion("INDUSTRY not like", value, "industry");
            return (Criteria) this;
        }

        public Criteria andIndustryIn(List<String> values) {
            addCriterion("INDUSTRY in", values, "industry");
            return (Criteria) this;
        }

        public Criteria andIndustryNotIn(List<String> values) {
            addCriterion("INDUSTRY not in", values, "industry");
            return (Criteria) this;
        }

        public Criteria andIndustryBetween(String value1, String value2) {
            addCriterion("INDUSTRY between", value1, value2, "industry");
            return (Criteria) this;
        }

        public Criteria andIndustryNotBetween(String value1, String value2) {
            addCriterion("INDUSTRY not between", value1, value2, "industry");
            return (Criteria) this;
        }

        public Criteria andProfessionenddateIsNull() {
            addCriterion("PROFESSIONENDDATE is null");
            return (Criteria) this;
        }

        public Criteria andProfessionenddateIsNotNull() {
            addCriterion("PROFESSIONENDDATE is not null");
            return (Criteria) this;
        }

        public Criteria andProfessionenddateEqualTo(Date value) {
            addCriterion("PROFESSIONENDDATE =", value, "professionenddate");
            return (Criteria) this;
        }

        public Criteria andProfessionenddateNotEqualTo(Date value) {
            addCriterion("PROFESSIONENDDATE <>", value, "professionenddate");
            return (Criteria) this;
        }

        public Criteria andProfessionenddateGreaterThan(Date value) {
            addCriterion("PROFESSIONENDDATE >", value, "professionenddate");
            return (Criteria) this;
        }

        public Criteria andProfessionenddateGreaterThanOrEqualTo(Date value) {
            addCriterion("PROFESSIONENDDATE >=", value, "professionenddate");
            return (Criteria) this;
        }

        public Criteria andProfessionenddateLessThan(Date value) {
            addCriterion("PROFESSIONENDDATE <", value, "professionenddate");
            return (Criteria) this;
        }

        public Criteria andProfessionenddateLessThanOrEqualTo(Date value) {
            addCriterion("PROFESSIONENDDATE <=", value, "professionenddate");
            return (Criteria) this;
        }

        public Criteria andProfessionenddateIn(List<Date> values) {
            addCriterion("PROFESSIONENDDATE in", values, "professionenddate");
            return (Criteria) this;
        }

        public Criteria andProfessionenddateNotIn(List<Date> values) {
            addCriterion("PROFESSIONENDDATE not in", values, "professionenddate");
            return (Criteria) this;
        }

        public Criteria andProfessionenddateBetween(Date value1, Date value2) {
            addCriterion("PROFESSIONENDDATE between", value1, value2, "professionenddate");
            return (Criteria) this;
        }

        public Criteria andProfessionenddateNotBetween(Date value1, Date value2) {
            addCriterion("PROFESSIONENDDATE not between", value1, value2, "professionenddate");
            return (Criteria) this;
        }

        public Criteria andProvincecodeIsNull() {
            addCriterion("PROVINCECODE is null");
            return (Criteria) this;
        }

        public Criteria andProvincecodeIsNotNull() {
            addCriterion("PROVINCECODE is not null");
            return (Criteria) this;
        }

        public Criteria andProvincecodeEqualTo(String value) {
            addCriterion("PROVINCECODE =", value, "provincecode");
            return (Criteria) this;
        }

        public Criteria andProvincecodeNotEqualTo(String value) {
            addCriterion("PROVINCECODE <>", value, "provincecode");
            return (Criteria) this;
        }

        public Criteria andProvincecodeGreaterThan(String value) {
            addCriterion("PROVINCECODE >", value, "provincecode");
            return (Criteria) this;
        }

        public Criteria andProvincecodeGreaterThanOrEqualTo(String value) {
            addCriterion("PROVINCECODE >=", value, "provincecode");
            return (Criteria) this;
        }

        public Criteria andProvincecodeLessThan(String value) {
            addCriterion("PROVINCECODE <", value, "provincecode");
            return (Criteria) this;
        }

        public Criteria andProvincecodeLessThanOrEqualTo(String value) {
            addCriterion("PROVINCECODE <=", value, "provincecode");
            return (Criteria) this;
        }

        public Criteria andProvincecodeLike(String value) {
            addCriterion("PROVINCECODE like", value, "provincecode");
            return (Criteria) this;
        }

        public Criteria andProvincecodeNotLike(String value) {
            addCriterion("PROVINCECODE not like", value, "provincecode");
            return (Criteria) this;
        }

        public Criteria andProvincecodeIn(List<String> values) {
            addCriterion("PROVINCECODE in", values, "provincecode");
            return (Criteria) this;
        }

        public Criteria andProvincecodeNotIn(List<String> values) {
            addCriterion("PROVINCECODE not in", values, "provincecode");
            return (Criteria) this;
        }

        public Criteria andProvincecodeBetween(String value1, String value2) {
            addCriterion("PROVINCECODE between", value1, value2, "provincecode");
            return (Criteria) this;
        }

        public Criteria andProvincecodeNotBetween(String value1, String value2) {
            addCriterion("PROVINCECODE not between", value1, value2, "provincecode");
            return (Criteria) this;
        }

        public Criteria andProvincenameIsNull() {
            addCriterion("PROVINCENAME is null");
            return (Criteria) this;
        }

        public Criteria andProvincenameIsNotNull() {
            addCriterion("PROVINCENAME is not null");
            return (Criteria) this;
        }

        public Criteria andProvincenameEqualTo(String value) {
            addCriterion("PROVINCENAME =", value, "provincename");
            return (Criteria) this;
        }

        public Criteria andProvincenameNotEqualTo(String value) {
            addCriterion("PROVINCENAME <>", value, "provincename");
            return (Criteria) this;
        }

        public Criteria andProvincenameGreaterThan(String value) {
            addCriterion("PROVINCENAME >", value, "provincename");
            return (Criteria) this;
        }

        public Criteria andProvincenameGreaterThanOrEqualTo(String value) {
            addCriterion("PROVINCENAME >=", value, "provincename");
            return (Criteria) this;
        }

        public Criteria andProvincenameLessThan(String value) {
            addCriterion("PROVINCENAME <", value, "provincename");
            return (Criteria) this;
        }

        public Criteria andProvincenameLessThanOrEqualTo(String value) {
            addCriterion("PROVINCENAME <=", value, "provincename");
            return (Criteria) this;
        }

        public Criteria andProvincenameLike(String value) {
            addCriterion("PROVINCENAME like", value, "provincename");
            return (Criteria) this;
        }

        public Criteria andProvincenameNotLike(String value) {
            addCriterion("PROVINCENAME not like", value, "provincename");
            return (Criteria) this;
        }

        public Criteria andProvincenameIn(List<String> values) {
            addCriterion("PROVINCENAME in", values, "provincename");
            return (Criteria) this;
        }

        public Criteria andProvincenameNotIn(List<String> values) {
            addCriterion("PROVINCENAME not in", values, "provincename");
            return (Criteria) this;
        }

        public Criteria andProvincenameBetween(String value1, String value2) {
            addCriterion("PROVINCENAME between", value1, value2, "provincename");
            return (Criteria) this;
        }

        public Criteria andProvincenameNotBetween(String value1, String value2) {
            addCriterion("PROVINCENAME not between", value1, value2, "provincename");
            return (Criteria) this;
        }

        public Criteria andCitycodeIsNull() {
            addCriterion("CITYCODE is null");
            return (Criteria) this;
        }

        public Criteria andCitycodeIsNotNull() {
            addCriterion("CITYCODE is not null");
            return (Criteria) this;
        }

        public Criteria andCitycodeEqualTo(String value) {
            addCriterion("CITYCODE =", value, "citycode");
            return (Criteria) this;
        }

        public Criteria andCitycodeNotEqualTo(String value) {
            addCriterion("CITYCODE <>", value, "citycode");
            return (Criteria) this;
        }

        public Criteria andCitycodeGreaterThan(String value) {
            addCriterion("CITYCODE >", value, "citycode");
            return (Criteria) this;
        }

        public Criteria andCitycodeGreaterThanOrEqualTo(String value) {
            addCriterion("CITYCODE >=", value, "citycode");
            return (Criteria) this;
        }

        public Criteria andCitycodeLessThan(String value) {
            addCriterion("CITYCODE <", value, "citycode");
            return (Criteria) this;
        }

        public Criteria andCitycodeLessThanOrEqualTo(String value) {
            addCriterion("CITYCODE <=", value, "citycode");
            return (Criteria) this;
        }

        public Criteria andCitycodeLike(String value) {
            addCriterion("CITYCODE like", value, "citycode");
            return (Criteria) this;
        }

        public Criteria andCitycodeNotLike(String value) {
            addCriterion("CITYCODE not like", value, "citycode");
            return (Criteria) this;
        }

        public Criteria andCitycodeIn(List<String> values) {
            addCriterion("CITYCODE in", values, "citycode");
            return (Criteria) this;
        }

        public Criteria andCitycodeNotIn(List<String> values) {
            addCriterion("CITYCODE not in", values, "citycode");
            return (Criteria) this;
        }

        public Criteria andCitycodeBetween(String value1, String value2) {
            addCriterion("CITYCODE between", value1, value2, "citycode");
            return (Criteria) this;
        }

        public Criteria andCitycodeNotBetween(String value1, String value2) {
            addCriterion("CITYCODE not between", value1, value2, "citycode");
            return (Criteria) this;
        }

        public Criteria andCitynameIsNull() {
            addCriterion("CITYNAME is null");
            return (Criteria) this;
        }

        public Criteria andCitynameIsNotNull() {
            addCriterion("CITYNAME is not null");
            return (Criteria) this;
        }

        public Criteria andCitynameEqualTo(String value) {
            addCriterion("CITYNAME =", value, "cityname");
            return (Criteria) this;
        }

        public Criteria andCitynameNotEqualTo(String value) {
            addCriterion("CITYNAME <>", value, "cityname");
            return (Criteria) this;
        }

        public Criteria andCitynameGreaterThan(String value) {
            addCriterion("CITYNAME >", value, "cityname");
            return (Criteria) this;
        }

        public Criteria andCitynameGreaterThanOrEqualTo(String value) {
            addCriterion("CITYNAME >=", value, "cityname");
            return (Criteria) this;
        }

        public Criteria andCitynameLessThan(String value) {
            addCriterion("CITYNAME <", value, "cityname");
            return (Criteria) this;
        }

        public Criteria andCitynameLessThanOrEqualTo(String value) {
            addCriterion("CITYNAME <=", value, "cityname");
            return (Criteria) this;
        }

        public Criteria andCitynameLike(String value) {
            addCriterion("CITYNAME like", value, "cityname");
            return (Criteria) this;
        }

        public Criteria andCitynameNotLike(String value) {
            addCriterion("CITYNAME not like", value, "cityname");
            return (Criteria) this;
        }

        public Criteria andCitynameIn(List<String> values) {
            addCriterion("CITYNAME in", values, "cityname");
            return (Criteria) this;
        }

        public Criteria andCitynameNotIn(List<String> values) {
            addCriterion("CITYNAME not in", values, "cityname");
            return (Criteria) this;
        }

        public Criteria andCitynameBetween(String value1, String value2) {
            addCriterion("CITYNAME between", value1, value2, "cityname");
            return (Criteria) this;
        }

        public Criteria andCitynameNotBetween(String value1, String value2) {
            addCriterion("CITYNAME not between", value1, value2, "cityname");
            return (Criteria) this;
        }

        public Criteria andCountycodeIsNull() {
            addCriterion("COUNTYCODE is null");
            return (Criteria) this;
        }

        public Criteria andCountycodeIsNotNull() {
            addCriterion("COUNTYCODE is not null");
            return (Criteria) this;
        }

        public Criteria andCountycodeEqualTo(String value) {
            addCriterion("COUNTYCODE =", value, "countycode");
            return (Criteria) this;
        }

        public Criteria andCountycodeNotEqualTo(String value) {
            addCriterion("COUNTYCODE <>", value, "countycode");
            return (Criteria) this;
        }

        public Criteria andCountycodeGreaterThan(String value) {
            addCriterion("COUNTYCODE >", value, "countycode");
            return (Criteria) this;
        }

        public Criteria andCountycodeGreaterThanOrEqualTo(String value) {
            addCriterion("COUNTYCODE >=", value, "countycode");
            return (Criteria) this;
        }

        public Criteria andCountycodeLessThan(String value) {
            addCriterion("COUNTYCODE <", value, "countycode");
            return (Criteria) this;
        }

        public Criteria andCountycodeLessThanOrEqualTo(String value) {
            addCriterion("COUNTYCODE <=", value, "countycode");
            return (Criteria) this;
        }

        public Criteria andCountycodeLike(String value) {
            addCriterion("COUNTYCODE like", value, "countycode");
            return (Criteria) this;
        }

        public Criteria andCountycodeNotLike(String value) {
            addCriterion("COUNTYCODE not like", value, "countycode");
            return (Criteria) this;
        }

        public Criteria andCountycodeIn(List<String> values) {
            addCriterion("COUNTYCODE in", values, "countycode");
            return (Criteria) this;
        }

        public Criteria andCountycodeNotIn(List<String> values) {
            addCriterion("COUNTYCODE not in", values, "countycode");
            return (Criteria) this;
        }

        public Criteria andCountycodeBetween(String value1, String value2) {
            addCriterion("COUNTYCODE between", value1, value2, "countycode");
            return (Criteria) this;
        }

        public Criteria andCountycodeNotBetween(String value1, String value2) {
            addCriterion("COUNTYCODE not between", value1, value2, "countycode");
            return (Criteria) this;
        }

        public Criteria andCountynameIsNull() {
            addCriterion("COUNTYNAME is null");
            return (Criteria) this;
        }

        public Criteria andCountynameIsNotNull() {
            addCriterion("COUNTYNAME is not null");
            return (Criteria) this;
        }

        public Criteria andCountynameEqualTo(String value) {
            addCriterion("COUNTYNAME =", value, "countyname");
            return (Criteria) this;
        }

        public Criteria andCountynameNotEqualTo(String value) {
            addCriterion("COUNTYNAME <>", value, "countyname");
            return (Criteria) this;
        }

        public Criteria andCountynameGreaterThan(String value) {
            addCriterion("COUNTYNAME >", value, "countyname");
            return (Criteria) this;
        }

        public Criteria andCountynameGreaterThanOrEqualTo(String value) {
            addCriterion("COUNTYNAME >=", value, "countyname");
            return (Criteria) this;
        }

        public Criteria andCountynameLessThan(String value) {
            addCriterion("COUNTYNAME <", value, "countyname");
            return (Criteria) this;
        }

        public Criteria andCountynameLessThanOrEqualTo(String value) {
            addCriterion("COUNTYNAME <=", value, "countyname");
            return (Criteria) this;
        }

        public Criteria andCountynameLike(String value) {
            addCriterion("COUNTYNAME like", value, "countyname");
            return (Criteria) this;
        }

        public Criteria andCountynameNotLike(String value) {
            addCriterion("COUNTYNAME not like", value, "countyname");
            return (Criteria) this;
        }

        public Criteria andCountynameIn(List<String> values) {
            addCriterion("COUNTYNAME in", values, "countyname");
            return (Criteria) this;
        }

        public Criteria andCountynameNotIn(List<String> values) {
            addCriterion("COUNTYNAME not in", values, "countyname");
            return (Criteria) this;
        }

        public Criteria andCountynameBetween(String value1, String value2) {
            addCriterion("COUNTYNAME between", value1, value2, "countyname");
            return (Criteria) this;
        }

        public Criteria andCountynameNotBetween(String value1, String value2) {
            addCriterion("COUNTYNAME not between", value1, value2, "countyname");
            return (Criteria) this;
        }

        public Criteria andControlflagIsNull() {
            addCriterion("CONTROLFLAG is null");
            return (Criteria) this;
        }

        public Criteria andControlflagIsNotNull() {
            addCriterion("CONTROLFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andControlflagEqualTo(String value) {
            addCriterion("CONTROLFLAG =", value, "controlflag");
            return (Criteria) this;
        }

        public Criteria andControlflagNotEqualTo(String value) {
            addCriterion("CONTROLFLAG <>", value, "controlflag");
            return (Criteria) this;
        }

        public Criteria andControlflagGreaterThan(String value) {
            addCriterion("CONTROLFLAG >", value, "controlflag");
            return (Criteria) this;
        }

        public Criteria andControlflagGreaterThanOrEqualTo(String value) {
            addCriterion("CONTROLFLAG >=", value, "controlflag");
            return (Criteria) this;
        }

        public Criteria andControlflagLessThan(String value) {
            addCriterion("CONTROLFLAG <", value, "controlflag");
            return (Criteria) this;
        }

        public Criteria andControlflagLessThanOrEqualTo(String value) {
            addCriterion("CONTROLFLAG <=", value, "controlflag");
            return (Criteria) this;
        }

        public Criteria andControlflagLike(String value) {
            addCriterion("CONTROLFLAG like", value, "controlflag");
            return (Criteria) this;
        }

        public Criteria andControlflagNotLike(String value) {
            addCriterion("CONTROLFLAG not like", value, "controlflag");
            return (Criteria) this;
        }

        public Criteria andControlflagIn(List<String> values) {
            addCriterion("CONTROLFLAG in", values, "controlflag");
            return (Criteria) this;
        }

        public Criteria andControlflagNotIn(List<String> values) {
            addCriterion("CONTROLFLAG not in", values, "controlflag");
            return (Criteria) this;
        }

        public Criteria andControlflagBetween(String value1, String value2) {
            addCriterion("CONTROLFLAG between", value1, value2, "controlflag");
            return (Criteria) this;
        }

        public Criteria andControlflagNotBetween(String value1, String value2) {
            addCriterion("CONTROLFLAG not between", value1, value2, "controlflag");
            return (Criteria) this;
        }

        public Criteria andOrganzationstartdateIsNull() {
            addCriterion("ORGANZATIONSTARTDATE is null");
            return (Criteria) this;
        }

        public Criteria andOrganzationstartdateIsNotNull() {
            addCriterion("ORGANZATIONSTARTDATE is not null");
            return (Criteria) this;
        }

        public Criteria andOrganzationstartdateEqualTo(Date value) {
            addCriterion("ORGANZATIONSTARTDATE =", value, "organzationstartdate");
            return (Criteria) this;
        }

        public Criteria andOrganzationstartdateNotEqualTo(Date value) {
            addCriterion("ORGANZATIONSTARTDATE <>", value, "organzationstartdate");
            return (Criteria) this;
        }

        public Criteria andOrganzationstartdateGreaterThan(Date value) {
            addCriterion("ORGANZATIONSTARTDATE >", value, "organzationstartdate");
            return (Criteria) this;
        }

        public Criteria andOrganzationstartdateGreaterThanOrEqualTo(Date value) {
            addCriterion("ORGANZATIONSTARTDATE >=", value, "organzationstartdate");
            return (Criteria) this;
        }

        public Criteria andOrganzationstartdateLessThan(Date value) {
            addCriterion("ORGANZATIONSTARTDATE <", value, "organzationstartdate");
            return (Criteria) this;
        }

        public Criteria andOrganzationstartdateLessThanOrEqualTo(Date value) {
            addCriterion("ORGANZATIONSTARTDATE <=", value, "organzationstartdate");
            return (Criteria) this;
        }

        public Criteria andOrganzationstartdateIn(List<Date> values) {
            addCriterion("ORGANZATIONSTARTDATE in", values, "organzationstartdate");
            return (Criteria) this;
        }

        public Criteria andOrganzationstartdateNotIn(List<Date> values) {
            addCriterion("ORGANZATIONSTARTDATE not in", values, "organzationstartdate");
            return (Criteria) this;
        }

        public Criteria andOrganzationstartdateBetween(Date value1, Date value2) {
            addCriterion("ORGANZATIONSTARTDATE between", value1, value2, "organzationstartdate");
            return (Criteria) this;
        }

        public Criteria andOrganzationstartdateNotBetween(Date value1, Date value2) {
            addCriterion("ORGANZATIONSTARTDATE not between", value1, value2, "organzationstartdate");
            return (Criteria) this;
        }

        public Criteria andOrganzationenddateIsNull() {
            addCriterion("ORGANZATIONENDDATE is null");
            return (Criteria) this;
        }

        public Criteria andOrganzationenddateIsNotNull() {
            addCriterion("ORGANZATIONENDDATE is not null");
            return (Criteria) this;
        }

        public Criteria andOrganzationenddateEqualTo(Date value) {
            addCriterion("ORGANZATIONENDDATE =", value, "organzationenddate");
            return (Criteria) this;
        }

        public Criteria andOrganzationenddateNotEqualTo(Date value) {
            addCriterion("ORGANZATIONENDDATE <>", value, "organzationenddate");
            return (Criteria) this;
        }

        public Criteria andOrganzationenddateGreaterThan(Date value) {
            addCriterion("ORGANZATIONENDDATE >", value, "organzationenddate");
            return (Criteria) this;
        }

        public Criteria andOrganzationenddateGreaterThanOrEqualTo(Date value) {
            addCriterion("ORGANZATIONENDDATE >=", value, "organzationenddate");
            return (Criteria) this;
        }

        public Criteria andOrganzationenddateLessThan(Date value) {
            addCriterion("ORGANZATIONENDDATE <", value, "organzationenddate");
            return (Criteria) this;
        }

        public Criteria andOrganzationenddateLessThanOrEqualTo(Date value) {
            addCriterion("ORGANZATIONENDDATE <=", value, "organzationenddate");
            return (Criteria) this;
        }

        public Criteria andOrganzationenddateIn(List<Date> values) {
            addCriterion("ORGANZATIONENDDATE in", values, "organzationenddate");
            return (Criteria) this;
        }

        public Criteria andOrganzationenddateNotIn(List<Date> values) {
            addCriterion("ORGANZATIONENDDATE not in", values, "organzationenddate");
            return (Criteria) this;
        }

        public Criteria andOrganzationenddateBetween(Date value1, Date value2) {
            addCriterion("ORGANZATIONENDDATE between", value1, value2, "organzationenddate");
            return (Criteria) this;
        }

        public Criteria andOrganzationenddateNotBetween(Date value1, Date value2) {
            addCriterion("ORGANZATIONENDDATE not between", value1, value2, "organzationenddate");
            return (Criteria) this;
        }

        public Criteria andTaxpayertypeIsNull() {
            addCriterion("TAXPAYERTYPE is null");
            return (Criteria) this;
        }

        public Criteria andTaxpayertypeIsNotNull() {
            addCriterion("TAXPAYERTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andTaxpayertypeEqualTo(String value) {
            addCriterion("TAXPAYERTYPE =", value, "taxpayertype");
            return (Criteria) this;
        }

        public Criteria andTaxpayertypeNotEqualTo(String value) {
            addCriterion("TAXPAYERTYPE <>", value, "taxpayertype");
            return (Criteria) this;
        }

        public Criteria andTaxpayertypeGreaterThan(String value) {
            addCriterion("TAXPAYERTYPE >", value, "taxpayertype");
            return (Criteria) this;
        }

        public Criteria andTaxpayertypeGreaterThanOrEqualTo(String value) {
            addCriterion("TAXPAYERTYPE >=", value, "taxpayertype");
            return (Criteria) this;
        }

        public Criteria andTaxpayertypeLessThan(String value) {
            addCriterion("TAXPAYERTYPE <", value, "taxpayertype");
            return (Criteria) this;
        }

        public Criteria andTaxpayertypeLessThanOrEqualTo(String value) {
            addCriterion("TAXPAYERTYPE <=", value, "taxpayertype");
            return (Criteria) this;
        }

        public Criteria andTaxpayertypeLike(String value) {
            addCriterion("TAXPAYERTYPE like", value, "taxpayertype");
            return (Criteria) this;
        }

        public Criteria andTaxpayertypeNotLike(String value) {
            addCriterion("TAXPAYERTYPE not like", value, "taxpayertype");
            return (Criteria) this;
        }

        public Criteria andTaxpayertypeIn(List<String> values) {
            addCriterion("TAXPAYERTYPE in", values, "taxpayertype");
            return (Criteria) this;
        }

        public Criteria andTaxpayertypeNotIn(List<String> values) {
            addCriterion("TAXPAYERTYPE not in", values, "taxpayertype");
            return (Criteria) this;
        }

        public Criteria andTaxpayertypeBetween(String value1, String value2) {
            addCriterion("TAXPAYERTYPE between", value1, value2, "taxpayertype");
            return (Criteria) this;
        }

        public Criteria andTaxpayertypeNotBetween(String value1, String value2) {
            addCriterion("TAXPAYERTYPE not between", value1, value2, "taxpayertype");
            return (Criteria) this;
        }

        public Criteria andTaxpayernoIsNull() {
            addCriterion("TAXPAYERNO is null");
            return (Criteria) this;
        }

        public Criteria andTaxpayernoIsNotNull() {
            addCriterion("TAXPAYERNO is not null");
            return (Criteria) this;
        }

        public Criteria andTaxpayernoEqualTo(String value) {
            addCriterion("TAXPAYERNO =", value, "taxpayerno");
            return (Criteria) this;
        }

        public Criteria andTaxpayernoNotEqualTo(String value) {
            addCriterion("TAXPAYERNO <>", value, "taxpayerno");
            return (Criteria) this;
        }

        public Criteria andTaxpayernoGreaterThan(String value) {
            addCriterion("TAXPAYERNO >", value, "taxpayerno");
            return (Criteria) this;
        }

        public Criteria andTaxpayernoGreaterThanOrEqualTo(String value) {
            addCriterion("TAXPAYERNO >=", value, "taxpayerno");
            return (Criteria) this;
        }

        public Criteria andTaxpayernoLessThan(String value) {
            addCriterion("TAXPAYERNO <", value, "taxpayerno");
            return (Criteria) this;
        }

        public Criteria andTaxpayernoLessThanOrEqualTo(String value) {
            addCriterion("TAXPAYERNO <=", value, "taxpayerno");
            return (Criteria) this;
        }

        public Criteria andTaxpayernoLike(String value) {
            addCriterion("TAXPAYERNO like", value, "taxpayerno");
            return (Criteria) this;
        }

        public Criteria andTaxpayernoNotLike(String value) {
            addCriterion("TAXPAYERNO not like", value, "taxpayerno");
            return (Criteria) this;
        }

        public Criteria andTaxpayernoIn(List<String> values) {
            addCriterion("TAXPAYERNO in", values, "taxpayerno");
            return (Criteria) this;
        }

        public Criteria andTaxpayernoNotIn(List<String> values) {
            addCriterion("TAXPAYERNO not in", values, "taxpayerno");
            return (Criteria) this;
        }

        public Criteria andTaxpayernoBetween(String value1, String value2) {
            addCriterion("TAXPAYERNO between", value1, value2, "taxpayerno");
            return (Criteria) this;
        }

        public Criteria andTaxpayernoNotBetween(String value1, String value2) {
            addCriterion("TAXPAYERNO not between", value1, value2, "taxpayerno");
            return (Criteria) this;
        }

        public Criteria andAgreementnoIsNull() {
            addCriterion("AGREEMENTNO is null");
            return (Criteria) this;
        }

        public Criteria andAgreementnoIsNotNull() {
            addCriterion("AGREEMENTNO is not null");
            return (Criteria) this;
        }

        public Criteria andAgreementnoEqualTo(String value) {
            addCriterion("AGREEMENTNO =", value, "agreementno");
            return (Criteria) this;
        }

        public Criteria andAgreementnoNotEqualTo(String value) {
            addCriterion("AGREEMENTNO <>", value, "agreementno");
            return (Criteria) this;
        }

        public Criteria andAgreementnoGreaterThan(String value) {
            addCriterion("AGREEMENTNO >", value, "agreementno");
            return (Criteria) this;
        }

        public Criteria andAgreementnoGreaterThanOrEqualTo(String value) {
            addCriterion("AGREEMENTNO >=", value, "agreementno");
            return (Criteria) this;
        }

        public Criteria andAgreementnoLessThan(String value) {
            addCriterion("AGREEMENTNO <", value, "agreementno");
            return (Criteria) this;
        }

        public Criteria andAgreementnoLessThanOrEqualTo(String value) {
            addCriterion("AGREEMENTNO <=", value, "agreementno");
            return (Criteria) this;
        }

        public Criteria andAgreementnoLike(String value) {
            addCriterion("AGREEMENTNO like", value, "agreementno");
            return (Criteria) this;
        }

        public Criteria andAgreementnoNotLike(String value) {
            addCriterion("AGREEMENTNO not like", value, "agreementno");
            return (Criteria) this;
        }

        public Criteria andAgreementnoIn(List<String> values) {
            addCriterion("AGREEMENTNO in", values, "agreementno");
            return (Criteria) this;
        }

        public Criteria andAgreementnoNotIn(List<String> values) {
            addCriterion("AGREEMENTNO not in", values, "agreementno");
            return (Criteria) this;
        }

        public Criteria andAgreementnoBetween(String value1, String value2) {
            addCriterion("AGREEMENTNO between", value1, value2, "agreementno");
            return (Criteria) this;
        }

        public Criteria andAgreementnoNotBetween(String value1, String value2) {
            addCriterion("AGREEMENTNO not between", value1, value2, "agreementno");
            return (Criteria) this;
        }

        public Criteria andAgreementnameIsNull() {
            addCriterion("AGREEMENTNAME is null");
            return (Criteria) this;
        }

        public Criteria andAgreementnameIsNotNull() {
            addCriterion("AGREEMENTNAME is not null");
            return (Criteria) this;
        }

        public Criteria andAgreementnameEqualTo(String value) {
            addCriterion("AGREEMENTNAME =", value, "agreementname");
            return (Criteria) this;
        }

        public Criteria andAgreementnameNotEqualTo(String value) {
            addCriterion("AGREEMENTNAME <>", value, "agreementname");
            return (Criteria) this;
        }

        public Criteria andAgreementnameGreaterThan(String value) {
            addCriterion("AGREEMENTNAME >", value, "agreementname");
            return (Criteria) this;
        }

        public Criteria andAgreementnameGreaterThanOrEqualTo(String value) {
            addCriterion("AGREEMENTNAME >=", value, "agreementname");
            return (Criteria) this;
        }

        public Criteria andAgreementnameLessThan(String value) {
            addCriterion("AGREEMENTNAME <", value, "agreementname");
            return (Criteria) this;
        }

        public Criteria andAgreementnameLessThanOrEqualTo(String value) {
            addCriterion("AGREEMENTNAME <=", value, "agreementname");
            return (Criteria) this;
        }

        public Criteria andAgreementnameLike(String value) {
            addCriterion("AGREEMENTNAME like", value, "agreementname");
            return (Criteria) this;
        }

        public Criteria andAgreementnameNotLike(String value) {
            addCriterion("AGREEMENTNAME not like", value, "agreementname");
            return (Criteria) this;
        }

        public Criteria andAgreementnameIn(List<String> values) {
            addCriterion("AGREEMENTNAME in", values, "agreementname");
            return (Criteria) this;
        }

        public Criteria andAgreementnameNotIn(List<String> values) {
            addCriterion("AGREEMENTNAME not in", values, "agreementname");
            return (Criteria) this;
        }

        public Criteria andAgreementnameBetween(String value1, String value2) {
            addCriterion("AGREEMENTNAME between", value1, value2, "agreementname");
            return (Criteria) this;
        }

        public Criteria andAgreementnameNotBetween(String value1, String value2) {
            addCriterion("AGREEMENTNAME not between", value1, value2, "agreementname");
            return (Criteria) this;
        }

        public Criteria andAgreementno1IsNull() {
            addCriterion("AGREEMENTNO1 is null");
            return (Criteria) this;
        }

        public Criteria andAgreementno1IsNotNull() {
            addCriterion("AGREEMENTNO1 is not null");
            return (Criteria) this;
        }

        public Criteria andAgreementno1EqualTo(String value) {
            addCriterion("AGREEMENTNO1 =", value, "agreementno1");
            return (Criteria) this;
        }

        public Criteria andAgreementno1NotEqualTo(String value) {
            addCriterion("AGREEMENTNO1 <>", value, "agreementno1");
            return (Criteria) this;
        }

        public Criteria andAgreementno1GreaterThan(String value) {
            addCriterion("AGREEMENTNO1 >", value, "agreementno1");
            return (Criteria) this;
        }

        public Criteria andAgreementno1GreaterThanOrEqualTo(String value) {
            addCriterion("AGREEMENTNO1 >=", value, "agreementno1");
            return (Criteria) this;
        }

        public Criteria andAgreementno1LessThan(String value) {
            addCriterion("AGREEMENTNO1 <", value, "agreementno1");
            return (Criteria) this;
        }

        public Criteria andAgreementno1LessThanOrEqualTo(String value) {
            addCriterion("AGREEMENTNO1 <=", value, "agreementno1");
            return (Criteria) this;
        }

        public Criteria andAgreementno1Like(String value) {
            addCriterion("AGREEMENTNO1 like", value, "agreementno1");
            return (Criteria) this;
        }

        public Criteria andAgreementno1NotLike(String value) {
            addCriterion("AGREEMENTNO1 not like", value, "agreementno1");
            return (Criteria) this;
        }

        public Criteria andAgreementno1In(List<String> values) {
            addCriterion("AGREEMENTNO1 in", values, "agreementno1");
            return (Criteria) this;
        }

        public Criteria andAgreementno1NotIn(List<String> values) {
            addCriterion("AGREEMENTNO1 not in", values, "agreementno1");
            return (Criteria) this;
        }

        public Criteria andAgreementno1Between(String value1, String value2) {
            addCriterion("AGREEMENTNO1 between", value1, value2, "agreementno1");
            return (Criteria) this;
        }

        public Criteria andAgreementno1NotBetween(String value1, String value2) {
            addCriterion("AGREEMENTNO1 not between", value1, value2, "agreementno1");
            return (Criteria) this;
        }

        public Criteria andAgreementname1IsNull() {
            addCriterion("AGREEMENTNAME1 is null");
            return (Criteria) this;
        }

        public Criteria andAgreementname1IsNotNull() {
            addCriterion("AGREEMENTNAME1 is not null");
            return (Criteria) this;
        }

        public Criteria andAgreementname1EqualTo(String value) {
            addCriterion("AGREEMENTNAME1 =", value, "agreementname1");
            return (Criteria) this;
        }

        public Criteria andAgreementname1NotEqualTo(String value) {
            addCriterion("AGREEMENTNAME1 <>", value, "agreementname1");
            return (Criteria) this;
        }

        public Criteria andAgreementname1GreaterThan(String value) {
            addCriterion("AGREEMENTNAME1 >", value, "agreementname1");
            return (Criteria) this;
        }

        public Criteria andAgreementname1GreaterThanOrEqualTo(String value) {
            addCriterion("AGREEMENTNAME1 >=", value, "agreementname1");
            return (Criteria) this;
        }

        public Criteria andAgreementname1LessThan(String value) {
            addCriterion("AGREEMENTNAME1 <", value, "agreementname1");
            return (Criteria) this;
        }

        public Criteria andAgreementname1LessThanOrEqualTo(String value) {
            addCriterion("AGREEMENTNAME1 <=", value, "agreementname1");
            return (Criteria) this;
        }

        public Criteria andAgreementname1Like(String value) {
            addCriterion("AGREEMENTNAME1 like", value, "agreementname1");
            return (Criteria) this;
        }

        public Criteria andAgreementname1NotLike(String value) {
            addCriterion("AGREEMENTNAME1 not like", value, "agreementname1");
            return (Criteria) this;
        }

        public Criteria andAgreementname1In(List<String> values) {
            addCriterion("AGREEMENTNAME1 in", values, "agreementname1");
            return (Criteria) this;
        }

        public Criteria andAgreementname1NotIn(List<String> values) {
            addCriterion("AGREEMENTNAME1 not in", values, "agreementname1");
            return (Criteria) this;
        }

        public Criteria andAgreementname1Between(String value1, String value2) {
            addCriterion("AGREEMENTNAME1 between", value1, value2, "agreementname1");
            return (Criteria) this;
        }

        public Criteria andAgreementname1NotBetween(String value1, String value2) {
            addCriterion("AGREEMENTNAME1 not between", value1, value2, "agreementname1");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}