package cn.com.libertymutual.sp.service.api;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpApplicant;
import cn.com.libertymutual.sp.bean.TbSpOrder;
import cn.com.libertymutual.sp.webService.einv.EinvoicePrintRequestDto;

public interface IOrderService {
	ServiceResult sendEinvPrintData( EinvoicePrintRequestDto einvPrintData);


	ServiceResult downPolicy(String policyNo,String proposalDate,String emailStr,String applicantName,HttpServletResponse response);
	ServiceResult orderList(String comCode,String insuranceDateStart,String insuranceDateEnd,String payDateStart,String payDateEnd,String createTimeStart,String createTimeEnd,String policyNo,
    		String proposalNo,String productName,String planName,
    		String applicantName,String idNo,String insuredPersonName,String insuredPersonNo,String paymentNo,String branchCode,String agreementNo,String status,
    		String refereeId,String refereeMobile,String userName,String mobile,String scoreUser,String scoreUserMobile,String successfulUrl,String shareUid,String callBackUrl,String orderNo,String dealUuid,int pageNumber,int pageSize);

	ServiceResult saveOrder(TbSpOrder spOrder);

	ServiceResult queryEinv(String policyNo, String identifyNo);
	
	ServiceResult findOrder(HttpServletRequest request,String valCode,String type,String findData,String policyNo);
	
	ServiceResult getOrderList(String sortfield,String sortfieldData,String sorttype,String sortData,int pageNumber,int pageSize,String status);
	void SetInvalidOrder(String userId);


	ServiceResult downBJPolicy(String policyNo, String proposalDate,
			String emailStr, String applicantName, HttpServletResponse response);


	ServiceResult createEPolicy(String documentNo);


	ServiceResult performance(String userCode, String queryType,
			String queryParam,String userCodeBs);


	ServiceResult performanceList(String userCode, String queryType,
			String queryParam,String userCodeBs);
	ServiceResult performanceDetail(String userCode, String queryType,
			String queryParam,String userCodeBs);
	
	ServiceResult performanceCountMonth(String userCode,String userCodeBs);


	ServiceResult findBySql(String sql);


	ServiceResult findPolicyInfo(String orderNo, String name, String mobile, int pageNumber, int pageSize);


	ServiceResult upOrder(TbSpOrder tbSpOrder);


	ServiceResult addPolicy(TbSpApplicant applicant);


	ServiceResult countProductList();


	ServiceResult branchProductList();


	ServiceResult myProductCountList(int pageNumber, int pageSize);
}
