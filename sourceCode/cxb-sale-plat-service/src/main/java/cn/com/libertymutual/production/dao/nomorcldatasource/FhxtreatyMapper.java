package cn.com.libertymutual.production.dao.nomorcldatasource;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import cn.com.libertymutual.production.model.nomorcldatasource.Fhxtreaty;
import cn.com.libertymutual.production.model.nomorcldatasource.FhxtreatyExample;
@Mapper
public interface FhxtreatyMapper {
    int countByExample(FhxtreatyExample example);

    int deleteByExample(FhxtreatyExample example);

    int deleteByPrimaryKey(String treatyno);

    int insert(Fhxtreaty record);

    int insertSelective(Fhxtreaty record);

    List<Fhxtreaty> selectByExample(FhxtreatyExample example);

    Fhxtreaty selectByPrimaryKey(String treatyno);

    int updateByExampleSelective(@Param("record") Fhxtreaty record, @Param("example") FhxtreatyExample example);

    int updateByExample(@Param("record") Fhxtreaty record, @Param("example") FhxtreatyExample example);

    int updateByPrimaryKeySelective(Fhxtreaty record);

    int updateByPrimaryKey(Fhxtreaty record);
    
    @Select(
            "SELECT * "
                    + " FROM FHXTREATY A "
                    + "WHERE A.UWYEAR = #{uwyear} "
                    + "  AND EXISTS "
                    + "(SELECT 1 FROM FHXSECTION B WHERE B.TREATYNO = A.TREATYNO) "
    )
    List<Fhxtreaty> findAllBySectionExisted(String uwyear);
}