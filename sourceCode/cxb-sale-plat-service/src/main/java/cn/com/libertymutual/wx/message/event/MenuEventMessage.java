package cn.com.libertymutual.wx.message.event;

public class MenuEventMessage extends BaseEventMessage {
	private String EventKey;

	public String getEventKey() {
		return this.EventKey;
	}

	public void setEventKey(String eventKey) {
		this.EventKey = eventKey;
	}
}
