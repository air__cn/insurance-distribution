package cn.com.libertymutual.sp.service.api;

import java.io.IOException;
import java.text.ParseException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.com.libertymutual.core.redis.util.RedisUtils;
import cn.com.libertymutual.core.web.ServiceResult;

/**
 * 图片验证码的工具类
 * @author AoYi
 */
public interface CaptchaService {

	/**Remarks: 输出图形验证码图像<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年10月8日下午3:04:32<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param response
	 * @param redis
	 * @throws ServletException
	 * @throws IOException
	 */
	public void outputCaptcha(HttpServletRequest request, HttpServletResponse response, RedisUtils redis) throws ServletException, IOException;

	/**Remarks: 验证图形验证码<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年10月17日下午2:08:48<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param sr
	 * @param redis
	 * @param imgCode 输入的验证码
	 * @return
	 * @throws ParseException
	 */
	public boolean validationCaptcha(HttpServletRequest request, ServiceResult sr, RedisUtils redis, String imgCode) throws ParseException;
}