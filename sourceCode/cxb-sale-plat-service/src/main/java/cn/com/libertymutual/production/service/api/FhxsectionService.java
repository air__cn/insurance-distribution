package cn.com.libertymutual.production.service.api;

import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.Fhxsection;

/**
 * @author steven.li
 * @create 2017/12/20
 */
public interface FhxsectionService {

    void insert(Fhxsection record);

    List<Fhxsection> findAll(String treatyno, String layerno);

}
