package cn.com.libertymutual.production.pojo.response;

import java.util.Date;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdkind;

public class PrpdkindV1 extends Prpdkind {

	private String riskcname;
	private String riskename;
	private Date riskStartDate;
	private Date riskEndDate;

	public String getRiskcname() {
		return riskcname;
	}

	public void setRiskcname(String riskcname) {
		this.riskcname = riskcname;
	}

	public String getRiskename() {
		return riskename;
	}

	public void setRiskename(String riskename) {
		this.riskename = riskename;
	}

	public Date getRiskStartDate() {
		return riskStartDate;
	}

	public void setRiskStartDate(Date riskStartDate) {
		this.riskStartDate = riskStartDate;
	}

	public Date getRiskEndDate() {
		return riskEndDate;
	}

	public void setRiskEndDate(Date riskEndDate) {
		this.riskEndDate = riskEndDate;
	}

}