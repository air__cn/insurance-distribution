package cn.com.libertymutual.production.service.api;


import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdefile;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdefileWithBLOBs;
import cn.com.libertymutual.production.pojo.request.PrpdEFileRequest;

import com.github.pagehelper.PageInfo;

public interface PrpdEFileService {

	/**
	 * 查询备案号信息
	 * @param prpdEFileRequest
	 * @return
	 */
	public PageInfo<PrpdefileWithBLOBs> findPrpdEfile(PrpdEFileRequest prpdEFileRequest);
	
	public void insert(PrpdefileWithBLOBs record);
	
	public void update(PrpdefileWithBLOBs record);
	
	/**
	 * 查询EFileCode的序列号，生成efilecode
	 * @param kindCode
	 * @return
	 */
	public String findEFileCode(String code);
	
	public List<PrpdefileWithBLOBs> fetchEFiles(String eFileCode, String kindCode, String kindVersion);
	
}
