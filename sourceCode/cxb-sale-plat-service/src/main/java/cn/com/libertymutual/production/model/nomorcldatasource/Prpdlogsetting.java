package cn.com.libertymutual.production.model.nomorcldatasource;

import java.util.Date;

public class Prpdlogsetting {
    private Long rowidObject;

    private String tablename;

    private String businessname;

    private String createdby;

    private Date createdate;

    public Long getRowidObject() {
        return rowidObject;
    }

    public void setRowidObject(Long rowidObject) {
        this.rowidObject = rowidObject;
    }

    public String getTablename() {
        return tablename;
    }

    public void setTablename(String tablename) {
        this.tablename = tablename == null ? null : tablename.trim();
    }

    public String getBusinessname() {
        return businessname;
    }

    public void setBusinessname(String businessname) {
        this.businessname = businessname == null ? null : businessname.trim();
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby == null ? null : createdby.trim();
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }
}