package cn.com.libertymutual.sp.dto.callback;

import java.util.Date;

public class Appnt {
	private String appntName;   // 投保人姓名
	private String appntCertificationCode; // 投保人证件号
	private Date appntBirthday; //投保人生日
	private String appntSex;   // 投保人性别
	private String appntAddress; // 投保人地址
	private String appntCertificationType; // 投保人证件类型
	private String appntEmail; // 投保人邮箱
	private String mobile; // 投保人手机号码
	public String getAppntName() {
		return appntName;
	}
	public void setAppntName(String appntName) {
		this.appntName = appntName;
	}
	public String getAppntCertificationCode() {
		return appntCertificationCode;
	}
	public void setAppntCertificationCode(String appntCertificationCode) {
		this.appntCertificationCode = appntCertificationCode;
	}
	public Date getAppntBirthday() {
		return appntBirthday;
	}
	public void setAppntBirthday(Date appntBirthday) {
		this.appntBirthday = appntBirthday;
	}
	public String getAppntSex() {
		return appntSex;
	}
	public void setAppntSex(String appntSex) {
		this.appntSex = appntSex;
	}
	public String getAppntAddress() {
		return appntAddress;
	}
	public void setAppntAddress(String appntAddress) {
		this.appntAddress = appntAddress;
	}
	public String getAppntCertificationType() {
		return appntCertificationType;
	}
	public void setAppntCertificationType(String appntCertificationType) {
		this.appntCertificationType = appntCertificationType;
	}
	public String getAppntEmail() {
		return appntEmail;
	}
	public void setAppntEmail(String appntEmail) {
		this.appntEmail = appntEmail;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	
}
