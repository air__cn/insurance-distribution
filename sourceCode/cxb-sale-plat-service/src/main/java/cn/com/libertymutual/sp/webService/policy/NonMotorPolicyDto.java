
package cn.com.libertymutual.sp.webService.policy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for nonMotorPolicyDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="nonMotorPolicyDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="prpTItemShipDto" type="{http://service.liberty.com/common/bean}prpTItemShipDto" minOccurs="0"/>
 *         &lt;element name="prpTMainCargoSubDto" type="{http://service.liberty.com/common/bean}prpTMainCargoSubDto" minOccurs="0"/>
 *         &lt;element name="prpTvoyageDto" type="{http://service.liberty.com/common/bean}prpTvoyageDto" minOccurs="0"/>
 *         &lt;element name="prptAdjustDtoList" type="{http://service.liberty.com/common/bean}prptAdjustDto" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="prptApplicantDto" type="{http://service.liberty.com/common/bean}prptApplicantDto" minOccurs="0"/>
 *         &lt;element name="prptBeneficiaryDtoList" type="{http://service.liberty.com/common/bean}prptBeneficiaryDto" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="prptCoinsDtoList" type="{http://service.liberty.com/common/bean}prptCoinsDto" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="prptCommissionDtoList" type="{http://service.liberty.com/common/bean}prptCommissionDto" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="prptEngageDtoList" type="{http://service.liberty.com/common/bean}prptEngageDto" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="prptFeeDto" type="{http://service.liberty.com/common/bean}prptFeeDto" minOccurs="0"/>
 *         &lt;element name="prptInsuredDtoList" type="{http://service.liberty.com/common/bean}prptInsuredDto" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="prptItemKindDtoList" type="{http://service.liberty.com/common/bean}prptItemKindDto" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="prptLimitDtoList" type="{http://service.liberty.com/common/bean}prptLimitDto" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="prptMainDto" type="{http://service.liberty.com/common/bean}prptMainDto" minOccurs="0"/>
 *         &lt;element name="prptMainpropDtoList" type="{http://service.liberty.com/common/bean}prptMainPropDto" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="prptPlanDtoList" type="{http://service.liberty.com/common/bean}prptPlanDto" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="prptReinscededDto" type="{http://service.liberty.com/common/bean}prptReinscededDto" minOccurs="0"/>
 *         &lt;element name="prptaddressDtoList" type="{http://service.liberty.com/common/bean}prptaddressDto" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="prptmaincargoDto" type="{http://service.liberty.com/common/bean}prptmaincargoDto" minOccurs="0"/>
 *         &lt;element name="prptnameDtoList" type="{http://service.liberty.com/common/bean}prptnameDto" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "nonMotorPolicyDto", propOrder = {
    "prpTItemShipDto",
    "prpTMainCargoSubDto",
    "prpTvoyageDto",
    "prptAdjustDtoList",
    "prptApplicantDto",
    "prptBeneficiaryDtoList",
    "prptCoinsDtoList",
    "prptCommissionDtoList",
    "prptEngageDtoList",
    "prptFeeDto",
    "prptInsuredDtoList",
    "prptItemKindDtoList",
    "prptLimitDtoList",
    "prptMainDto",
    "prptMainpropDtoList",
    "prptPlanDtoList",
    "prptReinscededDto",
    "prptaddressDtoList",
    "prptmaincargoDto",
    "prptnameDtoList"
})
public class NonMotorPolicyDto implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    protected PrpTItemShipDto prpTItemShipDto;
    protected PrpTMainCargoSubDto prpTMainCargoSubDto;
    protected PrpTvoyageDto prpTvoyageDto;
    @XmlElement(nillable = true)
    protected List<PrptAdjustDto> prptAdjustDtoList;
    protected PrptApplicantDto prptApplicantDto;
    @XmlElement(nillable = true)
    protected List<PrptBeneficiaryDto> prptBeneficiaryDtoList;
    @XmlElement(nillable = true)
    protected List<PrptCoinsDto> prptCoinsDtoList;
    @XmlElement(nillable = true)
    protected List<PrptCommissionDto> prptCommissionDtoList;
    @XmlElement(nillable = true)
    protected List<PrptEngageDto> prptEngageDtoList;
    protected PrptFeeDto prptFeeDto;
    @XmlElement(nillable = true)
    protected List<PrptInsuredDto> prptInsuredDtoList;
    @XmlElement(nillable = true)
    protected List<PrptItemKindDto> prptItemKindDtoList;
    @XmlElement(nillable = true)
    protected List<PrptLimitDto> prptLimitDtoList;
    protected PrptMainDto prptMainDto;
    @XmlElement(nillable = true)
    protected List<PrptMainPropDto> prptMainpropDtoList;
    @XmlElement(nillable = true)
    protected List<PrptPlanDto> prptPlanDtoList;
    protected PrptReinscededDto prptReinscededDto;
    @XmlElement(nillable = true)
    protected List<PrptaddressDto> prptaddressDtoList;
    protected PrptmaincargoDto prptmaincargoDto;
    @XmlElement(nillable = true)
    protected List<PrptnameDto> prptnameDtoList;

    /**
     * Gets the value of the prpTItemShipDto property.
     * 
     * @return
     *     possible object is
     *     {@link PrpTItemShipDto }
     *     
     */
    public PrpTItemShipDto getPrpTItemShipDto() {
        return prpTItemShipDto;
    }

    /**
     * Sets the value of the prpTItemShipDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link PrpTItemShipDto }
     *     
     */
    public void setPrpTItemShipDto(PrpTItemShipDto value) {
        this.prpTItemShipDto = value;
    }

    /**
     * Gets the value of the prpTMainCargoSubDto property.
     * 
     * @return
     *     possible object is
     *     {@link PrpTMainCargoSubDto }
     *     
     */
    public PrpTMainCargoSubDto getPrpTMainCargoSubDto() {
        return prpTMainCargoSubDto;
    }

    /**
     * Sets the value of the prpTMainCargoSubDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link PrpTMainCargoSubDto }
     *     
     */
    public void setPrpTMainCargoSubDto(PrpTMainCargoSubDto value) {
        this.prpTMainCargoSubDto = value;
    }

    /**
     * Gets the value of the prpTvoyageDto property.
     * 
     * @return
     *     possible object is
     *     {@link PrpTvoyageDto }
     *     
     */
    public PrpTvoyageDto getPrpTvoyageDto() {
        return prpTvoyageDto;
    }

    /**
     * Sets the value of the prpTvoyageDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link PrpTvoyageDto }
     *     
     */
    public void setPrpTvoyageDto(PrpTvoyageDto value) {
        this.prpTvoyageDto = value;
    }

    /**
     * Gets the value of the prptAdjustDtoList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prptAdjustDtoList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrptAdjustDtoList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PrptAdjustDto }
     * 
     * 
     */
    public List<PrptAdjustDto> getPrptAdjustDtoList() {
        if (prptAdjustDtoList == null) {
            prptAdjustDtoList = new ArrayList<PrptAdjustDto>();
        }
        return this.prptAdjustDtoList;
    }

    /**
     * Gets the value of the prptApplicantDto property.
     * 
     * @return
     *     possible object is
     *     {@link PrptApplicantDto }
     *     
     */
    public PrptApplicantDto getPrptApplicantDto() {
        return prptApplicantDto;
    }

    /**
     * Sets the value of the prptApplicantDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link PrptApplicantDto }
     *     
     */
    public void setPrptApplicantDto(PrptApplicantDto value) {
        this.prptApplicantDto = value;
    }

    /**
     * Gets the value of the prptBeneficiaryDtoList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prptBeneficiaryDtoList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrptBeneficiaryDtoList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PrptBeneficiaryDto }
     * 
     * 
     */
    public List<PrptBeneficiaryDto> getPrptBeneficiaryDtoList() {
        if (prptBeneficiaryDtoList == null) {
            prptBeneficiaryDtoList = new ArrayList<PrptBeneficiaryDto>();
        }
        return this.prptBeneficiaryDtoList;
    }

    /**
     * Gets the value of the prptCoinsDtoList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prptCoinsDtoList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrptCoinsDtoList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PrptCoinsDto }
     * 
     * 
     */
    public List<PrptCoinsDto> getPrptCoinsDtoList() {
        if (prptCoinsDtoList == null) {
            prptCoinsDtoList = new ArrayList<PrptCoinsDto>();
        }
        return this.prptCoinsDtoList;
    }

    /**
     * Gets the value of the prptCommissionDtoList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prptCommissionDtoList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrptCommissionDtoList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PrptCommissionDto }
     * 
     * 
     */
    public List<PrptCommissionDto> getPrptCommissionDtoList() {
        if (prptCommissionDtoList == null) {
            prptCommissionDtoList = new ArrayList<PrptCommissionDto>();
        }
        return this.prptCommissionDtoList;
    }

    /**
     * Gets the value of the prptEngageDtoList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prptEngageDtoList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrptEngageDtoList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PrptEngageDto }
     * 
     * 
     */
    public List<PrptEngageDto> getPrptEngageDtoList() {
        if (prptEngageDtoList == null) {
            prptEngageDtoList = new ArrayList<PrptEngageDto>();
        }
        return this.prptEngageDtoList;
    }

    /**
     * Gets the value of the prptFeeDto property.
     * 
     * @return
     *     possible object is
     *     {@link PrptFeeDto }
     *     
     */
    public PrptFeeDto getPrptFeeDto() {
        return prptFeeDto;
    }

    /**
     * Sets the value of the prptFeeDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link PrptFeeDto }
     *     
     */
    public void setPrptFeeDto(PrptFeeDto value) {
        this.prptFeeDto = value;
    }

    /**
     * Gets the value of the prptInsuredDtoList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prptInsuredDtoList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrptInsuredDtoList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PrptInsuredDto }
     * 
     * 
     */
    public List<PrptInsuredDto> getPrptInsuredDtoList() {
        if (prptInsuredDtoList == null) {
            prptInsuredDtoList = new ArrayList<PrptInsuredDto>();
        }
        return this.prptInsuredDtoList;
    }

    /**
     * Gets the value of the prptItemKindDtoList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prptItemKindDtoList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrptItemKindDtoList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PrptItemKindDto }
     * 
     * 
     */
    public List<PrptItemKindDto> getPrptItemKindDtoList() {
        if (prptItemKindDtoList == null) {
            prptItemKindDtoList = new ArrayList<PrptItemKindDto>();
        }
        return this.prptItemKindDtoList;
    }

    /**
     * Gets the value of the prptLimitDtoList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prptLimitDtoList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrptLimitDtoList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PrptLimitDto }
     * 
     * 
     */
    public List<PrptLimitDto> getPrptLimitDtoList() {
        if (prptLimitDtoList == null) {
            prptLimitDtoList = new ArrayList<PrptLimitDto>();
        }
        return this.prptLimitDtoList;
    }

    /**
     * Gets the value of the prptMainDto property.
     * 
     * @return
     *     possible object is
     *     {@link PrptMainDto }
     *     
     */
    public PrptMainDto getPrptMainDto() {
        return prptMainDto;
    }

    /**
     * Sets the value of the prptMainDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link PrptMainDto }
     *     
     */
    public void setPrptMainDto(PrptMainDto value) {
        this.prptMainDto = value;
    }

    /**
     * Gets the value of the prptMainpropDtoList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prptMainpropDtoList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrptMainpropDtoList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PrptMainPropDto }
     * 
     * 
     */
    public List<PrptMainPropDto> getPrptMainpropDtoList() {
        if (prptMainpropDtoList == null) {
            prptMainpropDtoList = new ArrayList<PrptMainPropDto>();
        }
        return this.prptMainpropDtoList;
    }

    /**
     * Gets the value of the prptPlanDtoList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prptPlanDtoList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrptPlanDtoList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PrptPlanDto }
     * 
     * 
     */
    public List<PrptPlanDto> getPrptPlanDtoList() {
        if (prptPlanDtoList == null) {
            prptPlanDtoList = new ArrayList<PrptPlanDto>();
        }
        return this.prptPlanDtoList;
    }

    /**
     * Gets the value of the prptReinscededDto property.
     * 
     * @return
     *     possible object is
     *     {@link PrptReinscededDto }
     *     
     */
    public PrptReinscededDto getPrptReinscededDto() {
        return prptReinscededDto;
    }

    /**
     * Sets the value of the prptReinscededDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link PrptReinscededDto }
     *     
     */
    public void setPrptReinscededDto(PrptReinscededDto value) {
        this.prptReinscededDto = value;
    }

    /**
     * Gets the value of the prptaddressDtoList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prptaddressDtoList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrptaddressDtoList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PrptaddressDto }
     * 
     * 
     */
    public List<PrptaddressDto> getPrptaddressDtoList() {
        if (prptaddressDtoList == null) {
            prptaddressDtoList = new ArrayList<PrptaddressDto>();
        }
        return this.prptaddressDtoList;
    }

    /**
     * Gets the value of the prptmaincargoDto property.
     * 
     * @return
     *     possible object is
     *     {@link PrptmaincargoDto }
     *     
     */
    public PrptmaincargoDto getPrptmaincargoDto() {
        return prptmaincargoDto;
    }

    /**
     * Sets the value of the prptmaincargoDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link PrptmaincargoDto }
     *     
     */
    public void setPrptmaincargoDto(PrptmaincargoDto value) {
        this.prptmaincargoDto = value;
    }

    /**
     * Gets the value of the prptnameDtoList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prptnameDtoList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrptnameDtoList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PrptnameDto }
     * 
     * 
     */
    public List<PrptnameDto> getPrptnameDtoList() {
        if (prptnameDtoList == null) {
            prptnameDtoList = new ArrayList<PrptnameDto>();
        }
        return this.prptnameDtoList;
    }

}
