package cn.com.libertymutual.production.service.api;

import java.math.BigDecimal;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdlogsetting;
import cn.com.libertymutual.production.pojo.request.LogRequest;

import com.github.pagehelper.PageInfo;

public interface PrpdLogSettingService {

	public Long insert(Prpdlogsetting record);
	
	public PageInfo<Prpdlogsetting> findLogByKeyValue(LogRequest logRequest);
	
}
