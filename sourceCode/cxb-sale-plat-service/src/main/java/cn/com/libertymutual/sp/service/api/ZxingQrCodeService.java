package cn.com.libertymutual.sp.service.api;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.com.libertymutual.core.web.ServiceResult;

public interface ZxingQrCodeService {

	/** 
	 * 邀请好友注册二维码<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年3月30日上午9:54:33<br>
	 * @param request
	 * @param response
	 * @param userCode 用户编码
	 * @param shareUrl 分享地址
	 * @param userHeadUrl 头像地址
	 * @param fileType 图片类型(jpg,png等)
	 * @return
	 * @throws Exception
	 */
	public ServiceResult registerZxingQrCode(HttpServletRequest request, HttpServletResponse response, String userCode, String shareUrl,
			String userHeadUrl, String fileType) throws Exception;

	/** 
	 * 店铺分享产品二维码<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年3月30日上午9:53:36<br>
	 * @param request
	 * @param response
	 * @param userCode 用户编码
	 * @param shareUrl 分享地址
	 * @param userHeadUrl 头像地址
	 * @param productCName 产品名称
	 * @param fontSize 字体大小
	 * @param fileType 图片类型(jpg,png等)
	 * @return
	 * @throws Exception
	 */
	public ServiceResult shareZxingQrCode(HttpServletRequest request, HttpServletResponse response, String userCode, String shareUrl,
			String userHeadUrl, String productCName, Integer fontSize, String fileType) throws Exception;

	/** 
	 * 批量创建用户<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年3月30日上午9:53:09<br>
	 * @param request
	 * @param response
	 * @param code
	 * @param productids
	 * @return
	 * @throws Exception
	 */
	public ServiceResult createUsers(HttpServletRequest request, HttpServletResponse response, String code, String productids) throws Exception;

	/** 
	 * 查询所有用户<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年3月30日上午9:53:21<br>
	 * @param request
	 * @param response
	 * @param code
	 * @param s_userid
	 * @param e_userid
	 * @return
	 * @throws Exception
	 */
	public ServiceResult findUsers(HttpServletRequest request, HttpServletResponse response, String code, Integer s_userid, Integer e_userid)
			throws Exception;

}
