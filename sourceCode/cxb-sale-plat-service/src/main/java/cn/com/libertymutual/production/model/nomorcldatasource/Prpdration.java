package cn.com.libertymutual.production.model.nomorcldatasource;

import java.math.BigDecimal;
import java.util.Date;

import cn.com.libertymutual.production.utils.LogFiled;

public class Prpdration extends PrpdrationKey {
	
    private String rationtype;

    private String plancname;

    private String planename;

    @LogFiled(chineseName="条款代码")
    private String kindcode;

    @LogFiled(chineseName="条款版本")
    private String kindversion;

    @LogFiled(chineseName="条款名称")
    private String kindcname;

    private String kindename;

    private Date startdate;

    private Date enddate;

    private String clausecode;

    private String relyonkindcode;

    private Date relyonstartdate;

    private Date relyonenddate;

    private String ownerriskcode;

    private String calculateflag;

    @LogFiled(chineseName="责任代码")
    private String itemcode;

    @LogFiled(chineseName="责任名称")
    private String itemcname;

    private String itemename;

    private String itemflag;

    private String freeitem1;

    private String freeitem2;

    private String freeitem3;

    private String freeitem4;

    private String freeitem5;

    private String freeitem6;

    @LogFiled(chineseName="数量")
    private Long quantity;

    @LogFiled(chineseName="币种")
    private String currency;

    @LogFiled(chineseName="保额")
    private BigDecimal amount;

    @LogFiled(chineseName="单位保额")
    private BigDecimal unitamount;

    @LogFiled(chineseName="最终费率")
    private BigDecimal rate;

    @LogFiled(chineseName="折扣率")
    private BigDecimal discount;

    @LogFiled(chineseName="保费")
    private BigDecimal premium;

    @LogFiled(chineseName="单位保费")
    private BigDecimal unitpremium;

    private String validstatus;

    private String flag;

    private String waysofcalc;

    private String delflag;

    public String getRationtype() {
        return rationtype;
    }

    public void setRationtype(String rationtype) {
        this.rationtype = rationtype == null ? null : rationtype.trim();
    }

    public String getPlancname() {
        return plancname;
    }

    public void setPlancname(String plancname) {
        this.plancname = plancname == null ? null : plancname.trim();
    }

    public String getPlanename() {
        return planename;
    }

    public void setPlanename(String planename) {
        this.planename = planename == null ? null : planename.trim();
    }

    public String getKindcode() {
        return kindcode;
    }

    public void setKindcode(String kindcode) {
        this.kindcode = kindcode == null ? null : kindcode.trim();
    }

    public String getKindversion() {
        return kindversion;
    }

    public void setKindversion(String kindversion) {
        this.kindversion = kindversion == null ? null : kindversion.trim();
    }

    public String getKindcname() {
        return kindcname;
    }

    public void setKindcname(String kindcname) {
        this.kindcname = kindcname == null ? null : kindcname.trim();
    }

    public String getKindename() {
        return kindename;
    }

    public void setKindename(String kindename) {
        this.kindename = kindename == null ? null : kindename.trim();
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public String getClausecode() {
        return clausecode;
    }

    public void setClausecode(String clausecode) {
        this.clausecode = clausecode == null ? null : clausecode.trim();
    }

    public String getRelyonkindcode() {
        return relyonkindcode;
    }

    public void setRelyonkindcode(String relyonkindcode) {
        this.relyonkindcode = relyonkindcode == null ? null : relyonkindcode.trim();
    }

    public Date getRelyonstartdate() {
        return relyonstartdate;
    }

    public void setRelyonstartdate(Date relyonstartdate) {
        this.relyonstartdate = relyonstartdate;
    }

    public Date getRelyonenddate() {
        return relyonenddate;
    }

    public void setRelyonenddate(Date relyonenddate) {
        this.relyonenddate = relyonenddate;
    }

    public String getOwnerriskcode() {
        return ownerriskcode;
    }

    public void setOwnerriskcode(String ownerriskcode) {
        this.ownerriskcode = ownerriskcode == null ? null : ownerriskcode.trim();
    }

    public String getCalculateflag() {
        return calculateflag;
    }

    public void setCalculateflag(String calculateflag) {
        this.calculateflag = calculateflag == null ? null : calculateflag.trim();
    }

    public String getItemcode() {
        return itemcode;
    }

    public void setItemcode(String itemcode) {
        this.itemcode = itemcode == null ? null : itemcode.trim();
    }

    public String getItemcname() {
        return itemcname;
    }

    public void setItemcname(String itemcname) {
        this.itemcname = itemcname == null ? null : itemcname.trim();
    }

    public String getItemename() {
        return itemename;
    }

    public void setItemename(String itemename) {
        this.itemename = itemename == null ? null : itemename.trim();
    }

    public String getItemflag() {
        return itemflag;
    }

    public void setItemflag(String itemflag) {
        this.itemflag = itemflag == null ? null : itemflag.trim();
    }

    public String getFreeitem1() {
        return freeitem1;
    }

    public void setFreeitem1(String freeitem1) {
        this.freeitem1 = freeitem1 == null ? null : freeitem1.trim();
    }

    public String getFreeitem2() {
        return freeitem2;
    }

    public void setFreeitem2(String freeitem2) {
        this.freeitem2 = freeitem2 == null ? null : freeitem2.trim();
    }

    public String getFreeitem3() {
        return freeitem3;
    }

    public void setFreeitem3(String freeitem3) {
        this.freeitem3 = freeitem3 == null ? null : freeitem3.trim();
    }

    public String getFreeitem4() {
        return freeitem4;
    }

    public void setFreeitem4(String freeitem4) {
        this.freeitem4 = freeitem4 == null ? null : freeitem4.trim();
    }

    public String getFreeitem5() {
        return freeitem5;
    }

    public void setFreeitem5(String freeitem5) {
        this.freeitem5 = freeitem5 == null ? null : freeitem5.trim();
    }

    public String getFreeitem6() {
        return freeitem6;
    }

    public void setFreeitem6(String freeitem6) {
        this.freeitem6 = freeitem6 == null ? null : freeitem6.trim();
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency == null ? null : currency.trim();
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getUnitamount() {
        return unitamount;
    }

    public void setUnitamount(BigDecimal unitamount) {
        this.unitamount = unitamount;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getPremium() {
        return premium;
    }

    public void setPremium(BigDecimal premium) {
        this.premium = premium;
    }

    public BigDecimal getUnitpremium() {
        return unitpremium;
    }

    public void setUnitpremium(BigDecimal unitpremium) {
        this.unitpremium = unitpremium;
    }

    public String getValidstatus() {
        return validstatus;
    }

    public void setValidstatus(String validstatus) {
        this.validstatus = validstatus == null ? null : validstatus.trim();
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag == null ? null : flag.trim();
    }

    public String getWaysofcalc() {
        return waysofcalc;
    }

    public void setWaysofcalc(String waysofcalc) {
        this.waysofcalc = waysofcalc == null ? null : waysofcalc.trim();
    }

    public String getDelflag() {
        return delflag;
    }

    public void setDelflag(String delflag) {
        this.delflag = delflag == null ? null : delflag.trim();
    }
}