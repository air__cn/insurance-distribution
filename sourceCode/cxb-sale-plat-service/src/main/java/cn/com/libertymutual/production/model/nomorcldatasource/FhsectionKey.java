package cn.com.libertymutual.production.model.nomorcldatasource;

public class FhsectionKey {
    private String treatyno;

    private String sectionno;

    public String getTreatyno() {
        return treatyno;
    }

    public void setTreatyno(String treatyno) {
        this.treatyno = treatyno == null ? null : treatyno.trim();
    }

    public String getSectionno() {
        return sectionno;
    }

    public void setSectionno(String sectionno) {
        this.sectionno = sectionno == null ? null : sectionno.trim();
    }
}