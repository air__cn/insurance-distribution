package cn.com.libertymutual.saleplat.bootstrap;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TestSms {

	
	public static void main(String[] arg) throws UnsupportedEncodingException
	  {
		System.out.println( URLEncoder.encode("%02%01%3C%3Fxml+version=%221.0%22+encoding%3D%22UTF-8%22%3F%3E%3CPACKET%3E%3CHEAD%3E%3CREQUEST_CODE%3ESMS%3C%2FREQUEST_CODE%3E%3CUSER%3Etest%3C%2FUSER%3E%3CPASSWORD%3Etest%3C%2FPASSWORD%3E%3C%2FHEAD%3E%3CBODY%3E%3CBASE_PART%3E%3CSendTarget%3E13098658403%3C%2FSendTarget%3E%3CSMContent%3E%E6%84%9F%E8%B0%A2%E6%82%A8%E9%80%89%E6%8B%A9%E5%88%A9%E5%AE%9D%E4%BA%92%E5%8A%A9%2C%E6%82%A8%E7%9A%84%E8%B5%94%E6%A1%88%E5%8F%B7%E6%98%AFCQ01015829.%E6%88%91%E4%BB%AC%E7%9A%84%E6%9C%8D%E5%8A%A1%E7%83%AD%E7%BA%BF%E6%98%AF400-888-2008%3C%2FSMContent%3E%3CRCompleteTimeBegin%3E2008-09-04%3C%2FRCompleteTimeBegin%3E%3CRCompleteTimeEnd%3E2008-09-04%3C%2FRCompleteTimeEnd%3E%3CRCompleteHourBegin%3E600%3C%2FRCompleteHourBegin%3E%3CRCompleteHourEnd%3E1080%3C%2FRCompleteHourEnd%3E%3C%2FBASE_PART%3E%3C%2FBODY%3E%3C%2FPACKET%3", "UTF-8") );
	    DataInputStream inStream = null;
	    DataOutputStream outStream = null;
	    String sUrl = "http://127.0.0.1:18008/sale/im/testSms?name=bob";

	    HttpURLConnection conn = null;
	    URL url = null;
	    try {
	      url = new URL(sUrl);
	      conn = (HttpURLConnection)url.openConnection();
	      conn.setDoOutput(true);
	      conn.setDoInput(true);
	      conn.setRequestMethod("POST");
	      conn.setConnectTimeout(0);
	      conn.setReadTimeout(0);

	      outStream = new DataOutputStream(conn.getOutputStream());

	      String str = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><PACKET type=\"REQUEST\" version=\"1.0\"><HEAD><REQUEST_CODE>02</REQUEST_CODE><USER>test_user</USER><PASSWORD>111111</PASSWORD></HEAD><BODY><BASE_PART><POLICY_NO>DBJM110083360900</POLICY_NO><LOSS_DATE>201001041029</LOSS_DATE></BASE_PART></BODY></PACKET>";

	      String str1 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><PACKET><HEAD><REQUEST_CODE>01</REQUEST_CODE></HEAD><BODY><BASE_PART><INSURED_NAME>李洋</INSURED_NAME><INSURED_RISK></INSURED_RISK><ENGINE_NO></ENGINE_NO><VIN_NO></VIN_NO><PRODUCT_LINE></PRODUCT_LINE><PAGE_INDEX>1</PAGE_INDEX></BASE_PART></BODY></PACKET>";

	      String str2 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><PACKET><HEAD><REQUEST_CODE>03</REQUEST_CODE></HEAD><BODY><BASE_PART><POLICY_NO>DHOM110181880700</POLICY_NO></BASE_PART></BODY></PACKET>";

	      String polpremium_coll = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><PACKET><HEAD><REQUEST_CODE>03</REQUEST_CODE></HEAD><BODY><BASE_PART><POLICY_NO>DHOM110115020700</POLICY_NO><POLICY_NO>DHOM110026100600</POLICY_NO><POLICY_NO>DHOM110163590700</POLICY_NO><POLICY_NO>DHOM110185390800</POLICY_NO><POLICY_NO>DHOM110185510700</POLICY_NO><POLICY_NO>DHOC110020150700</POLICY_NO><POLICY_NO>DHOM110183120700</POLICY_NO><POLICY_NO>DHOM110180710700</POLICY_NO></BASE_PART></BODY></PACKET>";

	      String SMS = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><PACKET><HEAD><REQUEST_CODE>SMS</REQUEST_CODE><USER>test</USER><PASSWORD>test</PASSWORD></HEAD><BODY><BASE_PART><SendTarget>13098658403</SendTarget><SMContent>感谢您选择利宝互助,您的赔案号是CQ01015829.我们的服务热线是400-888-2008</SMContent><RCompleteTimeBegin>2008-09-04</RCompleteTimeBegin><RCompleteTimeEnd>2008-09-04</RCompleteTimeEnd><RCompleteHourBegin>600</RCompleteHourBegin><RCompleteHourEnd>1080</RCompleteHourEnd></BASE_PART></BODY></PACKET>";
	      String Agent = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><PACKET type=\"REQUEST\" version=\"1.0\"><HEAD><REQUEST_CODE>AGENT</REQUEST_CODE><USER>test_user</USER><PASSWORD>111111</PASSWORD></HEAD></PACKET>";

	      String riskQuery = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><PACKET type=\"REQUEST\" version=\"1.0\"><HEAD><REQUEST_CODE>11</REQUEST_CODE><USER>test_user</USER><PASSWORD>111111</PASSWORD></HEAD><BODY><BASE_PART><POLICY_NO>DHOP120000020504</POLICY_NO><LOSS_DATE>200907021515</LOSS_DATE><INSURED_RISK_NAME>一般员工</INSURED_RISK_NAME><ID_CARD_NO></ID_CARD_NO><DESCRIPTION></DESCRIPTION><VIN></VIN><PAGE_INDEX>1</PAGE_INDEX></BASE_PART></BODY></PACKET>";

	      String riskRetr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><PACKET type=\"REQUEST\" version=\"1.0\"><HEAD><REQUEST_CODE>12</REQUEST_CODE><USER>test_user</USER><PASSWORD>111111</PASSWORD></HEAD><BODY><BASE_PART><POLICY_NO>DHOG110000970900</POLICY_NO><LOSS_DATE>200912071515</LOSS_DATE><RISK_NO>1</RISK_NO><RISK_GRP>0</RISK_GRP><ITEM_NO>1</ITEM_NO><INSURED_RISK_NAME></INSURED_RISK_NAME></BASE_PART></BODY></PACKET>";

	      outStream.writeUTF(SMS);
	      outStream.close();

	      long Times = System.currentTimeMillis();
	      Date ReqTime = new Date(Times);
	      SimpleDateFormat formattertoday = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSSS");
	      String requestTime = formattertoday.format(ReqTime);
	      System.out.println("conn.getReadTimeout():" + conn.getReadTimeout());
	      System.out.println("sendXML::" + requestTime);

	      InputStream in = conn.getInputStream();
	     /* System.out.println( in .available() );
	      byte[] b = new byte[16];
	      while( in.read(b, 0, 16) >= 0 ) {
	    	  System.out.println( new String(b) );
	      }*/
	      inStream = new DataInputStream(in);

	      StringBuffer xml = new StringBuffer();
	      do {
	        xml.append(inStream.readUTF());
	        System.out.println("111111111111");
	      }
	      while (xml.indexOf("</PACKET>") < 0);

	      FileWriter fw = new FileWriter("e:\\xml.txt");
	      fw.write(xml.toString(), 0, xml.toString().length());
	      fw.flush();

	      System.out.println(xml.toString());

	      inStream.close();

	      Times = System.currentTimeMillis();
	      Date ResTime = new Date(Times);
	      SimpleDateFormat formattertime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSSS");
	      String responseTime = formattertime.format(ResTime);
	      System.out.println("Request Time:" + requestTime);
	      System.out.println("Response Time:" + responseTime);

	      float diff = (float)(ResTime.getTime() - ReqTime.getTime()) / 1000.0F;
	      System.out.println("Time Diff:" + diff);
	      conn.disconnect();
	    }
	    catch (Exception e)
	    {
	      long Times = System.currentTimeMillis();
	      Date ReqTime = new Date(Times);
	      SimpleDateFormat formattertoday = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSSS");
	      String requestTime = formattertoday.format(ReqTime);

	      System.out.println("Error at SendXml::" + requestTime + "::" + e);
	      e.printStackTrace();
	    }
	  }

	
}
