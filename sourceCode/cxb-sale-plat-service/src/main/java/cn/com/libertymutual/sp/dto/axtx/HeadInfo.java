package cn.com.libertymutual.sp.dto.axtx;

import org.springframework.beans.factory.annotation.Value;



import org.springframework.stereotype.Component;

import cn.com.libertymutual.core.util.DateUtil;
import cn.com.libertymutual.core.util.uuid.UUID;


@Component
public class HeadInfo {
	@Value("${app.interface.partnerAccountCode}")
	private String partnerAccountCode;
	
	@Value("${app.interface.agreementNo}")
	private String agreementNo;
	@Value("${app.interface.recordCode}")
	private String recordCode;
	
	//获取flowId
	public String getFlowId(){
		/*String date = DateUtil.dateFromat(new Date());
		date = date.substring(0,4)+date.substring(5,7)+date.substring(8,10);*/
		String liu = UUID.getShortUuid();
		String flowId = partnerAccountCode+DateUtil.getNowDate(DateUtil.DATE_TIME_PATTERN3)+liu;
		return flowId;
	}
	
	//设置头部信息
	public void setHeadInfo(RequestBaseDto dto) {
		
		dto.setPartnerAccountCode(partnerAccountCode);
		dto.setFlowId(getFlowId());
		///dto.setOperatorDate(DateUtil.dateFromat(new Date(), DateUtil.DATE_TIME_PATTERN2));
		dto.setOperatorDate(DateUtil.getNowDate(DateUtil.DATE_TIME_PATTERN2));
		dto.setRecordCode(recordCode);
		dto.setAgreementNo(agreementNo);

	}
}
