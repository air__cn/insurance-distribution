package cn.com.libertymutual.sp.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.annotation.SystemValidate;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.dto.CommonQueryRequestDTO;
import cn.com.libertymutual.sp.dto.savePlan.PropsalSaveRequestDto;
import cn.com.libertymutual.sp.service.api.AxtxService;

@RestController
@RequestMapping("/nol/axtx")
public class AxtxInsureController {
	@Autowired
	private AxtxService axtxService;

	@PostMapping(value = "/insure")
	@SystemValidate(validate = false, description = "无需校验接口")
	public ServiceResult insure(HttpServletRequest request, HttpServletResponse response, String identCode,
			@RequestBody PropsalSaveRequestDto requestDto) {

		// return indexService.insureCode(data);urn adminService.adminList(userid,
		// username, pageNumber, pageSize);
		return axtxService.insure(requestDto);

	}

	@PostMapping(value = "/queryVin")
	// @SystemValidate(validate = false, description = "无需校验接口")
	public ServiceResult queryVin(HttpServletRequest request, HttpServletResponse response, String identCode,
			@RequestBody CommonQueryRequestDTO querydto) {

		// return indexService.insureCode(data);urn adminService.adminList(userid,
		// username, pageNumber, pageSize);
		return axtxService.findVin(querydto);

	}

}
