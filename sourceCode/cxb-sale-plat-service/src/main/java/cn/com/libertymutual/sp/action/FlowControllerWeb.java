package cn.com.libertymutual.sp.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpFlow;
import cn.com.libertymutual.sp.bean.TbSpFlowDetail;
import cn.com.libertymutual.sp.service.api.FlowService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/admin/flow")
public class FlowControllerWeb {

	private Logger log = LoggerFactory.getLogger(getClass());
	@Autowired
	private FlowService flowService;
	
	@ApiOperation(value = "审批流列表", notes = "审批流列表")
	@ApiImplicitParams(value = {
		@ApiImplicitParam(name = "pageNumber", value = "条数", required = true, paramType = "query", dataType = "Long"),
		@ApiImplicitParam(name = "pageSize", value = "页码", required = true, paramType = "query", dataType = "Long"),
	})
	@PostMapping(value = "/flowList")
	public ServiceResult flowList(int pageNumber,int pageSize) {
		ServiceResult sr = new ServiceResult();
		sr = flowService.flowList(pageNumber,pageSize);
		return sr;

	}
	
	
	@ApiOperation(value = "审批流节点列表", notes = "审批流节点列表")
	@ApiImplicitParams(value = {
		@ApiImplicitParam(name = "flowNo", value = "流程编号", required = true, paramType = "query", dataType = "String"),
	})
	@PostMapping(value = "/flowChildNodeList")
	public ServiceResult flowChildNodeList(String flowNo) {
		ServiceResult sr = new ServiceResult();
		sr = flowService.flowChildNodeList(flowNo);
		return sr;

	}
	
	
	
	@ApiOperation(value = "新增审批流", notes = "新增审批流")
	@PostMapping(value = "/addFlow")
	public ServiceResult addFlow(@RequestBody TbSpFlow flow) {
		ServiceResult sr = new ServiceResult();
		sr = flowService.addFlow(flow);
		return sr;

	}
	
	
	@ApiOperation(value = "追加审批流节点", notes = "追加审批流节点")
	@PostMapping(value = "/addChildNode")
	public ServiceResult addChildNode(@RequestBody TbSpFlowDetail flowDetail) {
		ServiceResult sr = new ServiceResult();
		sr = flowService.addChildNode(flowDetail);
		return sr;

	}
	
	
	@ApiOperation(value = "节点详情", notes = "节点详情")
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "flowNo", value = "流程编号", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "flowNode", value = "节点编号", required = true, paramType = "query", dataType = "Long"),
		})
	@PostMapping(value = "/nodeDetail")
	public ServiceResult nodeDetail(String flowNo,String flowNode) {
		ServiceResult sr = new ServiceResult();
		sr = flowService.nodeDetail(flowNo,flowNode);
		return sr;
	}
	
	
	@ApiOperation(value = "删除节点", notes = "删除节点")
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "flowNo", value = "流程编号", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "flowNode", value = "节点编号", required = true, paramType = "query", dataType = "Long"),
		})
	@PostMapping(value = "/removeNode")
	public ServiceResult removeNode(String flowNo,String flowNode) {
		ServiceResult sr = new ServiceResult();
		sr = flowService.removeNode(flowNo,flowNode);
		return sr;
	}
}
