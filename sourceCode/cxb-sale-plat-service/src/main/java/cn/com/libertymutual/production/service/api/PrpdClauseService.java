package cn.com.libertymutual.production.service.api;


import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdclause;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdclauseWithBLOBs;
import cn.com.libertymutual.production.pojo.request.PrpdClauseRequest;

import com.github.pagehelper.PageInfo;

public interface PrpdClauseService {

	/**
	 * 查询特约
	 * @return
	 */
	public PageInfo<PrpdclauseWithBLOBs> findPrpdClause(PrpdClauseRequest prpdClauseRequest);
	
	public void insert(PrpdclauseWithBLOBs record);
	
	public void update(PrpdclauseWithBLOBs record);
	
	public void delete(PrpdclauseWithBLOBs record);
	
	public List<PrpdclauseWithBLOBs> findClauseByCondition(String clauseCode, String titleFlag);
	
}
