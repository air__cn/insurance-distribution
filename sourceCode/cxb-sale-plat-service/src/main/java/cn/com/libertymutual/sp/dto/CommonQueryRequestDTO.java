package cn.com.libertymutual.sp.dto;

import java.io.Serializable;

import cn.com.libertymutual.core.base.dto.RequestBaseDto;


public class CommonQueryRequestDTO  extends RequestBaseDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4327671355137839832L;

	private String documentNo;//业务号：投保单号、批单号、保单号等

	public String getDocumentNo() {
		return documentNo;
	}

	public void setDocumentNo(String documentNo) {
		this.documentNo = documentNo;
	}
   
	
	

	
}
