package cn.com.libertymutual.sp.dto;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "T_CLM_CASE", schema = "", catalog = "")
public class TClaimCase {

	private int caseId;
	private String claimNo;// 报案号
	private String policyNo;// 投保单号
	private String insuredName;// 投保人姓名
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date accidentTime;// 报案时间
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date noticeTime;// 通知时间
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date registerTime;// 提交时间

	private String accidentPlace;// 地点
	private String accidentDesc;// 原因
	private String status;// 状态
	private String modifyRemark;// 备注
	private String employeeInvolved;
	private String isSpecial;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date openDate;
	private String reopenCause;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date closeDate;// 关闭时间
	private String closeType;// 关闭类型

	private double paidAmount;// 赔付
	private double totalCost;// 总费用

	@Id
	@Column(name = "CASE_ID")
	public int getCaseId() {
		return caseId;
	}

	public void setCaseId(int caseId) {
		this.caseId = caseId;
	}

	@Basic
	@Column(name = "CLAIM_NO")
	public String getClaimNo() {
		return claimNo;
	}

	public void setClaimNo(String claimNo) {
		this.claimNo = claimNo;
	}

	@Basic
	@Column(name = "POLICY_NO")
	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	@Basic
	@Column(name = "INSURED_NAME")
	public String getInsuredName() {
		return insuredName;
	}

	public void setInsuredName(String insuredName) {
		this.insuredName = insuredName;
	}

	@Basic
	@Column(name = "ACCIDENT_TIME")
	public Date getAccidentTime() {
		return accidentTime;
	}

	public void setAccidentTime(Date accidentTime) {
		this.accidentTime = accidentTime;
	}

	@Basic
	@Column(name = "NOTICE_TIME")
	public Date getNoticeTime() {
		return noticeTime;
	}

	public void setNoticeTime(Date noticeTime) {
		this.noticeTime = noticeTime;
	}

	@Basic
	@Column(name = "REGISTER_TIME")
	public Date getRegisterTime() {
		return registerTime;
	}

	public void setRegisterTime(Date registerTime) {
		this.registerTime = registerTime;
	}

	@Basic
	@Column(name = "ACCIDENT_PLACE")
	public String getAccidentPlace() {
		return accidentPlace;
	}

	public void setAccidentPlace(String accidentPlace) {
		this.accidentPlace = accidentPlace;
	}

	@Basic
	@Column(name = "ACCIDENT_DESC")
	public String getAccidentDesc() {
		return accidentDesc;
	}

	public void setAccidentDesc(String accidentDesc) {
		this.accidentDesc = accidentDesc;
	}

	@Basic
	@Column(name = "STATUS")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Basic
	@Column(name = "MODIFY_REMARK")
	public String getModifyRemark() {
		return modifyRemark;
	}

	public void setModifyRemark(String modifyRemark) {
		this.modifyRemark = modifyRemark;
	}

	@Basic
	@Column(name = "EMPLOYEE_INVOLVED")
	public String getEmployeeInvolved() {
		return employeeInvolved;
	}

	public void setEmployeeInvolved(String employeeInvolved) {
		this.employeeInvolved = employeeInvolved;
	}

	@Basic
	@Column(name = "IS_SPECIAL")
	public String getIsSpecial() {
		return isSpecial;
	}

	public void setIsSpecial(String isSpecial) {
		this.isSpecial = isSpecial;
	}

	@Basic
	@Column(name = "OPERATE_DATE")
	public Date getOpenDate() {
		return openDate;
	}

	public void setOpenDate(Date openDate) {
		this.openDate = openDate;
	}

	@Basic
	@Column(name = "REOPEN_CAUSE")
	public String getReopenCause() {
		return reopenCause;
	}

	public void setReopenCause(String reopenCause) {
		this.reopenCause = reopenCause;
	}

	@Basic
	@Column(name = "CLOSE_DATE")
	public Date getCloseDate() {
		return closeDate;
	}

	public void setCloseDate(Date closeDate) {
		this.closeDate = closeDate;
	}

	@Basic
	@Column(name = "CLOSE_TYPE")
	public String getCloseType() {
		return closeType;
	}

	public void setCloseType(String closeType) {
		this.closeType = closeType;
	}

	@Basic
	@Column(name = "PAID_AMOUNT")
	public double getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(double paidAmount) {
		this.paidAmount = paidAmount;
	}

	@Basic
	@Column(name = "TOTAL_COST")
	public double getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}

}
