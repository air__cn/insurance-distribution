package cn.com.libertymutual.sp.service.api;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpServiceconfig;

public interface ServiceMenuService {

	ServiceResult menuList(String sorttype);

	ServiceResult addService(TbSpServiceconfig tbSpServiceconfig);

	ServiceResult updateService(TbSpServiceconfig tbSpServiceconfig);

	ServiceResult removeService(String[] list);

	ServiceResult menuListWeb(String isValidate, String serviceCname, String serviceType, String toType, int pageNumber,
			int pageSize);

	ServiceResult thirdServiceInfo(String url,String userName,Integer pageNumber,Integer pageSize);

	ServiceResult updateServiceInfo(Integer id, String modifyDate,String url);
	
}
