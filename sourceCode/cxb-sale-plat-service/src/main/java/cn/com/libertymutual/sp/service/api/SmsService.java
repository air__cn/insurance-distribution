package cn.com.libertymutual.sp.service.api;

import java.io.IOException;
import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;

import cn.com.libertymutual.core.redis.util.RedisUtils;
import cn.com.libertymutual.core.web.ServiceResult;

/**
 * 短信息服务
 */
public interface SmsService {
	/**Remarks: 校验短信验证码<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年10月8日上午11:18:50<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param sr 结果集
	 * @param redis 数据获取源
	 * @param inCode 输入的验证码
	 * @param mobile 手机号
	 * @return
	 * @throws Exception
	 */
	public ServiceResult verifyCode(HttpServletRequest request, ServiceResult sr, RedisUtils redis, String inCode, String mobile) throws Exception;

	/**Remarks: 校验短信验证码<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年10月8日上午11:18:50<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param sr 结果集
	 * @param redis 数据获取源
	 * @param inCode 输入的验证码
	 * @param mobile 手机号
	 * @return true=验证码正确
	 * @throws Exception
	 */
	public boolean verifyCodeBool(HttpServletRequest request, ServiceResult sr, RedisUtils redis, String inCode, String mobile) throws Exception;

	/**Remarks: 发送短信,验证码请将contents传递null<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年10月10日上午10:11:40<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param sr
	 * @param mobile 目标手机
	 * @param contents 信息内容
	 * @param typeName 操作类型名
	 * @return
	 * @throws IOException
	 * @throws ParseException 
	 */
	public ServiceResult createSmsToMobile(HttpServletRequest request, ServiceResult sr, String mobile, String contents, String typeName)
			throws IOException, ParseException;

	/**Remarks: 发送短信其他内容<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年1月3日下午3:08:26<br>
	 * Project：liberty_sale_plat<br>
	 * @param mobile 接受信息的手机号
	 * @param contents 内容
	 * @return
	 */
	public ServiceResult smsToMobile(String mobile, String contents);

	/**Remarks: 校验手机号是否有效，用于登录<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年9月30日下午1:44:30<br>
	 * Project：liberty_sale_plat<br>
	 * @param sr
	 * @param mobile 手机号
	 * @return true 有效
	 */
	public ServiceResult phoneNumCheckForLogin(ServiceResult sr, String mobile);

	/**Remarks: 校验手机号是否有效<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年9月30日下午1:44:30<br>
	 * Project：liberty_sale_plat<br>
	 * @param sr
	 * @param mobile 手机号
	 * @return true 有效
	 */
	public ServiceResult phoneNumCheck(ServiceResult sr, String mobile);

	/**Remarks: 生成随机验证码<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年9月30日下午2:27:07<br>
	 * Project：liberty_sale_plat<br>
	 * @return 4位不含4的随机数字字符串
	 */
	public String createCode();

}
