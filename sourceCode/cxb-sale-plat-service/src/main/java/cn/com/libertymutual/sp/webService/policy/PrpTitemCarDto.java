
package cn.com.libertymutual.sp.webService.policy;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for prpTitemCarDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="prpTitemCarDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="brandName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="carInsuredRelation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="carKindCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="clauseType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dangerousCar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="engineNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="enrollDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="exhaustScale" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="fuelType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="licenseKindCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="licenseNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="modelCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="modelCodeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="newoldlogo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="otherNature" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="purchasePrice" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="runMiles" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="seatCount" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="tonCount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="useNatureCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="useyears" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="whethelicenses" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="whethercar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vINNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prpTitemCarDto", propOrder = {
    "brandName",
    "carInsuredRelation",
    "carKindCode",
    "clauseType",
    "dangerousCar",
    "engineNo",
    "enrollDate",
    "exhaustScale",
    "fuelType",
    "licenseKindCode",
    "licenseNo",
    "modelCode",
    "modelCodeName",
    "newoldlogo",
    "otherNature",
    "purchasePrice",
    "runMiles",
    "seatCount",
    "tonCount",
    "useNatureCode",
    "useyears",
    "whethelicenses",
    "whethercar",
    "vinNo"
})
public class PrpTitemCarDto implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -5395652919528013160L;
	protected String brandName;
    protected String carInsuredRelation;
    protected String carKindCode;
    protected String clauseType;
    protected String dangerousCar;
    protected String engineNo;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar enrollDate;
    protected double exhaustScale;
    protected String fuelType;
    protected String licenseKindCode;
    protected String licenseNo;
    protected String modelCode;
    protected String modelCodeName;
    protected String newoldlogo;
    protected String otherNature;
    protected double purchasePrice;
    protected String runMiles;
    protected int seatCount;
    protected double tonCount;
    protected String useNatureCode;
    protected String useyears;
    protected String whethelicenses;
    protected String whethercar;
    @XmlElement(name = "vINNo")
    protected String vinNo;

    /**
     * Gets the value of the brandName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrandName() {
        return brandName;
    }

    /**
     * Sets the value of the brandName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrandName(String value) {
        this.brandName = value;
    }

    /**
     * Gets the value of the carInsuredRelation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCarInsuredRelation() {
        return carInsuredRelation;
    }

    /**
     * Sets the value of the carInsuredRelation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCarInsuredRelation(String value) {
        this.carInsuredRelation = value;
    }

    /**
     * Gets the value of the carKindCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCarKindCode() {
        return carKindCode;
    }

    /**
     * Sets the value of the carKindCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCarKindCode(String value) {
        this.carKindCode = value;
    }

    /**
     * Gets the value of the clauseType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClauseType() {
        return clauseType;
    }

    /**
     * Sets the value of the clauseType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClauseType(String value) {
        this.clauseType = value;
    }

    /**
     * Gets the value of the dangerousCar property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDangerousCar() {
        return dangerousCar;
    }

    /**
     * Sets the value of the dangerousCar property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDangerousCar(String value) {
        this.dangerousCar = value;
    }

    /**
     * Gets the value of the engineNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEngineNo() {
        return engineNo;
    }

    /**
     * Sets the value of the engineNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEngineNo(String value) {
        this.engineNo = value;
    }

    /**
     * Gets the value of the enrollDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEnrollDate() {
        return enrollDate;
    }

    /**
     * Sets the value of the enrollDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEnrollDate(XMLGregorianCalendar value) {
        this.enrollDate = value;
    }

    /**
     * Gets the value of the exhaustScale property.
     * 
     */
    public double getExhaustScale() {
        return exhaustScale;
    }

    /**
     * Sets the value of the exhaustScale property.
     * 
     */
    public void setExhaustScale(double value) {
        this.exhaustScale = value;
    }

    /**
     * Gets the value of the fuelType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFuelType() {
        return fuelType;
    }

    /**
     * Sets the value of the fuelType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFuelType(String value) {
        this.fuelType = value;
    }

    /**
     * Gets the value of the licenseKindCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicenseKindCode() {
        return licenseKindCode;
    }

    /**
     * Sets the value of the licenseKindCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicenseKindCode(String value) {
        this.licenseKindCode = value;
    }

    /**
     * Gets the value of the licenseNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicenseNo() {
        return licenseNo;
    }

    /**
     * Sets the value of the licenseNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicenseNo(String value) {
        this.licenseNo = value;
    }

    /**
     * Gets the value of the modelCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelCode() {
        return modelCode;
    }

    /**
     * Sets the value of the modelCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelCode(String value) {
        this.modelCode = value;
    }

    /**
     * Gets the value of the modelCodeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelCodeName() {
        return modelCodeName;
    }

    /**
     * Sets the value of the modelCodeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelCodeName(String value) {
        this.modelCodeName = value;
    }

    /**
     * Gets the value of the newoldlogo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewoldlogo() {
        return newoldlogo;
    }

    /**
     * Sets the value of the newoldlogo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewoldlogo(String value) {
        this.newoldlogo = value;
    }

    /**
     * Gets the value of the otherNature property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOtherNature() {
        return otherNature;
    }

    /**
     * Sets the value of the otherNature property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOtherNature(String value) {
        this.otherNature = value;
    }

    /**
     * Gets the value of the purchasePrice property.
     * 
     */
    public double getPurchasePrice() {
        return purchasePrice;
    }

    /**
     * Sets the value of the purchasePrice property.
     * 
     */
    public void setPurchasePrice(double value) {
        this.purchasePrice = value;
    }

    /**
     * Gets the value of the runMiles property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRunMiles() {
        return runMiles;
    }

    /**
     * Sets the value of the runMiles property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRunMiles(String value) {
        this.runMiles = value;
    }

    /**
     * Gets the value of the seatCount property.
     * 
     */
    public int getSeatCount() {
        return seatCount;
    }

    /**
     * Sets the value of the seatCount property.
     * 
     */
    public void setSeatCount(int value) {
        this.seatCount = value;
    }

    /**
     * Gets the value of the tonCount property.
     * 
     */
    public double getTonCount() {
        return tonCount;
    }

    /**
     * Sets the value of the tonCount property.
     * 
     */
    public void setTonCount(double value) {
        this.tonCount = value;
    }

    /**
     * Gets the value of the useNatureCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUseNatureCode() {
        return useNatureCode;
    }

    /**
     * Sets the value of the useNatureCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUseNatureCode(String value) {
        this.useNatureCode = value;
    }

    /**
     * Gets the value of the useyears property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUseyears() {
        return useyears;
    }

    /**
     * Sets the value of the useyears property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUseyears(String value) {
        this.useyears = value;
    }

    /**
     * Gets the value of the whethelicenses property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWhethelicenses() {
        return whethelicenses;
    }

    /**
     * Sets the value of the whethelicenses property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWhethelicenses(String value) {
        this.whethelicenses = value;
    }

    /**
     * Gets the value of the whethercar property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWhethercar() {
        return whethercar;
    }

    /**
     * Sets the value of the whethercar property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWhethercar(String value) {
        this.whethercar = value;
    }

    /**
     * Gets the value of the vinNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVINNo() {
        return vinNo;
    }

    /**
     * Sets the value of the vinNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVINNo(String value) {
        this.vinNo = value;
    }

}
