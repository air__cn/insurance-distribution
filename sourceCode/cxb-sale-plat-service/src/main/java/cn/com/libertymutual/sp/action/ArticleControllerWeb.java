package cn.com.libertymutual.sp.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpArticle;
import cn.com.libertymutual.sp.service.api.ArticleService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value = "/admin/article")
public class ArticleControllerWeb {

	private Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private ArticleService articleService;
	/*
	 * 文章查询
	 */

	@ApiOperation(value = "文章查询", notes = "文章查询")
	@PostMapping(value = "/articleList")
	@ApiImplicitParams(value = {
		@ApiImplicitParam(name = "productName", value = "产品名称", required = false, paramType = "query", dataType = "String"),
		@ApiImplicitParam(name = "riskCode", value = "产品类型", required = false, paramType = "query", dataType = "String"),
		@ApiImplicitParam(name = "name", value = "文章名称", required = false, paramType = "query", dataType = "String"),
		@ApiImplicitParam(name = "type", value = "文章类型", required = false, paramType = "query", dataType = "String"),
		@ApiImplicitParam(name = "status", value = "文章状态", required = false, paramType = "query", dataType = "String"),
		@ApiImplicitParam(name = "pageNumber", value = "页码", required = true, paramType = "query", dataType = "Long"),
		@ApiImplicitParam(name = "pageSize", value = "每页条数", required = true, paramType = "query", dataType = "Long"), 
	})
	public ServiceResult allArticle(String productName, String riskCode, String name,String type,String status,Integer pageNumber, Integer pageSize) {
		ServiceResult sr = new ServiceResult();
		
		sr = articleService.allArticle(productName, riskCode, name,type,status,pageNumber,pageSize);
		return sr;

	}
	
	/*
	 * 新增或者修改文章
	 */

	@ApiOperation(value = "新增文章", notes = "新增文章")
	@PostMapping(value = "/addOrUpdateArticle")
	public ServiceResult addOrUpdateArticle(@RequestBody @ApiParam(name = "tbSpArticle", value = "tbSpArticle", required = true)TbSpArticle tbSpArticle) {
		ServiceResult sr = new ServiceResult();
		log.info("新增或者修改文章----");
		sr = articleService.addOrUpdateArticle(tbSpArticle);
		return sr;

	}
	
	
}
