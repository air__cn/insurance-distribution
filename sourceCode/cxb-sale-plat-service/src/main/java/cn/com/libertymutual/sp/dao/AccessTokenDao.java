package cn.com.libertymutual.sp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.com.libertymutual.sp.bean.TbSpAccessToken;

/**
 * @author AoYi
 * 
 * 注意@Modifying注解需要使用clearAutomatically=true;
 * 同一接口更新后立即查询获得更新后的数据,默认false查询还是更新前的数据
 *
 */
@Repository
public interface AccessTokenDao extends PagingAndSortingRepository<TbSpAccessToken, Integer>, JpaSpecificationExecutor<TbSpAccessToken> {

	@Query("from TbSpAccessToken where type = :type or appId = :appId")
	public List<TbSpAccessToken> findAccessToken(@Param("type") Integer type, @Param("appId") String appId);

	@Query("from TbSpAccessToken where type = :type and appId = :appId and secret = :secret")
	public List<TbSpAccessToken> findAccessTokenByenterprise(@Param("type") Integer type, @Param("appId") String appId,
			@Param("secret") String secret);
}
