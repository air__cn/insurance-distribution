package cn.com.libertymutual.production.dao.nomorcldatasource;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdprops;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdpropsExample;
@Mapper
public interface PrpdpropsMapper {
    int countByExample(PrpdpropsExample example);

    int deleteByExample(PrpdpropsExample example);

    int deleteByPrimaryKey(String propscode);

    int insert(Prpdprops record);

    int insertSelective(Prpdprops record);

    List<Prpdprops> selectByExample(PrpdpropsExample example);

    Prpdprops selectByPrimaryKey(String propscode);

    int updateByExampleSelective(@Param("record") Prpdprops record, @Param("example") PrpdpropsExample example);

    int updateByExample(@Param("record") Prpdprops record, @Param("example") PrpdpropsExample example);

    int updateByPrimaryKeySelective(Prpdprops record);

    int updateByPrimaryKey(Prpdprops record);
}