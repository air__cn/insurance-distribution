package cn.com.libertymutual.sp.bean.car;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "tb_mng_package_kind", catalog = "")
@IdClass(MngPackageKind.class)
public class MngPackageKind implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String pkId;
	private String packKind;
	private String packForehead;
	private String defauctibleValue;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "PACK_KIND")
	public String getPackKind() {
		return packKind;
	}

	public void setPackKind(String packKind) {
		this.packKind = packKind;
	}

	@Column(name = "PACK_FOREHEAD")
	public String getPackForehead() {
		return packForehead;
	}

	public void setPackForehead(String packForehead) {
		this.packForehead = packForehead;
	}

	@Column(name = "DEDUCTIBLE_VALUE")
	public String getDefauctibleValue() {
		return defauctibleValue;
	}

	public void setDefauctibleValue(String defauctibleValue) {
		this.defauctibleValue = defauctibleValue;
	}

	@Column(name = "PK_ID")
	public String getPkId() {
		return pkId;
	}

	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

}
