package cn.com.libertymutual.production.model.nomorcldatasource;

import java.math.BigDecimal;
import java.util.Date;

import cn.com.libertymutual.production.utils.LogFiled;

public class Prpdefile extends PrpdefileKey {
	
	@LogFiled(chineseName="匹配层级")
    private String matchlevel;
	@LogFiled(chineseName="备案号中文名称")
    private String efilecname;
	@LogFiled(chineseName="备案号英文名称")
    private String efileename;
	
    private String plancode;
	
    private String planname;
	
    private String riskname;
	@LogFiled(chineseName="条款名称")
    private String kindcname;
	@LogFiled(chineseName="备案号有效开始时间")
    private Date startdate;
	@LogFiled(chineseName="备案号有效结束时间")
	private Date enddate;

    private String path;
    @LogFiled(chineseName="备案号图片名称")
    private String imgname;

    private BigDecimal imglength;

    private BigDecimal imgwidth;

    private String flag;
    @LogFiled(chineseName="有效标识")
    private String validatestatus;

    private String attribute1;
    @LogFiled(chineseName="审核人备注")
    private String attribute2;

    private String attribute3;

    private String attribute4;

    private String attribute5;

    private String attribute6;
    
    private Date inserttime;

    private Date updatedate;

    private String imgname2;

    private BigDecimal imglength2;

    private BigDecimal imgwidth2;
    @LogFiled(chineseName="渠道")
    private String channel;
    @LogFiled(chineseName="报备单位")
    private String submitcom;
    @LogFiled(chineseName="产品名称")
    private String productname;
    @LogFiled(chineseName="报送类型")
    private String submittype;
    @LogFiled(chineseName="开发方式")
    private String devtype;
    @LogFiled(chineseName="产品类别")
    private String producttype;
    @LogFiled(chineseName="管理类别")
    private String managetype;
    @LogFiled(chineseName="险种类别")
    private String risktype;
    @LogFiled(chineseName="主/附险类型")
    private String kindtype;
    @LogFiled(chineseName="开办日期")
    private Date opendate;
    @LogFiled(chineseName="批复日期")
    private Date approvaldate;
    @LogFiled(chineseName="经营范围")
    private String busscope;
    @LogFiled(chineseName="经营区域")
    private String busarea;
    @LogFiled(chineseName="报送文件编号")
    private String docnum;
    @LogFiled(chineseName="年度")
    private String year;
    @LogFiled(chineseName="产品性质")
    private String productnature;
    @LogFiled(chineseName="费率或基础费率")
    private String rate;
    @LogFiled(chineseName="费率浮动区间")
    private String raterange;
    @LogFiled(chineseName="费率浮动系数")
    private String ratecoefficient;
    @LogFiled(chineseName="是否有电子保单")
    private String hasepolicy;
    @LogFiled(chineseName="保险期间")
    private String period;
    @LogFiled(chineseName="绝对免赔率（额）")
    private String deductible;
    @LogFiled(chineseName="相对免赔率（额）")
    private String reldeductibe;
    @LogFiled(chineseName="保险责任")
    private String insliab;

    private String remark1;

    private String remark2;
    @LogFiled(chineseName="产品联系人")
    private String linkname;
    @LogFiled(chineseName="产品联系方式")
    private String linknum;
    @LogFiled(chineseName="产品个团属性")
    private String productattr;
    @LogFiled(chineseName="签发人")
    private String signname;
    @LogFiled(chineseName="法律审核人")
    private String legalname;
    @LogFiled(chineseName="精算审核人")
    private String actuarialname;
    @LogFiled(chineseName="销售推广名称")
    private String salepromname;
    @LogFiled(chineseName="销售区域")
    private String salearea;
    @LogFiled(chineseName="销售渠道")
    private String salechannel;
    @LogFiled(chineseName="其他产品信息")
    private String otherinfo;

    private String remark3;

    public String getMatchlevel() {
        return matchlevel;
    }

    public void setMatchlevel(String matchlevel) {
        this.matchlevel = matchlevel == null ? null : matchlevel.trim();
    }

    public String getEfilecname() {
        return efilecname;
    }

    public void setEfilecname(String efilecname) {
        this.efilecname = efilecname == null ? null : efilecname.trim();
    }

    public String getEfileename() {
        return efileename;
    }

    public void setEfileename(String efileename) {
        this.efileename = efileename == null ? null : efileename.trim();
    }

    public String getPlancode() {
        return plancode;
    }

    public void setPlancode(String plancode) {
        this.plancode = plancode == null ? null : plancode.trim();
    }

    public String getPlanname() {
        return planname;
    }

    public void setPlanname(String planname) {
        this.planname = planname == null ? null : planname.trim();
    }

    public String getRiskname() {
        return riskname;
    }

    public void setRiskname(String riskname) {
        this.riskname = riskname == null ? null : riskname.trim();
    }

    public String getKindcname() {
        return kindcname;
    }

    public void setKindcname(String kindcname) {
        this.kindcname = kindcname == null ? null : kindcname.trim();
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path == null ? null : path.trim();
    }

    public String getImgname() {
        return imgname;
    }

    public void setImgname(String imgname) {
        this.imgname = imgname == null ? null : imgname.trim();
    }

    public BigDecimal getImglength() {
        return imglength;
    }

    public void setImglength(BigDecimal imglength) {
        this.imglength = imglength;
    }

    public BigDecimal getImgwidth() {
        return imgwidth;
    }

    public void setImgwidth(BigDecimal imgwidth) {
        this.imgwidth = imgwidth;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag == null ? null : flag.trim();
    }

    public String getValidatestatus() {
        return validatestatus;
    }

    public void setValidatestatus(String validatestatus) {
        this.validatestatus = validatestatus == null ? null : validatestatus.trim();
    }

    public String getAttribute1() {
        return attribute1;
    }

    public void setAttribute1(String attribute1) {
        this.attribute1 = attribute1 == null ? null : attribute1.trim();
    }

    public String getAttribute2() {
        return attribute2;
    }

    public void setAttribute2(String attribute2) {
        this.attribute2 = attribute2 == null ? null : attribute2.trim();
    }

    public String getAttribute3() {
        return attribute3;
    }

    public void setAttribute3(String attribute3) {
        this.attribute3 = attribute3 == null ? null : attribute3.trim();
    }

    public String getAttribute4() {
        return attribute4;
    }

    public void setAttribute4(String attribute4) {
        this.attribute4 = attribute4 == null ? null : attribute4.trim();
    }

    public String getAttribute5() {
        return attribute5;
    }

    public void setAttribute5(String attribute5) {
        this.attribute5 = attribute5 == null ? null : attribute5.trim();
    }

    public String getAttribute6() {
        return attribute6;
    }

    public void setAttribute6(String attribute6) {
        this.attribute6 = attribute6 == null ? null : attribute6.trim();
    }

    public Date getInserttime() {
        return inserttime;
    }

    public void setInserttime(Date inserttime) {
        this.inserttime = inserttime;
    }

    public Date getUpdatedate() {
        return updatedate;
    }

    public void setUpdatedate(Date updatedate) {
        this.updatedate = updatedate;
    }

    public String getImgname2() {
        return imgname2;
    }

    public void setImgname2(String imgname2) {
        this.imgname2 = imgname2 == null ? null : imgname2.trim();
    }

    public BigDecimal getImglength2() {
        return imglength2;
    }

    public void setImglength2(BigDecimal imglength2) {
        this.imglength2 = imglength2;
    }

    public BigDecimal getImgwidth2() {
        return imgwidth2;
    }

    public void setImgwidth2(BigDecimal imgwidth2) {
        this.imgwidth2 = imgwidth2;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel == null ? null : channel.trim();
    }

    public String getSubmitcom() {
        return submitcom;
    }

    public void setSubmitcom(String submitcom) {
        this.submitcom = submitcom == null ? null : submitcom.trim();
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname == null ? null : productname.trim();
    }

    public String getSubmittype() {
        return submittype;
    }

    public void setSubmittype(String submittype) {
        this.submittype = submittype == null ? null : submittype.trim();
    }

    public String getDevtype() {
        return devtype;
    }

    public void setDevtype(String devtype) {
        this.devtype = devtype == null ? null : devtype.trim();
    }

    public String getProducttype() {
        return producttype;
    }

    public void setProducttype(String producttype) {
        this.producttype = producttype == null ? null : producttype.trim();
    }

    public String getManagetype() {
        return managetype;
    }

    public void setManagetype(String managetype) {
        this.managetype = managetype == null ? null : managetype.trim();
    }

    public String getRisktype() {
        return risktype;
    }

    public void setRisktype(String risktype) {
        this.risktype = risktype == null ? null : risktype.trim();
    }

    public String getKindtype() {
        return kindtype;
    }

    public void setKindtype(String kindtype) {
        this.kindtype = kindtype == null ? null : kindtype.trim();
    }

    public Date getOpendate() {
        return opendate;
    }

    public void setOpendate(Date opendate) {
        this.opendate = opendate;
    }

    public Date getApprovaldate() {
        return approvaldate;
    }

    public void setApprovaldate(Date approvaldate) {
        this.approvaldate = approvaldate;
    }

    public String getBusscope() {
        return busscope;
    }

    public void setBusscope(String busscope) {
        this.busscope = busscope == null ? null : busscope.trim();
    }

    public String getBusarea() {
        return busarea;
    }

    public void setBusarea(String busarea) {
        this.busarea = busarea == null ? null : busarea.trim();
    }

    public String getDocnum() {
        return docnum;
    }

    public void setDocnum(String docnum) {
        this.docnum = docnum == null ? null : docnum.trim();
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year == null ? null : year.trim();
    }

    public String getProductnature() {
        return productnature;
    }

    public void setProductnature(String productnature) {
        this.productnature = productnature == null ? null : productnature.trim();
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate == null ? null : rate.trim();
    }

    public String getRaterange() {
        return raterange;
    }

    public void setRaterange(String raterange) {
        this.raterange = raterange == null ? null : raterange.trim();
    }

    public String getRatecoefficient() {
        return ratecoefficient;
    }

    public void setRatecoefficient(String ratecoefficient) {
        this.ratecoefficient = ratecoefficient == null ? null : ratecoefficient.trim();
    }

    public String getHasepolicy() {
        return hasepolicy;
    }

    public void setHasepolicy(String hasepolicy) {
        this.hasepolicy = hasepolicy == null ? null : hasepolicy.trim();
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period == null ? null : period.trim();
    }

    public String getDeductible() {
        return deductible;
    }

    public void setDeductible(String deductible) {
        this.deductible = deductible == null ? null : deductible.trim();
    }

    public String getReldeductibe() {
        return reldeductibe;
    }

    public void setReldeductibe(String reldeductibe) {
        this.reldeductibe = reldeductibe == null ? null : reldeductibe.trim();
    }

    public String getInsliab() {
        return insliab;
    }

    public void setInsliab(String insliab) {
        this.insliab = insliab == null ? null : insliab.trim();
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1 == null ? null : remark1.trim();
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2 == null ? null : remark2.trim();
    }

    public String getLinkname() {
        return linkname;
    }

    public void setLinkname(String linkname) {
        this.linkname = linkname == null ? null : linkname.trim();
    }

    public String getLinknum() {
        return linknum;
    }

    public void setLinknum(String linknum) {
        this.linknum = linknum == null ? null : linknum.trim();
    }

    public String getProductattr() {
        return productattr;
    }

    public void setProductattr(String productattr) {
        this.productattr = productattr == null ? null : productattr.trim();
    }

    public String getSignname() {
        return signname;
    }

    public void setSignname(String signname) {
        this.signname = signname == null ? null : signname.trim();
    }

    public String getLegalname() {
        return legalname;
    }

    public void setLegalname(String legalname) {
        this.legalname = legalname == null ? null : legalname.trim();
    }

    public String getActuarialname() {
        return actuarialname;
    }

    public void setActuarialname(String actuarialname) {
        this.actuarialname = actuarialname == null ? null : actuarialname.trim();
    }

    public String getSalepromname() {
        return salepromname;
    }

    public void setSalepromname(String salepromname) {
        this.salepromname = salepromname == null ? null : salepromname.trim();
    }

    public String getSalearea() {
        return salearea;
    }

    public void setSalearea(String salearea) {
        this.salearea = salearea == null ? null : salearea.trim();
    }

    public String getSalechannel() {
        return salechannel;
    }

    public void setSalechannel(String salechannel) {
        this.salechannel = salechannel == null ? null : salechannel.trim();
    }

    public String getOtherinfo() {
        return otherinfo;
    }

    public void setOtherinfo(String otherinfo) {
        this.otherinfo = otherinfo == null ? null : otherinfo.trim();
    }

    public String getRemark3() {
        return remark3;
    }

    public void setRemark3(String remark3) {
        this.remark3 = remark3 == null ? null : remark3.trim();
    }
}