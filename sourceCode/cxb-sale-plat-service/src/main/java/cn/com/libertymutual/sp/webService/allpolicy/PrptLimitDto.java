
package cn.com.libertymutual.sp.webService.allpolicy;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for prptLimitDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="prptLimitDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="calculateFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="currency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="limitCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="limitCurrencyName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="limitFee" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="limitFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="limitGrade" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="limitNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="limitType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="proposalNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="riskCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prptLimitDto", propOrder = {
    "calculateFlag",
    "currency",
    "limitCurrencyCode",
    "limitCurrencyName",
    "limitFee",
    "limitFlag",
    "limitGrade",
    "limitNo",
    "limitType",
    "proposalNo",
    "riskCode"
})
public class PrptLimitDto implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    protected String calculateFlag;
    protected String currency;
    protected String limitCurrencyCode;
    protected String limitCurrencyName;
    protected double limitFee;
    protected String limitFlag;
    protected String limitGrade;
    protected String limitNo;
    protected String limitType;
    protected String proposalNo;
    protected String riskCode;

    /**
     * Gets the value of the calculateFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalculateFlag() {
        return calculateFlag;
    }

    /**
     * Sets the value of the calculateFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalculateFlag(String value) {
        this.calculateFlag = value;
    }

    /**
     * Gets the value of the currency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Sets the value of the currency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrency(String value) {
        this.currency = value;
    }

    /**
     * Gets the value of the limitCurrencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLimitCurrencyCode() {
        return limitCurrencyCode;
    }

    /**
     * Sets the value of the limitCurrencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLimitCurrencyCode(String value) {
        this.limitCurrencyCode = value;
    }

    /**
     * Gets the value of the limitCurrencyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLimitCurrencyName() {
        return limitCurrencyName;
    }

    /**
     * Sets the value of the limitCurrencyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLimitCurrencyName(String value) {
        this.limitCurrencyName = value;
    }

    /**
     * Gets the value of the limitFee property.
     * 
     */
    public double getLimitFee() {
        return limitFee;
    }

    /**
     * Sets the value of the limitFee property.
     * 
     */
    public void setLimitFee(double value) {
        this.limitFee = value;
    }

    /**
     * Gets the value of the limitFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLimitFlag() {
        return limitFlag;
    }

    /**
     * Sets the value of the limitFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLimitFlag(String value) {
        this.limitFlag = value;
    }

    /**
     * Gets the value of the limitGrade property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLimitGrade() {
        return limitGrade;
    }

    /**
     * Sets the value of the limitGrade property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLimitGrade(String value) {
        this.limitGrade = value;
    }

    /**
     * Gets the value of the limitNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLimitNo() {
        return limitNo;
    }

    /**
     * Sets the value of the limitNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLimitNo(String value) {
        this.limitNo = value;
    }

    /**
     * Gets the value of the limitType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLimitType() {
        return limitType;
    }

    /**
     * Sets the value of the limitType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLimitType(String value) {
        this.limitType = value;
    }

    /**
     * Gets the value of the proposalNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProposalNo() {
        return proposalNo;
    }

    /**
     * Sets the value of the proposalNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProposalNo(String value) {
        this.proposalNo = value;
    }

    /**
     * Gets the value of the riskCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRiskCode() {
        return riskCode;
    }

    /**
     * Sets the value of the riskCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRiskCode(String value) {
        this.riskCode = value;
    }

}
