package cn.com.libertymutual.core.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Constants {

	/**Remarks: test Matcher<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年11月15日下午12:28:36<br>
	 * Project：liberty_sale_plat<br>
	 * @param args
	 */
	public static void main(String[] args) {
		// 查找匹配
		Pattern p = Pattern.compile(REGEX_NUMBER_PHONE);
		Matcher m = p.matcher("16584684686");
		System.out.println(m.find());
		System.out.println("16584684686".trim().matches(Constants.REGEX_NUMBER_PHONE));
	}

	/**UTF-8编码*/
	public static final String UTF8 = "UTF-8";
	public final static String PLAIN_TEXT_TYPE = "text/plain";
	/**提示信息*/
	public static final String MESSAGE = "message";
	/**cookie中的JSESSIONID名称*/
	public static final String JSESSION_COOKIE = "JSESSIONID";
	/**url中的jsessionid名称*/
	public static final String JSESSION_URL = "jsessionid";
	/**HTTP POST请求*/
	public static final String POST = "POST";
	/**HTTP GET请求*/
	public static final String GET = "GET";

	/**无需打印返回结果的controller名称列表*/
	public static final String[] LOG_NOT_PRINT_CONTROLLER_NAME = { "shareZxingQrCode", "shareZxingQrCode" };

	/**accessToken类型，1=公众号全局token*/
	public static final Integer ACCESS_TOKEN_TYPE_1 = 1;
	/**accessToken类型，2=企业全局token*/
	public static final Integer ACCESS_TOKEN_TYPE_2 = 2;

	/**
	 * session记录用户信息的key
	 */
	public static final String USER_INFO_KEY = "USER_INFO:KEY";
	public static final String LOGINING_UUID = "USER_INFO:LOGINING:UUID";

	public static final String USER_INFO_TOKEN_KEY = "USER_INFO:TOKEN_KEY";
	public static final String USER_INFO_TOKEN_KEY_OLD = "USER_INFO:TOKEN_KEY_OLD";

	public static final String MENU_INFO_KEY = "MENU:MENU_INFO_KEY";
	public static final String MENU_REL_INFO_KEY = "MENU:MENU_REL_INFO_KEY";
	public static final String ROLE_INFO_KEY = "_ROLE_INFO_KEY";

	public static final String MONITOR_SUBSCRIBE_URI = "/monitor/topic";

	/**关键词类型，0=公众号默认回复*/
	public static String WECHAT_KEYWORDS_TYPE0 = "0";
	/**关键词类型，1=单个关键词回复*/
	public static String WECHAT_KEYWORDS_TYPE1 = "1";
	/**关键词类型，2=多个关键词回复*/
	public static String WECHAT_KEYWORDS_TYPE2 = "2";

	public static String KEY_WORDS_AUTORETURNMSG = "autoReturnMsg";// 默认自动回复
	public static String KEY_WORDS_PRODUCT_LIST = "productlList";// 保险,更多,列表,产品
	public static String KEY_WORDS_PRODUCT_TYPE_3105 = "productType3105";// 家财险,房子,房产,火灾,台风
	public static String KEY_WORDS_SERVICE_DZBD = "service_dzbd";// 保单,生效,查保单
	public static String KEY_WORDS_SERVICE_DZFP = "service_dzfp";// 发票,开发票,电子发票
	public static String KEY_WORDS_SERVICE_MSBA = "service_msba";// 报案,救援
	public static String KEY_WORDS_SERVICE_LPCX = "service_lpcx";// 理赔,进度
	public static String KEY_WORDS_CAR_CODE = "carCode";// 验车码
	public static String KEY_WORDS_DIGITAL_REPLY = "digitalReply";// 旅行险,旅游线,出境游,随心游,鹏程万里
	public static String KEY_WORDS_DIGITAL_REPLY1 = "digitalReply_1";// 1
	public static String KEY_WORDS_DIGITAL_REPLY2 = "digitalReply_2";// 2
	public static String KEY_WORDS_DIGITAL_REPLY3 = "digitalReply_3";// 3
	public static String KEY_WORDS_DIGITAL_REPLY4 = "digitalReply_4";// 4
	public static String KEY_WORDS_DIGITAL_REPLY5 = "digitalReply_5";// 5
	public static String KEY_WORDS_DIGITAL_REPLY6 = "digitalReply_6";// 6
	public static String KEY_WORDS_DIGITAL_REPLY7 = "digitalReply_7";// 7
	public static String KEY_WORDS_DIGITAL_REPLY8 = "digitalReply_8";// 8
	public static String KEY_WORDS_DIGITAL_REPLY9 = "digitalReply_9";// 9
	public static String KEY_WORDS_SERVICE_CXY = "service_cxy";// 出行易
	public static String KEY_WORDS_SERVICE_JBAXTX = "service_jbaxtx";// 旧版安行天下

	/**0*/
	public static String FALSE = "0";
	/**1*/
	public static String TRUE = "1";

	public static String APPROVE_ADD = "2";// 待审批添加
	public static String APPROVE_UP_UPDATE = "3";// 修改原上架待审批
	public static String APPROVE_DOWN_UPDATE = "4";// 修改原下架待审批

	// 审批流类型
	public static String FLOW_PRODUCT = "1";// 产品
	public static String FLOW_ACTICLE = "2";// 文章
	public static String FLOW_POSTER = "3";// 海报
	public static String FLOW_COMMISSION_RATE = "4";// 佣金
	public static String FLOW_MANUAL_SCORE = "5";// 手工发放
	public static String FLOW_ACTIVITY = "6";// 活动

	// 积分
	public static String APPROVE_VALID = "2";// 待审批有效
	public static String APPROVE_STOP = "3";// 待审批停止
	public static String APPROVE_BUDGET = "4";// 待审批预算
	public static String APPROVE_FAIL = "5";// 审批未通过
	public static String EXPIRE = "6";// 到期结束
	public static String NOT_IN_TIME = "7";// 未及时审批

	public static final int MAX_PASSWORD = 30;// 最大用户密码长度

	public static final String Login_CheckWord = "kaptcha";
	public static final String LoginAdminUser = "Login_User_Info";
	public static final String Login = "Login_User";
	public static final String LoginMemberUser = "Login_Member_Info";

	/**代码*/
	public static final String LIST_CODE = "global_listCode";
	public static final String MAP_CODE = "global_mapCode";
	/* 按list存放code */
	public static final String lIMIT_LIST_CODE = "global_limitCode";
	public static final String lIMIT_LIST_CODE_TYPE = "global_limitCodeType";
	public static final String lIMIT_MAP_CODE_TYPE = "global_limitMapCodeType";
	/**获取条款信息*/
	public static final String CLAUSE_INFO = "clauseInfo";
	/**获取地区信息vo==>旅行地*/
	public static final String TRAVEL_ADDRESS_INFO = "travelAddressInfo";

	/**获取地区信息vo==>旅行地*/

	public static final String TRAVEL_ADDRESS_INFO_QUERY = "travelAddressInfoQuery";

	/* 车辆种类 */
	public static final String CAR_KIND = "CarKind";
	/* 能源种类 */
	public static final String FUEL_TYPE = "FuelType";
	/* 交管车种类 */
	public static final String VEHICLE_CATEGORY = "VehicleCategory";
	/* 号牌种类 */
	public static final String LICENSE_KIND_CODE = "LicenseKindCode";
	/* 车险套餐 */
	public static final String CAR_PACKAGE = "CarPackage";
	/* 车辆使用性质 */
	public static final String USE_NATURE = "UseNature";

	/**获取服务信息vo*/
	public static final String SYS_SERVICE_INFO = "sysServiceInfo";
	/**获取服务菜单信息vo*/
	public static final String SERVICE_MENU_INFO = "serviceMenuInfo";
	/**根据userName获取服务信息vo*/
	public static final String SERVICE_INFO_BY_USERNAME = "serviceInfoByUserName";
	/**获取广告信息vo*/
	public static final String AD_CONFIG_INFO = "adConfigInfo";
	/* 车险投保初始化 */
	public static final String CAR_OFFER_INIT = "carOfferInit";
	/**获取产品计划信息*/
	public static final String PLAN_INFO = "planInfo";
	/**获取险种编码和名称信息*/
	public static final String RISK_CODE_NAME_INFO = "riskCodeNameInfo";
	/**获取prpdCode职业类别*/
	public static final String PRPD_CODE_INFO = "prpdCodeInfo";
	/**职业类别*/
	public static final String OCCUPATIO_CATEGORY = "OccupatioCategory";
	/**获取地区编码和机构编码信息*/
	public static final String BRANCH_AREA_CODE_INFO = "branchAreaCodeInfo";
	//
	// /**获取机构下地区配置信息*/
	public static final String BRANCH_AREA_INFO = "branchAreaInfo";
	// /**店铺名称关键词校验*/
	public static final String SHOP_KEYWORD = "shopKeyword";
	/**获取积分配置信息vo*/
	public static final String SCORE_CONFIG_INFO = "scoreConfigInfo";
	// /**店铺名称关键词校验*/
	public static final String LIMIT_LICENSE_NO = "LimitLicenseNo";
	/* 定时任务运行标识 */
	public static final String TIMER_FLAG = "SPTimerFlag";
	/**请求类型*/
	public static final String REQUEST_TYPE_SAVE = "0";
	public static final String REQUEST_TYPE_COMMIT = "1";

	public static final String RATE_ISDEFAULT_YES = "Y";

	/**地区信息类型*/
	public static final String CODE_AREASORT = "AreaSort";
	/**
	 * @author bob.kuang
	 * @date 2016-12-23 11:45
	 * 0-注销、1-记录日志、2-不记录日志、3-只记录请求、4-只记录响应、5-请求和响应均不记录
	 */
	public static final String LOG_FLAG_ALL_RECORD = "1";
	/**
	 * 2-不记录日志
	 */
	public static final String LOG_FLAG_NO_RECORD = "2";
	/**
	 * 3-记录请求
	 */
	public static final String LOG_FLAG_ONLY_REQUEST_RECORD = "3";
	/**
	 * 4-记录响应
	 */
	public static final String LOG_FLAG_ONLY_RESPONSE_RECORD = "4";
	/**
	 * 5-请求和响应均不记录
	 */
	public static final String LOG_FLAG_NO_REQUEST_RESPONSE_RECORD = "5";

	/**
	 * 语言
	 */
	public static final String LANGUAGE_CN = "CN";// 中文
	public static final String LANGUAGE_EN = "EN";// 英文

	public static String VELOCITY_TEMPLATE_PATH = null;
	public static String PDF_TEMPFILE_PATH = null;

	/**
	 * sale_plat Session KEY
	 */
	public static final String USER_INFO_OPENID = "USER_INFO:OPENID"; // OPENID
	// 短地址加密参数
	public static final String AES_ENCRYPT_KEYS = "25d94c9ba6a01777350757a9a4702";
	public static final String AES_ENCRYPT_IV = "LIBERTY.MICR.COM";
	public static final String INVALID_DATES = "15";// 短地址接口的有效期InvalidDates

	// 接口
	public static final String JWT_TOKEN_NAME = "authorization"; // 生成接口访问的统一随机标识
	// public static final int JWT_TOKEN_LENGTH = 20; // 生成接口访问的统一随机标识长度
	// public static final int JWT_TOKEN_TIME = 172800; //
	// 生成接口访问的统一随机标识超时时间，172800=2天
	public static final String CHECK_CODE_TIME_OUT = "TIMEOUT"; // 请求超时标识

	public static final String SYS_ERROR_MSG = "系统繁忙";// 系统默认错误信息

	// REDIS零时数据
	public static final int SINGLE_REQUEST_TIMEOUT_SECONDS = 1800; // 单个请求超时时间,30分钟
	public static String BRANCH_OFFICE_CODE = "BranchOfficeCode";// 分公司机构代码
	public static final String AGREEMEN_TINFO = "agreement_info"; // 业务关系代码认证——当前用户认证的业务关系代码信息
	public static final String SALE_PERSON_TINFO = "sale_person_tinfo"; // 业务关系代码认证——当前用户选择的销售人员信息
	public static final String BANK_TINFO = "bank_tinfo"; // 绑定银行卡——当前用户正在绑定的银行卡信息
	public static final String BANK_LIST_DBMS = "bank_list_dbms"; // 数据库银行卡列表信息

	// 短信
	public final static int SMS_PHONE_MAX_SEND_NUMBER = 3;// 10分钟内短信最大发送次数
	public final static int SMS_VALIDATE_OUTTIME = 60;// 短信验证码过期时间,单位=秒
	public final static int IMG_VALIDATE_OUTTIME = 60;// 图片验证码过期时间,单位=秒
	public static String REDIS_IMG_VERIFY_CODE = "VerifyCode";// 存储图形验证码信息
	public static String REDIS_PHONE_VERIFY_CODE = "PhoneVerifyCode";// 存储短信验证码信息
	public static String REDIS_PHONE_VERIFY_CODE_SUPER = "000000";// 超级短信验证码
	// 短信模板，【所在地区】+【保险中介机构/代理人/非保险机构】+【实名认证姓名】申请业务关系代码。联系电话：注册的手机号。
	public static String SMS_BY_APPLY_AGRNO = "%s %s %s 申请业务关系代码，联系电话：%s。";// 申请业务关系代码发送模板
	public static String SMS_BY_PAY_MSG = "%s于%s通过您分享的链接投保了%s，保费%s元。";// 出单推送模板
	public static String SMS_BY_PAY_BEANS = "您将获得 %s 宝豆。";// 出单宝豆模板
	public static String SMS_BY_ACTIVITY_BEANS_PREFIX = "您已达成：";// 活动宝豆前缀模板
	public static String SMS_BY_ACTIVITY_BEANS_SUFFIX = "%s 的要求，获得 %s 宝豆";// 活动宝豆后缀模板
	public static String SMS_BY_ACTIVITY_TIMES = "您获得 %s 次抽奖机会 %s，<a href='https://m.libao.cn'>进入微门店</a>即可领取。";// 抽奖活动模板
	public static String SMS_BY_FRIENDS_CHANNEL_INSURE = "渠道用户 %s 于 %s 成功推广出单，您将获得 %s 宝豆。】";// 邀请的好友出单推送模板
	public static String SMS_BY_FRIENDS_PERSONAL_INSURE_ZJ = "好友 %s 于 %s 成功推广出单，您将获得 %s 宝豆。";// 邀请的【直接】好友出单推送模板
	public static String SMS_BY_FRIENDS_PERSONAL_INSURE_JJ = "好友 %s 的朋友于 %s 成功推广出单，您将获得 %s 宝豆。";// 邀请的【间接】好友出单推送模板
	public static String SMS_BY_ACCEPT_REGISTER = "您的好友 %s 受邀注册成功，可以在我的团队里查询好友信息。";// 邀请好友注册模板

	// 文件上传路径
	// public static final String FILE_PATH = "C:\\myUpload\\";
	// 文件上传路径
	// public static final String FILE_PATH =
	// "/src/main/resources/myUpload/headUpload/";
	/**
	 * 11位手机号号段<br>
	 * 中国电信:133,153,173,177,180,181,189;<br>
	 * 中国联通:130,131,132,145,155,156,185,186;<br>
	 * 中国移动:134,135,136,137,138,139,147,150,151,152,157,158,159,182,183,184,187,188
	 */
	public static final String MOBILE_BEFOR_THREE_NOS = "133,153,173,177,180,181,189,130,131,132,145,155,156,185,186,134,135,136,137,138,139,147,150,151,152,157,158,159,182,183,184,187,188";

	/** 验证码长度 */
	public static final int REGEX_NUMBER_CODE_LENGTH = 6;
	/** 验证码正则表达式 */
	public static final String REGEX_NUMBER_CODE = "^\\d{" + REGEX_NUMBER_CODE_LENGTH + "}$";
	/** 特殊字符正则表达式 */
	public static final String REGEX_SPECIALCHARACTERS_CODE = "[`~!@#$^&*+()=|{}\\\"':;'\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]|\\n|\\r|\\t";
	/** 11位手机号正则表达式 */
	public static final String REGEX_NUMBER_PHONE = "^\\d{11}$";
	/** date时间格式正则表达式,yyyy-mm-dd hh:mm:ss.SSS */
	public static final String REGEX_DATE_FORMART2 = "^\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}.\\d{3}$";

	public static final String TBSPORDER_SUCCESS_NO_POLICYNO = "4"; // 无效
	public static final String TBSPORDER_STATUS_INVALID = "3"; // 无效
	public static final String TBSPORDER_STATUS_NO_PAY = "2"; // 未支付
	public static final String TBSPORDER_STATUS_PAY = "1"; // 已支付
	public static final String TBSPORDER_FAIL_PAY = "0"; // 支付失败

	public static final String PAY_DOCUMENTNO = "PAY_DOCUMENTNO";

	public static final String PAY_POLICYNO = "PAY_POLICYNO";

	public static final String POLICY_NUM = "POLICY_NUM";
	public static final int PAY_FIND_DOCUMENTNO_OUTTIME = 3600;

	/** 业务关系代码客户类型 */
	public static final String AGREEMENT_NO_CUSTOMER_TYPE = "W0204,W0205";
	/** 业务关系代码 - 保存方式 - 录入 */
	public static final String AGREEMENT_NO_INPUT = "agreement_no_input";
	/** 业务关系代码 - 保存方式  - 常住地选择*/
	public static final String AGREEMENT_NO_SELECTAREA = "agreement_no_selectarea";

	/*
	 * sftp信息
	 */
	public static final String SFTP_REQ_HOST = "sftpHost";
	public static final String SFTP_REQ_USERNAME = "sftpUsername";
	public static final String SFTP_REQ_PASSWORD = "sftpPassword";
	public static final String SFTP_REQ_PORT = "sftpPort";
	public static final int SFTP_DEFAULT_PORT = 3000;

	/**
	 * plan
	 */
	// 方案
	public static final String SALE_QUERY_PLAN_SAVE = "SALE_PLAN_SAVE";

	public static final String SEQ_USER_CODE = "USER_CODE_SEQ";
	public static final String SEQ_ORDER_NO = "ORDER_NO_SEQ";

	public static final String POLICYS_AUTHEN_MARK = "POLICYS_AUTHEN_MARK";
	public static final String POLICYS_AUTHEN_ID_CAR = "POLICYS_AUTHEN_ID_CAR";
	public static final String POLICYS_AUTHEN = "POLICYS_AUTHEN";

	/**审批类型*/
	public static final String PRODUCT_APPROVE = "1";
	public static final String ARTICLE_APPROVE = "2";
	public static final String POSTER_APPROVE = "3";
	public static final String ACTIVITY_APPROVE = "4";// 发布活动审批
	public static final String ACTIVITY_STOP_APPROVE = "5";// 活动停止审批
	public static final String BUDGET_APPROVE = "6";// 添加预算审批
	public static final String MANUAL_APPROVE = "7";// 手工发放审批
	public static final String COMMISSION_APPROVE = "8";// 佣金配置审批
	public static final String AGREEMENT_COMMISSION_APPROVE = "9";// 业务关系代码佣金配置审批
	/**审批类型*/
	public static final String APPROVE_YES = "1";
	public static final String APPROVE_NO = "2";
	public static final String APPROVE_WAIT = "0";

	/**
	 * 积分：变更类型
	 */// 增加
	public static final String CHANGE_TYYPE_ACTIVITY = "1";// 活动
	public static final String CHANGE_TYYPE_RULE = "2";// 规则
	public static final String CHANGE_TYYPE_HANDLE = "3";// 手工
	// 消费
	public static final String CHANGE_TYYPE_CHANGE = "4";// 兑换
	public static final String CHANGE_TYYPE_TB = "5";// 退保
	public static final String CHANGE_TYYPE_INVALID = "6";// 过期
	public static final String CHANGE_TYYPE_REBACK = "7";// 商城退还
	public static final String CHANGE_TYYPE_TBBalance = "8";// 退保差额,余额不足
	public static final String CHANGE_TYYPE_CASH = "9";// 提现
	public static final String CHANGE_TYYPE_REDUBalance = "10";// 扣减差额,余额不足
	public static final String DIRECT_FRIEND_ISSUE = "11";// 直接好友出单
	public static final String INDIRECT_FRIEND_ISSUE = "12";// 间接好友出单
	public static final String CHANNEL_ISSUE = "13";// 渠道出单
	public static final String DIRECT_FRIEND_TB = "14";// 直接好友退保
	public static final String INDIRECT_FRIEND_TB = "15";// 间接好友退保
	public static final String CHANNEL_TB = "16";// 渠道退保
	public static final String CHANGE_TYYPE_ISSUEER = "17";// 出单员收益
	public static final String CHANGE_TYYPE_CLERK = "18";// 业务员收益
	public static final String CHANGE_TYYPE_BRANCH = "19";// 机构收益
	// public static final String INDIRECT_CHANNEL_ISSUE = "4";// 间接渠道出单

	/**
	 * 活动类型:1出单活动2使用推广3抽奖活动
	 */
	public static final String ISSUE_ACTIVITY = "1";
	public static final String EXTENSION_ACTIVITY = "2";
	public static final String LUCK_DRAW_ACTIVITY = "3";

	/**
	 * 触发类型:1出单活动2使用推广3分享
	 */
	public static final String ISSUE_ACTIVITY_DO = "1";
	public static final String EXTENSION_ACTIVITY_DO = "2";
	public static final String EXTENSION_DRAW_ACTIVITY_DO = "3";

	/**
	 * JWT Token超时状态码
	 * @author bob.kuang
	 * @date 20180204
	 */
	public static final int JWT_EXPIRED_ERROR_CODE = 5403;

	/**
	 * 文件上传 附件类型:1:投保必传资料;0:其他资料 1：是核保勾选的必传资料  0：不是核保勾选的资料
	 */
	public static final String UPLOAD_SORT_0 = "0";
	public static final String UPLOAD_SORT_1 = "1";

	/**
	 * 文件上传 系统来源  出单易传:UI; 第三方传值SOA;网销传值:INT;电销传值:TEL
	 */
	public static final String UPLOAD_FROMSYSTEM_UI = "UI";
	public static final String UPLOAD_FROMSYSTEM_SOA = "SOA";
	public static final String UPLOAD_FROMSYSTEM_INT = "INT";
	public static final String UPLOAD_FROMSYSTEM_TEL = "TEL";
	public static final String UPLOAD_FROM_SYSTEM = "SOA";

	public static final String UPLOAD_FILE_TITLE = "其他";

	public static final String UPLOAD_CODE = "IMG04";
	// 用户类型:1游客,2普通客户,3专业代理,4商户,5销售人员
	public static final String USER_TYPE_1 = "1";
	public static final String USER_TYPE_2 = "2";
	public static final String USER_TYPE_3 = "3";
	public static final String USER_TYPE_4 = "4";

	public static final String USER_TYPE_5 = "5";

	/** 用户开店状态-0=未开店 */
	public static final String USER_STORE_FLAG_0 = "0";
	/** 用户开店状态-1=已开店 */
	public static final String USER_STORE_FLAG_1 = "1";

	/** 用户登录状态-0=未登录 */
	public static final String USER_LOGIN_STATE_0 = "0";
	/** 用户登录状态-1=已登录 */
	public static final String USER_LOGIN_STATE_1 = "1";
	/** 用户登录状态-2=已锁定 */
	public static final String USER_LOGIN_STATE_2 = "2";

	/** 用户状态-0=失效 */
	public static final String USER_STATE_0 = "0";
	/** 用户状态-1=有效 */
	public static final String USER_STATE_1 = "1";
	/** 用户状态-2=挂失 */
	public static final String USER_STATE_2 = "2";

	/** 用户注册类型-0=wx自主/分享注册(个人用户) */
	public static final String USER_REGISTER_TYPE_0 = "0";
	/** 用户注册类型-1=后台注册(渠道用户) */
	public static final String USER_REGISTER_TYPE_1 = "1";
	/** 用户注册类型-2=分享注册(渠道用户) */
	public static final String USER_REGISTER_TYPE_2 = "2";

	/** 证件号码类型-1身份证 */
	public static final String USER_ID_CARD_TYPE_1 = "1";
	/** 证件号码类型-2社会信用代码 */
	public static final String USER_ID_CARD_TYPE_2 = "2";

	// 恒华职级变更类型
	public static final String ENTERR_DEPARTMENT = "1";// 入司
	public static final String BECOME_OFFICIAL = "2";// 转正
	public static final String JOIN_IN = "3";// 加盟
	public static final String BE_REINSTATED = "4";// 复职
	public static final String BE_PROMOTED = "5";// 升职
	public static final String BE_DEMOTION = "6";// 降职
	public static final String BE_DISMISSAL = "7";// 免职
	public static final String BE_RESIGNATION = "8";// 辞职
	public static final String BE_TRANSFER_POST = "9";// 转岗
	public static final String BE_REDEPLOYMENT = "10";// 调职
	public static final String BE_TRANSFER = "11";// 转职

}
