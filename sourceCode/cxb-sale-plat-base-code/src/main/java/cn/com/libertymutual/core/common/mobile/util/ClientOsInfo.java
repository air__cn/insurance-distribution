package cn.com.libertymutual.core.common.mobile.util;

import cn.com.libertymutual.core.common.util.ValueWidget;



public class ClientOsInfo {
	/***
	 * 比如 Android_3.0
	 */
	private String osTypeVersion;
	/***
	 * Pad或Phone
	 */
	private String deviceType;
	/***
	 * 只是版本号,例如"4.1.1"
	 */
	private String version;
	public String getOsTypeVersion() {
		return osTypeVersion;
	}
	public void setOsTypeVersion(String osTypeVersion) {
		this.osTypeVersion = osTypeVersion;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	/***
	 * 是否是移动设备
	 * @return
	 */
	public boolean isMobile(){
		return (!ValueWidget.isNullOrEmpty(this.deviceType));
	}
	
	/**
	 * 是否Android
	 * @return
	 */
	public boolean isAndroid() {
		return isMobile() && osTypeVersion.startsWith(HeaderUtil.OSTYPE_ANDROID) ? true : false;
	}

	/**
	 * 是否IOS
	 * @return
	 */
	public boolean isIOS() {
		return isMobile() && osTypeVersion.startsWith(HeaderUtil.OSTYPE_IOS) ? true : false;
	}
	
	/**
	 * 是否WinPhone
	 * @return
	 */
	public boolean isWinPhone() {
		return isMobile() && osTypeVersion.startsWith(HeaderUtil.OSTYPE_WP) ? true : false;
	}
	
	/**
	 * 是否黑莓
	 * @return
	 */
	public boolean isBlackBerry() {
		return isMobile() && osTypeVersion.startsWith(HeaderUtil.OSTYPE_BLACKBERRY) ? true : false;
	}
	
	public String toString() {
		StringBuilder buf = new StringBuilder(32);
		
		buf.append("前端设备：");
		if( isMobile() ) {
			buf.append( isAndroid() ? "Android" : isIOS() ? "IOS" : isWinPhone() ? "WinPhone" : isBlackBerry() ? "BlackBerry" : "OtherMobile "+osTypeVersion);
		}
		else buf.append("PC端浏览器");
		
		return buf.toString();
	}
}
