package cn.com.libertymutual.core.util.enums;

public enum AccessTypeEnum {
	平台为发送方("1"),
	平台为接收方("2");

	
	private String accessType;
	private AccessTypeEnum( String accessType ) {
		this.accessType = accessType;
	}
	
	public String getAccessType() {
		return accessType;
	}
}
