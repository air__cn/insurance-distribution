package cn.com.libertymutual.core.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import cn.com.libertymutual.sys.bean.SysCodeNode;
//import cn.com.libertymutual.sys.bean.SysCodeType;

public class SystemCodeUtil {
	private static Logger log = LoggerFactory.getLogger(SystemCodeUtil.class);
	
	public SystemCodeUtil() {
		
	}
	
	/*public static List<SysCodeNode >   getCodeListWithBranch(String codeType ,String branchCode){
		
		RedisUtils redisUtils = ApplicationContextUtil.getBean("redisUtils", RedisUtils.class);
		
		List<SysCodeNode > list = null;
		
		Map<String, List<SysCodeNode>> map = (Map)redisUtils.get(Constants.lIMIT_LIST_CODE);
		
		if( branchCode != null && !branchCode.isEmpty()){
			list =  map.get(branchCode+"-"+codeType);
		}else{
			list =  map.get(codeType);
		}
		
		return list;
		
	}
	
	*//**
	 * 根据codeType 和codeNode 查出配置
	 * @param codeType
	 * @param codeNode
	 * @return
	 *//*
	public static SysCodeNode getCodeNodeByCodeType(String codeType ,String codeNode){
		RedisUtils redisUtils = ApplicationContextUtil.getBean("redisUtils", RedisUtils.class);
		Map<String, SysCodeNode > nodeMap = null;
		Map<String, Map<String, SysCodeNode>> map = (Map<String, Map<String, SysCodeNode>>)redisUtils.get(Constants.MAP_CODE);
		if(map!= null){
			nodeMap =  (Map<String, SysCodeNode>) map.get(codeType);
			if(nodeMap==null){
				return null;
			}
			return  nodeMap.get(codeNode);
		}else{
			return null;
			
		}
		
	}

	*//**
	 * 检查所传代码是否在码表中
	 * @param codeType	代码类型
	 * @param code	所要查询的代码
	 * @param codeTypeMsg 自定义提示内容,允许为null
	 *//*
	public static  void checkCodeType(String codeType, String code ,String codeTypeMsg) {
		RedisUtils redisUtils = ApplicationContextUtil.getBean("redisUtils", RedisUtils.class);
		Map<String, Map<String, SysCodeNode>> codeNodeMap = (Map<String, Map<String, SysCodeNode>>)redisUtils.get(Constants.MAP_CODE);
		if(code==null){
			throw new AppException(BusinessExceptionEnums.PARAMETER_DATA_ERROR,"所传 "+codeTypeMsg+" 查询code不允许为null!" );
		}
		
		//单位证件类型
		Map<String, SysCodeNode>  codeMap  = codeNodeMap.get(codeType);
		if( !codeMap.containsKey(code)){
			if(!Strings.isNullOrEmpty(codeTypeMsg)){
				throw new AppException(BusinessExceptionEnums.PARAMETER_DATA_ERROR,"所传"+codeTypeMsg+"在数据字典中未找到对应的数据,请确认数据是否正确!" );
				
			}else{
				Map< String,SysCodeType> codeNodeTypeMap = (Map<String, SysCodeType>)redisUtils.get(Constants.lIMIT_MAP_CODE_TYPE);
				SysCodeType type = codeNodeTypeMap.get(codeType);
				
				if(type!= null){
					throw new AppException(BusinessExceptionEnums.PARAMETER_DATA_ERROR,"所传"+type.getCodeTypeName()+"在数据字典中未找到对应的数据,请确认数据是否正确!" );
				}else{
					throw new AppException(BusinessExceptionEnums.PARAMETER_DATA_ERROR,"所传内容在数据字典中未找到对应的数据,请确认数据是否正确!" );
					
				}
			}
		}
		
	}*/

	
}
