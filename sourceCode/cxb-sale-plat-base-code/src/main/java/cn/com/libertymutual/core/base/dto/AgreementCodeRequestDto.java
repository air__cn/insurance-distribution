package cn.com.libertymutual.core.base.dto;

/**
 * 查询业务关系代码接收请求参数实体
 * @author AoYi
 *
 */
public class AgreementCodeRequestDto extends RequestBaseDto {

	private static final long serialVersionUID = 1L;
	
	private String agreementCode;// 业务关系代码
	private String flowId;
	private String operatorDate;// 时间
	private String partnerAccountCode;

	public String getAgreementCode() {
		return agreementCode;
	}

	public void setAgreementCode(String agreementCode) {
		this.agreementCode = agreementCode;
	}

	public String getFlowId() {
		return flowId;
	}

	public void setFlowId(String flowId) {
		this.flowId = flowId;
	}

	public String getOperatorDate() {
		return operatorDate;
	}

	public void setOperatorDate(String operatorDate) {
		this.operatorDate = operatorDate;
	}

	public String getPartnerAccountCode() {
		return partnerAccountCode;
	}

	public void setPartnerAccountCode(String partnerAccountCode) {
		this.partnerAccountCode = partnerAccountCode;
	}

}
