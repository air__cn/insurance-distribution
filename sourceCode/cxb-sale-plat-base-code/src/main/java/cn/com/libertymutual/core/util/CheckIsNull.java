/**============================================================
 * 版权：  版权所有 (c) 2002 - 2012
 * 包： com.spark.lib.character
 * 修改记录：
 * 日期                作者           内容
 * =============================================================
 * 2011-4-30       Administrator        
 * ============================================================*/

package cn.com.libertymutual.core.util;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

/**
 * <p>检查是否为空公共方法</p>
 *
 * <p>Copyright: 版权所有 (c) 2002 - 2008<br>

 *
 * @author xiangzhongqiu
 * @version 2011-4-30
 */

public class CheckIsNull{

	/**
	 * 检查当前对象是否不为空  包括NULL和空字符串 
	 * 如果不为空返回true
	 * 
	 * @param obj  要检查的对象
	 * @return boolean 返回true或false
	 */
	public static boolean isNotEmpty(Object obj){
		return !isEmpty(obj);
	}

	/**
	 * 检查当前对象是否为空  包括NULL和空字符串
	 * 如果为空返回true
	 * @param obj
	 * @return boolean
	 */
	public static boolean isEmpty(Object obj){
		boolean isTrue = false;
		if(null == obj){
			isTrue = true;
		}
		if(obj instanceof String){
			if(((String)obj).trim().length() == 0){
				isTrue = true;
			}
			else{
				isTrue = false;
			}
		}
		if(obj instanceof List<?>){
			if(((List<?>)obj).size() <= 0){
				isTrue = true;
			}
		}
		if(obj instanceof Map<?,?>){
			if(((Map<?,?>)obj).size() <= 0){
				isTrue = true;
			}
		}
		return isTrue;
	}
	/**
	 * 如果对象或对象属性为空,返回true.
	 * @param obj
	 * @return
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException
	 */
	public static  boolean checkObjFieldIsNull(Object obj) throws IllegalArgumentException, IllegalAccessException {
	    boolean flag = true; 
	    if(isEmpty(obj)){return flag ;}
	    
	    for(Field f : obj.getClass().getDeclaredFields()){
	    	 int m =f.getModifiers();
	    	 int i = 8 & m;
	    	/**
	    	 * 如果i==0表示非static，否则就是有static修饰
				或者i==8(本身)表示static，反之为否。
	    	 */
	        f.setAccessible(true);
	        if(i==8 ) continue;
	        if(f.get(obj) != null){
	            flag = false;
	            return flag;
	        }
	        
	    }
	    return flag;
	}
	

}
