package cn.com.libertymutual.core.zxing;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Binarizer;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.EncodeHintType;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;

public class TestQrCode {

	public static void main(String[] args) throws Exception {
		// testEncode();
		// testDecode();

		// 二维码表示的内容
		String content = "https://uat-lm.libertymutual.com.cn/sticcxb/#/sticcxb/details?&shareId=4a2d38565109c109e3ca0fc2e65f2c69&pro=177";

		// width:图片完整的宽;height:图片完整的高
		// 因为要在二维码下方附上文字，所以把图片设置为长方形（高大于宽）
		int width = 400;
		int height = 460;

		String fileType = "jpg";
		// 存放logo的文件夹
		String filePath = "F://imgDemo//";
		String fileName = width + "×" + height + "." + fileType;
		// String domainName = "https://" + request.getServerName() + "/sticcxb/";
		// String logoFullName = domainName + "upload/assets/x3/logo." + fileType;
		String logoFullName = "F://imgDemo//logo//logo." + fileType;

		Zxing.wirteImg(content, filePath, fileName, fileType, logoFullName, new String[] { "“随心游”境外旅行险", "" }, width, height);
	}

	/** 
	 * 生成二维码 
	 *  
	 * @throws WriterException 
	 * @throws IOException 
	 */
	public static void testEncode() throws WriterException, IOException {
		String fileType = "jpg";// 图像类型
		String filePath = "F://imgDemo//";
		String fileName = "44000001001-“安居乐”家财险-恒华." + fileType;
		String fullName = filePath + fileName;

		String content = "https://uat-lm.libertymutual.com.cn/sticcxb/#/sticcxb/details?&shareId=4a2d38565109c109e3ca0fc2e65f2c69&pro=177";
		int width = 600; // 图像宽度
		int height = 600; // 图像高度
		Map<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>();
		hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
		BitMatrix bitMatrix = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, width, height, hints);// 生成矩阵

		Path path = FileSystems.getDefault().getPath(filePath, fileName);
		MatrixToImageWriter.writeToPath(bitMatrix, fileType, path);// 输出图像
		System.out.println("输出成功.");
	}

	/** 
	 * 解析二维码 
	 */
	public static void testDecode() {
		String filePath = "F://imgDemo//zxing.png";
		BufferedImage image;
		try {
			image = ImageIO.read(new File(filePath));
			LuminanceSource source = new BufferedImageLuminanceSource(image);
			Binarizer binarizer = new HybridBinarizer(source);
			BinaryBitmap binaryBitmap = new BinaryBitmap(binarizer);
			Map<DecodeHintType, Object> hints = new HashMap<DecodeHintType, Object>();
			hints.put(DecodeHintType.CHARACTER_SET, "UTF-8");
			Result result = new MultiFormatReader().decode(binaryBitmap, hints);// 对图像进行解码
			System.out.println("图片中内容：  ");
			System.out.println("author： " + result.getText());
			System.out.println("图片中格式：  ");
			System.out.println("encode： " + result.getBarcodeFormat());
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NotFoundException e) {
			e.printStackTrace();
		}
	}

}
