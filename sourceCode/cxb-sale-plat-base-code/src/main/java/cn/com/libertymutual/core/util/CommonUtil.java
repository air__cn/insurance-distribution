package cn.com.libertymutual.core.util;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class CommonUtil {
	private static Logger logger = LoggerFactory.getLogger(CommonUtil.class);
	/**
	 * 替换字符串
	 * @author xrxianga
	 * @param str 被替换的字符串
	 * @param src 替换的字符串
	 * @param beginIndex 替换的开始位置
	 * @param endIndex 替换的结束位置
	 * @return
	 * @throws Exception
	 * @date 2014-9-27下午05:54:37
	 */
	public static String doReplaceStr(String str,String src,int beginIndex,int endIndex)throws Exception{
		if(null!=str && !"".equals(str)){
			int size = str.length();
			if(size>beginIndex){
				String sb = "";
				if(size>endIndex){
					sb = str.substring(beginIndex, endIndex);
				}else{
					sb = str.substring(beginIndex, size);
				}
				if(!"".equals(sb)){
					str = str.replace(sb, src);
				}
			}
		}
		return str;
	}
	/**
	 * 判断是否null
	 * @autor xrxianga
	 * @param obj
	 * @return
	 * @throws Exception
	 * @date 2014-7-1下午04:20:16
	 */
	public static String doEmpty(Object obj)throws Exception{
		return obj==null?"":obj.toString().trim();
	}
	/**
	 * String to date
	 * @autor xrxianga
	 * @param dateStr
	 * @param formart
	 * @return
	 * @throws Exception
	 * @date 2014-7-1下午04:22:23
	 */
	public static Date toDate(String dateStr,String formart)throws Exception{
		if(null!=dateStr && !"".equals(dateStr)){
			SimpleDateFormat myFormatter = new SimpleDateFormat(formart);
			return myFormatter.parse(dateStr);
		}else{
			return null;
		}
	}
	/**
	 * date to String
	 * @author xrxianga
	 * @param date
	 * @param formart
	 * @return
	 * @throws Exception
	 * @date 2014-8-7下午12:15:24
	 */
	public static String toString(Date date,String formart)throws Exception{
		if(null != date){
			SimpleDateFormat myFormatter = new SimpleDateFormat(formart);
			return myFormatter.format(date);
		}else{
			return null;
		}
	}
	/**
	 * 获取对应properties文件内容
	 * @autor xrxianga
	 * @param propName properties文件
	 * @return properties
	 * @throws Exception
	 * @date 2014-7-14上午10:24:55
	 */
	public static Properties getPropertiesValue(String propName)throws Exception{
		Properties ppt = null;
		InputStream ips = null;
		try {
			ppt = new Properties();
			ips = CommonUtil.class.getResourceAsStream(propName);
			ppt.load(ips);
		} catch (Exception e) {
			logger.error("获取properties文件内容时异常，异常原因为："+e.getMessage(),e);
			throw new Exception(e.getMessage(),e);
		}finally{
			if(null!=ips){
				ips.close();
			}
		}
		return ppt;
	}
	

	/**
	 * 生成邮件模板
	 * 
	 * @param insured
	 * @return
	 */
	public static String fetchSendMessage(String insuredName) {
		StringBuilder content = new StringBuilder();
		content.append("<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">")
				.append("<title></title><style type=\"text/css\">html, body { height : 100%;font-size:12px;}")
				.append(".labelspan {text-align: right; width: 120px;color:black;line-height: 20px; vertical-align: middle;}")
				.append(" .bcolor { background-color: yellow; } .tbborder { border: solid 1px #B4B4B4;      border-collapse: collapse; }")
				.append("td { border-top: solid 1px #B4B4B4;   border-right: solid 1px #B4B4B4;  text-align : center; }")
				.append("</style></head><body>尊敬的 <b>").append(insuredName)
				.append("</b> ,<div style=\"margin-left:30px;width:600px;\">您好！</div>")
				.append("<div style=\"margin-left:30px;width:600px;\">您投保相关的资料请见附件。</div>")
				.append("<div align=\"left\" style=\"width:600px;\"><br/>Regards,<br/>利宝保险<br/><br/><br/>")
				.append("The email is automatically send by the system,please do not reply it .<br/>")
				.append("这封邮件是由系统自动发出，请勿回复。</div></body></html>");
		return content.toString();
	}
}
