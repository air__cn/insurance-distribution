package cn.com.libertymutual.core.base;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class LoginComCode implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4745124784337889084L;
	private String loginComcodeCode ;// 机构代码
    private String loginComcodeName ;// 机构代码中文名称
    @JsonIgnore
    private List<String> roleIds;	//角色
    
	public String getLoginComcodeCode() {
		return loginComcodeCode;
	}
	public void setLoginComcodeCode(String loginComcodeCode) {
		this.loginComcodeCode = loginComcodeCode;
	}
	public String getLoginComcodeName() {
		return loginComcodeName;
	}
	public void setLoginComcodeName(String loginComcodeName) {
		this.loginComcodeName = loginComcodeName;
	}
	public List<String> getRoleIds() {
		return roleIds;
	}
	public void setRoleIds(List<String> roleIds) {
		this.roleIds = roleIds;
	}

}
