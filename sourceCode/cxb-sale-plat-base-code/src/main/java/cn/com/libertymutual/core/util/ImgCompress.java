package cn.com.libertymutual.core.util;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.imageio.ImageIO;

//import com.sun.image.codec.jpeg.JPEGCodec;
//import com.sun.image.codec.jpeg.JPEGImageEncoder;

/**
 * 缩小图片
 * @author xrxianga
 * @date 2014-10-9
 */
public class ImgCompress {
	/**
	 * 缩略图片
	 * @author xrxianga
	 * @param inFile 读取文件路径
	 * @param outFile 输出文件路径
	 * @throws Exception
	 * @date 2014-10-9下午07:58:08
	 */
	public static void doImgCompress(String inFile, String outFile)
			throws Exception {
		File file = new File(inFile);
		Image img = ImageIO.read(file);
		int width = img.getWidth(null);
		int height = img.getHeight(null);
		resizeFix(img, width, height, 800, 800, outFile);
	}
	public static void doImgCompress(InputStream in, String outFile)
			throws Exception {
		Image img = ImageIO.read(in);
		int width = img.getWidth(null);
		int height = img.getHeight(null);
		resizeFix(img, width, height, 800, 800, outFile);
	}
	public static void doImgCompress(File file, String outFile)
			throws Exception {
		//File file = new File(inFile);
		Image img = ImageIO.read(file);
		int width = img.getWidth(null);
		int height = img.getHeight(null);
		resizeFix(img, width, height, 800, 800, outFile);
	}

	private static void resizeFix(Image image, int width, int height,
			int newWidth, int newHeigth, String file) throws IOException {
		if (width / height > newWidth / newHeigth) {
			int hg = (int) (height * newWidth / width);
			resize(image, newWidth, hg, file);
		} else {
			int wd = (int) (width * newHeigth / height);
			resize(image, wd, newHeigth, file);
		}
	}
	
	private static void resize(Image image, int w, int h, String file)
			throws IOException {
		BufferedImage bi = new BufferedImage(w, h, BufferedImage.SCALE_SMOOTH);
		/*bi.getGraphics().drawImage(image, 0, 0, w, h, null);
		File destFile = new File(file);
		FileOutputStream out = new FileOutputStream(destFile);
		JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
		encoder.encode(bi);
		out.close();*/
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		 ImageIO.write(bi, "jpg", out);
         
         out.flush();
         //byte[] b = out.toByteArray();
         out.close();
         bi.flush();
         bi = null;
	}
	public static void inputstreamtofile(InputStream ins,File file){
		OutputStream os=null;
		try {
			os = new FileOutputStream(file);
		int bytesRead = 0;
		byte[] buffer = new byte[8192];
		
		while ((bytesRead = ins.read(buffer, 0, 8192)) != -1) {
				os.write(buffer, 0, bytesRead);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			if( os != null )
				os.close();
			ins.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
	public static void main(String[] args) throws Exception{
//		doImgCompress("D:\\liberty_photo\\upload\\IMG_20140607_153923.jpg", "D:\\liberty_photo\\upload\\IMG_2.jpg");
	}
}
