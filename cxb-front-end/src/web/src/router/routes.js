import {
  RouteUrl
} from 'src/common/const';

//========================================企业微信运动
const AddSport = r => require.ensure([], () => r(require('../views/sport/AddSport')));
const SportRanking = r => require.ensure([], () => r(require('../views/sport/SportRanking')));

//========================================智通
const InsureOne = r => require.ensure([], () => r(require('../views/zhitong/InsureOne')));
const InsureTwo = r => require.ensure([], () => r(require('../views/zhitong/InsureTwo')));

//========================================自销营平台
const Index = r => require.ensure([], () => r(require('../views/index/Index')));
const Home = r => require.ensure([], () => r(require('../views/Home')));
const Insurance = r => require.ensure([], () => r(require('../views/insurance/Insurance')));
const Service = r => require.ensure([], () => r(require('../views/service/Service')));
const My = r => require.ensure([], () => r(require('../views/my/My')));
// const Message = r => require.ensure([], () => r(require('../views/my/Message')));
// const Activity = r => require.ensure([], () => r(require('../views/my/Activity')));
const Report = r => require.ensure([], () => r(require('../views/my/Report')));
// const Customer = r => require.ensure([], () => r(require('../views/my/Customer')));
const Friends = r => require.ensure([], () => r(require('../views/my/Friends')));
const Demo = r => require.ensure([], () => r(require('../views/Demo')));
// const Shop = r => require.ensure([], () => r(require('../views/my/shop')));
const Order = r => require.ensure([], () => r(require('../views/my/Order')));
const HistoricalRC = r => require.ensure([], () => r(require('../views/my/HistoricalRC')));
const Income = r => require.ensure([], () => r(require('../views/my/Income')));
// const Classroom = r => require.ensure([], () => r(require('../views/my/Classroom')));
const About = r => require.ensure([], () => r(require('../views/my/About')));
// const Feedback = r => require.ensure([], () => r(require('../views/my/Feedback')));
const Account = r => require.ensure([], () => r(require('../views/my/Account')));
const MgtPassword = r => require.ensure([], () => r(require('../views/my/MgtPassword')));
// const MgtUserName = r => require.ensure([], () => r(require('../views/my/MgtUserName')));
const MgtMobile = r => require.ensure([], () => r(require('../views/my/MgtMobile')));
const MgtIdNumber = r => require.ensure([], () => r(require('../views/my/MgtIdNumber')));
const MgtBanks = r => require.ensure([], () => r(require('../views/my/MgtBanks')));
const CodeAuthentication = r => require.ensure([], () => r(require('../views/my/CodeAuthentication')));
// const Autograph = r => require.ensure([], () => r(require('../components/Autograph')));
const Policy = r => require.ensure([], () => r(require('../views/Insure/other/Policy')));
const Insuresuccess = r => require.ensure([], () => r(require('../views/Insure/other/Insuresuccess')));
const Login = r => require.ensure([], () => r(require('../views/login/Login')));
const Register = r => require.ensure([], () => r(require('../views/login/Register')));
const Iframe = r => require.ensure([], () => r(require('../views/other/Iframe')));
const ConfigUrl = r => require.ensure([], () => r(require('../views/other/ConfigUrl')));
const AdActivity = r => require.ensure([], () => r(require('../views/other/Activity')));
const InsureConfirm = r => require.ensure([], () => r(require('../views/Insure/other/InsureConfirm')));
const Details = r => require.ensure([], () => r(require('../views/Insure/details/Details')));
const Insure = r => require.ensure([], () => r(require('../views/Insure/other/Insure')));
const IdCarAuthentication = r => require.ensure([], () => r(require('../views/service/IdCarAuthentication')));
const FollowUp = r => require.ensure([], () => r(require('../views/service/FollowUp')));
const PolicyList = r => require.ensure([], () => r(require('../views/service/PolicyList')));
const Einv = r => require.ensure([], () => r(require('../views/my/Einv')));
const ReportImmediately = r => require.ensure([], () => r(require('../views/Service/ReportImmediately')));
const ReportUploadFiles = r => require.ensure([], () => r(require('../views/Service/ReportUploadFiles')));
const Msg = r => require.ensure([], () => r(require('../views/other/Msg')));
const ClauseRead = r => require.ensure([], () => r(require('../views/other/ClauseRead')));
const InsureAxtx = r => require.ensure([], () => r(require('../views/Insure/axtx/InsureAxtx')));
const InsureConfirmAxtx = r => require.ensure([], () => r(require('../views/Insure/axtx/InsureConfirmAxtx')));
const setShop = r => require.ensure([], () => r(require('../views/shop/setShop')));
const applyAgrNo = r => require.ensure([], () => r(require('../views/shop/applyAgrNo')));
const shopIndex = r => require.ensure([], () => r(require('../views/shop/shopIndex')));
const shopManage = r => require.ensure([], () => r(require('../views/shop/shopManage')));
const productManage = r => require.ensure([], () => r(require('../views/shop/productManage')));
const performanceStatistics = r => require.ensure([], () => r(require('../views/shop/performanceStatistics')));
const performanceDetails = r => require.ensure([], () => r(require('../views/shop/performanceDetails')));
const marketingSoftPaper = r => require.ensure([], () => r(require('../views/shop/marketingSoftPaper')));
const softPaper = r => require.ensure([], () => r(require('../views/shop/SoftPaper')));
const marketingPoster = r => require.ensure([], () => r(require('../views/shop/marketingPoster')));
const showPoster = r => require.ensure([], () => r(require('../views/shop/ShowPoster')));
const OrderDetails = r => require.ensure([], () => r(require('../views/service/OrderDetails')));
const PayDetails = r => require.ensure([], () => r(require('../views/service/PayDetails')));
const PayOrderList = r => require.ensure([], () => r(require('../views/service/PayOrderList')));
const MyIntegral = r => require.ensure([], () => r(require('../views/integral/MyIntegral')));
const IntegralList = r => require.ensure([], () => r(require('../views/integral/IntegralList')));
const IntegralDetail = r => require.ensure([], () => r(require('../views/integral/IntegralDetail')));
const IntegralExchange = r => require.ensure([], () => r(require('../views/integral/IntegralExchange')));
const IntegralExchangeList = r => require.ensure([], () => r(require('../views/integral/IntegralExchangeList')));
const CarInsure = r => require.ensure([], () => r(require('../views/car/CarInsure')));
const CarOffer = r => require.ensure([], () => r(require('../views/car/CarOffer')));
const OfferResult = r => require.ensure([], () => r(require('../views/car/OfferResult')));

const InsureCarInfo = r => require.ensure([], () => r(require('../views/car/InsureCarInfo')));
const InsureInfo = r => require.ensure([], () => r(require('../views/car/InsureInfo')));
const Distribution = r => require.ensure([], () => r(require('../views/car/Distribution')));
const SpecialAgreement = r => require.ensure([], () => r(require('../views/car/SpecialAgreement')));

const CarInfo = r => require.ensure([], () => r(require('../views/car/CarInfo')));
const CarInsureInfo = r => require.ensure([], () => r(require('../views/car/CarInsureInfo')));
const AddCarInfo = r => require.ensure([], () => r(require('../views/car/AddCarInfo')));
const CarSelect = r => require.ensure([], () => r(require('../views/car/CarSelect')));
const Jump = r => require.ensure([], () => r(require('../views/other/Jump')));
const BatchProcessQrcode = r => require.ensure([], () => r(require('../views/BatchProcessQrcode')));
const ShopQrcode = r => require.ensure([], () => r(require('../views/shop/shopQrcode')));
const activityGreatBenefit = r => require.ensure([], () => r(require('../views/shop/activityGreatBenefit')));
const Questionnaire = r => require.ensure([], () => r(require('../views/Insure/other/Questionnaire')));

//五期 - 渠道/层级
const MySuperior = r => require.ensure([], () => r(require('../views/channel/MySuperior')));
const MyFriends = r => require.ensure([], () => r(require('../views/channel/MyFriends')));
const RegisterPoster = r => require.ensure([], () => r(require('../views/channel/RegisterPoster')));
const RegisterInvite = r => require.ensure([], () => r(require('../views/channel/RegisterInvite')));
const RegisterAccept = r => require.ensure([], () => r(require('../views/channel/RegisterAccept')));
const RegisterChannel = r => require.ensure([], () => r(require('../views/channel/RegisterChannel')));

let routes = [{
  path: '/',
  component: Home,
  children: [

    //========================================企业微信运动
    {
      path: RouteUrl.ADD_SPORT,
      component: AddSport
    },
    {
      path: RouteUrl.SPORT_RANKING,
      component: SportRanking
    },

    //========================================智通
    {
      path: RouteUrl.INSURE_ONE,
      component: InsureOne
    },
    {
      path: RouteUrl.INSURE_TWO,
      component: InsureTwo
    },

    //========================================自销营平台
    {
      path: RouteUrl.INDEX,
      component: Index
    },
    {
      path: RouteUrl.REGISTER_CHANNEL,
      component: RegisterChannel
    },
    {
      path: RouteUrl.REGISTER_ACCEPT,
      component: RegisterAccept
    },
    {
      path: RouteUrl.REGISTER_INVITE,
      component: RegisterInvite
    },
    {
      path: RouteUrl.REGISTER_POSTER,
      component: RegisterPoster
    },
    {
      path: RouteUrl.MY_FRIENDS,
      component: MyFriends
    },
    {
      path: RouteUrl.MY_SUPERIOR,
      component: MySuperior
    },
    {
      path: RouteUrl.SPECIAL_AGREEMENT,
      component: SpecialAgreement
    },
    {
      path: RouteUrl.QUESTIONNAIRE,
      component: Questionnaire
    },
    {
      path: RouteUrl.INSURE_INFO,
      component: InsureInfo
    },
    {
      path: RouteUrl.INSURE_CAR_INFO,
      component: InsureCarInfo
    },
    {
      path: RouteUrl.DISTRIBUTION,
      component: Distribution
    },
    {
      path: RouteUrl.CAR_OFFER,
      component: CarOffer
    },
    {
      path: RouteUrl.OFFER_RESULT,
      component: OfferResult
    },
    {
      path: RouteUrl.ACTIVITY_GREAT_BENEFIT,
      component: activityGreatBenefit
    },
    {
      path: RouteUrl.CAR_INFO,
      component: CarInfo
    },
    {
      path: RouteUrl.CAR_INSURE_INFO,
      component: CarInsureInfo
    },
    {
      path: RouteUrl.ADD_CAR_INFO,
      component: AddCarInfo
    },
    {
      path: RouteUrl.CAR_SELECT,
      component: CarSelect
    },
    {
      path: RouteUrl.JUMP,
      component: Jump
    },
    {
      path: RouteUrl.SHOP_QRCODE,
      component: ShopQrcode
    },
    {
      path: RouteUrl.PAY_DETAILS,
      component: PayDetails
    },
    {
      path: RouteUrl.CAR_INSURE,
      component: CarInsure
    },
    {
      path: RouteUrl.CONFIG_URL,
      component: ConfigUrl
    },
    {
      path: RouteUrl.PAY_ORDER_LIST,
      component: PayOrderList
    },
    {
      path: RouteUrl.PERFORMANCE_STATISTICS,
      component: performanceStatistics
    },
    {
      path: RouteUrl.ORDER_DETAILS,
      component: OrderDetails
    },
    {
      path: RouteUrl.MARKETING_POSTER,
      component: marketingPoster
    },
    {
      path: RouteUrl.SHOW_POSTER,
      component: showPoster
    },
    {
      path: RouteUrl.DEMO,
      component: Demo
    },
    {
      path: RouteUrl.MARKETING_SOFT_PAPER,
      component: marketingSoftPaper
    },
    {
      path: RouteUrl.SOFT_PAPER,
      component: softPaper
    },
    {
      path: RouteUrl.PERFORMANCE_DETAILS,
      component: performanceDetails
    },
    {
      path: RouteUrl.PRODUCT_MANAGE,
      component: productManage
    },
    {
      path: RouteUrl.SHOP_MANAGE,
      component: shopManage
    },
    {
      path: RouteUrl.SHOP_INDEX,
      component: shopIndex
    },
    {
      path: RouteUrl.MSG,
      component: Msg
    },
    {
      path: RouteUrl.SET_SHOP,
      component: setShop
    },
    {
      path: RouteUrl.APPLY_AGR_NO,
      component: applyAgrNo
    },
    {
      path: RouteUrl.INSURE_CONFIRM_AXTX,
      component: InsureConfirmAxtx
    },
    {
      path: RouteUrl.INSURE_AXTX,
      component: InsureAxtx
    },
    {
      path: RouteUrl.MSG,
      component: Msg
    },
    {
      path: RouteUrl.CLAUSE_READ,
      component: ClauseRead
    },
    {
      path: RouteUrl.FOLLOW_UP,
      component: FollowUp
    },
    {
      path: RouteUrl.POLICY_LIST,
      component: PolicyList
    },
    {
      path: RouteUrl.REPORT_IMMEDIATELY,
      component: ReportImmediately
    },
    {
      path: RouteUrl.REPORT_UPLOAD_FILES,
      component: ReportUploadFiles
    },
    {
      path: RouteUrl.EINV,
      component: Einv
    },
    {
      path: RouteUrl.ID_CAR_AUTHENTICATION,
      component: IdCarAuthentication
    },
    {
      path: RouteUrl.INSURESUCCESS,
      component: Insuresuccess
    },
    {
      path: RouteUrl.DETAILS,
      component: Details
    },
    {
      path: RouteUrl.INSURE,
      component: Insure
    },
    {
      path: RouteUrl.INSURE_CONFIRM,
      component: InsureConfirm
    },
    {
      path: RouteUrl.INSURANCE,
      component: Insurance
    },
    {
      path: RouteUrl.SERVICE,
      component: Service
    },
    {
      path: RouteUrl.AD_ACTIVITY,
      component: AdActivity
    },
    {
      path: RouteUrl.MY,
      component: My
    },
    {
      path: RouteUrl.IFRAME,
      component: Iframe
    },
    // {
    //   path: RouteUrl.MESSAGE,
    //   component: Message
    // },
    // {
    //   path: RouteUrl.ACTIVITY,
    //   component: Activity
    // },
    {
      path: RouteUrl.REPORT,
      component: Report
    },
    // {
    //   path: RouteUrl.CUSTOMER,
    //   component: Customer
    // },
    // {
    //   path: RouteUrl.SHOP,
    //   component: Shop
    // },
    {
      path: RouteUrl.ORDER,
      component: Order
    },
    {
      path: RouteUrl.HISTORICAL_RC,
      component: HistoricalRC
    },
    // {
    //   path: RouteUrl.INCOME,
    //   component: Income
    // },
    {
      path: RouteUrl.FRIENDS,
      component: Friends
    },
    // {
    //   path: RouteUrl.CLASSROOM,
    //   component: Classroom
    // },
    {
      path: RouteUrl.ABOUT,
      component: About
    },
    // {
    //   path: RouteUrl.FEEDBACK,
    //   component: Feedback
    // },
    {
      path: RouteUrl.ACCOUNT,
      component: Account
    },
    {
      path: RouteUrl.MGT_PASSWORD,
      component: MgtPassword
    },
    // {
    //   path: RouteUrl.MGT_USER_NAME,
    //   component: MgtUserName
    // },
    {
      path: RouteUrl.MGT_MOBILE,
      component: MgtMobile
    },
    {
      path: RouteUrl.MGT_IDNUMBER,
      component: MgtIdNumber
    },
    {
      path: RouteUrl.MGT_BANKS,
      component: MgtBanks
    },
    {
      path: RouteUrl.CODE_AUTHENTICATION,
      component: CodeAuthentication
    },
    {
      path: RouteUrl.LOGIN,
      component: Login
    },
    {
      path: RouteUrl.REGISTER,
      component: Register
    },
    {
      path: RouteUrl.POLICY_INQUIRY,
      component: Policy
    },
    {
      path: RouteUrl.MY_INTEGRAL,
      component: MyIntegral
    },
    {
      path: RouteUrl.INTEGRAL_IN_OR_OUT_LIST,
      component: IntegralList
    },
    {
      path: RouteUrl.INTEGRAL_DETAIL,
      component: IntegralDetail
    },
    {
      path: RouteUrl.INTEGRAL_EXCHANGE,
      component: IntegralExchange
    },
    {
      path: RouteUrl.INTEGRAL_EXCHANGE_LIST,
      component: IntegralExchangeList
    },
    {
      path: RouteUrl.BATCH_PROCESS_QRCODE,
      component: BatchProcessQrcode
    },
    {
      path: '*',
      component: Index
    }
  ]
}];
export default routes;
