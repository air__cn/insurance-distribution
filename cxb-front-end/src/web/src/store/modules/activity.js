import {
    RequestUrl
} from 'src/common/url';
import {
    Mutations, RouteUrl
} from 'src/common/const';
export default {
    state: {
        isShowRouteRed: false,
        isShowRed: false,
        ruleNum: 5,
        drawRes: "",
        redMsg: "",
        msgState: true,
    },
    mutations: {
        [Mutations.IS_SHOW_ROUTE_RED](state, type) {
            state.isShowRouteRed = type;
        },
        [Mutations.IS_SHOW_RED](state, _this) {
            //获得当前用户的抽奖次数
            if (_this.user.isLogin && _this.user.userDto.storeFlag == "1") {
                // state.isShowRed = true;
                let query = {
                    TYPE: "FORM",
                    userCode: _this.user.userDto.userCode
                }
                // alert(_this.user.userDto.userCode);
                _this.$http.post(RequestUrl.QUERY_DRAW_TIMES, query)
                    .then(function (res) {
                        // alert(res);
                        // alert();
                        if (res.success) {
                            state.ruleNum = res.result;
                        } else {
                            state.ruleNum = 0;
                        }
                        state.isShowRed = true;
                    })
            } else {
                state.isShowRed = false;
            }
        },
        [Mutations.RULE_NUM](state, num) {
            ruleNum = ruleNum - num;
            if (ruleNum < 0) {
                ruleNum = 0;
            }
        },
        //获得当前用户的抽奖次数
        // [Mutations.QUERY_DRAW_TIMES](state, activity) {
        //     let _this = activity._this;
        //     let query = {
        //         TYPE: "FORM",
        //         userCode: activity.userCode
        //     }
        //     _this.$http.post(RequestUrl.QUERY_DRAW_TIMES, query)
        //         .then(function (res) {

        //         })

        // },
        //抽奖
        [Mutations.ACTIVITY_DRAW](state, _this) {
            state.ruleNum--;
            if (state.ruleNum < 0) {
                state.ruleNum = 0;
            }
            // debugger
            if (_this.user.isLogin && _this.user.userDto.storeFlag == "1") {
                let query = {
                    TYPE: "FORM",
                    userCode: _this.user.userDto.userCode
                }
                state.redMsg = "";
                let tir = {
                    state: false
                }
                _this.$common.storeCommit(_this, Mutations.RED_TRIGGER, tir);
                _this.$http.post(RequestUrl.ACTIVITY_DRAW, query)
                    .then(function (res) {
                        if (res.success) {
                            state.msgState = true;
                            state.drawRes = res.result;
                        } else {
                            state.msgState = false;
                            state.redMsg = res.result;
                        }
                        _this.$common.storeCommit(_this, Mutations.IS_SHOW_RED, _this);
                        let tir = {
                            state: true
                        }
                        _this.$common.storeCommit(_this, Mutations.RED_TRIGGER, tir);
                    })
            }
        },
    },
    actions: {

    },
    getters: {

    }
}