import {
    Mutations, RouteUrl
} from 'src/common/const';
import OrderConfig from 'src/common/config/orderConfig';
import {
    RequestUrl
} from 'src/common/url';
export default {
    state: {
        curPlanId: "",
        saveClause: {},
        curClauseTitleList: [],
        arrowDirection: [],
        curProtocol: {},
    },
    mutations: {
        //根据标题查询单个协议
        [Mutations.CUR_CLAUSE_TITLE_ONE](state, data) {
            let _this = data._this;
            if (typeof (state.curProtocol[data.planId]) == 'undefined' || state.curProtocol[data.planId] == null) {
                _this.$http.post(RequestUrl.QUERY_CLAUSE, data).then(function (res) {
                    if (res.state == 0) {
                        state.curProtocol = res.result
                        _this.readText = state.curProtocol.clauseContent;//协议内容
                    }
                });
            }
        },
        [Mutations.CUR_CLAUSE_TITLE_LIST](state, queryData) {
            // let clauseTitle = queryData.clauseTitle;
            state.curPlanId = queryData.planId;
            let planId = queryData.planId;
            let _this = queryData._this;
            // state.curClauseTitleList = clauseTitle.split(',');
            if (typeof (state.saveClause[planId]) == "undefined") {
                state.saveClause[planId] = {};
                let queryTitle = {
                    planId: planId,
                    TYPE: "FORM"
                }
                _this.$http.post(RequestUrl.QUERY_CLAUSE_TITLE, queryTitle)
                    .then(function (res) {
                        let titleList = [];
                        for (let i = 0; i < res.result.length; i++) {
                            titleList.push(res.result[i]);
                        }
                        state.saveClause[planId].curClauseTitleList = titleList;
                        state.curClauseTitleList = state.saveClause[planId].curClauseTitleList;
                        for (let i = 0; i < state.curClauseTitleList.length; i++) {
                            // arrowDirection.push(false);
                            state.saveClause[planId][state.curClauseTitleList[i]] = "";
                        }
                    });

                // let arrowDirection = [];
                // for (let i = 0; i < state.curClauseTitleList.length; i++) {
                //     // arrowDirection.push(false);
                //     state.saveClause[planId][state.curClauseTitleList[i]] = "";
                // }
                // state.saveClause[planId].arrowDirection = arrowDirection;
                // state.arrowDirection = state.saveClause[planId].arrowDirection;
            } else {
                // state.arrowDirection = state.saveClause[planId].arrowDirection;
                state.curClauseTitleList = state.saveClause[planId].curClauseTitleList;
            }
            _this.$common.goUrl(_this, RouteUrl.CLAUSE_READ);
        },
        [Mutations.GET_CLAUSE_STORE_ITEM](state, queryData) {
            let title = queryData.title;
            let _this = queryData._this;
            if (typeof (state.saveClause[state.curPlanId][title] == "")) {
                let query = {
                    TYPE: "FORM",
                    planId: state.curPlanId,
                    clauseTitle: title
                }
                _this.$http.post(RequestUrl.QUERY_CLAUSE, query)
                    .then(function (res) {
                        state.saveClause[state.curPlanId][title] = res.result.clauseContent;
                        let query = {
                            activityName: title,
                            activityDiv: res.result.clauseContent
                            //     activityDiv: menu.detail,
                            //     activityName: clause,
                        };
                        // _this.$store.commit(Mutations.FOOTER_SHOW, FooterShow.FALSE);
                        _this.$store.commit(Mutations.AD_ACTIVITY, query);
                        _this.$common.goUrl(_this, RouteUrl.AD_ACTIVITY);
                    });
            } else {
                let query = {
                    activityName: title,
                    activityDiv: state.saveClause[state.curPlanId][title]
                };
                // _this.$store.commit(Mutations.FOOTER_SHOW, FooterShow.FALSE);
                _this.$store.commit(Mutations.AD_ACTIVITY, query);
                _this.$common.goUrl(_this, RouteUrl.AD_ACTIVITY);
            }
        },
    },
    actions: {

    },
    getters: {

    }
}