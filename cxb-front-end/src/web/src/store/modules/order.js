import { RouteUrl, Mutations, BaseService } from 'src/common/const';
import OrderConfig from 'src/common/config/orderConfig';
import { RequestUrl } from 'src/common/url';
import DateUtil from 'src/common/util/dateUtil';
export default {
    state: {
        isPayQuery: false,
        payOrderListAlert: false,
        payOrderList: {
            // state: false,
            // "5017120000000052": {
            //     "paymentNo": "5017120000000052",
            //     "sumPremium": 6437.76,
            //     "paySatus": "0",
            //     "documentList": [
            //         {
            //             "proposalNo": "9805025000170003280000",
            //             "appliname": "白宇",
            //             "insuredname": "白宇",
            //             "licenseno": "渝A12313",
            //             "sumpremium": "5457.76",
            //             "carTax": "0",
            //             "riskcode": "0502",
            //             "startdate": "2017-12-09",
            //             "enddate": "2018-12-08",
            //             "status": "8",
            //             "riskcname": "机动车辆保险(示范)",
            //             "itemKinds": [

            //             ]
            //         },
            //         {
            //             "proposalNo": "9805085000170003087000",
            //             "appliname": "白宇",
            //             "insuredname": "白宇",
            //             "licenseno": "渝A12313",
            //             "sumpremium": "950",
            //             "carTax": "30",
            //             "riskcode": "0508",
            //             "startdate": "2017-12-07",
            //             "enddate": "2018-12-06",
            //             "status": "8",
            //             "riskcname": "机动车交强险",
            //             "itemKinds": [

            //             ]
            //         }
            //     ]
            // },
            // "5017110000000145": {
            //     "paymentNo": "5017110000000145",
            //     "sumPremium": 855,
            //     "paySatus": "0",
            //     "documentList": [
            //         {
            //             "proposalNo": "9805085000170003063000",
            //             "appliname": "白宇",
            //             "insuredname": "白宇",
            //             "licenseno": "渝A1231",
            //             "sumpremium": "855",
            //             "carTax": "0",
            //             "riskcode": "0508",
            //             "startdate": "2018-01-25",
            //             "enddate": "2019-01-24",
            //             "status": "8",
            //             "riskcname": "机动车交强险",
            //             "itemKinds": [

            //             ]
            //         }
            //     ]
            // },
            // "5017120000000053": {
            //     "paymentNo": "5017120000000053",
            //     "sumPremium": 365,
            //     "paySatus": "0",
            //     "documentList": [
            //         {
            //             "proposalNo": "9827245008170000005000",
            //             "appliname": "白宇",
            //             "insuredname": "白宇",
            //             "licenseno": "null",
            //             "sumpremium": "365",
            //             "carTax": "0",
            //             "riskcode": "2724",
            //             "startdate": "2018-01-25",
            //             "enddate": "2019-01-24",
            //             "status": "8",
            //             "riskcname": "驾乘人员意外伤害保险",
            //             "itemKinds": [

            //             ]
            //         }
            //     ]
            // },
            // "5017110000000149": {
            //     "paymentNo": "5017110000000149",
            //     "sumPremium": 855,
            //     "paySatus": "0",
            //     "documentList": [
            //         {
            //             "proposalNo": "9805085000170003068000",
            //             "appliname": "白宇",
            //             "insuredname": "白宇",
            //             "licenseno": "渝A123123",
            //             "sumpremium": "855",
            //             "carTax": "0",
            //             "riskcode": "0508",
            //             "startdate": "2018-01-25",
            //             "enddate": "2019-01-24",
            //             "status": "8",
            //             "riskcname": "机动车交强险",
            //             "itemKinds": [

            //             ]
            //         }
            //     ]
            // }
        },

        idCariPolicys: {},
        poIsQuery: false, //是否在进行请求，一直刷新操作  从而重复请求
        poIsEnd: false,
        // curOrderList: [],
        // orderState: {},
        // isEnd: false,

        // orderList: [],
        // pageNumber: 0,
        // pageSize: 10,
        // sortfield: "userId",
        // sorttype: 'ASC',
        showPolicyEmail: false,
        downPolicyQueryData: {
            TYPE: "FORM",
            policyNo: '',
            proposalDate: '',
            emailStr: '',
            applicantName: '',
        },

        isFind: false,
        findData: {},
        showEinv: false,
        seleOrder: {},

        curShowOrder: [],
        isQuery: false, //是否在进行请求，一直刷新操作  从而重复请求
        isEnd: false,
        orderData: {},
        findEinvData: {},

        reportPolicyNo: "",
        menu: {},

        policyQueryData: {},

        isShowReportIm: false,   //是否显示马上报案
        prpCmainList: [],//马上报案查询保单记录
        isShowReportIm: false,   //是否显示马上报案

        claimList: [],//历史赔案记录
        reportsList: [],//报案记录


        orderSeleState: "all",


        orderDetailData: {
            type: "",
            data: {
                premiumCmc: "",
                comNumCmc: "",
                comBeginDateCmc: "",
                comEndDateCmc: "",
                comPlatesCmc: "",
                comPlatesCmc: "",
                frameNumCmc: "",
                engineNumCmc: "",
                ratifiedCmc: "",
                modelCmc: "",
                carTypeCmc: "",
                seatsCmc: "",
                beginDate: "",
                endDate: "",
                insured: "",
                num: "",
                policyType: "",
                policyholder: "",
                premiumAmount: "",
            }
        },
        carInsured: "",
        carAppli: "",
        carPolicyNo: "",
        carData: {
            comBeginDate: "",
            comEndDate: "",
            comPlates: "",   //车牌号
            frameNum: "",  //车架号
            engineNum: "", //发动机号
            ratified: "",  //核定载质量
            model: "",  // 厂牌车型
            carType: "",//车辆种类
            seats: "", // 座位数
            vesselTaxAmount: "",//车船税
            premium: "",
            isShowPremium: true,
        }

    },
    mutations: {
        [Mutations.ORDER_SELE_STATE](state, orderSeleState) {
            state.orderSeleState = orderSeleState;
        },
        [Mutations.CLEAN_PRP_CMAIN_LIST](state, orderData) {
            state.prpCmainList = [];
        },
        [Mutations.ORDER_POLICYS](state, orderData) {
            let _this = orderData._this;
            let operation = orderData.operation;
            let seleState = sessionStorage["ID_CAR"];
            let type = orderData.type;
            let baseQueryData = {
                pageNumber: 0,
                pageSize: 10,
            }

            if (typeof (state.idCariPolicys[seleState + type]) == 'undefined') {
                let baseDataInfo = {
                    isisEnd: false,
                    queryData: baseQueryData,
                    orderList: [],
                    type: "",
                }
                state.prpCmainList = [];
                state.poIsEnd = false;
                state.idCariPolicys[seleState + type] = baseDataInfo;
            }

            if (operation == "refresh") {
                //上拉刷新操作  赋初始值
                state.poIsEnd = false;
                state.idCariPolicys[seleState + type].queryData = baseQueryData;
                state.idCariPolicys[seleState + type].isisEnd = false;
                state.idCariPolicys[seleState + type].orderList = [];
                state.idCariPolicys[seleState + type].queryData.pageNumber++;
            }

            if (!state.poIsQuery && !state.idCariPolicys[seleState + type].isisEnd) {
                if (operation == "infinite") {
                    //下拉刷新操作 页码加一
                    state.idCariPolicys[seleState + type].queryData.pageNumber++;
                }

                //开始进行请求操作
                state.poIsQuery = true;
                let url;
                if (type == BaseService.PAY) {
                    url = RequestUrl.QUERY_PROPOSAL;
                } else {
                    url = RequestUrl.FIND_POLICYS;
                }
                if (type == BaseService.INVOICE) {
                    state.idCariPolicys[seleState + type].queryData.type = "einv";
                }
                _this.$http.post(url, state.idCariPolicys[seleState + type].queryData)
                    .then(function (res) {
                        if (res.state = false) {
                            _this.$common.goUrl(_this, RouteUrl.SERVICE);
                        }
                        let insure = res.result.policys;
                        for (let i = 0; i < insure.length; i++) {
                            // if (insure[i].status == ORDER_STATE_TYPE.NO_PAY) {
                            //     let time = new Date().getTime();
                            //     if (time > insure[i].startDate) {
                            //         insure[i].status = ORDER_STATE_TYPE.INVALID;
                            //     }
                            // }
                            // insure[i].stateShow = OrderConfig.getOrderStateShowMenu(insure[i].status);
                            state.idCariPolicys[seleState + type].orderList.push(insure[i]);

                        }
                        if (state.idCariPolicys[seleState + type].queryData.pageNumber * baseQueryData.pageSize > res.result.totalCount) {
                            state.idCariPolicys[seleState + type].isisEnd = true;
                            state.poIsEnd = true;
                        }
                        // if (res.result.policys.length < state.idCariPolicys[seleState + type].queryData.pageSize) {
                        //     state.idCariPolicys[seleState + type].isisEnd = true;
                        //     state.poIsEnd = true;
                        // }
                        state.prpCmainList = state.idCariPolicys[seleState + type].orderList.slice();

                        state.poIsQuery = false;
                    })


            } else {
                state.poIsEnd = state.idCariPolicys[seleState + type].isisEnd;
                state.prpCmainList = state.idCariPolicys[seleState + type].orderList.slice();
            }

        },
        [Mutations.SET_MENU](state, menu) {
            state.menu = menu;
        },
        [Mutations.IS_SHOW_REPORT_IM](state, data) {
            state.reportPolicyNo = data;
        },
        [Mutations.GET_PAY_DATA](state, data) {
            let _this = data;
            state.isPayQuery = true;
            _this.$common.goUrl(_this, RouteUrl.PAY_ORDER_LIST);
            let query = {
            }
            state.payOrderList = [];
            _this.$http.post(RequestUrl.QUERY_PAY_LIST, query)
                .then(function (res) {
                    // payOrderList
                    //成功查询
                    if (res.state == 0) {
                        // res.result.payMap = [];
                        let isFrist = true;
                        for (var key in res.result.payMap) {
                            isFrist = false;
                            break;
                        }
                        if (isFrist) {
                            // _this.$vux.alert.show({ content: _this.$common.alertMsg("您没有待支付的单子！", "red;") });
                            state.payOrderList = res.result.payMap;
                            state.payOrderListAlert = true;
                        } else {
                            state.payOrderListAlert = false;
                            state.payOrderList = res.result.payMap;
                        }
                        // state.payOrderList = res.result.payMap;
                    } else {
                        _this.$vux.alert.show({ content: _this.$common.alertMsg("查询失败！", "red;") });
                    }

                });

        },
        //认证接口
        [Mutations.QUERY_POLICYS_SERVICE](state, data) {
            let _this = data._this;
            let service = data.baseService;
            //查询保单记录
            data = { id: data.id, imgCode: data.imgCode, verCode: data.verCode, mobileNo: data.mobileNo, policyNo: data.policyNo, userCode: data.userCode, valType: service };
            _this.$common.storeCommit(_this, Mutations.UPDATE_LOADING_STATUS, true);//loading
            _this.$http.post(RequestUrl.QUERY_POLICYS_AUTHEN, data).then(function (res) {
                // console.log(1111);
                _this.$common.storeCommit(_this, Mutations.UPDATE_LOADING_STATUS, false);//un-loading
                if (res.state == 0) {
                    //状态保持,防止更换账户查询未加载新用户信息
                    // state.prpCmainList = res.result.resList;
                    // if (res.result.resList.length > 0) {
                    //     if (!data.type) {
                    _this.$common.storeCommit(_this, Mutations.CLEAN_PRP_CMAIN_LIST, "");
                    sessionStorage["ID_CAR"] = data.id;
                    if (service == BaseService.PAY) {
                        state.isPayQuery = false;
                        state.payOrderList = [];
                        _this.$common.goUrl(_this, RouteUrl.PAY_ORDER_LIST);
                    } else if (service == BaseService.FNOL || service == BaseService.POLICY || service == BaseService.INVOICE) {
                        _this.$common.goUrl(_this, RouteUrl.POLICY_LIST);
                    } else if (service == BaseService.CLAIM_QUERY) {
                        // console.log(22222);
                        let data = { _this: _this, order: { id: "" }, type: "id" }
                        _this.$common.storeCommit(_this, Mutations.SHOW_HISTORY_CASE, data);
                    } else {
                        let query = {
                            _this: _this
                        }
                        _this.$common.storeCommit(_this, Mutations.QUERY_REPORTS_SERVICE, query);
                    }
                    //     }
                    //     return;
                    // } else {
                    //     _this.$vux.alert.show({ content: _this.$common.alertMsg("暂无保单！", "red;") });
                    //     _this.$common.goBack(_this);
                    // }
                } else {
                    if (_this.$common.isEmpty(data.imgCode) || _this.$common.isEmpty(data.verCode) || _this.$common.isEmpty(data.mobileNo)) {
                        let alert = ""
                        if (typeof (res.result) != "undefined") {
                            alert = res.result;
                        }
                        // else {
                        //     alert = "没有查询到对应的信息";
                        // }
                        _this.$vux.alert.show({ content: _this.$common.alertMsg(alert, "red;") });
                        return;
                    }
                    if (res.result == "图形码过期" || res.result == "图形码错误") {
                        _this.changeImgCode();//更换图形验证码
                    }
                    _this.errorMessage = res.result;
                    _this.haveError = true;
                }
            });
        },
        //进度查询-查询报案列表
        [Mutations.QUERY_REPORTS_SERVICE](state, data) {
            let _this = data._this;
            //查询报案记录
            // data = { id: data.id, imgCode: data.imgCode, verCode: data.verCode, mobileNo: data.mobileNo };
            data = {};
            _this.$common.storeCommit(_this, Mutations.UPDATE_LOADING_STATUS, true);//loading
            _this.$http.post(RequestUrl.QUERY_REPORTS, data).then(function (res) {
                _this.$common.storeCommit(_this, Mutations.UPDATE_LOADING_STATUS, false);//un-loading
                if (res.state == 0) {
                    //状态保持,防止更换账户查询未加载新用户信息
                    state.reportsList = res.result;
                    if (res.result.length > 0) {
                        _this.$common.goUrl(_this, RouteUrl.FOLLOW_UP);
                        return;
                    } else {
                        _this.$vux.alert.show({ content: _this.$common.alertMsg("暂无报案记录！", "red;") });
                    }
                } else {
                    if (_this.$common.isEmpty(data.imgCode) || _this.$common.isEmpty(data.verCode) || _this.$common.isEmpty(data.mobileNo)) {
                        _this.$vux.alert.show({ content: _this.$common.alertMsg(res.result, "red;") });
                        return;
                    }
                    if (res.result == "图形码过期" || res.result == "图形码错误") {
                        _this.changeImgCode();//更换图形验证码
                    }
                    _this.errorMessage = res.result;
                    _this.haveError = true;
                }
            });
        },
        //历史赔案查询
        [Mutations.SHOW_HISTORY_CASE](state, data) {
            // console.log(333);
            let _this = data._this;
            let type = data.type;
            let order = data.order;
            let queryNumber = "";
            let queryData = {
                policyNo: "",
                id: "",
                type: data.type
            }
            if (type == "policyNo") {
                queryData.policyNo = order.policyNo;
                // queryNumber = order.policyNo;
            } else {
                queryData.id = order.id;
                // queryNumber = order.id;
            }
            //投保单号不为空
            // if (!_this.$common.isEmpty(queryNumber)) {
            //查询赔案记录
            // let data = { policyNo: order.policyNo }
            _this.$common.storeCommit(_this, Mutations.UPDATE_LOADING_STATUS, true);//loading
            _this.$http.post(RequestUrl.QUERY_CLAIMS, queryData).then(function (res) {
                _this.$common.storeCommit(_this, Mutations.UPDATE_LOADING_STATUS, false);//un-loading
                // let l = res.claimList.length;
                if (!_this.$common.isEmpty(res) && res.claimList.length > 0) {
                    let claimList = res.claimList;
                    // claimList[0].status = 0;
                    for (let i = 0; i < claimList.length; i++) {
                        claimList[i].cliaimsType = OrderConfig.getCliaimsType(claimList[i].status);
                    }
                    //状态保持
                    state.claimList = claimList;
                    _this.$common.goUrl(_this, RouteUrl.HISTORICAL_RC);
                    return;
                } else {
                    state.claimList = [];
                    _this.$common.goUrl(_this, RouteUrl.HISTORICAL_RC);
                    // _this.$vux.alert.show({ content: _this.$common.alertMsg("暂无历史赔案", "red;") });
                    return;
                }
            });
            // } else {
            //     _this.$vux.alert.show({ content: _this.$common.alertMsg("暂无历史赔案", "red;") });
            // }
        },
        [Mutations.ORDER_OPERATION](state, orderData) {
            let ORDER_STATE_TYPE = OrderConfig.getOrderStateType();
            let _this = orderData._this;
            let operation = orderData.operation;
            let seleState = orderData.seleState;
            let user = orderData.user;
            let userId;
            if (user.isLogin) {
                userId = user.userDto.userCode;
            } else {
                userId = "0";
            }
            let baseQueryData = {
                TYPE: 'FORM',
                sortfield: "userId",
                status: seleState,
                sorttype: 'DESC',
                sortData: 'id',
                pageNumber: 0,
                pageSize: 10,
                sortfieldData: userId,
            }

            if (typeof (state.orderData[seleState]) == 'undefined') {
                let baseDataInfo = {
                    isisEnd: false,
                    queryData: baseQueryData,
                    orderList: [],
                }
                state.curShowOrder = [];
                state.isEnd = false;
                state.orderData[seleState] = baseDataInfo;
            }

            if (operation == "refresh") {
                //上拉刷新操作  赋初始值
                state.isEnd = false;
                state.orderData[seleState].queryData = baseQueryData;
                state.orderData[seleState].isisEnd = false;
                state.orderData[seleState].orderList = [];
                state.orderData[seleState].queryData.pageNumber++;
            }

            if (!state.isQuery && !state.orderData[seleState].isisEnd) {
                if (operation == "infinite") {
                    //下拉刷新操作 页码加一
                    state.orderData[seleState].queryData.pageNumber++;
                }

                //开始进行请求操作
                state.isQuery = true;
                _this.$http.post(RequestUrl.GET_ORDER_LIST, state.orderData[seleState].queryData)
                    .then(function (res) {
                        let insure = res.result.content;
                        for (let i = 0; i < insure.length; i++) {
                            // if (insure[i].status == ORDER_STATE_TYPE.NO_PAY) {
                            //     let time = new Date().getTime();
                            //     if (time > insure[i].startDate) {
                            //         insure[i].status = ORDER_STATE_TYPE.INVALID;
                            //     }
                            // }
                            var timestamp = new Date().getTime();
                            let endDate = DateUtil.getDateByStrYmdOrYmdhms(insure[i].endDate).getTime();
                            if (endDate < timestamp) {
                                insure[i].status = "3";
                            }

                            let stateShow = OrderConfig.getOrderStateShowMenu(insure[i].status);
                            if (_this.$common.isEmpty(stateShow)) {
                                stateShow = {     //有效
                                    policy: false,  // 电子保单
                                    invoice: false, // 电子发票
                                    case: false, //马上报案
                                    history: false, //历史赔案
                                    pay: false, //付款
                                    clause: false, //条款下载
                                };
                            }
                            insure[i].stateShow = stateShow;
                            state.orderData[seleState].orderList.push(insure[i]);

                        }
                        if (res.result.content.length < state.orderData[seleState].queryData.pageSize) {
                            state.orderData[seleState].isisEnd = true;
                            state.isEnd = true;
                        }
                        state.curShowOrder = state.orderData[seleState].orderList.slice();

                        state.isQuery = false;
                    })


            } else {
                state.isEnd = state.orderData[seleState].isisEnd;
                state.curShowOrder = state.orderData[seleState].orderList.slice();
            }

        },
        [Mutations.SET_FIND_DATA](state, findData) {
            state.isFind = findData.isFind;
            if (findData.isFind == true) {
                findData.findData.stateShow = OrderConfig.getOrderStateShowMenu(findData.findData.status);
                state.findData = findData.findData;
            }
        },
        [Mutations.ORDER_QUERY_EINV](state, data) {
            let _this = data._this;
            let order = data.order;
            state.seleOrder = data.order;
            let queryData = {
                TYPE: "FORM",
                policyNo: order.policyNo,
                identifyNo: order.idNo
            }
            // queryData.policyNo = "8505023301170000015000";
            // queryData.identifyNo = "33010519910801504X";
            // _this.$common.downloadfile("http://ei.51fapiao.cn:9080/51ptProxy_dj/fpxz?p=fc93dfed41ca4a24a737631949c17e32", "电子发票", _this);

            _this.$http.post(RequestUrl.QUERY_EINV, queryData)
                .then(function (res) {
                    //未开具电子发票
                    if (res.state == '1' || res.state == '2') {
                        _this.$vux.alert.show({ content: res.result });
                        setTimeout(() => {
                            _this.$vux.alert.hide();
                        }, 3000);
                    } else if (res.resCode == '0' || res.resCode == 0 || res.resCode == '-1' || res.resCode == -1 || (typeof (res.result.PdfUrl) != "undefined" && (res.result.PdfUrl == null || res.result.PdfUrl == "null"))) {
                        // _this.showEinv = true;
                        _this.seleOrder = order;
                        state.findEinvData = res.result;
                        // _this.$common.storeCommit(_this, Mutations.ORDER_SET_SHOW_EINV, true);
                        $("#initInsure").click();
                        _this.$common.goUrl(_this, RouteUrl.EINV);
                    } else if (res.resCode == '1' || res.resCode == 1) {
                        // 0未开，1已开，-1红冲
                        if (typeof (res.result.PdfUrl) != "undefined" && res.result.PdfUrl != "" && res.result.PdfUrl != null && res.result.PdfUrl != "null") {
                            _this.$common.downloadfile(res.result.PdfUrl, "电子发票", _this);
                        } else {
                            _this.$vux.alert.show({ content: "文件获取失败,请联系管理员!" });
                            setTimeout(() => {
                                _this.$vux.alert.hide();
                            }, 3000);
                        }
                        //开具了电子发票
                        // let pdfUrl = res.result.PdfUrl;
                    } else {
                        _this.$vux.alert.show({ content: "查询失败,请联系管理员" });
                        setTimeout(() => {
                            _this.$vux.alert.hide();
                        }, 3000);
                    }
                    // console.log(res);
                })

        },
        [Mutations.ORDER_SET_SHOW_EINV](state, data) {
            state.showEinv = data;
        },
        [Mutations.ORDER_SET_EMAIL](state, data) {
            state.showPolicyEmail = data;
        },
        [Mutations.SEND_POLICY_EMIAL](state, data) {
            if (data.showPolicyEmail) {
                state.downPolicyQueryData = data;
            }
            state.showPolicyEmail = data.showPolicyEmail;
        },
        [Mutations.GET_ORDER_DETAIL](state, data) {
            let _this = data._this;
            let query = {
                policyNo: data.policyNo,
                TYPE: "FORM"
            }
            let type = query.policyNo.substring(2, 4);
            if (type == "05") {
                state.orderDetailData.type = "car";
            } else {
                state.orderDetailData.type = "other";
            }
            let init = {
                riskCode: "",
                comBeginDate: "",
                comEndDate: "",
                comPlates: "",   //车牌号
                frameNum: "",  //车架号
                engineNum: "", //发动机号
                ratified: "",  //核定载质量
                model: "",  // 厂牌车型
                carType: "",//车辆种类
                seats: "", // 座位数
                vesselTaxAmount: "",//车船税
                premium: "",
            };
            state.carData = init;
            // store.commit(Mutations.UPDATE_LOADING_STATUS, true)
            _this.$common.storeCommit(_this, Mutations.UPDATE_LOADING_STATUS, true);
            state.carData.isShowPremium = true;
            _this.$http.post(RequestUrl.QUERY_POLICY_DETAIL, query).then(function (res) {
                _this.$common.storeCommit(_this, Mutations.UPDATE_LOADING_STATUS, false);
                _this.$common.goUrl(_this, RouteUrl.ORDER_DETAILS);
                state.orderDetailData.data = res.result;
                if (state.orderDetailData.type == "car") {
                    state.carPolicyNo = query.policyNo;
                    let riskCode = query.policyNo.substring(2, 6);
                    // carData:{
                    //     comPlates:"",   //车牌号
                    //     frameNum:"",  //车架号
                    //     engineNum:"", //发动机号
                    //     ratified:"",  //核定载质量
                    //     model:"",  // 厂牌车型
                    //     carType:"",//车辆种类
                    //     seats:"", // 座位数
                    //      comBeginDate:"",
                    //      comEndDate:"",
                    // }
                    state.carData.riskCode = riskCode;
                    if (riskCode == "0508") {  //交强险
                        state.carInsured = res.result.comInsuredCpsr;
                        state.carAppli = res.result.comPolicyholderCpsr;
                        state.carData.comPlates = res.result.comPlatesCpsr;
                        state.carData.frameNum = res.result.frameNumCpsr;
                        state.carData.engineNum = res.result.engineNumCpsr;
                        state.carData.ratified = res.result.ratifiedCpsr;
                        state.carData.model = res.result.modelCpsr;
                        state.carData.carType = res.result.carTypeCpsr;
                        state.carData.seats = res.result.seatsCpsr;
                        state.carData.comBeginDate = res.result.comBeginDateCpsr;
                        state.carData.comEndDate = res.result.comEndDateCpsr;
                        state.carData.vesselTaxAmount = res.result.underwritingDetailsCpsr[0].vesselTaxAmount;
                        state.carData.premium = res.result.underwritingDetailsCpsr[0].premiumAmount;
                    } else {
                        state.carData.comBeginDate = res.result.comBeginDateCmc;
                        state.carInsured = res.result.comInsuredCmc;
                        state.carData.comEndDate = res.result.comEndDateCmc;
                        state.carAppli = res.result.comPolicyholderCmc;
                        state.carData.comPlates = res.result.comPlatesCmc;
                        state.carData.frameNum = res.result.frameNumCmc;
                        state.carData.engineNum = res.result.engineNumCmc;
                        state.carData.ratified = res.result.ratifiedCmc;
                        state.carData.model = res.result.modelCmc;
                        state.carData.carType = res.result.carTypeCmc;
                        state.carData.seats = res.result.seatsCmc;
                        state.carData.premium = res.result.premiumCmc;
                    }
                    if (state.carInsured.indexOf("恒昌德盛") > -1 || state.carAppli.indexOf("恒昌德盛") > -1) {
                        state.carData.isShowPremium = false;
                    }
                } else {
                    if (state.orderDetailData.data.policyholder.indexOf("恒昌德盛") > -1 || state.orderDetailData.data.insured.indexOf("恒昌德盛") > -1) {
                        state.carData.isShowPremium = false;
                    }
                }
            });
        },
    },
    actions: {

    },
    getters: {

    }
}
